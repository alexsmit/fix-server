#!/bin/bash

[ -d build ] && rm -rf build

rm *.lst 2>/dev/null

for dirr in token-server profile-server dbutil dbutil-linux dbutil-web dbutil-profile test
do
    if [ -e $dirr/cleanup.sh ] ; then
	pushd $dirr >/dev/null
	    [ -d build ] && rm -rf build
	popd >/dev/null
    fi
done

