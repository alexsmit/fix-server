﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace netcore_api.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ValuesController : ControllerBase
  {
    // GET api/user
    [HttpGet]
    [Authorize(Roles = "user")]
    [Route("/api/user")]
    public /*ActionResult<IEnumerable<string>>*/ object GetUser()
    {
      return User.Claims.Select(c =>
        new
        {
          Type = c.Type,
          Value = c.Value
        });
    }

    // GET api/admin
    [HttpGet]
    [Authorize(Roles = "admin")]
    [Route("/api/admin")]
    public /*ActionResult<IEnumerable<string>>*/ object GetAdmin()
    {
      return User.Claims.Select(c =>
        new
        {
          Type = c.Type,
          Value = c.Value
        });
    }


    // GET api/other
    [HttpGet]
    [Authorize(Roles = "other")]
    [Route("/api/other")]
    public /*ActionResult<IEnumerable<string>>*/ object GetOther()
    {
      return User.Claims.Select(c =>
        new
        {
          Type = c.Type,
          Value = c.Value
        });
    }


  }
}
