#!/bin/bash
for i in netcore-* ; do
    [ ! -z "$i" ] && [ -d $i/bin ] && rm -rf $i/bin
    [ ! -z "$i" ] && [ -d $i/obj ] && rm -rf $i/obj
done

find . -name 'node_modules' -type d -prune -exec rm -rf '{}' +
