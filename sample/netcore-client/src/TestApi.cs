using System;
using System.Net.Http;
using System.Text;
using netcore_client;
using Newtonsoft.Json;
using vx_api;

class TestApi {

  private BaseApi api;

  public TestApi(BaseApi baseapi) {
      api = baseapi;
  }

  public void CallTest(string baseurl, string url, int code)
  {
    HttpClient client = new HttpClient();
    var request = new HttpRequestMessage
    {
      RequestUri = new Uri(baseurl + url),
      Method = HttpMethod.Get,
      Headers = { { "Authorization", $"bearer {api.token.access_token}" } }
    };

    try
    {
      HttpResponseMessage asMsg = client.SendAsync(request).Result;
      if ((int)asMsg.StatusCode == code)
        System.Console.WriteLine($"[✓] {url} api ok");
      else
        System.Console.WriteLine($"[-] {url} api returns invalid code {asMsg.StatusCode}");
      //        Console.WriteLine(asMsg.Content.);
    }
    catch (System.Exception ex)
    {
      System.Console.WriteLine($"[E] {url} api failed." + ex.Message);
      System.Environment.Exit(1);
    }
  }
  public void PostTest(string baseurl, string url, int code, DataItem item)
  {
    var jsonObject = JsonConvert.SerializeObject(item);

    HttpClient client = new HttpClient();
    var request = new HttpRequestMessage
    {
      RequestUri = new Uri(baseurl + url),
      Method = HttpMethod.Post,
      Headers = { { "Authorization", $"bearer {api.token.access_token}" } },
      Content = new StringContent(jsonObject, Encoding.UTF8, "application/json"),
    };

    try
    {
      HttpResponseMessage asMsg = client.SendAsync(request).Result;
      if ((int)asMsg.StatusCode == code)
        System.Console.WriteLine($"[✓] {url} api ok");
      else
        System.Console.WriteLine($"[-] {url} api returns invalid code {asMsg.StatusCode}");
      //        Console.WriteLine(asMsg.Content.);
    }
    catch (System.Exception ex)
    {
      System.Console.WriteLine($"[E] {url} api failed." + ex.Message);
      System.Environment.Exit(1);
    }
  }



}