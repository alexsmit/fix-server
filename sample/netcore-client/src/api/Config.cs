namespace vx_api
{
  public class Config
  {
    //public static string urlProfileServer = "https://asmith.replaytrader.com:8481/";
    //public static string urlTokenServer = "https://asmith.replaytrader.com:8482/";
    public static string urlProfileServer = "https://vxpro.com:8481/";
    public static string urlTokenServer = "https://vxpro.com:8482/";
    //public static string urlProfileServer = "https://localhost:8481/";
    //public static string urlTokenServer = "https://localhost:8482/";

    public static string urlLocalServer = "http://localhost:5000/";

    public static string userName = "netuser";
    public static string userPassword = "123";
    public static string userPin = "1234";
    public static string userEmail = "alx.kuzza@replaytrader.com";

    public static string clientId = "aaa";
    public static string clientSecret = "111";
  }
}