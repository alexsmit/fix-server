using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using netcore_client;
using Newtonsoft.Json;

namespace vx_api
{
  class BaseApi
  {
    public Token token;

    public bool CallGrant(OauthGrant grant, string operation, bool exitOnFail = true, bool showResult = true)
    {
      var jsonGrant = JsonConvert.SerializeObject(grant);
      //   System.Console.WriteLine(jsonGrant);

      HttpClient client = new HttpClient();

      int max_count = (!exitOnFail && !showResult) ? 100 : 1;

      for (var ii = 0; ii < max_count; ii++)
      {
        var request = new HttpRequestMessage
        {
          RequestUri = new Uri(Config.urlTokenServer + "oauth/token"),
          Method = HttpMethod.Post,
          Content = new StringContent(jsonGrant, Encoding.UTF8, "application/json"),
        };

        var headers = request.Headers;
        headers.Add("X-Client-Id", Config.clientId);
        headers.Add("X-Client-Secret", Config.clientSecret);
        if (token != null)
        {
          headers.Add("Authorization", $"bearer {token.access_token}");
        }

        try
        {
          HttpResponseMessage asMsg = client.SendAsync(request).Result;
          string result = asMsg.Content.ReadAsStringAsync().Result;
          if (asMsg.StatusCode == System.Net.HttpStatusCode.OK)
          {
            token = JsonConvert.DeserializeObject<Token>(result);
            if (showResult) System.Console.WriteLine($"[✓] {operation} ok");
            return true;
          }
          return false;
        }
        catch (System.Exception ex)
        {
          System.Console.WriteLine($"[E] {operation} failed. Attemnt: {ii + 1}. {ex.Message}");
          if (exitOnFail) System.Environment.Exit(1);
          if (max_count > 1)
          {
            Thread.Sleep(500 + ii * 100);
            continue;
          }
          return false;
        }
      }

      return false;
    }

    public string SendDataItem(DataItem item, string endpoint, bool quiet = false, bool useToken = false)
    {
      var jsonItem = JsonConvert.SerializeObject(item);

      var request = new HttpRequestMessage
      {
        RequestUri = new Uri(endpoint),
        Method = HttpMethod.Post,
        Content = new StringContent(jsonItem, Encoding.UTF8, "application/json"),
      };

      var headers = request.Headers;
      if (!useToken)
      {
        headers.Add("X-Client-Id", Config.clientId);
        headers.Add("X-Client-Secret", Config.clientSecret);
      }
      else
      {
        if (token != null)
        {
          headers.Add("Authorization", $"bearer {token.access_token}");
        }
      }

      try
      {
        HttpClient client = new HttpClient();
        HttpResponseMessage asMsg = client.SendAsync(request).Result;
        if (asMsg.StatusCode == System.Net.HttpStatusCode.OK)
        {
          if (!quiet)
            System.Console.WriteLine($"[✓] {endpoint} ok");
        }
        else
        {
          if (!quiet)
            System.Console.WriteLine($"[E] {endpoint} failed. invalid status code");
        }
        return asMsg.Content.ReadAsStringAsync().Result;
      }
      catch (System.Exception ex)
      {
        System.Console.WriteLine($"[E] {endpoint} failed." + ex.Message);
        System.Environment.Exit(1);
        return "";
      }
    }

  }

}