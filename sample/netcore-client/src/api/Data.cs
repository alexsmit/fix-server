
using System.Collections.Generic;

namespace vx_api
{

  public class DataItem
  {
  }

  public class Token : DataItem
  {
    public string access_token { get; set; }
    public string refresh_token { get; set; }
    public string token_type { get; set; }
  }

  public class UserProperties : DataItem
  {
    public string description { get; set; }
    public string prop1 { get; set; }
    public string prop2 { get; set; }
  }

  public class UserData : DataItem
  {
    public string password { get; set; }
    public string pin { get; set; }
    public string email { get; set; }
    public List<string> roles { get; set; }
    public UserProperties properties { get; set; }
  }

  public class ResetPassword : DataItem
  {
    public string email { get; set; }
  }

  public class UserRegistration : DataItem
  {
    public string username { get; set; }
    public string password { get; set; }
    public string email { get; set; }
    public string pin { get; set; }
  }

  public class ChangePassword : DataItem
  {
    public string password { get; set; }
    public string password_verify { get; set; }
    public string password_old { get; set; }
  }
  public class ChangePin : DataItem
  {
    public string pin { get; set; }
    public string pin_verify { get; set; }
  }

  public class DataOrError : DataItem
  {
    public string error { get; set; }
    public string error_description { get; set; }
  }

  public class RegistrationResult : DataOrError
  {
    public string registration_code { get; set; }
  }

  public class RegistrationStatus : DataOrError
  {
    public string status { get; set; }
  }

}