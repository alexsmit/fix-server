namespace vx_api
{
    public class OauthGrant {
    public OauthGrant(string grantName) {
      grant_type = grantName;
    }
    public string grant_type { get; private set; }
  }
  public class PasswordGrant : OauthGrant
  {
    public PasswordGrant() : base("password") {}
    public string username { get; set; }
    public string password { get; set; }
  }

  public class RefreshGrant : OauthGrant
  {
    public RefreshGrant() : base("refresh_token") {}
    public string refresh_token { get; set; }
  }

  public class PINGrant : OauthGrant
  {
    public PINGrant() : base("pin") {}
    public string pin { get; set; }
  }

}