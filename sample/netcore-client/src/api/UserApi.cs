using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using netcore_client;
using Newtonsoft.Json;

namespace vx_api
{
    class UserAPI : BaseApi
    {
        public void CreateUser()
        {
            CreateUser(Config.userName, Config.userEmail, Config.userPassword, Config.userPin);
        }

        public void CreateUser(string userName, string userEmail, string userPassword, string userPin)
        {
            UserData user = new UserData
            {
                password = userPassword,
                pin = userPin,
                email = userEmail,
                roles = new List<string> { "admin", "user" },
                properties = new UserProperties
                {
                    description = "netuser description",
                    prop1 = "prop1 value",
                    prop2 = "prop2 value"
                }
            };

            var jsonUser = JsonConvert.SerializeObject(user);
            //   System.Console.WriteLine(jsonUser);

            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(Config.urlProfileServer + "users/" + userName),
                Method = HttpMethod.Post,
                Headers = {
              {"X-Client-Id", Config.clientId},
              {"X-Client-Secret", Config.clientSecret}
          },
                Content = new StringContent(jsonUser, Encoding.UTF8, "application/json"),
            };

            try
            {
                HttpResponseMessage asMsg = client.SendAsync(request).Result;
                System.Console.WriteLine("[✓] Create user ok");
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Create User failed." + ex.Message);
                System.Environment.Exit(1);
            }
        }

        public void ResetPassword(string userEmail)
        {
            ResetPassword reset = new ResetPassword
            {
                email = userEmail,
            };
            var result = SendDataItem(reset, Config.urlTokenServer + "reset", true);
            var st = JsonConvert.DeserializeObject<DataOrError>(result);
            if (string.IsNullOrEmpty(st.error))
            {
                System.Console.WriteLine("[✓] Password reset request ok");
            }
            else
            {
                System.Console.WriteLine("[E] Password reset request failed");
                System.Environment.Exit(1);
            }
        }

        public void DeleteUser(string userName)
        {
            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(Config.urlProfileServer + "users/" + userName),
                Method = HttpMethod.Delete,
                Headers = {
              {"X-Client-Id", Config.clientId},
              {"X-Client-Secret", Config.clientSecret}
          }
            };

            try
            {
                HttpResponseMessage asMsg = client.SendAsync(request).Result;
                System.Console.WriteLine("[✓] Delete user ok");
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine("Create User failed." + ex.Message);
                System.Environment.Exit(1);
            }
        }

        public string RegisterUser(string _email)
        {
            UserRegistration reg = new UserRegistration
            {
                username = Config.userName,
                email = _email,
                password = Config.userPassword,
                pin = Config.userPin
            };

            return SendDataItem(reg, Config.urlTokenServer + "register", true);
        }

        public void RefreshTokenPIN(string _pin)
        {
            PINGrant grant = new PINGrant
            {
                pin = _pin
            };
            CallGrant(grant, "Refresh token with PIN");
        }

        public void RefreshToken()
        {
            RefreshGrant grant = new RefreshGrant
            {
                refresh_token = token.refresh_token
            };
            CallGrant(grant, "Refresh token");
        }

        public bool GetToken(string _userName, string _userPassword, bool exitOnFail = true, bool showResult = true)
        {
            PasswordGrant grant = new PasswordGrant
            {
                username = _userName,
                password = _userPassword
            };
            return CallGrant(grant, "Get access token", exitOnFail, showResult);
        }

    }

}