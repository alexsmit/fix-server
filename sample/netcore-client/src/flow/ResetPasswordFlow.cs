using Newtonsoft.Json;
using vx_api;

namespace netcore_client
{
  class ResetPasswordFlow
  {
    public void Run(string email)
    {
      UserAPI api = new UserAPI();

      // 1 - re-create a user with provided email
      api.DeleteUser(Config.userName);
      api.CreateUser(Config.userName, email, "password", "1234");

      // 2 - reset the password, email will be sent
      api.ResetPassword(email);
      System.Console.Write("An email has been sent, confirm and update the password to \"ZZZ\" and press enter");
      var result = System.Console.ReadLine();

      //3 - try registration again for the same user/same email. it should fail
      if (api.GetToken(Config.userName, "ZZZ"))
      {
        System.Console.WriteLine($"[✓] Login is successful");
      }
      else
      {
        System.Console.WriteLine($"[E] Login failed.");
      }
    }
  }
}
