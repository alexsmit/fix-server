using Newtonsoft.Json;
using vx_api;

namespace netcore_client
{
  class ChangePasswordFlow
  {
    public void Run(string email)
    {
      UserAPI api = new UserAPI();

      // 1 - re-create a user with provided email
      api.DeleteUser(Config.userName);
      api.CreateUser(Config.userName, email, "password", "1234");

      if (api.GetToken(Config.userName, "password", true))
      {
        System.Console.WriteLine($"[✓] Initial login is successful");
      }
      else
      {
        System.Console.WriteLine($"[E] Initial login failed.");
      }

      var ch = new ChangePassword()
      {
        password_old = "password",
        password = "gavgav",
        password_verify = "gavgav"
      };

      var sresult = api.SendDataItem(ch, Config.urlTokenServer + "password", false, true);
      if (api.GetToken(Config.userName, "gavgav"))
      {
        System.Console.WriteLine($"[✓] Login is successful");
      }
      else
      {
        System.Console.WriteLine($"[E] Login failed.");
      }

      System.Console.WriteLine("Password updated");
    }

  }
}
