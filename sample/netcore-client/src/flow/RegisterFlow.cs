using System.Threading;
using Newtonsoft.Json;
using vx_api;
//using namespace System::Threading;

namespace netcore_client
{
  class RegisterFlow
  {
    public void Run(string email)
    {
      UserAPI api = new UserAPI();
      // 1 - delete user, otherwise registraiton will fail
      api.DeleteUser(Config.userName);

      // 2 - call registration API
      string result = api.RegisterUser(email);
      var reg = JsonConvert.DeserializeObject<RegistrationResult>(result);
      if (reg.registration_code == null)
      {
        System.Console.WriteLine($"[E] registartion failed. Error: {reg.error} {reg.error_description}");
        return;
      }
      string first_code = reg.registration_code;
      System.Console.WriteLine($"[✓] registration is successful. Will fail when clicking on the link in the first email.");

      System.Console.WriteLine("Waiting for 2 secs to make sure a confirmation emails will appear in order");
      Thread.Sleep(2000);

      // 3 - Call same registration again , it should not fail, but we will get another registration code
      result = api.RegisterUser(email);
      reg = JsonConvert.DeserializeObject<RegistrationResult>(result);
      if (reg.registration_code == null)
      {
        System.Console.WriteLine($"[E] registartion failed. Error: {reg.error} {reg.error_description}");
        return;
      }
      if (reg.registration_code == first_code)
      {
        System.Console.WriteLine($"[E] registartion code should be different");
        return;
      }

      System.Console.WriteLine($"[✓] registration is successful. Confirm by clicking on the link in the second email.");

      // 4 - Validate registration by polling the status. When a link in the email is clicked a status will be "completed"
      // and we will try to login
      System.Console.Write("Validating ");
      var bCont = true;
      do
      {
        result = api.SendDataItem(reg, Config.urlTokenServer + "register/validate", true);
        if (string.IsNullOrEmpty(result))
        {
          System.Console.WriteLine("[E] validateion failed.");
          return;
        }
        else
        {
          var st = JsonConvert.DeserializeObject<RegistrationStatus>(result);
          if (st.status == "completed")
          {
            System.Console.WriteLine($"\n[✓] email confirmed");
            break;
          }
          else if (st.status == "pending")
          {
            System.Console.Write(".");
            System.Console.Out.Flush();
            Thread.Sleep(1000);
          }
          else
          {
            if (!string.IsNullOrEmpty(st.error))
              System.Console.WriteLine("\n Error: " + st.error);
            else
              System.Console.WriteLine("\n Unknown Error");
            return;
          }
        }

      } while (bCont);

      //5 - try to login (get the token)
      api.GetToken(Config.userName, Config.userPassword);

      //6 - try registration again for the same user/same email. it should fail
      result = api.RegisterUser(email);
      reg = JsonConvert.DeserializeObject<RegistrationResult>(result);
      if (reg.registration_code == null)
      {
        System.Console.WriteLine($"[✓] registartion failed. Error: {reg.error} {reg.error_description}");
      }
      else
      {
        System.Console.WriteLine($"[E] registartion should fail.");
      }
    }
  }
}