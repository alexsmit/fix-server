using Newtonsoft.Json;
using vx_api;

namespace netcore_client
{
  class ChangePinFlow
  {
    public void Run(string email)
    {
      UserAPI api = new UserAPI();

      // 1 - re-create a user with provided email
      api.DeleteUser(Config.userName);
      api.CreateUser(Config.userName, email, "password", "1234");

      if (api.GetToken(Config.userName, "password", true))
      {
        System.Console.WriteLine($"[✓] Initial login is successful");
      }
      else
      {
        System.Console.WriteLine($"[E] Initial login failed.");
      }

      var ch = new ChangePin()
      {
        pin = "9876",
        pin_verify = "9876"
      };

      var sresult = api.SendDataItem(ch, Config.urlTokenServer + "pin", false, true);

      api.RefreshTokenPIN(ch.pin);

      System.Console.WriteLine("Pin updated");
    }

  }
}
