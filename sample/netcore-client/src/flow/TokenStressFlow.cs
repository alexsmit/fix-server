using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using vx_api;

namespace netcore_client
{
  class StressOneHundred
  {
    public void Run() {
      UserAPI uapi = new UserAPI();
      TestApi api = new TestApi(uapi);
      for(var i=0;i<100;i++) {
        uapi.GetToken(Config.userName, Config.userPassword, false, false);
      }
    }
  }

  class TokenStressFlow
  {
    public void Run()
    {
      UserAPI uapi = new UserAPI();
      TestApi api = new TestApi(uapi);

      var s1 = Stopwatch.StartNew();
      var count = 5000;
      var cycles =  count / 100;
      List<Thread> threads = new List<Thread>();
      for(var i=0; i<cycles; i++) {
        var s = new StressOneHundred();
        var t = new Thread(new ThreadStart(s.Run));
        t.Start();
        threads.Add(t);
      }
      
      foreach(var t in threads) t.Join();

      s1.Stop();
      var el = (count*1000.0 / s1.ElapsedMilliseconds);
      Console.WriteLine($"Total requests: {count}");
      var els = String.Format("{0:00}:{1:00}", s1.Elapsed.Minutes, s1.Elapsed.Seconds);
      Console.WriteLine($"Elapsed : {els}");
      Console.WriteLine($"Token/s: {Math.Round(el,2)}");
    }
  }
}
