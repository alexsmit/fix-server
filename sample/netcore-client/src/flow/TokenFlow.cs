using vx_api;

namespace netcore_client
{

  class TestItem : DataItem
  {
    public string data;
  }

  class TokenFlow
  {
    public void Run()
    {
      UserAPI uapi = new UserAPI();
      TestApi api = new TestApi(uapi);

      uapi.CreateUser();
      uapi.GetToken(Config.userName, Config.userPassword);
      uapi.RefreshToken();
      uapi.RefreshTokenPIN(Config.userPin);

      TestItem data = new TestItem
      {
        data = "test"
      };

      // You need to run local API server to test tokens  
      api.PostTest(Config.urlTokenServer, "test/user", 200, data);
      api.PostTest(Config.urlTokenServer, "test/admin", 200, data);
      api.PostTest(Config.urlTokenServer, "test/other", 401, data);
    }
  }
}