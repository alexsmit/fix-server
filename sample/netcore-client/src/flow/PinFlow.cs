using vx_api;

namespace netcore_client
{
  class PinFlow
  {
    public void Run()
    {
      UserAPI uapi = new UserAPI();
      TestApi api = new TestApi(uapi);

      uapi.CreateUser();
      uapi.GetToken(Config.userName, Config.userPassword);
      uapi.RefreshToken();
      uapi.RefreshTokenPIN(Config.userPin);

      // You need to run local API server to test tokens  
      api.CallTest(Config.urlLocalServer, "api/user", 200);
      api.CallTest(Config.urlLocalServer, "api/admin", 200);
      api.CallTest(Config.urlLocalServer, "api/other", 403);
    }
  }
}