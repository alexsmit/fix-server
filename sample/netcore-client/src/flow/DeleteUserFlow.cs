using vx_api;

namespace netcore_client
{
  class DeleteUserFlow
  {
    public void Run()
    {
      UserAPI api = new UserAPI();

      api.CreateUser();
      api.DeleteUser(Config.userName);
      var login = api.GetToken(Config.userName, Config.userPassword, false);
      if (!login)
        System.Console.WriteLine($"[✓] login failed for deleted user - ok");
      else
        System.Console.WriteLine($"[E] should not be able to login. Failed.");
    }
  }
}