﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using vx_api;

namespace netcore_client
{

    class Program
    {
        static void Main(string[] args)
        {

            if (File.Exists("config.json"))
            {
                var confdef = new
                {
                    urlProfileServer = "",
                    urlTokenServer = "",
                    clientId = "",
                    clientSecret = "",
                    userName = "netuser",
                    userPassword = "123",
                    userPin = "1234",
                    userEmail = "alx.kuzza@replaytrader.com",
                };

                string sconfig = File.ReadAllText("config.json");
                var config = JsonConvert.DeserializeAnonymousType(sconfig, confdef);
                Config.urlProfileServer = config.urlProfileServer;
                Config.urlTokenServer = config.urlTokenServer;
                Config.clientId = config.clientId;
                Config.clientSecret = config.clientSecret;
                Config.userName = config.userName;
                Config.userPassword = config.userPassword;
                Config.userPin = config.userPin;
                Config.userEmail = config.userEmail;
            }

            string flow = "";
            string email = Config.userEmail;

            if (args.Length != 0)
                flow = args[0];
            if (args.Length > 1)
                email = args[1];

            switch (flow)
            {

                case "stress":
                    {
                        TokenStressFlow tok = new TokenStressFlow();
                        tok.Run();
                    }
                    break;

                case "token":
                    {
                        TokenFlow tok = new TokenFlow();
                        tok.Run();
                    }
                    break;
                case "pin":
                    {
                        PinFlow pin = new PinFlow();
                        pin.Run();
                    }
                    break;
                case "register":
                    {
                        RegisterFlow flowreg = new RegisterFlow();
                        flowreg.Run(email);
                    }
                    break;

                case "reset":
                    {
                        ResetPasswordFlow reset = new ResetPasswordFlow();
                        reset.Run(email);
                    }
                    break;
                case "delete":
                    {
                        DeleteUserFlow del = new DeleteUserFlow();
                        del.Run();
                    }
                    break;

                case "change_password":
                    {
                        ChangePasswordFlow ch = new ChangePasswordFlow();
                        ch.Run(email);
                    }
                    break;

                case "change_pin":
                    {
                        ChangePinFlow ch = new ChangePinFlow();
                        ch.Run(email);
                    }
                    break;

                default:
                    System.Console.WriteLine(
                      "Usage: dotnet run {stress|token|pin|delete|register} [email]\n" +
                      "  REMOTE server test\n" +
                      "token   - create user, obtain token and call token server sample API.\n" +
                      "delete  - delete user flow, create and delete the user\n" +
                      "regiser - register a user with a provided email, will send 2 emails. an email shluld be then confirmed to finish the flow\n" +
                      "  LOCAL server test\n" +
                      "pin     - create user, obtain token and call local API. local .net api server should be running.\n" +
                      "          WARNING! use your real email to test."
                    );
                    break;
            }
        }
    }
}
