// Read environment variables from "testenv" in the repository root. Override environment vars if they are already set.
const path = require('path');
const dotenv = require('dotenv');
const fs = require('fs');

const envfile = process.env.ENVFILE || 'testenv'

console.log(">>> using env file:", envfile);
process.exit(1);
sleep(1000);

const TESTENV = path.resolve(__dirname, '..', envfile);
if (fs.existsSync(TESTENV)) {
  const envConfig = dotenv.parse(fs.readFileSync(TESTENV));
  Object.keys(envConfig).forEach((k) => {
    process.env[k] = envConfig[k];
  });
}
process.env.CLIENT_ID = process.env.CLIENT_ID || process.env.SPA_CLIENT_ID;
process.env.SCP = process.env.SCP || "openid profile email";
