#!/bin/bash

echo Restarting services

pushd build >/dev/null
### sudo zypper --no-gpg-checks install -f -y fix-server-0.1.1-Linux.rpm 

for i in profile-server token-server
do
    sudo cp $i/web/* /usr/local/share/vxpro/$i/web
    sudo cp $i/sql/* /usr/local/share/vxpro/$i/sql
done

sudo systemctl restart profile-server
sudo systemctl restart token-server
sudo systemctl status profile-server
sudo systemctl status token-server

popd > /dev/null
