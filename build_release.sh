#!/bin/bash

time (
    if [ ! -z "$1" ] ; then
	./build.sh -c -f -i build.release -j $1 build env
    else
	./build.sh -c -f -i build.release build env
    fi
)
