#!/bin/bash

# 1 - dir
# 2 - pattern
function find_dir() {
    local di=$1
    if [ "$di" == "." ] ; then
	di="$di -maxdepth 1"
    fi

        while read filer
        do
    	    # ignore symbolik links
    	    if [ -L $filer ] ; then
    		continue
    	    fi
    	    lines=$(cat $filer | wc -l)
    	    siz=$(stat -c "%s" $filer)
    	    echo $filer \: $lines - $siz
    	    let dirlines=$dirlines+$lines
    	    let totsize=$totsize+$siz
    	    let dirfiles=$dirfiles+1
        done < <(find $di -name "$2")

}

alllines=0
alltestlines=0
allsrclines=0
allfiles=0
alltestfiles=0
allsrcfiles=0
for dirr in dbutil-linux dbutil-web dbutil-profile token-server profile-server
do
    if [ ! -d $dirr ] ; then
	continue
    fi

    echo '*** ' $dirr Source files
    dirfiles=0
    dirlines=0
    totsize=0
    pushd $dirr > /dev/null
	find_dir . "*.cpp"
	find_dir src "*.cpp"
	find_dir . "*.h"
	find_dir src "*.h"
    popd > /dev/null
    echo lines for folder $dirr \: $dirlines
    let alllines=$alllines+$dirlines
    let allfiles=$allfiles+$dirfiles
    let allsrcfiles=$allsrcfiles+$dirfiles
    let allsrclines=$allsrclines+$dirlines

    echo '*** ' $dirr Test files
    dirfiles=0
    dirlines=0
    totsize=0
    pushd $dirr > /dev/null
	find_dir test "*.cpp"
	find_dir test "*.h"
    popd > /dev/null

    echo test lines for folder $dirr \: $dirlines
    let alllines=$alllines+$dirlines
    let allfiles=$allfiles+$dirfiles
    let alltestfiles=$alltestfiles+$dirfiles
    let alltestlines=$alltestlines+$dirlines
done

echo 'grand total lines        :' $alllines
echo 'grand total source lines :' $allsrclines
echo 'grand total test lines   :' $alltestlines
echo 'grand total size         :' $totsize
echo 'grand total files        :' $allfiles
echo 'grand total source files :' $allsrcfiles
echo 'grand total test files   :' $alltestfiles


