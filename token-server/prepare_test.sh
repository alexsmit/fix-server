#!/bin/bash
. ./prepare_build.sh dev -j 18 -c run
if [ $? -ne 0 ] ; then
    echo '*** BUILD FAILED ***'
else
pushd build >/dev/null
    killall -9 token-server
    killall -9 profile-server
    ./token-server-test $1 $2 $3 $4 $5
popd >/dev/null
fi
