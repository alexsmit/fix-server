var updateValidation = undefined;
var validatorRules = {};
var validationForm = undefined;
var validators = {
  input: function (id, rule) {
    var validity = '';
    var val = $(`#${id}`).val();
    if (rule.minLength && val.length < rule.minLength) {
      validity = 'len';
    } else if (rule.maxLength && val.length > rule.maxLength) {
      validity = 'len';
    }
    return validity;
  },
  confirm: function (id, rules) {
    var validity = validators.input(id, rules);
    if (validity) return validity;

    if (!rules["target_id"]) return validity;
    var target_id = rules["target_id"];
    var target_val = $(`#${target_id}`).val();

    var val = $(`#${id}`).val();
    if (val && target_val && val !== target_val) return "equal";

    return validity;
  }
}

var runValidation = function (ev) {
  var iid = ev.target.id;
  var r = validatorRules[iid];
  if (!r || !r.type) return;
  var rv = validators[r.type];
  if (!rv) return;

  var validity = rv(iid, r);

  $(`#${iid}`).get(0).setCustomValidity(validity);
  validationForm.get(0).checkValidity();
  validationForm.addClass("was-validated");
}

var updateValidation = function () {
  Object.keys(validatorRules).forEach(function (x) {
    if ($(`#${x}`).length > 0) {
      var ev = { target: { id: x } };
      runValidation(ev);
    }
  })
}

var setupValidator = function (id, rules) {
  validatorRules = rules;
  validationForm = $(`#${id}`);
  Object.keys(validatorRules).forEach(function (x) {
    if ($(`#${x}`).length > 0) {
      $(`#${x}`).change(function (ev) { runValidation(ev); });
      $(`#${x}`).keyup(function (ev) { runValidation(ev); });
    }
  })

  validationForm.submit(
    function (event) {
      var form = event.target;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }
  );
}


