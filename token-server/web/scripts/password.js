$(document).ready(function () {
  var validation_rules = {
    password: {
      type: 'input',
      minLength: 6,
      maxLength: 16,
      regex: ''
    },
    confirm_password: {
      type: 'confirm',
      minLength: 6,
      maxLength: 16,
      target_id: 'password'
    }
  };
  setupValidator('vx_form', validation_rules);
});

