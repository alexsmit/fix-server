var base64_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
var unknown_error = "Unknown error. Try to reload the page";
var context = {};

function random_alpha(len) {
  let result = '';
  let idx;
  for (let i = 0; i < len; i++) {
    idx = Math.floor(Math.random() * 62);
    result += base64_table[idx];
  }
  return result;
}

async function digest256(message) {
  const encoder = new TextEncoder();
  const data = encoder.encode(message);
  const hash = await crypto.subtle.digest('SHA-256', data);
  return hash;
}

function buffer2base64(buf) {
  var b64 = btoa(String.fromCharCode.apply(null, new Uint8Array(buf)));
  return b64;
}

function displayError(error_msg) {
  if (!error_msg) error_msg = unknown_error;
  $("#login_error").text(error_msg);
  $("#login_error").css("display", "block");
}

async function getToken(code, code_verifier) {
  var authorize_url = `/oauth2/${context.server_id}/v1/token`;
  var payload = {
    code: code,
    code_verifier: code_verifier,
    grant_type: "authorization_code",
    scope: context.scope,
    redirect_uri: context.redirect_uri,
    client_id: context.client_id
  };

  fetch(authorize_url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    cache: "no-cache",
    credentials: "same-origin",
    body: JSON.stringify(payload),
  }).then(response => response.json())
    .then(tokenObj => {
      if (tokenObj.error_description) throw Error(tokenObj.error_description);
      window.localStorage.setItem("token", JSON.stringify(tokenObj));
      $("#vx-login").modal("hide");
    })
    .catch(e => {
      displayError(e.message);
    });
}

async function runAuthorize(result) {
  if (!result) return;
  if (!context.server_id || !context.client_id) {
    displayError(); return;
  }

  // put profile into a local storage
  var state = random_alpha(16);
  var profile = {};
  if (result.profile) 
    profile = result.profile;
  else if(result["_embedded"]) {
    profile = result["_embedded"].user.profile;
    profile.id = result["_embedded"].id;
  }
  
  result.profile;
  profile.state = state;
  var code_verifier = random_alpha(16);
  window.localStorage.setItem("profile", JSON.stringify(profile));

  var nonce = random_alpha(16);
  context.redirect_uri = window.location.protocol + "//" + window.location.host + "/callback";
  var authorize_url = `/oauth2/${context.server_id}/v1/authorize`;
  var code_challenge_hash = await digest256(code_verifier);
  var code_challenge = buffer2base64(code_challenge_hash);
  var payload = {
    sessionToken: result.sessionToken,
    client_id: context.client_id,
    redirect_uri: context.redirect_uri,
    nonce: nonce,
    response_type: "code",
    scope: context.scope,
    code_challenge: code_challenge,
    code_challenge_method: "S256",
    prompt: "none",
    state: profile.state
  };

  fetch(authorize_url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    cache: "no-cache",
    credentials: "same-origin",
    body: JSON.stringify(payload)
  }).then(response => {
    var u = new URL(response.url);
    var returnCode = u.searchParams.get("code");
    var returnState = u.searchParams.get("state");
    var returnError = u.searchParams.get("error");
    var ok = true;
    var profile = undefined;
    if (!returnCode) ok = false;
    if (ok) {
      profile = JSON.parse(window.localStorage.getItem("profile"));
      if (!profile) ok = false;
      else {
        if (profile.state != returnState) ok = false;
      }
    }
    if (!ok) {
      if (returnError === "login_required")
        return Promise.resolve({});
      throw unknown_error;
    }
    return Promise.resolve({ code: returnCode });
  })
    .then(codeObj => {
      if (codeObj.code)
        getToken(codeObj.code, code_verifier);
    })
    .catch(e => {
      displayError(e);
    });
}

function onLogin(event) {
  var form = document.getElementById("vx-login-form");
  if (!form) return;
  updateValidation();
  if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
  } else {
    event.preventDefault();
    event.stopPropagation();
    var username = $("#username").val();
    $.ajax({
      type: "POST",
      data: JSON.stringify({
        username,
        password: $("#password").val(),
        requestId: $("#requestId").val()
      }),
      contentType: "application/json",
      url: "/login",
      dataType: "json",
      success: async (result) => await runAuthorize(result),
      error: function (error) {
        var resp = JSON.parse(error.responseText);
        console.log(error.responseText);
        var error_msg = resp.error_description ?? "Unknown error";
        $("#login_error").text(error_msg);
        $("#login_error").css("display", "block");
      },
      async: true
    });

  }
  form.classList.add('was-validated');
}

$(document).ready(function () {
  var setupPKCE = function () {
    if ($("#vx-login-form").length == 0) return;
    context.server_id = $("#serverId").val() ?? "default";
    context.client_id = $("#clientId").val();
    context.scope = $("#scope").val() ?? "openid";
  };

  var validation_rules = {
    password: {
      type: 'input',
      minLength: 6,
      maxLength: 16,
      regex: ''
    },
    username: {
      type: 'input',
      minLength: 4,
      maxLength: 16,
      regex: ''
    }
  };

  setupValidator('vx-login-form', validation_rules);

  setupPKCE();
  runAuthorize({ profile: {} });
});

