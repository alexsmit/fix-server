ALTER TABLE `user_tokens` 

DROP FOREIGN KEY `user_tokens_ibfk_1`;

ALTER TABLE `user_tokens` 
CHANGE COLUMN `user_name` `user_name` VARCHAR(32) NOT NULL ,
DROP INDEX `fk_user_id` ;

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 3 );
