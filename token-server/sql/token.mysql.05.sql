ALTER TABLE `user_tokens` 
ADD COLUMN `nonce` varchar(16) AFTER `user_agent`;

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 5 );
