ALTER TABLE `user_tokens` 
ADD COLUMN `token` varchar(36) AFTER `user_agent`;

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 6 );
