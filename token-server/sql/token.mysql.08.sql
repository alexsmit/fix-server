ALTER TABLE `user_tokens` 
ADD COLUMN `hits` int(10) unsigned AFTER `user_agent`;

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 8 );
