ALTER TABLE `user_tokens` 
CHANGE COLUMN `nonce` `nonce` VARCHAR(36);

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 7 );
