ALTER TABLE `user_tokens` 
ADD INDEX `ix_expires` (`expires` ASC);

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 1 );
