DROP TABLE `user_tokens`;

CREATE TABLE `user_tokens` (
  `id` varchar(36) NOT NULL,
  `user_name` int(11) unsigned NOT NULL,
  `type` varchar(100) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_id` (`user_name`),
  KEY `ix_expires` (`expires`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_name`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DELETE from `token_version`;
INSERT INTO `token_version` (version) VALUES ( 2 );
