#ifndef _STDAFX_TOKEN_SERVER_H
#define _STDAFX_TOKEN_SERVER_H

#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <memory>
#include <algorithm>
#include <vector>
#include <string>
#include <chrono>
#include <utility>

#include <boost/variant.hpp>

// #define BOOST_SPIRIT_THREADSAFE
// #include <boost/filesystem.hpp>
// #include <boost/archive/basic_archive.hpp>
// #include <boost/property_tree/ptree.hpp>
// #include <boost/property_tree/ptree_serialization.hpp>
// #include <boost/optional.hpp>
// #include <boost/foreach.hpp>

// #include <client_http.hpp>
// #include <client_https.hpp>
// using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
// using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

/// Common debug parameters, used for unit tests only
struct DebugEnvironment {
  std::string DEBUG_HOST;           //!< will map to current host and specific port
  std::string DEBUG_HOST_PROFILE;   //!< profile server host:port
  // std::string DEBUG_HOST_PROFILE2;  //!< alternative profile server host:port
  std::string TOKEN_SERVER_HOST;    //!< static token server host:port
  static DebugEnvironment env;      //!< initialized instance for debug environment, used in cppunit tests
  void init();                      //!< setup debut enviromnent. Eacn unit test should implement this function.
};

#define DEBUG_HOST DebugEnvironment::env.DEBUG_HOST
#define DEBUG_HOST_PROFILE DebugEnvironment::env.DEBUG_HOST_PROFILE
// #define DEBUG_HOST_PROFILE2 DebugEnvironment::env.DEBUG_HOST_PROFILE2
#define TOKEN_SERVER_HOST DebugEnvironment::env.TOKEN_SERVER_HOST
// #define DEBUG_HOST "localhost:9998"
// #define DEBUG_HOST_PROFILE "localhost:9997"
// #define DEBUG_HOST_PROFILE2 "localhost:9996"
// #define TOKEN_SERVER_HOST "token-server:9998"

#define SERVER_TYPE "token"
#define SERVER_TOKEN

#ifdef NDEBUG
#define DEFAULT_TTL 300L
#define DEFAULT_TTL_REFRESH (30 * 86400L)
#define DEFAULT_TTL_REGISTRATION 86400L
#else
#define DEFAULT_TTL 300L
#define DEFAULT_TTL_REFRESH 600L
#define DEFAULT_TTL_REGISTRATION 600L
#endif

#define DEFAULT_HTTP_PORT 8082
#define DEFAULT_HTTPS_PORT 8482

#define DEBUG_TTL 3UL

#define POOL_SIZE 4

/// parameters to start token server
struct RunParams {
  // positional parameters
  int port;                      //!< http port
  int ports;                     //!< https port or -1 to disable
  unsigned long ttl;             //!< TTL for tokens
  int debug;                     //!< non-zero to run in a debug mode
  bool no_https;                 //!< endpoints will be registered on non-https hanlder
  unsigned long reg_ttl;         //!< registration token TTL
  bool no_mail;                  //!< do not send any emails
  unsigned int shutdown;         //!< auto-shutdown timeout in seconds
  unsigned long refresh_ttl;     //!< refresh token TTL
  unsigned int threads;          //!< number of threads. NOTE: increasing this number may have detremental effect
  std::string profile_provider;  //!< profile provider name
  unsigned int test;             //!< n/a
  bool doclean;                  //!< n/a
  unsigned int clean_timeout;    //!< inteval between cleanup events
};

#endif
