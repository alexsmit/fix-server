#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /api/v1/authn (public authentication)
      class AuthHandlerTestAuthn : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestAuthn);

        CPPUNIT_TEST(testAuthn);
        CPPUNIT_TEST(testAuthnAPI);
        CPPUNIT_TEST(testAuthnAPICompatibility);
        CPPUNIT_TEST(testAuthnWrongCredentials);
        CPPUNIT_TEST(testAuthnBadUsername);
        CPPUNIT_TEST(testAuthnBadPassword);
        CPPUNIT_TEST(testAuthnOptions);
        CPPUNIT_TEST(testAuthResponseToCompat);
        CPPUNIT_TEST(testAuthResponseFromCompat);
        CPPUNIT_TEST(testAuthResponseFromNoExp);

        CPPUNIT_TEST_SUITE_END();

      public:
        AuthHandlerTestAuthn();
        ~AuthHandlerTestAuthn();

        void testAuthn();                  //!< happy path, correct credentials, expect session token and profile
        void testAuthnAPI();               //!< happy path, using just API, correct credentials, expect session token and profile
        void testAuthnAPICompatibility();  //!< happy path,
        void testAuthnWrongCredentials();  //!< wrong credentails, expect not authorized
        void testAuthnBadUsername();       //!< missing username in the request
        void testAuthnBadPassword();       //!< missing password in the request
        void testAuthnOptions();           //!< pre-flight test OPTIONS request
        void testAuthResponseToCompat();   //!< AuthResponse serialize test
        void testAuthResponseFromCompat();   //!< AuthResponse parse from Compat
        void testAuthResponseFromNoExp();   //!< AuthResponse parse w/o expired
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
