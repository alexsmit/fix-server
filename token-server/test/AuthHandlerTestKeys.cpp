/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestKeys.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestKeys::AuthHandlerTestKeys() {}
AuthHandlerTestKeys::~AuthHandlerTestKeys() {}

/**
 * @brief Testing JWKS keys urls
 *
 * URLs are:
 * server/.well-known/jwks.json
 * server/oauth2/v1/keys
 * server/oauth2/.well-known/jwks.json
 */
class KeysRequestFix : public AuthorizeFixBase {
public:
  string url;            //!< keys URL
  string urljwks;        //!< set to well-known jwks url
  string urlAuthServer;  //!< set to default server prefix
  int mode;              //!< type of request, see constructor
  bool useGet;           //!< GET or OPTIONS
  ModelBase& req;        //!< request object

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param mode 0 - default server keys, 1 - global jwks url, 2 - default server well known jwks
  /// @param useGet true - using GET, false - OPTIONS call
  KeysRequestFix(shared_ptr<TokenServerContext> context,
                 ModelBase& req,
                 int mode,
                 bool useGet) : AuthorizeFixBase(context), req(req), mode(mode), useGet(useGet) {
    urlAuthServer = "/oauth2/default";
    url = "/v1/keys";
    urljwks = "/.well-known/jwks.json";
  }

  /// run request, parse result
  void Handle(const string& method = "POST") {
    SimpleWeb::CaseInsensitiveMultimap qmap;
    if (!req.client_id.empty()) {
      qmap.emplace("client_id", req.client_id);
    }

    string query = SimpleWeb::QueryString::create(qmap);

    stringstream sin;
    stringstream sout;

    header.emplace("Origin", "https://jwt.io");

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    string request_url;
    switch (mode) {
      case 0:
        request_url = urlAuthServer + url;
        break;
      case 1:
        request_url = urljwks;
        break;
      case 2:
        request_url = urlAuthServer + urljwks;
        break;
    }
    env.no_req->path = request_url;
    env.no_req->method = (useGet) ? "GET" : "OPTIONS";
    env.no_req->header = header;
    env.no_req->query_string = query;
    env.Handle();

    ParseResult(sout);
  }
};

void AuthHandlerTestKeys::KeysWithInvalidClientId() {
  ModelBase r;
  r.client_id = "wrong_client_id";
  KeysRequestFix fix(context, r, 0, true);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("client_id is invalid"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestKeys::KeysAuthServer() {
  Keys(0, true);
}

void AuthHandlerTestKeys::KeysJwksBase() {
  Keys(1, true);
}

void AuthHandlerTestKeys::KeysJwksAuthServer() {
  Keys(2, true);
}

void AuthHandlerTestKeys::KeysAuthServerOpt() {
  Keys(0, false);
}

void AuthHandlerTestKeys::KeysJwksBaseOpt() {
  Keys(1, false);
}

void AuthHandlerTestKeys::KeysJwksAuthServerOpt() {
  Keys(2, false);
}

void AuthHandlerTestKeys::KeysAuthServerOptRotation() {
  context->section->setParam("key_rotation", "1000");
  Keys(0, false, 1000);
}

void AuthHandlerTestKeys::KeysJwksBaseOptRotation() {
  context->section->setParam("key_rotation", "1000");
  Keys(1, false, 1000);
}

void AuthHandlerTestKeys::KeysJwksAuthServerOptRotation() {
  context->section->setParam("key_rotation", "1000");
  Keys(2, false, 1000);
}

void AuthHandlerTestKeys::Keys(int mode, bool useGet, int age) {
  ModelBase r;
  r.client_id = CLIENT_ID;
  KeysRequestFix fix(context, r, mode, useGet);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  if (!useGet) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Allow", string("GET,OPTIONS"), fix.pt_header.get("Allow", ""));
  }
  auto sage = (boost::format("max-age=%d, must-revalidate") % age).str();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Cache-Control", sage, fix.pt_header.get("Cache-Control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Origin", string("https://jwt.io"), fix.pt_header.get("Access-Control-Allow-Origin", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Credentials", string("true"), fix.pt_header.get("Access-Control-Allow-Credentials", ""));

  if (!useGet) {
    return;
  }

  auto sec = context->config->Section("token_provider");
  auto algo = sec["algo"].str();

  auto& pt = fix.pt_body;
  CPPUNIT_ASSERT_MESSAGE("Keys", pt.count("keys") > 0);
  auto c = pt.get_child("keys");
  for (auto& kv : c) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("kty", string("RSA"), kv.second.get("kty", ""));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("use", string("sig"), kv.second.get("use", ""));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("alg", string("RS256"), kv.second.get("alg", ""));
    CPPUNIT_ASSERT_MESSAGE("n", !kv.second.get("n", "").empty());
    CPPUNIT_ASSERT_MESSAGE("e", !kv.second.get("e", "").empty());
    CPPUNIT_ASSERT_MESSAGE("kid", !kv.second.get("kid", "").empty());
  }
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestKeys);

#endif
