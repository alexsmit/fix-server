#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <boost/lexical_cast.hpp>
#include <chrono>
#include <vector>

#include "TokenProviderBaseTest.h"

namespace vx {
  namespace openid {
    namespace test {
      /// token provider, mysql implementation test
      class TokenProviderSociTest : public CppUnit::TestFixture, private TokenProviderBaseTest {
        CPPUNIT_TEST_SUITE(TokenProviderSociTest);

        CPPUNIT_TEST(testKey);
        CPPUNIT_TEST(testCreateToken);
        CPPUNIT_TEST(testCreateTokenWithRefreshRecord);
        CPPUNIT_TEST(testCreateTokenRefresh);
        CPPUNIT_TEST(testCreateTokenWithTTL);
        CPPUNIT_TEST(testHit);
        CPPUNIT_TEST(testHitReset);
        CPPUNIT_TEST(testValidateToken);
        CPPUNIT_TEST(testValidateNonce);
        CPPUNIT_TEST(testClean);
        CPPUNIT_TEST(testSetTTL);
        CPPUNIT_TEST(testSetRefreshTTL);
        CPPUNIT_TEST(testNonce);
        CPPUNIT_TEST(testMetadata);
        CPPUNIT_TEST(testGetTokenByNonce);
        CPPUNIT_TEST(testDeleteTokenByNonce);
        CPPUNIT_TEST(testDeleteTokenByNonceEmpty);

        CPPUNIT_TEST(testGetTokenByRefreshToken);
        CPPUNIT_TEST(testAccessTokenDeleted);
        CPPUNIT_TEST(testRefreshTokenDeleted);

        CPPUNIT_TEST(testUserTokenMetadata);
        CPPUNIT_TEST(testTokenMetadataMap);

        CPPUNIT_TEST(testRefreshReused);

        CPPUNIT_TEST(testName);

        CPPUNIT_TEST_SUITE_END();

      public:
        TokenProviderSociTest();
        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
