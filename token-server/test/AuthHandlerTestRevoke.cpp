/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestRevoke.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

AuthHandlerTestRevoke::AuthHandlerTestRevoke() {}
AuthHandlerTestRevoke::~AuthHandlerTestRevoke() {}

/// fixture class to verify /v1/revoke endpoint
class RevokeRequestFix : public AuthorizeFixBase {
public:
  string url;              //!< default or specific URL, set by the constructor
  IntrospectRequest& req;  //!< request object, set by the constructor
  string error_result;     //!< error output to cerr

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param url default or specific URL
  RevokeRequestFix(shared_ptr<TokenServerContext> context,
                   IntrospectRequest& req,
                   string url = "/oauth2/default/v1/revoke") : AuthorizeFixBase(context), req(req), url(url) {
  }

  /// run request, parse result
  void Handle(const string& method = "POST") {
    stringstream sin;
    stringstream sout;
    string slen;

    if (method == "POST") {
      ptree pt;
      toPtree<IntrospectRequest>(req, pt);
      stringstream pt_out;
      write_json(pt_out, pt);

      string str_out = pt_out.str();
      slen = boost::lexical_cast<string>(str_out.length());
      sin.str(pt_out.str());
    }

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = method;

    if (method == "POST") {
      env.no_req->header = header;
      env.no_req->header.emplace("Content-Type", "application/json");
      env.no_req->header.emplace("Content-Length", slen);
    }
    else {
      env.no_req->header.emplace("Origin", "https://jwt.io");
    }

    // capture cerr
    stringstream out;
    std::streambuf* out_buf(std::cerr.rdbuf(out.rdbuf()));
    env.Handle();
    std::cerr.rdbuf(out_buf);
    error_result = out.str();

    ParseResult(sout);
  }
};

void AuthHandlerTestRevoke::RevokeWrongClientId() {
  IntrospectRequest r;
  r.client_id = "wrong_client_id";
  RevokeRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestRevoke::RevokeNoToken() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  RevokeRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("token is not provided"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestRevoke::RevokeWrongTokenType() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = "token";
  r.token_type_hint = "wrong_hint";
  RevokeRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid token type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestRevoke::RevokeWrongRefreshToken() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token_type_hint = "refresh_token";
  r.token = "wrong_refresh";
  RevokeRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking wrong refresh token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeRefreshExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  RevokeRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking expired refresh token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 30;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  RevokeRequestFix fix(context, r);

  auto tok_refresh = token_provider->GetTokenByNonce(r.token);
  CPPUNIT_ASSERT_MESSAGE("refresh token is valid", !tok_refresh.nonce.empty() && !tok_refresh.token.empty());

  fix.Handle();

  tok_refresh = token_provider->GetTokenByNonce(r.token);
  CPPUNIT_ASSERT_MESSAGE("refresh token is not valid", tok_refresh.nonce.empty());

  auto tok_access = token_provider->GetTokenByRefresh(r.token);
  CPPUNIT_ASSERT_MESSAGE("access token is not valid", tok_access.nonce.empty());

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
}

void AuthHandlerTestRevoke::RevokeRefreshBasicAuth() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFhOjExMQ==");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
}

void AuthHandlerTestRevoke::RevokeAccessInvalid() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = "wrong access token";
  r.token_type_hint = "access_token";
  RevokeRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking an invalid or expired token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeAccess() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 2;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  auto refresh_token = ops.token_result.get("refresh_token", "");
  RevokeRequestFix fix(context, r);

  auto tok_refresh = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("refresh token is valid", !tok_refresh.nonce.empty() && !tok_refresh.token.empty());

  fix.Handle();

  tok_refresh = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("refresh token valid and has no reference to access token", !tok_refresh.nonce.empty() && tok_refresh.token.empty());

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
}

void AuthHandlerTestRevoke::RevokeAccessBasicAuth() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFhOjExMQ==");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
}

void AuthHandlerTestRevoke::RevokeAccessExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  context->ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  RevokeRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking an invalid or expired token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeId() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 200;
  context->ttl = 100;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Using ID token for revoke\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeIdBasicAuth() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 200;
  context->ttl = 100;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFhOjExMQ==");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Using ID token for revoke\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeIdExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  context->ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "access_token";
  RevokeRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking an invalid or expired token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeTwiceRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix0(context, r);
  fix0.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix0.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string(""), fix0.error_result);

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking wrong refresh token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeTwiceAccess() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix0(context, r);
  fix0.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix0.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string(""), fix0.error_result);

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking an invalid or expired token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeRefreshAfterAccess() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  RevokeRequestFix fix0(context, r);
  fix0.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix0.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string(""), fix0.error_result);

  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string("Revoking wrong refresh token\n"), fix.error_result);
}

void AuthHandlerTestRevoke::RevokeAccessAfterRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 30;
  context->ttl = 2;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.client_secret = CLIENT_SECRET;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  RevokeRequestFix fix0(context, r);
  fix0.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix0.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error output", string(""), fix0.error_result);

  auto tok_refresh = token_provider->GetTokenByNonce(r.token);
  CPPUNIT_ASSERT_MESSAGE("refresh token is not valid", tok_refresh.nonce.empty() /* && !tok_refresh.token.empty()*/);

  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  RevokeRequestFix fix(context, r);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty body", string("0"), fix.pt_header.get("Content-Length", ""));
  CPPUNIT_ASSERT_MESSAGE("no error output", fix.error_result.empty());
}

void AuthHandlerTestRevoke::RevokeOptions() {
  IntrospectRequest r;
  RevokeRequestFix fix(context, r);
  fix.Handle("OPTIONS");

  fix.assert_no_cache();
  fix.assert_cors();
  fix.assert_options("POST,OPTIONS");
}

void AuthHandlerTestRevoke::RevokeRefreshNoClientSecret() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
}

void AuthHandlerTestRevoke::RevokeRefreshBasicJustClientID() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
}

void AuthHandlerTestRevoke::RevokeAccessNoClientSecret() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
}

void AuthHandlerTestRevoke::RevokeAccessBasicJustClientID() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
}

void AuthHandlerTestRevoke::RevokePKCE(TokenOps& ops) {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";
  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3c. Token (with code_verifier) -- success
  ops.token(authorize_response, token_scope, code_verifier);
}

void AuthHandlerTestRevoke::RevokePKCERefreshNoClientSecret() {
  TokenOps ops(context);
  RevokePKCE(ops);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

void AuthHandlerTestRevoke::RevokePKCERefreshBasicJustClientID() {
  TokenOps ops(context);
  RevokePKCE(ops);

  IntrospectRequest r;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

void AuthHandlerTestRevoke::RevokePKCEAccessNoClientSecret() {
  TokenOps ops(context);
  RevokePKCE(ops);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";

  RevokeRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

void AuthHandlerTestRevoke::RevokePKCEAccessBasicJustClientID() {
  TokenOps ops(context);
  RevokePKCE(ops);

  IntrospectRequest r;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";

  RevokeRequestFix fix(context, r);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestRevoke);

#endif
