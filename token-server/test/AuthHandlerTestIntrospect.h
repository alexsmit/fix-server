#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /v1/introspect
      class AuthHandlerTestIntrospect : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestIntrospect);

        CPPUNIT_TEST(IntrospectNoClientId);
        CPPUNIT_TEST(IntrospectWrongClientId);
        CPPUNIT_TEST(IntrospectNoToken);
        CPPUNIT_TEST(IntrospectWrongTokenType);
        CPPUNIT_TEST(IntrospectRefreshWrongToken);
        CPPUNIT_TEST(IntrospectRefresh);
        CPPUNIT_TEST(IntrospectRefreshExpired);
        CPPUNIT_TEST(IntrospectAccessInvalid);
        CPPUNIT_TEST(IntrospectAccess);
        CPPUNIT_TEST(IntrospectAccessExpired);
        CPPUNIT_TEST(IntrospectIdInvalid);
        CPPUNIT_TEST(IntrospectId);
        CPPUNIT_TEST(IntrospectIdExpired);
        CPPUNIT_TEST(IntrospectAccessRevoked);
        CPPUNIT_TEST(IntrospectIdRevoked);

        CPPUNIT_TEST_SUITE_END();


      public:
        AuthHandlerTestIntrospect();
        ~AuthHandlerTestIntrospect();

        void IntrospectNoClientId();         //!< wrong input parameters: no client_id
        void IntrospectWrongClientId();      //!< wrong input parameters: wrong client_id
        void IntrospectNoToken();            //!< token parameter is missing
        void IntrospectWrongTokenType();     //!< token_type_hint is wrong
        void IntrospectRefreshWrongToken();  //!< refresh_token (wrong value)
        void IntrospectRefresh();            //!< refresh_token
        void IntrospectRefreshExpired();     //!< refresh_token (expired)
        void IntrospectAccessInvalid();      //!< access_token (invalid)
        void IntrospectAccess();             //!< access_token
        void IntrospectAccessExpired();      //!< access_token (expired)
        void IntrospectIdInvalid();          //!< id_token (invalid)
        void IntrospectId();                 //!< id_token
        void IntrospectIdExpired();          //!< id_token (expired)
        void IntrospectAccessRevoked();      //!< revoke and introspect
        void IntrospectIdRevoked();          //!< revoke and introspect
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
