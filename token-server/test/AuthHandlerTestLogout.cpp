/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/format.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/openid/models/Models.h>
#include <vx/StringUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestLogout.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestLogout::AuthHandlerTestLogout() {}

AuthHandlerTestLogout::~AuthHandlerTestLogout() {}

//-----------------------------------------

/// fixture class to test /v1/logout enpoint
class LogoutRequestFix : public AuthorizeFixBase {
public:
  LogoutRequest& req;  //!< request object, set by the constructor
  string url;          //!< url, set by the constructor
  string cookie;       //!< session cookie value, set by the constructor

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param cookie session cookie value to be set
  /// @param url default or specific url
  LogoutRequestFix(shared_ptr<TokenServerContext> context,
                   LogoutRequest& req,
                   string cookie,
                   string url = "/oauth2/default/v1/logout") : AuthorizeFixBase(context), req(req), url(url), cookie(cookie) {
  }

  /// run request and parse result
  void Handle(const string& method = "GET") {
    stringstream sin;
    stringstream sout;

    if (!cookie.empty()) {
      string coo = (boost::format("%s=%s") % SESSION_COOKIE % cookie).str();
      header.emplace("Cookie", coo);
    }

    header.emplace("Origin", "https://jwt.io");

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = method;
    env.no_req->header = header;
    env.no_req->query_string << req;
    env.Handle();
    ParseResult(sout);
  }

  /// verify session cookie is set to be removed
  void assert_cookie() {
    auto coo = pt_header.get("Set-Cookie", "");
    CPPUNIT_ASSERT_MESSAGE("Should have set-cookie header", !coo.empty());
    auto vcoo = StringUtil::split(coo, '=');
    CPPUNIT_ASSERT_EQUAL_MESSAGE("cookie name", string(SESSION_COOKIE), vcoo[0]);
    set<string> cookie_items;
    boost::split(cookie_items, coo, boost::is_any_of(";"));
    CPPUNIT_ASSERT_MESSAGE("Should have max-age=0", cookie_items.find("max-age=0") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have HttpOnly", cookie_items.find("HttpOnly") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have Path=/", cookie_items.find("Path=/") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have SameSite=None", cookie_items.find("SameSite=None") != cookie_items.end());
  }
};

//-----------------------------------------

void AuthHandlerTestLogout::AuthnLogoutConfig() {
  auto clogout = context->section->getTree("").get_child_optional("logout_uri");
  CPPUNIT_ASSERT_MESSAGE("logout_uri", !!clogout);
}

void AuthHandlerTestLogout::AuthnLogoutWrongUri() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "some_url";

  LogoutRequestFix fix(context, req, "coo");
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid logout url"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestLogout::AuthnLogoutNoToken() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";

  LogoutRequestFix fix(context, req, "coo");
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("token not provided"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestLogout::AuthnLogoutWrongToken() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";
  req.id_token_hint = "wrong_token";

  LogoutRequestFix fix(context, req, "coo");
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestLogout::AuthnLogoutNonIdToken() {
  string token_scope = "openid email";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";
  req.id_token_hint = ops.token_result.get("access_token", "");

  LogoutRequestFix fix(context, req, "coo");
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestLogout::AuthnLogoutNoCookie() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";

  LogoutRequestFix fix(context, req, "");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Location header", req.post_logout_redirect_uri, location);

  fix.assert_cookie();
}

void AuthHandlerTestLogout::AuthnLogoutNoCookieState() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";
  req.state = "state";

  LogoutRequestFix fix(context, req, "");
  fix.Handle();

  auto expected_url = (boost::format("%s?state=state") % req.post_logout_redirect_uri).str();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Location header", expected_url, location);

  fix.assert_cookie();
}

void AuthHandlerTestLogout::AuthnLogout() {
  string token_scope = "openid email";
  context->refresh_ttl = 10;
  context->ttl = 10;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  // write_json(cout, ops.token_metadata);

  auto rec = provider->LoadUser(USER_NAME, ProfileKey::username);

  auto session_cookie = token_provider->CreateTokenMeta(
      rec.userid,
      "session",
      "",
      10,
      {{"access_token", ops.token_metadata.get("access_token", "")},
       {"id_token", ops.token_metadata.get("id_token", "")}});

  LogoutRequest req;
  req.post_logout_redirect_uri = "http://127.0.0.1:8080/logout";
  req.id_token_hint = ops.token_result.get("id_token", "");
  req.state = "state";

  LogoutRequestFix fix(context, req, session_cookie.nonce);
  fix.Handle();

  auto expected_url = (boost::format("%s?state=state") % req.post_logout_redirect_uri).str();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Location header", expected_url, location);

  auto id_token_rec = token_provider->GetTokenByNonce(ops.token_metadata.get("id_token", ""));
  auto access_token_rec = token_provider->GetTokenByNonce(ops.token_metadata.get("access_token", ""));
  if (!id_token_rec.nonce.empty()) {
    cout << "Not deleted id_token: " << id_token_rec.nonce << endl;
  }
  if (!access_token_rec.nonce.empty()) {
    cout << "Not deleted access_token: " << access_token_rec.nonce << endl;
  }

  CPPUNIT_ASSERT_MESSAGE("Should delete id token", id_token_rec.nonce.empty());
  CPPUNIT_ASSERT_MESSAGE("Should delete id token", access_token_rec.nonce.empty());

  // check cookie reset
  fix.assert_cookie();
}

void AuthHandlerTestLogout::AuthnLogoutOptions() {
  LogoutRequest req;

  LogoutRequestFix fix(context, req, "");
  fix.Handle("OPTIONS");

  fix.assert_no_cache();
  fix.assert_cors();
  fix.assert_options("GET,OPTIONS");
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestLogout);

#endif
