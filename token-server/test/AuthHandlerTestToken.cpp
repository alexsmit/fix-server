/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/regex.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestToken.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

AuthHandlerTestToken::AuthHandlerTestToken() {}
AuthHandlerTestToken::~AuthHandlerTestToken() {}

/// fixture class to test /v1/token endpoints
class TokenRequestFix : public AuthorizeFixBase {
public:
  string url;         //!< default or specific URL, set by the constructor
  TokenRequest& req;  //!< request object, set by the constructor

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param url default or specific URL
  TokenRequestFix(shared_ptr<TokenServerContext> context,
                  TokenRequest& req,
                  string url = "/oauth2/default/v1/token") : AuthorizeFixBase(context), req(req), url(url) {
  }

  /// run request and parse reslut
  void Handle(const string& method = "POST") {
    stringstream sout;
    stringstream sin;
    string slen;

    if (method == "POST") {
      string query;
      query << req;
      slen = boost::lexical_cast<string>(query.size());
      sin.str(query);
    }

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = method;
    env.no_req->header = header;

    if (method == "POST") {
      env.no_req->header.emplace("Content-Type", "application/x-www-form-urlencoded");
      env.no_req->header.emplace("Content-Length", slen);
    }
    else {
      env.no_req->header.emplace("Origin", "https://jwt.io");
    }

    env.Handle();

    ParseResult(sout);
  }
};

TokenOps::TokenOps(shared_ptr<TokenServerContext> context) : context(context) {
}

AuthResponse TokenOps::Authn() {
  AuthRequest authn_request = {USER_NAME, USER_PASSWORD};
  AuthnFlow flow_authn(*context, authn_request);
  flow_authn.Execute();
  auto authn_result = flow_authn.GetResult();
  AuthResponse authn_response{};
  authn_result >> authn_response;
  return authn_response;
}

AuthorizeResponse TokenOps::Authorize(AuthResponse& authn_response, const string& token_scope, const string& code_verifier, const string& code_challenge_method) {
  AuthorizeRequest authorize_request;
  authorize_request.client_id = CLIENT_ID;
  authorize_request.redirect_uri = "http://localhost:8080/authorize_callback";
  authorize_request.nonce = "nonce";
  authorize_request.response_type = "code";
  authorize_request.scope = token_scope;
  authorize_request.state = "state";
  authorize_request.prompt = "none";
  authorize_request.sessionToken = authn_response.sessionToken;
  if (!code_verifier.empty()) {
    string rsa_method = (code_challenge_method == "S256") ? "sha256" : "sha512";
    authorize_request.code_challenge_method = code_challenge_method;
    authorize_request.code_challenge = RSAKeyInfo::getHash(code_verifier, rsa_method);
  }

  AuthorizeFlow flow_authorize(*context, authorize_request, "");
  flow_authorize.Execute();
  auto authorize_result = flow_authorize.GetResult();
  AuthorizeResponse authorize_response;
  authorize_result >> authorize_response;
  return authorize_response;
}

void TokenOps::token(AuthorizeResponse& authorize_response, const string& token_scope, const string& code_verifier) {
  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  if (code_verifier.empty()) {
    token_request.client_secret = CLIENT_SECRET;
  }
  token_request.code = authorize_response.code;
  token_request.grant_type = "authorization_code";
  token_request.redirect_uri = "http://localhost:8080/authorize_callback";
  token_request.scope = token_scope;
  token_request.code_verifier = code_verifier;
  token_flow.reset(new TokenFlow(*context, token_request));
  token_flow->Execute();
  token_result = token_flow->GetResult();
}

void TokenOps::token(const string& user, const string& password, const string& token_scope) {
  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "password";
  token_request.username = user;
  token_request.password = password;
  token_request.redirect_uri = "http://localhost:8080/authorize_callback";
  token_request.scope = token_scope;
  token_flow.reset(new TokenFlow(*context, token_request));
  token_flow->Execute();
  token_result = token_flow->GetResult();
  token_metadata = token_flow->GetMetadata();
}

void TokenOps::token(const string& refresh_token, const string& token_scope) {
  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;
  token_request.scope = token_scope;
  token_flow.reset(new TokenFlow(*context, token_request));
  token_flow->Execute();
  token_result = token_flow->GetResult();
}

void TokenOps::assert_scope(const string& expected, const string& actual) {
  auto vexp = StringUtil::split(expected, " ,");
  auto vact = StringUtil::split(actual, " ,");
  set<string> sexp, sact;
  sexp.insert(vexp.begin(), vexp.end());
  sact.insert(vact.begin(), vact.end());
  string msg = (boost::format("compare expected: [%s] and actual [%s]") % expected % actual).str();
  CPPUNIT_ASSERT_MESSAGE(msg, sexp == sact);
}

void TokenOps::assert_token_success(const string& token_scope, const string& token_nonce) {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, (unsigned)token_flow->GetResultCode());

  CPPUNIT_ASSERT_MESSAGE("access_token", !token_result.get("access_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("id_token", !token_result.get("id_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("refresh_token", !token_result.get("refresh_token", "").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("Bearer"), token_result.get("token_type", ""));
  string sttl = boost::lexical_cast<string>(DEBUG_TTL);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expires_in", sttl, token_result.get("expires_in", ""));
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", token_scope, token_result.get("scope", ""));
  assert_scope(token_scope, token_result.get("scope", ""));

  auto head = token_flow->GetResultHeader();

  CPPUNIT_ASSERT_MESSAGE("Should have Cache-Control", head.find("Cache-Control") != head.end());
  auto cache_value = head.find("Cache-Control")->second;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Caqche-Control no-store", string("no-store"), cache_value);

  CPPUNIT_ASSERT_MESSAGE("Should have Pragma", head.find("Pragma") != head.end());
  auto pragma_value = head.find("Pragma")->second;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Pragma no-cache", string("no-cache"), pragma_value);

  // refresh token validation
  assert_token(token_result.get("access_token", ""), token_nonce);
  assert_token(token_result.get("id_token", ""), token_nonce);
}

void TokenOps::assert_token(const string& token, const string& token_nonce) {
  // cout << "token: " << token << '\n';
  auto vparts = StringUtil::split(token, '.');
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token parts", (size_t)3, vparts.size());
  auto token_body_str = random_util().base64decode(vparts[1]);
  stringstream s(token_body_str);
  ptree token_body;
  read_json(s, token_body);

  if (token_nonce.empty())
    CPPUNIT_ASSERT_MESSAGE("token nonce", !token_body.get("nonce", "").empty());
  else
    CPPUNIT_ASSERT_EQUAL_MESSAGE("token nonce", token_nonce, token_body.get("nonce", ""));
}

int TokenOps::assert_token_error(unsigned code, const string& error, const string& error_description) {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", code, (unsigned)token_flow->GetResultCode());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", error, token_result.get("error", ""));

  boost::regex reg(error_description);
  boost::smatch match;
  bool matched = false;
  int idx = -1;
  if (boost::regex_search(token_result.get("error_description", ""), match, reg)) {
    matched = true;
    auto val_a = match["a"].str();
    auto val_b = match["b"].str();
    if (!val_a.empty())
      idx = 1;
    else if (!val_b.empty())
      idx = 2;
    else
      idx = 0;
  }

  CPPUNIT_ASSERT_MESSAGE("has error description", matched);

  // if (SimpleWeb::regex::regex_match(path, sm_res, h.key)) {
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", error_description, token_result.get("error_description", ""));

  return idx;
}

void TokenOps::assert_token_scope(const string& token, set<string> scopes) {
  auto vparts = StringUtil::split(token, '.');
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token parts", (size_t)3, vparts.size());
  auto token_body_str = random_util().base64decode(vparts[1]);
  stringstream s(token_body_str);
  ptree token_body;
  read_json(s, token_body);
  auto scp = token_body.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("array of scopes: scp", !!scp);

  string extra_scope;
  ;
  for (auto& kv : *scp) {
    auto u = kv.second.data();
    if (scopes.find(u) != scopes.end()) continue;
    extra_scope = u;
    break;
  }

  if (extra_scope.length() > 0)
    cerr << "extra scope: " << extra_scope << '\n';

  CPPUNIT_ASSERT_MESSAGE("should not have extra scopes", extra_scope.empty());
}

//---------------------------------------------------
//--- tests
//---------------------------------------------------

void AuthHandlerTestToken::AuthTokenNotAuthorizedAPINoClientId() {
  TokenRequest req;
  TokenFlow flow(*context, req);
  flow.Execute();
  auto result = flow.GetResult();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, (unsigned)flow.GetResultCode());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, result.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenNotAuthorizedAPIWrongClientId() {
  TokenRequest req;
  req.client_id = "wrong_client";
  TokenFlow flow(*context, req);
  flow.Execute();
  auto result = flow.GetResult();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, (unsigned)flow.GetResultCode());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, result.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenNotAuthorizedAPINoClientSecret() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  TokenFlow flow(*context, req);
  flow.Execute();
  auto result = flow.GetResult();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, (unsigned)flow.GetResultCode());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("unsupported_grant_type"), result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), result.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenNotAuthorized() {
  stringstream sout;
  TokenRequest req;
  TokenRequestFix fix(context, req);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));

  req.client_id = "wrong_client";
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));

  req.client_id = CLIENT_ID;
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("unsupported_grant_type"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantEmpty() {
  stringstream sout;
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("unsupported_grant_type"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantWrong() {
  stringstream sout;
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "wrong";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("unsupported_grant_type"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantPassword() {
  stringstream sout;
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "password";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantPasswordNoScope() {
  stringstream sout;
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "password";
  req.password = "pass";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("scope is missing"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantPasswordNoUsername() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "password";
  req.password = "pass";
  req.scope = "scope";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("username is missing"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantCode() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "authorization_code";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenWrongGrantCodeNoUri() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "authorization_code";
  req.code = "code";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("redirect_uri is not defined"), fix.pt_body.get("error_description", ""));
}


void AuthHandlerTestToken::AuthTokenWrongGrantRefreshToken() {
  stringstream sout;
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "refresh_token";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_grant"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid grant_type"), fix.pt_body.get("error_description", ""));
}

//-------------------------------------
//--- using internal API
//-------------------------------------

void AuthHandlerTestToken::AuthTokenExpiredCode() {
  string token_scope = "openid email";

  auto svc2 = context->getService("token");
  TokenProvider* token_svc = dynamic_cast<TokenProvider*>(svc2.get());
  if (token_svc == NULL) throw runtime_error("unable to get a service 'token'");
  auto token = token_svc->CreateToken("temp", "temp", "", 1);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  // Note: created token is invalid, so this test works only when it is expired. In such
  // situation we do not check for internal data

  TokenOps ops(context);
  AuthorizeResponse authorize_response;
  authorize_response.code = token.nonce;
  ops.token(authorize_response, token_scope, "");

  ops.assert_token_error(401, "not_authorized", "invalid or expired code");
}

void AuthHandlerTestToken::AuthTokenWrongCode() {
  string token_scope = "openid email";

  TokenOps ops(context);
  AuthorizeResponse authorize_response;
  authorize_response.code = "some_wrong_code";
  ops.token(authorize_response, token_scope, "");

  ops.assert_token_error(401, "not_authorized", "invalid or expired code");
}

void AuthHandlerTestToken::AuthTokenCode() {
  string token_scope = "openid email offline_access";

  TokenOps ops(context);

  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize (no code verifier)
  auto authorize_response = ops.Authorize(authn_response, token_scope, "");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3. Token (mp code_verifier) -- success
  ops.token(authorize_response, token_scope, "");

  ops.assert_token_success(token_scope);
}

void AuthHandlerTestToken::AuthTokenCodePKCENoVerifier() {
  string token_scope = "openid email";
  string code_verifier = "test123";

  TokenOps ops(context);

  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3a. Token (w/o code_verifier) -- fail
  ops.token(authorize_response, token_scope, "");

  ops.assert_token_error(401, "not_authorized", "code verifier is not provided");
}

void AuthHandlerTestToken::AuthTokenCodePKCEWrongVerifier() {
  string token_scope = "openid email";
  string code_verifier = "test123";

  TokenOps ops(context);

  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3b. Token (wrong code_verifier) -- fail
  ops.token(authorize_response, token_scope, "some_wrong_verifier");

  ops.assert_token_error(401, "not_authorized", "code verifier is not valid");
}

void AuthHandlerTestToken::AuthTokenCodePKCE() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);

  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3c. Token (with code_verifier) -- success
  ops.token(authorize_response, token_scope, code_verifier);

  ops.assert_token_success(token_scope);
}

void AuthHandlerTestToken::AuthTokenCodePKCE512() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);

  // 1. Authn
  auto authn_response = ops.Authn();
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authn_response.sessionToken.empty());
  // 2. Authorize
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S512");
  CPPUNIT_ASSERT_MESSAGE("session token from /authn", !authorize_response.code.empty());
  // 3c. Token (with code_verifier) -- success
  ops.token(authorize_response, token_scope, code_verifier);

  ops.assert_token_success(token_scope);
}

void AuthHandlerTestToken::AuthTokenWrongPassword() {
  string token_scope = "openid email";
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD2, token_scope);
  ops.assert_token_error(401, "not_authorized", "wrong username or password");
}

void AuthHandlerTestToken::AuthTokenPassword() {
  string token_scope = "openid email offline_access";
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  ops.assert_token_success(token_scope, "");  // note: not testing nonce for equality, only for existence
}

void AuthHandlerTestToken::AuthTokenWrongRefresh() {
  string token_scope = "openid email";
  TokenOps ops(context);
  ops.token("some_refresh", token_scope);
  ops.assert_token_error(401, "not_authorized", "invalid refresh token");
}

void AuthHandlerTestToken::AuthTokenExpiredRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  TokenOps ops(context);

  int old_idx = -1;
  bool multi_scenario = false; // set to true if we can get both expired and invalid token

  // cleanup thread is running, so, out token may get removed by the cleanup
  // in that case we will get "invalid refresh token" error, we need to cycle
  // again and try to use refresh token between moments when cleanup thread is running
  // to get another error - "expired refresh token"

  for(auto ii=0;ii<15;ii++) {
    ops.token(USER_NAME, USER_PASSWORD, token_scope);
    auto refresh_token = ops.token_result.get("refresh_token", "");
    cout << "W" << flush;
    std::this_thread::sleep_for(chrono::milliseconds(4000));
    ops.token(refresh_token, token_scope);
    int idx = ops.assert_token_error(401, "not_authorized", "(?<a>expired refresh token)|(?<b>invalid refresh token)");
    if (old_idx == -1) old_idx = idx;
    else {
      if (old_idx != idx) {
        multi_scenario = true;
        break;
      }
    }
  }

  CPPUNIT_ASSERT_MESSAGE("refresh token should be both invalid and expired", multi_scenario);

}

void AuthHandlerTestToken::AuthTokenRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 10;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");
  ops.token(refresh_token, token_scope);
  ops.assert_token_success(token_scope, "");
}

void AuthHandlerTestToken::AuthTokenRefreshSameRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 10;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  // load refresh token record
  UserToken token1_before = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("Refresh token exists", !token1_before.nonce.empty());

  ops.token(refresh_token, token_scope);
  auto refresh_token2 = ops.token_result.get("refresh_token", "");

  // load refresh token record after use (should not exist)
  UserToken token1_after = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("Refresh token deleted", token1_after.nonce.empty());

  CPPUNIT_ASSERT_MESSAGE("New refresh_token is different", refresh_token2 != refresh_token);


  // TokenOps ops2(context);
  // ops2.token(refresh_token, token_scope);
  // auto refresh_token2 = ops2.token_result.get("refresh_token", "");
  // cout << refresh_token << '\n';

  // UserToken old_token_after_refresh = token_provider->GetTokenByNonce(refresh_token);
  // CPPUNIT_ASSERT_MESSAGE("Refresh token should be deleted after being used", old_token_after_refresh.nonce.empty());
  // context->section->setParam("refresh_token_rotation","off");
  //  CPPUNIT_ASSERT_MESSAGE("New refresh_token should be the same", refresh_token2 == refresh_token);
}

void AuthHandlerTestToken::AuthTokenRefreshSameRefreshReuse() {
  context->section->setParam("refresh_token_rotation", "off");

  string token_scope = "openid email offline_access";
  context->refresh_ttl = 60;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  // load refresh token record
  UserToken token1_before = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("Refresh token exists", !token1_before.nonce.empty());

  ops.token(refresh_token, token_scope);
  auto refresh_token2 = ops.token_result.get("refresh_token", "");

  // load refresh token record after use (should not exist)
  UserToken token1_after = token_provider->GetTokenByNonce(refresh_token);
  CPPUNIT_ASSERT_MESSAGE("Refresh token still exists", !token1_after.nonce.empty());

  CPPUNIT_ASSERT_MESSAGE("New refresh_token is the same", refresh_token2 == refresh_token);
}

void AuthHandlerTestToken::AuthTokenOptions() {
  TokenRequest req;
  TokenRequestFix fix(context, req);

  fix.Handle("OPTIONS");

  fix.assert_options("POST,OPTIONS");
  fix.assert_cors();
  fix.assert_no_cache();
}

void AuthHandlerTestToken::AuthTokenPasswordNoRefresh() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "password";
  req.username = USER_NAME;
  req.password = USER_PASSWORD;
  req.scope = "openid";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_MESSAGE("No refresh token", fix.pt_body.get("refresh_token", "").empty());
}

void AuthHandlerTestToken::AuthTokenPasswordWithRefresh() {
  TokenRequest req;
  req.client_id = CLIENT_ID;
  req.client_secret = CLIENT_SECRET;
  req.grant_type = "password";
  req.username = USER_NAME;
  req.password = USER_PASSWORD;
  req.scope = "openid offline_access";
  TokenRequestFix fix(context, req);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_MESSAGE("With refresh token", !fix.pt_body.get("refresh_token", "").empty());
}

//-----------------------------------
//--- token with different scopes
//-----------------------------------

void AuthHandlerTestToken::AuthTokenRefreshSubsetScope() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 100;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;
  token_request.scope = "openid offline_access";

  TokenRequestFix fix(context, token_request);
  fix.Handle();
  auto access_token = fix.pt_body.get("access_token", "");
  // cout << access_token << '\n';
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  ops.assert_token_scope(access_token, {"openid", "offline_access"});

  // second refresh
  token_request.refresh_token = fix.pt_body.get("refresh_token", "");
  token_request.scope = "openid";
  TokenRequestFix fix2(context, token_request);
  fix2.SetHeader("Authorization", "Basic YWFh");
  fix2.Handle();
  access_token = fix2.pt_body.get("access_token", "");
  // cout << access_token << '\n';
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix2.code);
  ops.assert_token_scope(access_token, {"openid"});
}

void AuthHandlerTestToken::AuthTokenCodePKCESubsetScope() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);
  auto authn_response = ops.Authn();
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");

  TokenRequest token_request;
  token_request.code = authorize_response.code;
  token_request.grant_type = "authorization_code";
  token_request.redirect_uri = "http://localhost:8080/authorize_callback";
  token_request.scope = "openid";
  token_request.code_verifier = code_verifier;

  TokenRequestFix fix(context, token_request);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();
  auto access_token = fix.pt_body.get("access_token", "");
  // cout << access_token << '\n';
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  ops.assert_token_scope(access_token, {"openid"});
}


//-----------------------------------
//--- use cases without client_secret
//-----------------------------------

void AuthHandlerTestToken::AuthTokenPKCEBasicNoSecret() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);
  auto authn_response = ops.Authn();
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");

  TokenRequest token_request;
  token_request.code = authorize_response.code;
  token_request.grant_type = "authorization_code";
  token_request.redirect_uri = "http://localhost:8080/authorize_callback";
  token_request.scope = token_scope;
  token_request.code_verifier = code_verifier;

  TokenRequestFix fix(context, token_request);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  // CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenPKCENoSecret() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);
  auto authn_response = ops.Authn();
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");

  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  // token_request.client_secret = CLIENT_SECRET;
  token_request.code = authorize_response.code;
  token_request.grant_type = "authorization_code";
  token_request.redirect_uri = "http://localhost:8080/authorize_callback";
  token_request.scope = token_scope;
  token_request.code_verifier = code_verifier;

  TokenRequestFix fix(context, token_request);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  // CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenPasswordBasicNoSecret() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";
  TokenRequest token_request;
  token_request.grant_type = "password";
  token_request.username = USER_NAME;
  token_request.password = USER_PASSWORD;
  token_request.scope = token_scope;

  TokenRequestFix fix(context, token_request);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenPasswordNoSecret() {
  string token_scope = "openid email offline_access";
  string code_verifier = "test123";
  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  // token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "password";
  token_request.username = USER_NAME;
  token_request.password = USER_PASSWORD;
  token_request.scope = token_scope;

  TokenRequestFix fix(context, token_request);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenRefreshBasicNoSecret() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 10;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  TokenRequest token_request;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;

  TokenRequestFix fix(context, token_request);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenRefreshNoSecret() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 10;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  // token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;

  TokenRequestFix fix(context, token_request);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestToken::AuthTokenPKCERefreshNoSecret() {
  context->refresh_ttl = 10;

  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);
  auto authn_response = ops.Authn();
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  ops.token(authorize_response, token_scope, code_verifier);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  TokenRequest token_request;
  token_request.client_id = CLIENT_ID;
  // token_request.client_secret = CLIENT_SECRET;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;

  TokenRequestFix fix(context, token_request);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

void AuthHandlerTestToken::AuthTokenPKCERefreshBasicNoSecret() {
  context->refresh_ttl = 10;

  string token_scope = "openid email offline_access";
  string code_verifier = "test123";

  TokenOps ops(context);
  auto authn_response = ops.Authn();
  auto authorize_response = ops.Authorize(authn_response, token_scope, code_verifier, "S256");
  ops.token(authorize_response, token_scope, code_verifier);
  auto refresh_token = ops.token_result.get("refresh_token", "");

  TokenRequest token_request;
  token_request.grant_type = "refresh_token";
  token_request.refresh_token = refresh_token;

  TokenRequestFix fix(context, token_request);
  fix.SetHeader("Authorization", "Basic YWFh");
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestToken);

#endif
