/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include <BaseTest.h>

#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTest.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::openid::test;

//-----------------------------------------

AuthorizeFixBase::AuthorizeFixBase(shared_ptr<TokenServerContext> context)
    : context(context) {
}

AuthorizeFixBase::~AuthorizeFixBase() {}

void AuthorizeFixBase::ParseResult(stringstream& sout) {
  BeastParser<be::string_body> parser(sout);
  in_query = false;
  in_fragment = false;

  // auto soutstr = sout.str();
  // cout << soutstr;

  parser.Parse();
  for (auto& k : parser.resp) {
    pt_header.put(get_from_beast_stringview(k.name_string()), get_from_beast_stringview(k.value()));
  }
  auto bbody = parser.resp.body();
  body.resize(bbody.size());
  std::copy(bbody.begin(), bbody.end(), body.begin());

  if (!body.empty()) {
    try {
      stringstream sbody(body);
      read_json(sbody, pt_body);
    }
    catch (const std::exception& e) {  // ignored, because a response could be not a JSON
    }
  }

  auto location = pt_header.get("Location", "");
  if (!location.empty()) {
    auto qix = location.find('?');

    if (qix == string::npos) {
      qix = location.find('#');
      if (qix != string::npos) in_fragment = true;
    }
    else
      in_query = true;

    if (qix != string::npos) {
      auto query = location.substr(qix + 1);
      auto querymap = SimpleWeb::QueryString::parse(query);
      for (auto& kv : querymap) {
        pt_params.put(kv.first, kv.second);
      }
    }
  }

  code = parser.resp.result_int();
}

void AuthorizeFixBase::SetHeader(const string& key, const string& val) {
  header.emplace(key, val);
}

void AuthorizeFixBase::assert_cors() {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Origin", string("https://jwt.io"), pt_header.get("Access-Control-Allow-Origin", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Credentials", string("true"), pt_header.get("Access-Control-Allow-Credentials", ""));
}

void AuthorizeFixBase::assert_options(const string& accepted_methods) {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Allow", accepted_methods, pt_header.get("Allow", ""));
}

void AuthorizeFixBase::assert_no_cache() {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Pragma", string("no-cache"), pt_header.get("Pragma", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Cache-Control", string("no-cache, no-store"), pt_header.get("Cache-Control", ""));
}


#endif
