#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#if BOOST_VERSION >= 106600


#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// all token server endpoints tests
      class RegistrationSVCHandlerTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(RegistrationSVCHandlerTest);

        CPPUNIT_TEST(testRegisterSuccess);
        CPPUNIT_TEST(testRegisterFailure);
        CPPUNIT_TEST(testRegisterSuccessHandler);
        CPPUNIT_TEST(testRegisterFailureHandler);

        CPPUNIT_TEST(testResetSuccess);
        CPPUNIT_TEST(testResetFailure);
        CPPUNIT_TEST(testResetSuccessHandler);
        CPPUNIT_TEST(testResetFailureHandler);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ProfileProvider> provider;
        shared_ptr<HttpClient> client;
        string password_request, password_wrong_request, password_bad_request;
        string password_request_form;
        shared_ptr<TokenServerContext> context;

        void getToken(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token);
        void getTokenForm(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token);
        void validateJWKS(string jwks);

      public:
        RegistrationSVCHandlerTest();
        ~RegistrationSVCHandlerTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

        int get_assert_pattern(const string& data, const string& pattern);

        void testRegisterSuccess();
        void testRegisterFailure();
        void testRegisterSuccessHandler();
        void testRegisterFailureHandler();

        void testResetSuccess();
        void testResetFailure();
        void testResetSuccessHandler();
        void testResetFailureHandler();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif