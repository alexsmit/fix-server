/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef _AUTH_MODELS_TEST_H
#define _AUTH_MODELS_TEST_H

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

namespace vx {
  namespace openid {
    namespace test {
      /// test models (requests and response objects)
      class AuthModelsTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(AuthModelsTest);

        CPPUNIT_TEST(testAuthnResponseSerialize);
        CPPUNIT_TEST(testAuthnResponseDeserialize);

        CPPUNIT_TEST(testRequestSerialize);
        CPPUNIT_TEST(testRequestDeserialize);

        CPPUNIT_TEST(testAuthorizeRequestDeserialize);
        CPPUNIT_TEST(testAuthorizeRequestSerialize);

        CPPUNIT_TEST(testTokenRequestDeserialize);
        CPPUNIT_TEST(testTokenRequestSerialize);

        CPPUNIT_TEST(testAuthorizeRequestDeserialize);
        CPPUNIT_TEST(testAuthorizeRequestSerialize);

        CPPUNIT_TEST(testIntrospectDeserializeStream);
        CPPUNIT_TEST(testIntrospectDeserializeTemplate);
        CPPUNIT_TEST(testIntrospectRequestDeserialize);

        CPPUNIT_TEST(testLogoutRequestDeserialize);
        CPPUNIT_TEST(testLogoutRequestSerialize);

        CPPUNIT_TEST_SUITE_END();

      private:
      public:
        AuthModelsTest();
        ~AuthModelsTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< ccpunit teardown

        void testAuthnResponseSerialize();    //!< Serialize response to ptree
        void testAuthnResponseDeserialize();  //!< De-serialize ptree to response

        void testRequestSerialize();    //!< Serialize request to ptree
        void testRequestDeserialize();  //!< De-serialize ptree to request. Note: from web handler a payload already parsed into ptree.

        void testAuthorizeRequestDeserialize();  //!< convert params -> authorize request
        void testAuthorizeRequestSerialize();    //!< convert authorize request -> string (query params)

        void testTokenRequestDeserialize();  //!< convert params -> token request
        void testTokenRequestSerialize();    //!< convert token request -> string (query params)

        void testAuthorizeResponseDeserialize();  //!< convert to AuthorizeResponse
        void testAuthorizeResponseSerialize();    //!< convert from AuthorizeResponse

        void testIntrospectDeserializeStream();    //!< loading Introspect object from index object
        void testIntrospectDeserializeTemplate();  //!< loading Introspect object from index object using just template

        void testIntrospectRequestDeserialize();  //!< loading introspect object from body index object

        void testLogoutRequestDeserialize();  //!< loading logout object from body index object
        void testLogoutRequestSerialize();    //!< logout request to string
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
