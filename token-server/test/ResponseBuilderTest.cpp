/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "ResponseBuilderTest.h"
#include <vx/HttpParser.h>
#include <AuthHandlerTest.h>
#include "ResponseBuilderTest.h"
#include <TokenHandler.h>
#include <VxMessage.h>
#include <ResponseBuilder.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceProfileFactory.h>

using namespace std;
using namespace VxServer;
using namespace vx::web::test;
using namespace vx::openid::test;

/// fixture class to test /oauth/token endpoint
class ResponseBuilderTestFix : public AuthorizeFixBase {
public:
  enum ExceptionType {
    none = 0,
    generic = 1,
    auth = 2,
    argument = 3
  };

  ptree& pt_request;               //!< request object, set by the constructor
  map<string, string> fix_header;  //!< headers to add to the request
  string exception_message;        //!< captured exception
  ExceptionType excpetion_type;    //!< exception message


  /// create fixture object
  /// @param context current server context, should use created by setup
  /// @param req request object as ptree
  /// @param url default or specific url for the test
  ResponseBuilderTestFix(shared_ptr<TokenServerContext> context,
                         ptree& req,
                         map<string, string> fix_header) : AuthorizeFixBase(context), pt_request(req), fix_header(fix_header) {
  }

  void Handle(const string& method = "POST") {
    Run();
  }

  /// run request and parse result
  void Run(std::function<void(auth::ResponseBuilder& builder)> fun = nullptr) {
    stringstream sout;
    stringstream sin;
    string slen;

    // serialize JSON
    stringstream tmp;
    write_json(tmp, pt_request);
    sin.str(tmp.str());

    HandlerEnvironment env(sin, sout, true);
    env.no_req->header.emplace("Content-Type", "application/json");
    slen = boost::lexical_cast<string>(tmp.str().length());
    env.no_req->header.emplace("Content-Length", slen);
    for (auto& kv : fix_header) {
      env.no_req->header.emplace(kv.first, kv.second);
    }

    excpetion_type = ExceptionType::none;

    try {
      auth::ResponseBuilder builder(env.response, env.request, *context);
      if (fun != nullptr) {
        fun(builder);
      }
      else {
        builder.Exec();
        ParseResult(sout);
      }
    }
    catch (const std::invalid_argument& ea) {
      exception_message = ea.what();
      excpetion_type = ExceptionType::argument;
    }
    catch (const auth::auth_exception& eau) {
      exception_message = eau.what();
      excpetion_type = ExceptionType::auth;
    }
    catch (const std::exception& e) {
      exception_message = e.what();
      excpetion_type = ExceptionType::generic;
    }
  }
};


//-----------------------------------------

ResponseBuilderTest::ResponseBuilderTest() {}

ResponseBuilderTest::~ResponseBuilderTest() {}

void ResponseBuilderTest::setUp() {
  AppConfig::resetInstance();

  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());

  auto sttl = boost::lexical_cast<string>(DEBUG_TTL);
  auto sttl_refresh = boost::lexical_cast<string>(DEBUG_TTL * 2);
  context->section->setParam("expiration", sttl);
  context->section->setParam("refresh_expiration", sttl_refresh);
  context->section->setParam("pin_after_expiration", "off");
  context->section->setParam("refresh_token_rotation", "on");

  factory.reset(new DataFactory());
#if defined(VX_USE_MYSQL)
  provider = factory->Connect<ProfileProviderMySQL>();
#elif defined(VX_USE_SOCI)
  provider = factory->Connect<ProfileProviderSOCI>();
#endif

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});

  token_provider = context->GetTokenProvider();
}

void ResponseBuilderTest::tearDown() {
  context.reset();
  token_provider.reset();
  provider.reset();
}

string ResponseBuilderTest::getToken(string nonce, bool addUserid, const string& Audience) {
  auto sec = context->config->Section("token_provider");
  auto issuer = sec["issuer"].str();
  auto algo = sec["algo"].str();
  auto audience = sec["audience"].str();
  if (audience.empty()) audience = "api";

  map<string, string> params;
  TokenInfo info;

  auto rec = provider->LoadUser(USER_NAME);

  JWT tokenapi(*context->info);
  if (addUserid) {
    params.emplace(make_pair("userid", rec.userid));
  }

  if (nonce == "-") {
    info = token_provider->CreateToken(rec.userid.c_str(), "web", "", 0, "", true);
    // refresh_token = info.token;
    nonce = info.nonce;
  }
  if (!nonce.empty())
    params.emplace(make_pair("nonce", nonce));

  tokenapi.setIssuer(issuer);
  tokenapi.setAlgo(algo);
  tokenapi.setRoles({"role1", "role2"});
  tokenapi.setCliam("scope", "openid");
  if (Audience == "-")
    tokenapi.setAudience(audience);
  else if (!Audience.empty())
    tokenapi.setAudience(Audience);

  auto access_token = tokenapi.Sign(params, 2);
  return access_token;
}

void ResponseBuilderTest::testInvalidGrant() {
  ptree pt;
  ResponseBuilderTestFix fix1(context, pt, {});
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Invalid or unsupported grant type"), fix1.exception_message);
}

void ResponseBuilderTest::testInvalidBearerPin() {
  ptree pt;
  pt.put("grant_type", "pin");

  ResponseBuilderTestFix fix1(context, pt, {});
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization header missing"), fix1.exception_message);

  ResponseBuilderTestFix fix2(context, pt, {{"Authorization", "Ber data"}});
  fix2.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix2.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Invalid Authorization header"), fix2.exception_message);

  ResponseBuilderTestFix fix3(context, pt, {{"Authorization", "Bearer"}});
  fix3.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix3.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Invalid Authorization header"), fix3.exception_message);

  ResponseBuilderTestFix fix4(context, pt, {{"Authorization", "Bearer garbage"}});
  fix4.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix4.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Unable to validate token"), fix4.exception_message);
}

void ResponseBuilderTest::testInvalidBearerValidate() {
  ptree pt;
  pt.put("grant_type", "validate");

  ResponseBuilderTestFix fix1(context, pt, {});
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization header missing"), fix1.exception_message);

  ResponseBuilderTestFix fix2(context, pt, {{"Authorization", "Ber data"}});
  fix2.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix2.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Invalid Authorization header"), fix2.exception_message);

  ResponseBuilderTestFix fix3(context, pt, {{"Authorization", "Bearer"}});
  fix3.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix3.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Invalid Authorization header"), fix3.exception_message);

  ResponseBuilderTestFix fix4(context, pt, {{"Authorization", "Bearer garbage"}});
  fix4.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix4.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Unable to validate token"), fix4.exception_message);
}

void ResponseBuilderTest::testWithPinAuthAudMissiing() {
  auto access_token = getToken("", false, "");
  ptree pt;
  pt.put("grant_type", "pin");
  pt.put("pin", USER_PIN);

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization failed. Invalid audience."), fix1.exception_message);
}

void ResponseBuilderTest::testWithPinAuthAudWrong() {
  auto access_token = getToken("", false, "wrong");
  ptree pt;
  pt.put("grant_type", "pin");
  pt.put("pin", USER_PIN);

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization failed. Invalid audience."), fix1.exception_message);
}

void ResponseBuilderTest::testWithPinAuthNonceMissiing() {
  auto access_token = getToken("", false);
  ptree pt;
  pt.put("grant_type", "pin");
  pt.put("pin", USER_PIN);

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Nonce is empty"), fix1.exception_message);
}

void ResponseBuilderTest::testValidateNonceMissiing() {
  auto access_token = getToken("", false);
  ptree pt;
  pt.put("grant_type", "validate");
  pt.put("pin", USER_PIN);

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Nonce is empty"), fix1.exception_message);
}

void ResponseBuilderTest::testValidateNoUserid() {
  auto access_token = getToken("-", false);
  ptree pt;
  pt.put("grant_type", "validate");

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization failed. Invalid token."), fix1.exception_message);
}

void ResponseBuilderTest::testValidateExpired() {
  auto access_token = getToken("-", true);
  ptree pt;
  pt.put("grant_type", "validate");

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Run(
      [&](auth::ResponseBuilder& builder) {
        cout << "W" << flush;
        std::this_thread::sleep_for(std::chrono::milliseconds(3500));
        builder.Bearer();
      });
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization failed. Invalid token."), fix1.exception_message);
}

void ResponseBuilderTest::testValidate() {
  auto access_token = getToken("-", true);
  ptree pt;
  pt.put("grant_type", "validate");

  map<string, string> header{{"Authorization", string("Bearer ") + access_token}};

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("no auth exception", ResponseBuilderTestFix::ExceptionType::none, fix1.excpetion_type);
  CPPUNIT_ASSERT_MESSAGE("empty response", fix1.body.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("200", (unsigned int)200, fix1.code);
}

void ResponseBuilderTest::testAuthenticateScope() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", USER_PASSWORD);
  pt.put("scope", "openid");

  map<string, string> header;

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("no auth exception", ResponseBuilderTestFix::ExceptionType::none, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("200", (unsigned int)200, fix1.code);
  auto tok = fix1.pt_body.get("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("has token", !tok.empty());

  JWT j(*context->info);
  auto& result = j.Verify(tok);
  CPPUNIT_ASSERT_MESSAGE("has scope", result.has_payload_claim("scp"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("openid"), result.get_payload_claim("scp"));
}

void ResponseBuilderTest::testAuthenticateNoRoles() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", USER_PASSWORD);
  pt.put("scope", "openid");

  provider->SetRoles(USER_NAME, {});

  map<string, string> header;

  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("Authorization failed. Access denied."), fix1.exception_message);
}

void ResponseBuilderTest::testCreateClientCredentials() {
  ptree pt;
  pt.put("grant_type", "client_credentials");
  pt.put("client_id", "aaa");
  pt.put("client_secret", "111");
  pt.put("scope", "api");

  map<string, string> header;
  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();

  auto tok = fix1.pt_body.get("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("has token", !tok.empty());

  auto result = fix1.context->validateToken(tok);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("has scheme basic", string("basic"), result.scheme);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("aaa"), result.data.get("client_id",""));
}

void ResponseBuilderTest::testCreateClientCredentialsFail() {
  ptree pt;
  pt.put("grant_type", "client_credentials");
  pt.put("client_id", "invalid");
  pt.put("client_secret", "invalid");
  pt.put("scope", "api");

  map<string, string> header;
  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("invalid client"), fix1.exception_message);
}

void ResponseBuilderTest::testCreateClientCredentialsInvalid() {
  ptree pt;
  pt.put("grant_type", "client_credentials");

  map<string, string> header;
  ResponseBuilderTestFix fix1(context, pt, header);
  fix1.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth exception", ResponseBuilderTestFix::ExceptionType::auth, fix1.excpetion_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("auth message", string("invalid client"), fix1.exception_message);
}

CPPUNIT_TEST_SUITE_REGISTRATION(ResponseBuilderTest);

#endif
