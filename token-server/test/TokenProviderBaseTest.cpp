/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <chrono>
#include <vector>

#include "../include/BaseTest.h"

#include "TokenProviderBaseTest.h"

using namespace vx::openid::test;

TokenProviderBaseTest::TokenProviderBaseTest(string provider_type) : provider_type(provider_type) {
}

TokenProviderBaseTest::~TokenProviderBaseTest() {
}

void TokenProviderBaseTest::setUpInternal() {
  AppConfig::resetInstance();

  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("",true));

  token_provider = context->GetTokenProvider(provider_type);
}

void TokenProviderBaseTest::tearDownInternal() {
  context.reset();
  token_provider.reset();
  DBIOEnvironment::CloseEnvironment();
}

/// @brief verify a key for db/mysql providers
void TokenProviderBaseTest::testKey() {
  string expected =  string("token.") + provider_type;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token provider key", expected, token_provider->key());
}

/**
 * Scenario: Create token, Refresh, Refresh with bad nonce
 * Expected: refresh if token is valid
 **/
void TokenProviderBaseTest::testCreateToken() {
  const char *idu = "_bad_nonce";
  token_provider->SetTTL(TTL);
  // create new token
  auto id_db = token_provider->CreateToken("_user", "_agent", "", 1);
  CPPUNIT_ASSERT_MESSAGE("nonce should not be empty", !id_db.nonce.empty());

  // create with refresh token (no create new)
  auto id_db2 = token_provider->CreateToken("_user", "_agent", id_db.nonce);
  CPPUNIT_ASSERT_MESSAGE("id should be not empty", !id_db2.token.empty());
  CPPUNIT_ASSERT_MESSAGE("refresh token should be the same", id_db.nonce == id_db2.token);

  // try to refresh non existing token
  auto id_db3 = token_provider->CreateToken("_user", "_agent", idu);
  CPPUNIT_ASSERT_MESSAGE("refresh should be empty", id_db3.token.empty());
  //CPPUNIT_ASSERT_MESSAGE("refresh token be different", id_db3.token != idu);
}

void TokenProviderBaseTest::testCreateTokenWithRefreshRecord(){
  token_provider->SetTTL(TTL);
  auto id_db1 = token_provider->CreateToken("_user", "_agent", "", 1, "", true);
  auto id_db2 = token_provider->CreateToken("_user", "_agent", id_db1.token, 1,"", true);

  CPPUNIT_ASSERT_MESSAGE("nonce is different", id_db1.token != id_db2.token);
}

/**
 * Scenario: Create token and refresh  token
 * Expected: refresh token is valid
 **/
void TokenProviderBaseTest::testCreateTokenRefresh() {
  auto token_withref = token_provider->CreateToken("_user", "_agent", "", 1, "meta", true);
  CPPUNIT_ASSERT_MESSAGE("has refresh token", !token_withref.token.empty());
}

/// create token and set explicit TTL
void TokenProviderBaseTest::testCreateTokenWithTTL() {
  token_provider->SetTTL(TTL + 2);
  auto id_db = token_provider->CreateToken("_user", "_agent", "", 1);
  auto cur_time_exp = ::time(NULL) + 1;
  CPPUNIT_ASSERT_MESSAGE("should have expired property", id_db.expried <= cur_time_exp && id_db.expried != 0);

  TokenInfo tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  auto result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);

  // wait
  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(2));

  tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("db should not validate user", !result);
}

/// Hit method records number of invalid attempts to refresh a token (for example wrong pin flow)
void TokenProviderBaseTest::testHit() {
  token_provider->SetTTL(TTL + 3);
  auto id_db = token_provider->CreateToken("_user", "_agent", "");

  TokenInfo tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  auto result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);

  // record bad hit 3 times
  // 1
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // 2
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // 3 final
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should not validate user (token deleted after 3 hits)", !result);
}

/// Hit refresh record for less than 3 times, reset and hit again for less than 3 times
/// After all operations a token should remain valid
void TokenProviderBaseTest::testHitReset() {
  token_provider->SetTTL(TTL + 3);
  auto id_db = token_provider->CreateToken("_user", "_agent", "");

  TokenInfo tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  auto result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);

  // record bad hit 2 times
  // 1
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // 2
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // reset
  token_provider->Hit(id_db.nonce, -1);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // record bad hit 2 times
  // 1
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
  // 2
  token_provider->Hit(id_db.nonce, 3);
  result = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result);
}

/// create token, try to validate with valid and not valid nonce
void TokenProviderBaseTest::testValidateNonce() {
  token_provider->SetTTL(TTL);
  auto id_db = token_provider->CreateToken("_user", "_agent", "");
  TokenInfo tinfo = {.token = "", .nonce = id_db.nonce};
  auto result_db = token_provider->ValidateToken("_user", tinfo, true);
  CPPUNIT_ASSERT_MESSAGE("should validate user with nonce", result_db);

  tinfo = {.token = "", .nonce = "_bad_nonce"};
  result_db = token_provider->ValidateToken("_user", tinfo, true);
  CPPUNIT_ASSERT_MESSAGE("should not validate user with nonce", !result_db);

  tinfo = {.token = id_db.token, .nonce = "_invalid_nonce"};
  result_db = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should not validate user", !result_db);
}

/// create token, wait refresh expriration, try to validate
void TokenProviderBaseTest::testValidateToken() {
  token_provider->SetTTL(1000);
  token_provider->SetRefreshTTL(TTL);
  auto id_db = token_provider->CreateToken("_user", "_agent", "");
  TokenInfo tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  auto result_db = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("should validate user", result_db);

  // wait
  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(TTL + 1));

  tinfo = {.token = id_db.token, .nonce = id_db.nonce};
  result_db = token_provider->ValidateToken("_user", tinfo);
  CPPUNIT_ASSERT_MESSAGE("db should not validate user", !result_db);
}

/// create token, wait, clean, try to valideate
void TokenProviderBaseTest::testClean() {
  token_provider->SetTTL(1000);
  token_provider->SetRefreshTTL(TTL);
  auto id_db = token_provider->CreateToken("_user", "_agent", "");

  // wait and clean
  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(TTL + 2));
  token_provider->Clean();

  // using refresh token created before
  auto id_db2 = token_provider->CreateToken("_user", "_agent", id_db.nonce);
  CPPUNIT_ASSERT_MESSAGE("refresh should be empty", id_db2.token.empty());
}

/// create garbage record in the database, make sure it will get cleaned (DB provider only)
void TokenProviderBaseTest::testCleanGarbage() {
  shared_ptr<TokenProviderDB> db_token_provider = std::dynamic_pointer_cast<TokenProviderDB>(token_provider);
  if (!db_token_provider) return;

  auto dbname = context->getParam(provider_name + ".database");

  auto db = DBIOEnvironment::GetDatabase(dbname.c_str());
  db->Put("key","garbage");
  db->Put("key:metadata","garbage");

  token_provider->SetTTL(1000);
  token_provider->SetRefreshTTL(TTL);
  // auto id_db = token_provider->CreateToken("_user", "_agent", "");

  // // wait and clean
  cout << "W" << flush;
  CPPUNIT_ASSERT_MESSAGE("garbage still in place", db->ContainsKey("key"));
  CPPUNIT_ASSERT_MESSAGE("garbage still in place", db->ContainsKey("key:metadata"));

  std::this_thread::sleep_for(std::chrono::seconds(TTL + 2));
  token_provider->Clean();

  CPPUNIT_ASSERT_MESSAGE("garbage still in place", !db->ContainsKey("key"));
  CPPUNIT_ASSERT_MESSAGE("garbage still in place", !db->ContainsKey("key:metadata"));
}

/// Check default configured for DEBUG TTL. Update TTL, should set to provider.
void TokenProviderBaseTest::testSetTTL() {
  auto effective_ttl = token_provider->GetEffectiveTTL();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should have default TTL (debug)", 30UL,  effective_ttl);
  token_provider->SetTTL(TTL);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should have TTL", (unsigned long)TTL, token_provider->GetEffectiveTTL());
}


/// Check default configured for DEBUG refresh TTL. Update refresh TTL, should set to provider.
void TokenProviderBaseTest::testSetRefreshTTL() {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should have default refresh TTL",
                               (unsigned long)DEFAULT_TTL_REFRESH,
                               token_provider->GetEffectiveRefreshTTL());
  token_provider->SetRefreshTTL(TTL);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should have refresh TTL", (unsigned long)TTL, token_provider->GetEffectiveRefreshTTL());
}

/// create nonce, should have expected length
void TokenProviderBaseTest::testNonce() {
  auto nonce = token_provider->CreateNonce();
  CPPUNIT_ASSERT_MESSAGE("nonce should have 12/3*4 chars", nonce.length() == 16);
}

/// save token metadata, should be cleaned up when expired
void TokenProviderBaseTest::testMetadata() {
  ptree pt;
  pt.put("key", "value");
  stringstream ss;
  write_json(ss, pt);
  string ssmeta = ss.str();

  auto info = token_provider->CreateToken("_user", "_agent", "", 1, ssmeta);

  auto ssmetaread = token_provider->GetMetadata(info.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("metadata", ssmeta, ssmetaread);

  pt.put("key2", "value2");
  stringstream ss2;
  write_json(ss2, pt);
  string ssmeta2 = ss2.str();

  auto metaresult = token_provider->SetMetadata(info.nonce, ssmeta2);
  CPPUNIT_ASSERT_MESSAGE("update successful", metaresult);

  ssmetaread = token_provider->GetMetadata(info.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("metadata after set", ssmeta2, ssmetaread);

  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));
  token_provider->Clean();

  ssmetaread = token_provider->GetMetadata(info.nonce);
  CPPUNIT_ASSERT_MESSAGE("metadata should be deleted on cleanup", ssmetaread.empty());

  CPPUNIT_ASSERT_MESSAGE("should not set metadata for wrong nonce", !token_provider->SetMetadata("some_nonce", "some_metadata"));
}

/// load token by nonce
void TokenProviderBaseTest::testGetTokenByNonce() {
  auto info = token_provider->CreateToken("_user", "_agent", "", 1, "meta");
  auto token = token_provider->GetTokenByNonce(info.nonce);
  auto expected_expired = ::time(NULL) + 1;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string("_user"), token.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("agent", string("_agent"), token.agent);
  CPPUNIT_ASSERT_MESSAGE("expired", token.expried >= expected_expired);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("metadata", string("meta"), token.metadata);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nonce", info.nonce, token.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("refresh_token", info.token, token.token);
}

/// delete token by nonce
void TokenProviderBaseTest::testDeleteTokenByNonce() {
  auto info = token_provider->CreateToken("_user", "_agent", "", 1, "meta");
  token_provider->DeleteTokenByNonce(info.nonce);
  auto token = token_provider->GetTokenByNonce(info.nonce);
  CPPUNIT_ASSERT_MESSAGE("should not load a token", token.nonce.empty());
}

/// no exception when deleting with empty nonce
void TokenProviderBaseTest::testDeleteTokenByNonceEmpty() {
  CPPUNIT_ASSERT_NO_THROW_MESSAGE(
      "Should not fail if trying to delete and use empty key",
      {
        token_provider->DeleteTokenByNonce("");
      });
}

/// create token and refresh token records, load token by refresh token nonce
void TokenProviderBaseTest::testGetTokenByRefreshToken() {
  auto info = token_provider->CreateToken("_user", "_agent", "", 1, "meta", true);
  auto token = token_provider->GetTokenByRefresh(info.token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should load a token: nonce", info.nonce, token.nonce);
  CPPUNIT_ASSERT_MESSAGE("should load a token: metadata", !token.metadata.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should load a token: refresh_token", info.token, token.token);
}

/// create access token and refresh token, delete access. refresh should still exists
void TokenProviderBaseTest::testAccessTokenDeleted() {
  auto info = token_provider->CreateToken("_user", "_agent", "", 1, "meta", true);
  token_provider->DeleteTokenByNonce(info.nonce);

  // refresh token should still exists
  auto token_refresh = token_provider->GetTokenByNonce(info.token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should load a refresh_token: nonce", info.token, token_refresh.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should load a refresh_token: meta", string("meta"), token_refresh.metadata);
  CPPUNIT_ASSERT_MESSAGE("should have empty reference to a token", token_refresh.token.empty());

  // access token should be deleted
  auto token = token_provider->GetTokenByNonce(info.nonce);
  CPPUNIT_ASSERT_MESSAGE("should not load an access_token: nonce", token.nonce.empty());

  // access token cannot be loaded by refresh token
  token = token_provider->GetTokenByRefresh(info.token);
  CPPUNIT_ASSERT_MESSAGE("should not load a access_token: nonce", token.nonce.empty());
  CPPUNIT_ASSERT_MESSAGE("should not load a access_token: refresh_token", token.token.empty());
}

/// create access and refresh tokens. delete refresh token. access should be loaded, but cannot load refresh token
void TokenProviderBaseTest::testRefreshTokenDeleted() {
  auto info = token_provider->CreateToken("_user", "_agent", "", 1, "meta", true);
  token_provider->DeleteTokenByNonce(info.token);

  // access token cannot be loaded by refresh token
  auto token = token_provider->GetTokenByRefresh(info.token);
  CPPUNIT_ASSERT_MESSAGE("should not load a access_token: nonce", token.nonce.empty());
  CPPUNIT_ASSERT_MESSAGE("should not load a access_token: refresh_token", token.token.empty());

  // access token should exist, but refresh_token should be empty
  token = token_provider->GetTokenByNonce(info.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should load a access_token: nonce", info.nonce, token.nonce);
  CPPUNIT_ASSERT_MESSAGE("should have empty refresh token field", token.token.empty());
}

/// parse token metadata
void TokenProviderBaseTest::testUserTokenMetadata() {
  UserToken tok;
  tok.metadata = "{\"foo\":\"bar\"}";
  ptree pt = tok.parse_metadata();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("metatdata foo", string("bar"), pt.get("foo", ""));

  tok.metadata = "";
  pt = tok.parse_metadata();
  CPPUNIT_ASSERT_MESSAGE("no metatdata foo", pt.get("foo", "").empty());

  tok.metadata = "{{{{{\"foo\"";
  START_CAPTURE
  pt = tok.parse_metadata();
  STOP_CAPTURE

  CPPUNIT_ASSERT_MESSAGE("no metatdata foo", pt.get("foo", "").empty());
  CPPUNIT_ASSERT_MESSAGE("had error ", cerr_output.find("UserToken metadata error") != string::npos );
}

/// set metadata as a map
void TokenProviderBaseTest::testTokenMetadataMap() {
  auto token = token_provider->CreateTokenMeta("_user", "_agent", "", 1, {{"foo", "bar"}});
  auto token2 = token_provider->GetTokenByNonce(token.nonce);

  auto meta = token2.parse_metadata();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("foo", string("bar"), meta.get("foo", ""));
}

/// create access and refresh token. create access token using same refresh token. Refresh token should be the same.
void TokenProviderBaseTest::testRefreshReused() {
  auto token = token_provider->CreateToken("_user", "_agent", "", 1, "meta", true);
  CPPUNIT_ASSERT_MESSAGE("with refresh token", !token.token.empty());
  auto refresh_token_data = token_provider->GetTokenByNonce(token.token);

  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

  auto token2 = token_provider->CreateToken("_user", "_agent", token.token, 1, "meta");
  CPPUNIT_ASSERT_MESSAGE("with refresh token", !token2.token.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same refresh token", token.token, token2.token);

  auto refresh_token_data2 = token_provider->GetTokenByNonce(token2.token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("agent", refresh_token_data.agent, refresh_token_data2.agent);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("metadata", refresh_token_data.metadata, refresh_token_data2.metadata);
  CPPUNIT_ASSERT_MESSAGE("expired", refresh_token_data.expried < refresh_token_data2.expried);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", refresh_token_data.username, refresh_token_data2.username);
}

void TokenProviderBaseTest::testName() {
  auto provider_base = std::dynamic_pointer_cast<TokenProviderBase>(token_provider);
  auto name = provider_base->Name();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Name", provider_name, name);
}

//CPPUNIT_TEST_SUITE_REGISTRATION(TokenProviderBaseTest<TokenProviderDB>);
