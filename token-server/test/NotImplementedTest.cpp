/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "NotImplementedTest.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>

using namespace std;
using namespace VxServer;
using namespace vx::openid::test;

//-----------------------------------------

NotImplementedTest::NotImplementedTest() {}

NotImplementedTest::~NotImplementedTest() {}

void NotImplementedTest::setUp() {}

void NotImplementedTest::tearDown() {}

/// Validate all required parameters are provided in the config.json
void NotImplementedTest::testNotImpl() {
  CPPUNIT_FAIL("not implemented");
}


CPPUNIT_TEST_SUITE_REGISTRATION(NotImplementedTest);

#endif
