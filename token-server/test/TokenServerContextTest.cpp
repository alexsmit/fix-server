/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <stdlib.h>
#include <sys/stat.h>
#include <atomic>
#include <chrono>
#include <vector>

#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <VxMessage.h>
#include <vx/FileUtil.h>
#include <vx/URL.h>
#include <vx/dbm/DBIO.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <ProfileProviderRemote.h>

#include <TokenServerContext.h>

#include "BaseTest.h"
#include "BaseServiceTest.h"
#include "TokenServerContextTest.h"

#include <vx/ServiceProfileFactory.h>
#include <vx/ServiceEmailFactory.h>
#include "vx/SimpleMail.h"

using namespace std;
using namespace vx::openid::test;
using namespace vx::providers;

TokenServerContextTest::TokenServerContextTest() {
}

TokenServerContextTest::~TokenServerContextTest() {}

void TokenServerContextTest::setUp() {
  AppConfig::resetInstance();

  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "mysql");
  context->setParam("token_provider.type", "db");

  // auto p = (unsigned long) AppConfig::getInstancePtr().get();
  // cout << "init: config ptr: " << p << '\n';

  provider = context->GetProfileProvider();
  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});
}

void TokenServerContextTest::tearDown() {
  if (provider) provider->DeleteUser(USER_NAME);
  // provider->DeleteRole(ROLE_1);
  // provider->DeleteRole(ROLE_2);
  provider.reset();
  // token_provider.reset();
  context.reset();
}


/**
 * Scenario: get token service from the registry
 * Expected: ok
 **/
void TokenServerContextTest::testTokenService() {
  auto svc_token = context->getService("token");
  auto token_service = dynamic_cast<TokenProvider *>(svc_token.get());
  CPPUNIT_ASSERT_MESSAGE("token service should exist", token_service != NULL);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token provider jwt ttl", DEBUG_TTL, token_service->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token provider refresh ttl", DEBUG_TTL * 2, token_service->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the registry, expected to get from the cache (ttl=60)
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceSame() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 60));

  auto svc_token = context->getService("token");
  auto token_service1 = dynamic_cast<TokenProvider *>(svc_token.get());
  svc_token = context->getService("token");
  auto token_service2 = dynamic_cast<TokenProvider *>(svc_token.get());

  CPPUNIT_ASSERT_MESSAGE("token provider is the same", token_service1 == token_service2);
}

/**
 * Scenario: get token service from the registry, expected not to get from the cache (ttl=0)
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceDiff() {
  auto svc_token = context->getService("token");
  auto token_service1 = dynamic_cast<TokenProvider *>(svc_token.get());
  svc_token = context->getService("token");
  auto token_service2 = dynamic_cast<TokenProvider *>(svc_token.get());

  CPPUNIT_ASSERT_MESSAGE("token provider is not the same", token_service1 != token_service2);
}

/**
 * Scenario: get token service from the context. type "db"
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceDB() {
  context->setParam("token_provider.type", "db");
  auto prov = context->GetTokenProvider();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("db provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("db provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the context. type "mysql"
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceMysql() {
  context->setParam("token_provider.type", "mysql");
  auto prov = context->GetTokenProvider();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the context. type "mysql", but invalid backend
 * Expected: FAIL
 **/
void TokenServerContextTest::testTokenServiceMysqlBadBackend() {
  context->setParam("token_provider.type", "mysql");
  context->setParam("token.mysql.backend", "dummy");
  
  CPPUNIT_ASSERT_THROW_MESSAGE("cannot get a provider", {
    auto prov = context->GetTokenProvider();
  },invalid_argument);
}

/**
 * Scenario: get token service from the context. type "mysql", but invalid backend
 * Expected: FAIL
 **/
void TokenServerContextTest::testTokenServiceMysqlBadType() {
  context->setParam("token_provider.type", "mysql");
  context->setParam("token.mysql.type", "dummy");
  
  CPPUNIT_ASSERT_THROW_MESSAGE("cannot get a provider", {
    auto prov = context->GetTokenProvider();
  },invalid_argument);
}

/**
 * Scenario: get token service from the context. type "soci"
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceSoci() {
  context->setParam("token_provider.type", "soci");
  auto prov = context->GetTokenProvider();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("key", string("token.soci"), prov->key());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the context. type "db" using param
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceDBParam() {
  context->setParam("token_provider.type", "-unknown-");
  auto prov = context->GetTokenProvider("db");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("db provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("db provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the context. type "mysql" using param
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceMysqlParam() {
  context->setParam("token_provider.type", "-unknown-");
  auto prov = context->GetTokenProvider("mysql");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("key", string("token.mysql"), prov->key());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get token service from the context. type "soci" using param
 * Expected: ok
 **/
void TokenServerContextTest::testTokenServiceSociParam() {
  context->setParam("token_provider.type", "-unknown-");
  auto prov = context->GetTokenProvider("soci");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("key", string("token.soci"), prov->key());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider jwt ttl", DEBUG_TTL, prov->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql provider refresh ttl", DEBUG_TTL * 2, prov->GetEffectiveRefreshTTL());
}

/**
 * Scenario: get unknown token service from the context.
 * Expected: fail
 **/
void TokenServerContextTest::testTokenServiceUnknown() {
  context->setParam("token_provider.type", "-unknown-");
  CPPUNIT_ASSERT_THROW_MESSAGE("should throw for unknown token provider type",
                               auto prov = context->GetTokenProvider();
                               , std::runtime_error);
}

/**
 * @brief DB should be oepend when after using token provider
 *
 */
void TokenServerContextTest::testTokenServiceDBOpen() {
  context->setParam("token_provider.type", "db");
  AppConfig &appConfig = AppConfig::getInstance();
  auto dbname = appConfig["token.db.database"].str();
  auto dbpath = DBIOEnvironment::GetPath(dbname.c_str());
  CPPUNIT_ASSERT_MESSAGE("token db is closed", !FileUtil::IsOpen(dbname));

  auto prov = context->GetTokenProvider();
  auto info = prov->CreateToken(USER_NAME, "test", "", 3);
  CPPUNIT_ASSERT_MESSAGE("token db is open", FileUtil::IsOpen(dbname));
}

/**
 * @brief DB should be closed when the last instance to token provider is out of scope
 *
 */
void TokenServerContextTest::testTokenServiceDBClose() {
  // disable provider cache
  context->provider_ttl = 0;

  context->setParam("token_provider.type", "db");
  AppConfig &appConfig = AppConfig::getInstance();
  auto dbname = appConfig["token.db.database"].str();
  auto dbpath = DBIOEnvironment::GetPath(dbname.c_str());

  {
    auto prov = context->GetTokenProvider();
    auto info = prov->CreateToken(USER_NAME, "test", "", 3);
    CPPUNIT_ASSERT_MESSAGE("token db is open", FileUtil::IsOpen(dbname));
  }
  CPPUNIT_ASSERT_MESSAGE("token db is closed", !FileUtil::IsOpen(dbname));
}

/**
 * Scenario: get profile service from the context.
 * Expected: ok
 **/
void TokenServerContextTest::testProfileService() {
  auto svc = context->getService("profile_service");
  auto profile_service = dynamic_cast<ProfileSVC *>(svc.get());
  CPPUNIT_ASSERT_MESSAGE("profile service should exist", profile_service != NULL);
}

/**
 * Scenario: get profile service from the context. type "mysql"
 * Expected: ok
 **/
void TokenServerContextTest::testProfileServiceMysql() {
  context->setParam("profile_provider.type", "mysql");
  auto svc = context->GetProfileSVC();
  auto provptr = svc->GetProvider().get();
#if defined(VX_USE_MYSQL)
  auto prov = dynamic_cast<ProfileProviderMySQL *>(provptr);
#elif defined(VX_USE_SOCI)
  auto prov = dynamic_cast<ProfileProviderSOCI *>(provptr);
#endif
  CPPUNIT_ASSERT_MESSAGE("profile provider is mysql type", prov != NULL);
}

/**
 * Scenario: get profile service from the context. type "mysql"
 * Expected: ok
 **/
void TokenServerContextTest::testProfileServiceSOCI() {
  // context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "soci");
  auto svc = context->GetProfileSVC();
  auto provptr = svc->GetProvider().get();
  auto prov = dynamic_cast<ProfileProviderSOCI *>(provptr);
  CPPUNIT_ASSERT_MESSAGE("profile provider is soci type", prov != NULL);
}

/**
 * Scenario: get profile service from the context. type "remote"
 * Expected: ok
 **/
void TokenServerContextTest::testProfileServiceRemote() {
  // context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "remote");

  // auto p = (unsigned long) AppConfig::getInstancePtr().get();
  // cout << "test: config ptr: " << p << '\n';
  // auto &pt = context->config->Section("").getTree("");
  // cout << "test type = " << pt.get("profile_provider.type","") << '\n';
  // write_json(cout, pt);

  auto svc = context->GetProfileSVC();
  auto provptr = svc->GetProvider().get();
  auto prov = dynamic_cast<ProfileProviderRemote *>(provptr);
  CPPUNIT_ASSERT_MESSAGE("profile provider is remote type", prov != NULL);
}

/**
 * Scenario: get profile service type "mysql", context could be anything
 * Expected: ok
 **/
void TokenServerContextTest::testProfileServiceMysqlParam() {
  // context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "-anything-");
  auto svc = context->GetProfileSVC("mysql");
  auto provptr = svc->GetProvider().get();
#if defined(VX_USE_MYSQL)
  auto prov = dynamic_cast<ProfileProviderMySQL *>(provptr);
#elif defined(VX_USE_SOCI)
  auto prov = dynamic_cast<ProfileProviderSOCI *>(provptr);
#endif
  CPPUNIT_ASSERT_MESSAGE("profile provider is mysql type", prov != NULL);
}

/**
 * Scenario: get profile service type "remote", context could be anything
 * Expected: ok
 **/
void TokenServerContextTest::testProfileServiceRemoteParam() {
  // context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "-anything-");
  auto svc = context->GetProfileSVC("remote");
  auto provptr = svc->GetProvider().get();
  auto prov = dynamic_cast<ProfileProviderRemote *>(provptr);
  CPPUNIT_ASSERT_MESSAGE("profile provider is remote type", prov != NULL);
}

/**
 * Scenario: get profile service from the context. tyoe "unknown"
 * Expected: faile
 **/
void TokenServerContextTest::testProfileServiceUnknown() {
  // context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", false, 0));

  context->setParam("profile_provider.type", "-unknown-");
  CPPUNIT_ASSERT_THROW_MESSAGE(
      "get mysql profile provider",
      auto prov = context->GetProfileSVC(),
      std::invalid_argument);
}

void TokenServerContextTest::testProfileServiceCmdMysql() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("mysql"));
  auto p = context->GetProfileProvider();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql", string("profile.mysql"), p->key());
}

void TokenServerContextTest::testProfileServiceCmdSoci() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("soci"));

  auto p = context->GetProfileProvider();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql", string("profile.soci"), p->key());
}

void TokenServerContextTest::testProfileServiceCmdRemote() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("remote"));

  auto p = context->GetProfileProvider();
  auto key = p->key();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("mysql", string("profile.remote"), p->key());
}

void TokenServerContextTest::testProfileServiceCmdThrow() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("unknown"));

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "get unknown",
      auto prov = context->GetProfileProvider(),
      std::invalid_argument);
}

void TokenServerContextTest::testGetWrongServiceThrow() {
  CPPUNIT_ASSERT_THROW_MESSAGE(
      "get unknown service",
      auto svc = context->getService("some-unknown-service"),
      std::invalid_argument);
}


void TokenServerContextTest::testGetProfileNE() {
  context.reset(new TokenServerContext(8082, 0, DEBUG_TTL, LogLevel::none, 5, DEBUG_TTL * 2, ""));
  context->Init("token_provider", "config.json");
  CPPUNIT_ASSERT_THROW_MESSAGE(
      "profile svc not registerd", {
        auto svc = context->GetProfileProvider();
      },
      std::invalid_argument);
}

void TokenServerContextTest::testGetProfileWrong() {
  context.reset(new TokenServerContext(8082, 0, DEBUG_TTL, LogLevel::none, 5, DEBUG_TTL * 2, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceDummyFactoryProfile>(10, "profile_provider"));
  CPPUNIT_ASSERT_THROW_MESSAGE(
      "profile svc not registerd", {
        auto svc = context->GetProfileProvider();
      },
      std::invalid_argument);
}

void TokenServerContextTest::testGetProfileInvalidType() {
  AppConfig &conf = AppConfig::getInstance();
  conf["profile_provider.config.some_wrong_type"] = "profile.some_wrong_type";
  conf["profile.some_wrong_type.type"] = "wrong_db";

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "profile svc not registerd", {
        auto svc = context->GetProfileProvider("some_wrong_type");
      },
      std::invalid_argument);
}

void TokenServerContextTest::testGetTokenProviderBadExpiration() {
  AppConfig &conf = AppConfig::getInstance();
  conf["token.db.expiration"] = "wrong_value";
  conf["token_provider.expiration"] = "0";
  conf["token_provider.refresh_expiration"] = "0";

  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceTokenFactory>(10, "token_provider"));

  shared_ptr<TokenProvider> svc;
  CPPUNIT_ASSERT_NO_THROW_MESSAGE("success with wrong params", {
    svc = context->GetTokenProvider();
  });

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ttl", (unsigned long)DEFAULT_TTL, svc->GetEffectiveTTL());
}

void TokenServerContextTest::testGetTokenProviderBadRefresh() {
  AppConfig &conf = AppConfig::getInstance();
  conf["token.db.refresh_expiration"] = "wrong_value";
  conf["token_provider.expiration"] = "0";
  conf["token_provider.refresh_expiration"] = "0";

  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceTokenFactory>(10, "token_provider"));

  shared_ptr<TokenProvider> svc;
  CPPUNIT_ASSERT_NO_THROW_MESSAGE("success with wrong params", {
    svc = context->GetTokenProvider();
  });

  CPPUNIT_ASSERT_EQUAL_MESSAGE("refresh ttl", (unsigned long)DEFAULT_TTL_REFRESH, svc->GetEffectiveRefreshTTL());
}

#include <TokenServerContextTestFix.h>

void TokenServerContextTest::testEmailFactoryService() {
  AppConfig &conf = AppConfig::getInstance();
  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceEmailFactory>(0, "email_provider"));

  shared_ptr<EmailService> email_svc;
  email_svc = std::dynamic_pointer_cast<EmailService>(context->getService("email"));

  CPPUNIT_ASSERT_EQUAL_MESSAGE("email id", string("email"), email_svc->id());
}

void TokenServerContextTest::testEmailFactory() {
  AppConfig &conf = AppConfig::getInstance();
  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceEmailFactory>(0, "email_provider"));

  // modify config to use non-email utility to avoid real email to be sent
  SimpleMail::mail_config[SimpleMailType::multi_part] = {"/bin/cat", "cat -"};
  SimpleMail::mail_config[SimpleMailType::html] = {"/bin/cat", "cat -"};

  shared_ptr<EmailService> email_svc;
  CPPUNIT_ASSERT_NO_THROW_MESSAGE("success", {
    auto svc = context->getService("email");
    email_svc = std::dynamic_pointer_cast<EmailService>(svc);
  });

  CPPUNIT_ASSERT_MESSAGE("service is registered", !!email_svc);

  TokenServerContextTestFix fix(*context);
  bool result = false;

  fix.Run([&]() {
    result = email_svc->SendCodeURL("email_confirm", "support@vxpro.io", "support@vxpro.io", "title", "https://domein.tld/register/confirm");
  });
  CPPUNIT_ASSERT_MESSAGE("no errors", fix.std_err_output.empty());

  CPPUNIT_ASSERT_MESSAGE("ok", result);
}

void TokenServerContextTest::testEmailFactoryFailUtility() {
  AppConfig &conf = AppConfig::getInstance();
  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceEmailFactory>(0, "email_provider"));

  // modify config to use non-email utility to avoid real email to be sent
  // however that path should not exist and email send operation will fail
  // this may happen in misconfigured server when even mail or mailx does not exist
  SimpleMail::mail_config[SimpleMailType::multi_part] = {"/bin/not_an_email", "/bin/not_an_email -"};
  SimpleMail::mail_config[SimpleMailType::html] = {"/bin/not_an_email", "/bin/not_an_email -"};

  shared_ptr<EmailService> email_svc;
  email_svc = std::dynamic_pointer_cast<EmailService>(context->getService("email"));

  TokenServerContextTestFix fix(*context);
  bool result = true;
  fix.Run([&]() {
    result = email_svc->SendCodeURL("email_confirm", "support@vxpro.io", "support@vxpro.io", "title", "https://domein.tld/register/confirm");
  });

  CPPUNIT_ASSERT_MESSAGE("has errors", !fix.std_err_output.empty());

  CPPUNIT_ASSERT_MESSAGE("fail", !result);
}

void TokenServerContextTest::testEmailFactoryFailTemplate() {
  AppConfig &conf = AppConfig::getInstance();
  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceEmailFactory>(0, "email_provider"));

  // modify config to use non-email utility to avoid real email to be sent
  // however that path should not exist and email send operation will fail
  // this may happen in misconfigured server when even mail or mailx does not exist
  SimpleMail::mail_config[SimpleMailType::multi_part] = {"/bin/cat", "/bin/cat -"};
  SimpleMail::mail_config[SimpleMailType::html] = {"/bin/cat", "/bin/cat -"};

  shared_ptr<EmailService> email_svc;
  email_svc = std::dynamic_pointer_cast<EmailService>(context->getService("email"));

  TokenServerContextTestFix fix(*context);
  fix.Run([&]() {
    email_svc->SendCodeURL("non_existing_template", "support@vxpro.io", "support@vxpro.io", "title", "https://domein.tld/register/confirm");
  });

  CPPUNIT_ASSERT_MESSAGE("has exception", fix.has_exception);
}

void TokenServerContextTest::testEmailFactoryRenderErrors() {
  AppConfig &conf = AppConfig::getInstance();
  context.reset(new TokenServerContext(8082, 0, 0, LogLevel::none, 5, 0, ""));
  context->Init("token_provider", "config.json");
  context->AddProvider(make_shared<vx::ServiceEmailFactory>(0, "email_provider"));

  // modify config to use non-email utility to avoid real email to be sent
  // however that path should not exist and email send operation will fail
  // this may happen in misconfigured server when even mail or mailx does not exist
  SimpleMail::mail_config[SimpleMailType::multi_part] = {"/bin/cat", "cat -"};
  SimpleMail::mail_config[SimpleMailType::html] = {"/bin/cat", "cat -"};

  shared_ptr<EmailService> email_svc;
  email_svc = std::dynamic_pointer_cast<EmailService>(context->getService("email"));

  TokenServerContextTestFix fix(*context);
  bool result = false;
  fix.Run([&]() {
    result = email_svc->SendCodeURL("test_email", "support@vxpro.io", "support@vxpro.io", "title", "https://domein.tld/register/confirm");
  });

  CPPUNIT_ASSERT_MESSAGE("has exception", fix.has_exception);
}

CPPUNIT_TEST_SUITE_REGISTRATION(TokenServerContextTest);
