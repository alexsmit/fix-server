#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing jwks endpoints (default server, well-known)
      class AuthHandlerTestKeys : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestKeys);

        CPPUNIT_TEST(KeysWithInvalidClientId);
        CPPUNIT_TEST(KeysJwksBase);
        CPPUNIT_TEST(KeysAuthServer);
        CPPUNIT_TEST(KeysJwksAuthServer);
        CPPUNIT_TEST(KeysJwksBaseOpt);
        CPPUNIT_TEST(KeysAuthServerOpt);
        CPPUNIT_TEST(KeysJwksAuthServerOpt);
        CPPUNIT_TEST(KeysAuthServerOptRotation);
        CPPUNIT_TEST(KeysJwksBaseOptRotation);
        CPPUNIT_TEST(KeysJwksAuthServerOptRotation);

        CPPUNIT_TEST_SUITE_END();


      public:
        AuthHandlerTestKeys();
        ~AuthHandlerTestKeys();

        void KeysWithInvalidClientId();                       //!< passing invalid client_id to /keys
        void Keys(int mode, bool useGet, int age = 2592000);  //!< base method to run test
        void KeysAuthServer();                                //!< looking at {auth server}/keys
        void KeysJwksBase();                                  //!< looking at /.well-known/jwks.json
        void KeysJwksAuthServer();                            //!< looking at {auth server}/.well-known/jwks.json
        void KeysAuthServerOpt();                             //!< OPTIONS looking at {auth server}/keys
        void KeysJwksBaseOpt();                               //!< OPTIONS looking at /.well-known/jwks.json
        void KeysJwksAuthServerOpt();                         //!< OPTIONS looking at {auth server}/.well-known/jwks.json
        void KeysAuthServerOptRotation();                     //!< OPTIONS looking at {auth server}/keys, set different rotation time
        void KeysJwksBaseOptRotation();                       //!< OPTIONS looking at /.well-known/jwks.json, set different rotation time
        void KeysJwksAuthServerOptRotation();                 //!< OPTIONS looking at {auth server}/.well-known/jwks.json, set different rotation time
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
