/*
 * Copyright 2022 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/format.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/openid/models/Models.h>
#include <vx/StringUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestLogin.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestLogin::AuthHandlerTestLogin() {}

AuthHandlerTestLogin::~AuthHandlerTestLogin() {}

//-----------------------------------------

/// fixture class to test login
class LoginRequestFix : public AuthorizeFixBase {
public:
  LogoutRequest& req;  //!< request object, set by the constructor
  string url;          //!< url, set by the constructor
  string cookie;       //!< session cookie value, set by the constructor

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param cookie session cookie value to be set
  /// @param url default or specific url
  LoginRequestFix(shared_ptr<TokenServerContext> context,
                   LogoutRequest& req,
                   string cookie,
                   string url = "/oauth2/default/v1/logout") : AuthorizeFixBase(context), req(req), url(url), cookie(cookie) {

  }

};

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestLogin);

#endif
