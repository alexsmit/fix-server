#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// all token server endpoints tests
      class TokenServerContextTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(TokenServerContextTest);

        CPPUNIT_TEST(testTokenService);
        CPPUNIT_TEST(testTokenServiceSame);
        CPPUNIT_TEST(testTokenServiceDiff);
        CPPUNIT_TEST(testTokenServiceDB);
        CPPUNIT_TEST(testTokenServiceMysql);
        CPPUNIT_TEST(testTokenServiceMysqlBadBackend);
        CPPUNIT_TEST(testTokenServiceMysqlBadType);
        CPPUNIT_TEST(testTokenServiceSoci);
        CPPUNIT_TEST(testTokenServiceDBParam);
        CPPUNIT_TEST(testTokenServiceMysqlParam);
        CPPUNIT_TEST(testTokenServiceSociParam);
        CPPUNIT_TEST(testTokenServiceUnknown);
        CPPUNIT_TEST(testTokenServiceDBOpen);
        CPPUNIT_TEST(testTokenServiceDBClose);
        CPPUNIT_TEST(testProfileService);
        CPPUNIT_TEST(testProfileServiceMysql);
        CPPUNIT_TEST(testProfileServiceSOCI);
        CPPUNIT_TEST(testProfileServiceRemote);
        CPPUNIT_TEST(testProfileServiceMysqlParam);
        CPPUNIT_TEST(testProfileServiceRemoteParam);
        CPPUNIT_TEST(testProfileServiceUnknown);

        CPPUNIT_TEST(testProfileServiceCmdMysql);
        CPPUNIT_TEST(testProfileServiceCmdRemote);
        CPPUNIT_TEST(testProfileServiceCmdSoci);
        CPPUNIT_TEST(testProfileServiceCmdThrow);

        CPPUNIT_TEST(testGetWrongServiceThrow);
        CPPUNIT_TEST(testGetProfileNE);
        CPPUNIT_TEST(testGetProfileWrong);
        CPPUNIT_TEST(testGetProfileInvalidType);

        CPPUNIT_TEST(testGetTokenProviderBadExpiration);
        CPPUNIT_TEST(testGetTokenProviderBadRefresh);

        CPPUNIT_TEST(testEmailFactoryService);
        CPPUNIT_TEST(testEmailFactory);
        CPPUNIT_TEST(testEmailFactoryFailUtility);
        CPPUNIT_TEST(testEmailFactoryFailTemplate);
        CPPUNIT_TEST(testEmailFactoryRenderErrors);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ProfileProvider> provider;
        shared_ptr<TokenServerContext> context;

      public:
        TokenServerContextTest();
        ~TokenServerContextTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

        void testTokenService();
        void testTokenServiceSame();
        void testTokenServiceDiff();
        void testTokenServiceDB();
        void testTokenServiceMysql();
        void testTokenServiceMysqlBadBackend();
        void testTokenServiceMysqlBadType();
        void testTokenServiceSoci();
        void testTokenServiceDBParam();
        void testTokenServiceMysqlParam();
        void testTokenServiceSociParam();
        void testTokenServiceUnknown();

        void testTokenServiceDBOpen();
        void testTokenServiceDBClose();

        void testProfileService();
        void testProfileServiceMysql();
        void testProfileServiceSOCI();
        void testProfileServiceRemote();
        void testProfileServiceMysqlParam();
        void testProfileServiceRemoteParam();
        void testProfileServiceUnknown();
        void testProfileServiceCmdMysql();   //!< init context using mysql profile provider
        void testProfileServiceCmdRemote();  //!< init context using remote profile provider
        void testProfileServiceCmdSoci();    //!< init context using soci profile provider
        void testProfileServiceCmdThrow();   //!< init context with unknown profile provider type
        void testGetWrongServiceThrow();     //!< throw when getting non-existent service
        void testGetProfileNE();             //!< profile service is not registered
        void testGetProfileWrong();          //!< profile service registered but bad
        void testGetProfileInvalidType();    //!< get profile using invalid type

        void testGetTokenProviderBadExpiration();  //!< try to get a token provider with wrong expiration
        void testGetTokenProviderBadRefresh();     //!< try to get a token provider with wrong refresh_expiration

        void testEmailFactoryService();       //!< check properties of email service
        void testEmailFactory();              //!< try to get an email service
        void testEmailFactoryFailUtility();   //!< sending email failed (email utility doesn't exist)
        void testEmailFactoryFailTemplate();  //!< sending email failed (template is bad)
        void testEmailFactoryRenderErrors();  //!< sending email some rendering errors
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
