/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _TOKEN_PROVIDER_BASE_TEST_H
#define _TOKEN_PROVIDER_BASE_TEST_H

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/JWT.h>
#include <vx/PtreeUtil.h>
#include <TokenProviderMySQL.h>
#include <TokenProviderDB.h>
#include <TokenServerContext.h>

using namespace std;

#define TTL 2

namespace vx {
  namespace openid {
    namespace test {
      /// testing token provider api
      class TokenProviderBaseTest {
      protected:
        string provider_type;                      //!< token provider (db, mysql, ....)
        string provider_name;                      //!< expected provider Name
        shared_ptr<TokenProvider> token_provider;  //!< created token provider instance
        shared_ptr<TokenServerContext> context;    //!< current server context

      public:
        TokenProviderBaseTest(string provider_type);  //!< constructor. provider_type - configuration name from config.json
        virtual ~TokenProviderBaseTest();             //!< virtual destructor. noop

        void setUpInternal();     //!< common setup, should be called from setUp fixture function
        void tearDownInternal();  //!< common teardown, should be called from tearDown fixture function

        virtual void testKey();
        virtual void testCreateToken();
        virtual void testCreateTokenWithRefreshRecord();
        virtual void testCreateTokenRefresh();
        virtual void testCreateTokenWithTTL();
        virtual void testHit();
        virtual void testHitReset();
        virtual void testValidateToken();
        virtual void testValidateNonce();
        virtual void testClean();
        virtual void testCleanGarbage();
        virtual void testSetTTL();
        virtual void testSetRefreshTTL();
        virtual void testNonce();
        virtual void testMetadata();
        virtual void testGetTokenByNonce();
        virtual void testDeleteTokenByNonce();
        virtual void testDeleteTokenByNonceEmpty();

        virtual void testGetTokenByRefreshToken();
        virtual void testAccessTokenDeleted();   //!< delete access token, refresh token should still be available
        virtual void testRefreshTokenDeleted();  //!< delete refresh token, access token should still exists with empty refresh_toekn field

        virtual void testRefreshReused();

        virtual void testName();  //!< check provider name, should return section name

        // --- internal tests
        void testUserTokenMetadata();
        void testTokenMetadataMap();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
