#pragma once
/*
 * Copyright 2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// dummy test, can be used as a template
      class NotImplementedTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(NotImplementedTest);

        // CPPUNIT_TEST(testNotImpl);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<TokenServerContext> context;
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;

      public:
        NotImplementedTest();
        ~NotImplementedTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< ccpunit teardown

        void testNotImpl();  //!< dummy test
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
