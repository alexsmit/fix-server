#include "ProfileSVCRemoteHttpsTest.h"

using namespace vx::openid::test;

ProfileSVCRemoteHttpsTest::ProfileSVCRemoteHttpsTest()
    : ProfileSVCBaseTest("remotes") {}
void ProfileSVCRemoteHttpsTest::setUp() { setUpInternal(); }
void ProfileSVCRemoteHttpsTest::tearDown() { tearDownInternal(); }

// CPPUNIT_TEST_SUITE_REGISTRATION(ProfileSVCRemoteHttpsTest);
