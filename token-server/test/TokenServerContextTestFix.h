#pragma once
/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <vx/web/NoRequest.h>
#include <TokenServerContext.h>

using namespace VxServer;

/// Fixture class to initialize requests and responses
/// NOTE: uses CGI-stype headers
class TokenServerContextTestFix {
public:
  TokenServerContext &context;    //!< server context instance
  shared_ptr<NoRequest> no_req;   //!< inner request
  shared_ptr<NoResponse> no_res;  //!< innder response
  shared_ptr<Request> request;    //!< request object could be used in the handlers
  shared_ptr<Response> response;  //!< response object could be used in the handlers
  stringstream sin;               //!< input stream
  stringstream sout;              //!< output stream

  string std_output,   //!< captured stdout output
      std_err_output;  //!< captured stderr output
    
  bool has_exception;
  string exception_message;

  // parsed response (draft)

  string code;  //!< parsed code (should run Parse())
  string body;  //!< parsed string body (should run Parse())
  ptree pt_body;
  SimpleWeb::CaseInsensitiveMultimap headers;

  /// Fixture class instance
  TokenServerContextTestFix(TokenServerContext &context, const string &content = "", map<string, string> headers = {});

  ~TokenServerContextTestFix();

  /// default constructor for capturing only
  // TokenServerContextTestFix();

  /// capture stderr & stdout
  void StartCapture();

  /// stop capture
  void StopCapture();

  /// parse stdout
  void Parse();

  /// run something
  void Run(std::function<void()> fun);

private:
  bool capture_started = false;  //!< StartCapture executed, we need to do StopCapture on destruction and avoid double-stop
  std::streambuf *buf_output,    //!< saved stdout buffer
      *buf_err_output;           //!< saved stderr buffer
  stringstream out,              //!< captured stdout output
      out_err;                   //!< captured stderr output
};
