/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/variant.hpp>

//#define USE_BOOST_ALGO
#ifdef USE_BOOST_ALGO
#include <boost/algorithm/string.hpp>
#endif

#include <vx/StringUtil.h>
#include <vx/web/NoRequest.h>
#include <vx/openid/models/Models.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

/*
void funn() {
  AuthorizeRequest rr;
  rr.client_id = "client";
  rr.code_challenge = "challenge";
  rr.code_challenge_method = "s256";
  rr.display = "none";
  rr.idp = "idp";
  rr.idp_scope = "scope1";
  rr.login_hint = "hint";
  rr.max_age = "600";
  rr.nonce = "nonce";
  rr.prompt = "none";
  rr.redirect_uri = "redirect";
  rr.request = "jwt";
  rr.response_mode = "query";
  rr.response_type = "code";
  rr.scope = "scope";
  rr.sessionToken = "session";
  rr.state = "state";
  string ss;
  ss << rr;
  // AuthResponse r = {"2021-08-01T10:40:00.000Z", "SUCCESS", "session-token", {"user-id", "username", "user@vxpro.com"}};
  // ptree pt;
  // pt << r;
}
*/

#define GOGO
#ifdef GOGO

#include "../include/BaseTest.h"
#include "AuthModelsTest.h"
using namespace vx::openid::test;

//-----------------------------------------

AuthModelsTest::AuthModelsTest() {}

AuthModelsTest::~AuthModelsTest() {}

void AuthModelsTest::setUp() {
}

void AuthModelsTest::tearDown() {}

void AuthModelsTest::testAuthnResponseSerialize() {
  AuthResponse r = {"2021-08-01T10:40:00.000Z", "SUCCESS", "session-token", {"user-id", "username", "user@vxpro.com"}};
  ptree pt;
  pt << r;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expiresAt", string("2021-08-01T10:40:00.000Z"), pt.get("expiresAt", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), pt.get("status", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", string("session-token"), pt.get("sessionToken", ""));

  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.id", string("user-id"), pt.get("profile.id", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", string("username"), pt.get("profile.username", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.email", string("user@vxpro.com"), pt.get("profile.email", ""));
}

void AuthModelsTest::testAuthnResponseDeserialize() {
  AuthResponse r = {};
  ptree pt;

  pt.put("expiresAt", "2021-08-01T10:40:00.000Z");
  pt.put("status", "SUCCESS");
  pt.put("sessionToken", "session-token");
  pt.put("profile.id", "user-id");
  pt.put("profile.username", "username");
  pt.put("profile.email", "user@vxpro.com");

  pt >> r;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expiresAt", string("2021-08-01T10:40:00"), r.expiresAt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), r.status);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", string("session-token"), r.sessionToken);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.id", string("user-id"), r.profile.id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", string("username"), r.profile.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.email", string("user@vxpro.com"), r.profile.email);
}


void AuthModelsTest::testRequestSerialize() {
  AuthRequest r = {"user", "password", {false, true}};
  ptree pt;
  pt << r;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Username", string("user"), pt.get("username", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Password", string("password"), pt.get("password", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("options.multiOptionalFactorEnroll", string("false"), pt.get("options.multiOptionalFactorEnroll", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("options.warnBeforePasswordExpired", string("true"), pt.get("options.warnBeforePasswordExpired", ""));
}

void AuthModelsTest::testRequestDeserialize() {
  AuthRequest request = {};
  ptree pt;
  pt.put("username", "user");
  pt.put("password", "password");
  pt.put("options.multiOptionalFactorEnroll", true);
  pt.put("options.warnBeforePasswordExpired", false);

  pt >> request;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Username", string("user"), request.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Password", string("password"), request.password);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("options.multiOptionalFactorEnroll", true, request.options.multiOptionalFactorEnroll);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("options.warnBeforePasswordExpired", false, request.options.warnBeforePasswordExpired);
}

void AuthModelsTest::testAuthorizeRequestDeserialize() {
  stringstream sin;
  shared_ptr<NoRequest> nr;
  shared_ptr<Request> request;

  nr.reset(new NoRequest(sin));
  nr->query_string =
      "client_id=aaa"
      "&prompt=none"
      "&response_type=code"
      "&scope=openid"
      "&redirect_uri=http://localhost:8080/authorize_callback"
      "&state=some-state"
      "&nonce=some-nonce"
      "&sessionToken=session-token"
      "&code_challenge=challenge"
      "&code_challenge_method=S256";
  request.reset(new RequestSpec<shared_ptr<NoRequest>>(nr, MAX_POST_SIZE));

  AuthorizeRequest r;
  request->params >> r;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("aaa"), r.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("openid"), r.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("redirect_uri", string("http://localhost:8080/authorize_callback"), r.redirect_uri);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("some-state"), r.state);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nonce", string("some-nonce"), r.nonce);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", string("session-token"), r.sessionToken);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("prompt", string("none"), r.prompt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("response_type", string("code"), r.response_type);
  // for PKCE
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_challenge", string("challenge"), r.code_challenge);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_challenge_method", string("S256"), r.code_challenge_method);
}

void AuthModelsTest::testAuthorizeRequestSerialize() {
  AuthorizeRequest rr;
  rr.client_id = "client";
  rr.code_challenge = "challenge";
  rr.code_challenge_method = "s256";
  rr.display = "none";
  rr.idp = "idp";
  rr.idp_scope = "scope1";
  rr.login_hint = "hint";
  rr.max_age = "600";
  rr.nonce = "nonce";
  rr.prompt = "none";
  rr.redirect_uri = "redirect";
  rr.request = "jwt";
  rr.response_mode = "query";
  rr.response_type = "code";
  rr.scope = "scope";
  rr.sessionToken = "session";
  rr.state = "state";
  string ss;
  ss << rr;

  // parse string "manually" and create a map
  vector<string> vv;
  map<string, string> mm;

#ifdef USE_BOOST_ALGO
  boost::split(vv, ss, boost::is_any_of("&"));
  for (auto& val : vv) {
    vector<string> vval;
    boost::split(vval, val, boost::is_any_of("="));
    string mk = vval[0];
    string mv;
    if (vval.size() > 1) mv = vval[1];
    mm[mk] = mv;
  }
#else
  vv = StringUtil::split(ss, "&");
  for (auto& val : vv) {
    vector<string> vval = StringUtil::split(val, "=");
    string mk = vval[0];
    string mv;
    if (vval.size() > 1) mv = vval[1];
    mm[mk] = mv;
  }
#endif
  // check all the keys
  CPPUNIT_ASSERT_EQUAL_MESSAGE("keys", (size_t)17, mm.size());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("client"), mm["client_id"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_challenge", string("challenge"), mm["code_challenge"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_challenge_method", string("s256"), mm["code_challenge_method"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("display", string("none"), mm["display"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("idp", string("idp"), mm["idp"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("idp_scope", string("scope1"), mm["idp_scope"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("login_hint", string("hint"), mm["login_hint"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("max_age", string("600"), mm["max_age"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nonce", string("nonce"), mm["nonce"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("prompt", string("none"), mm["prompt"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("redirect_uri", string("redirect"), mm["redirect_uri"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("request", string("jwt"), mm["request"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("response_mode", string("query"), mm["response_mode"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("response_type", string("code"), mm["response_type"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("scope"), mm["scope"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", string("session"), mm["sessionToken"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), mm["state"]);
}

void AuthModelsTest::testTokenRequestDeserialize() {
  stringstream sin;
  shared_ptr<NoRequest> nr;
  shared_ptr<Request> request;

  nr.reset(new NoRequest(sin));
  nr->query_string =
      "code=code"
      "&code_verifier=some"
      "&grant_type=grant"
      "&password=pass"
      "&redirect_uri=http://localhost:8080/authorize_callback"
      "&refresh_token=some-token"
      "&scope=some-scope"
      "&username=user"
      "&client_id=id"
      "&client_secret=secret";
  request.reset(new RequestSpec<shared_ptr<NoRequest>>(nr, MAX_POST_SIZE));

  TokenRequest r;
  request->params >> r;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", string("code"), r.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_verifier", string("some"), r.code_verifier);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("grant_type", string("grant"), r.grant_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("password", string("pass"), r.password);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("redirect_uri", string("http://localhost:8080/authorize_callback"), r.redirect_uri);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("refresh_token", string("some-token"), r.refresh_token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("some-scope"), r.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string("user"), r.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("id"), r.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_secret", string("secret"), r.client_secret);
}

void AuthModelsTest::testTokenRequestSerialize() {
  TokenRequest rr;
  rr.code = "code";
  rr.code_verifier = "verifier";
  rr.grant_type = "grant";
  rr.password = "pass";
  rr.redirect_uri = "http://url";
  rr.refresh_token = "token";
  rr.scope = "scope";
  rr.username = "name";
  rr.client_id = "id";
  rr.client_secret = "secret";
  string ss;
  ss << rr;

  // parse string "manually" and create a map
  vector<string> vv;
  map<string, string> mm;
#ifdef USE_BOOST_ALGO
  boost::split(vv, ss, boost::is_any_of("&"));
  for (auto& val : vv) {
    vector<string> vval;
    boost::split(vval, val, boost::is_any_of("="));
    string mk = vval[0];
    string mv;
    if (vval.size() > 1) mv = vval[1];
    mm[mk] = mv;
  }
#else
  vv = StringUtil::split(ss, "&");
  for (auto& val : vv) {
    vector<string> vval = StringUtil::split(val, "=");
    string mk = vval[0];
    string mv;
    if (vval.size() > 1) mv = vval[1];
    mm[mk] = mv;
  }
#endif

  // check all the keys
  CPPUNIT_ASSERT_EQUAL_MESSAGE("keys", (size_t)10, mm.size());

  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", string("code"), mm["code"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code_verifier", string("verifier"), mm["code_verifier"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("grant_type", string("grant"), mm["grant_type"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("password", string("pass"), mm["password"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("redirect_uri", string("http%3A%2F%2Furl"), mm["redirect_uri"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("refresh_token", string("token"), mm["refresh_token"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("scope"), mm["scope"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string("name"), mm["username"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("id"), mm["client_id"]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_secret", string("secret"), mm["client_secret"]);
}


void AuthModelsTest::testAuthorizeResponseDeserialize() {
  AuthorizeResponse request = {};
  ptree pt;
  pt.put("state", "state");
  pt.put("code", "code");

  pt >> request;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("State", string("state"), request.state);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Code", string("code"), request.code);
}

void AuthModelsTest::testAuthorizeResponseSerialize() {
  AuthorizeResponse r{.state = "state", .code = "code"};
  ptree pt;
  pt << r;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("State", string("state"), pt.get("state", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Code", string("code"), pt.get("code", ""));
}

/// dummy VxIndex class implementation
class FakeRequestBody : public VxServer::VxIndex {
public:
  ptree pt;  //!< body key-value pairs
  /// index access operator
  std::string operator[](const std::string& key) const override {
    return pt.get(key, "");
  }
};

void AuthModelsTest::testIntrospectDeserializeStream() {
  FakeRequestBody body;
  IntrospectResponse resp;
  body.pt.put("active", true);
  body.pt.put("aud", "aud");
  body.pt.put("client_id", "client_id");
  body.pt.put("device_id", "device_id");
  body.pt.put("exp", 123);
  body.pt.put("iat", 456);
  body.pt.put("iss", "iss");
  body.pt.put("jti", "jti");
  body.pt.put("nbf", 789);
  body.pt.put("scope", "scope");
  body.pt.put("sub", "sub");
  body.pt.put("token_type", "token_type");
  body.pt.put("uid", "uid");
  body.pt.put("username", "username");

  body >> resp;

  CPPUNIT_ASSERT_MESSAGE("active", resp.active);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("aud", string("aud"), resp.aud);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("client_id"), resp.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("device_id", string("device_id"), resp.device_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("exp", (int)123, resp.exp);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("iat", (int)456, resp.iat);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("iss", string("iss"), resp.iss);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("jti", string("jti"), resp.jti);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nbf", (int)789, resp.nbf);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("scope"), resp.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", string("sub"), resp.sub);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("token_type"), resp.token_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("uid", string("uid"), resp.uid);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string("username"), resp.username);
}

void AuthModelsTest::testIntrospectDeserializeTemplate() {
  FakeRequestBody body;
  IntrospectResponse resp;
  body.pt.put("active", true);
  body.pt.put("aud", "aud");
  body.pt.put("client_id", "client_id");
  body.pt.put("device_id", "device_id");
  body.pt.put("exp", 123);
  body.pt.put("iat", 456);
  body.pt.put("iss", "iss");
  body.pt.put("jti", "jti");
  body.pt.put("nbf", 789);
  body.pt.put("scope", "scope");
  body.pt.put("sub", "sub");
  body.pt.put("token_type", "token_type");
  body.pt.put("uid", "uid");
  body.pt.put("username", "username");

  fromVxIndex(body, resp);

  CPPUNIT_ASSERT_MESSAGE("active", resp.active);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("aud", string("aud"), resp.aud);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("client_id"), resp.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("device_id", string("device_id"), resp.device_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("exp", (int)123, resp.exp);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("iat", (int)456, resp.iat);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("iss", string("iss"), resp.iss);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("jti", string("jti"), resp.jti);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nbf", (int)789, resp.nbf);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", string("scope"), resp.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", string("sub"), resp.sub);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("token_type"), resp.token_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("uid", string("uid"), resp.uid);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string("username"), resp.username);
}

void AuthModelsTest::testIntrospectRequestDeserialize() {
  FakeRequestBody body;
  IntrospectRequest req;
  body.pt.put("token", "token");
  body.pt.put("token_type_hint", "token_type_hint");
  body.pt.put("client_id", "client_id");

  body >> req;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("token", string("token"), req.token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type_hint", string("token_type_hint"), req.token_type_hint);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("client_id"), req.client_id);
}

void AuthModelsTest::testLogoutRequestDeserialize() {
  FakeRequestBody body;
  LogoutRequest req;
  body.pt.put("state", "state");
  body.pt.put("id_token_hint", "id_token_hint");
  body.pt.put("post_logout_redirect_uri", "post_logout_redirect_uri");

  body >> req;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), req.state);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id_token_hint", string("id_token_hint"), req.id_token_hint);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("post_logout_redirect_uri", string("post_logout_redirect_uri"), req.post_logout_redirect_uri);
}

void AuthModelsTest::testLogoutRequestSerialize() {
  LogoutRequest req;
  req.post_logout_redirect_uri = "uri";
  req.id_token_hint = "id";
  req.state = "state";

  string query;
  query << req;

  auto m = SimpleWeb::QueryString::parse(query);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("post_logout_redirect_uri", string("uri"), m.find("post_logout_redirect_uri")->second);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id_token_hint", string("id"), m.find("id_token_hint")->second);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), m.find("state")->second);
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthModelsTest);
#endif