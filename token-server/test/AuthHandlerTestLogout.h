#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /v1/logout
      class AuthHandlerTestLogout : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestLogout);

        CPPUNIT_TEST(AuthnLogoutConfig);
        CPPUNIT_TEST(AuthnLogoutWrongUri);
        CPPUNIT_TEST(AuthnLogoutNoToken);
        CPPUNIT_TEST(AuthnLogoutWrongToken);
        CPPUNIT_TEST(AuthnLogoutNonIdToken);
        CPPUNIT_TEST(AuthnLogoutNoCookie);
        CPPUNIT_TEST(AuthnLogoutNoCookieState);
        CPPUNIT_TEST(AuthnLogout);
        CPPUNIT_TEST(AuthnLogoutOptions);

        CPPUNIT_TEST_SUITE_END();


      public:
        AuthHandlerTestLogout();
        ~AuthHandlerTestLogout();

        void AuthnLogoutConfig();         //!< verify logout config
        void AuthnLogoutWrongUri();       //!< callback logout url is wrong
        void AuthnLogoutNoToken();        //!< token is not provided
        void AuthnLogoutWrongToken();     //!< wrong token or garbage
        void AuthnLogoutNonIdToken();     //!< access token instead of ID token
        void AuthnLogoutNoCookie();       //!< cookie is not set before logout
        void AuthnLogoutNoCookieState();  //!< no cookie, state is provided
        void AuthnLogout();               //!< happy path scenario
        void AuthnLogoutOptions();        //!< OPTIONS
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
