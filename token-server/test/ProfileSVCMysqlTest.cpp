#include "ProfileSVCMysqlTest.h"

using namespace vx::openid::test;

ProfileSVCMysqlTest::ProfileSVCMysqlTest()
    : ProfileSVCBaseTest("mysql") {}

void ProfileSVCMysqlTest::setUp() { setUpInternal(); }
void ProfileSVCMysqlTest::tearDown() { tearDownInternal(); }

CPPUNIT_TEST_SUITE_REGISTRATION(ProfileSVCMysqlTest);
