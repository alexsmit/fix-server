#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {

      /// testing TokenAPI
      class AuthHandlerTestApi : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestApi);

        CPPUNIT_TEST(ApiIdtokenOnly);
        CPPUNIT_TEST(ApiAccessTokenOnly);
        CPPUNIT_TEST(ApiIdAccessNoRefresh);
        CPPUNIT_TEST(ApiAllTokens);
        CPPUNIT_TEST(ApiAccessAddressClaim);
        CPPUNIT_TEST(ApiAccessPhoneClaim);
        CPPUNIT_TEST(ApiAccessProfileClaim);
        CPPUNIT_TEST(ApiAccessUnknownClaim);
        CPPUNIT_TEST(ApiGetPropertyNE);
        CPPUNIT_TEST(ApiIgnoredNonce);
        CPPUNIT_TEST(ApiGetError);

        CPPUNIT_TEST_SUITE_END();

      public:
        AuthHandlerTestApi();
        ~AuthHandlerTestApi();

        void ApiIdtokenOnly();         //!< request id token only
        void ApiAccessTokenOnly();     //!< request access token only
        void ApiIdAccessNoRefresh();   //!< both tokens no refresh
        void ApiAllTokens();           //!< id, access and refresh
        void ApiAccessAddressClaim();  //!< get address scope
        void ApiAccessPhoneClaim();    //!< get phone scope
        void ApiAccessProfileClaim();  //!< get profile scope
        void ApiAccessUnknownClaim();  //!< using "unknown" scope, should be ignored
        void ApiGetPropertyNE();       //!< get invalid property
        void ApiIgnoredNonce();        //!< ignore nonce scope even if configured in config.json
        void ApiGetError();
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
