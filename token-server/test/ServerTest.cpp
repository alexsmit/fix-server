/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <stdlib.h>
#include <sys/stat.h>
#include <atomic>
#include <chrono>
#include <vector>

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/regex.hpp>

#include "BaseTest.h"
#include "ServerTest.h"
#include <vx/URL.h>

#include <VxMessage.h>

#include <vx/ServiceProfileFactory.h>

#include <vx/web/VxHandler.h>
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTest.h"
#include "HandlerTestFix.h"

using namespace std;
using namespace vx::openid::test;

ServerTest::ServerTest() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());
}

ServerTest::~ServerTest() {}

void ServerTest::setUp() {
  client.reset(new HttpClient(DEBUG_HOST));

  context->setParam("profile_provider.type", "mysql");
  context->setParam("token_provider.type", "db");

  provider = context->GetProfileProvider();
  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});

  password_request =
      (boost::format("{\"grant_type\":\"password\",\"username\":\"%s\",\"password\":\"%s\"}") % USER_NAME % USER_PASSWORD).str();

  password_request_form =
      (boost::format("grant_type=password&username=%s&password=%s&client_id=aaa&client_secret=111") % USER_NAME % USER_PASSWORD).str();

  password_wrong_request =
      (boost::format("{\"grant_type\":\"unknown\",\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();

  password_bad_request =
      (boost::format("{\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();
}

void ServerTest::tearDown() {
  if (provider) provider->DeleteUser(USER_NAME);
  // provider->DeleteRole(ROLE_1);
  // provider->DeleteRole(ROLE_2);
  provider.reset();
  // token_provider.reset();
}


void ServerTest::testJWKS() {
  // cout << "ServerTest::testJWKS()" << endl;
  auto result = client->request("GET", "/.well-known/jwks.json");
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  auto content = result->content.string();
  // cout << content << endl;
  CPPUNIT_ASSERT_MESSAGE("Response should not be empty", !content.empty());

  validateJWKS(content);
}

void ServerTest::testOpenid() {
  auto result = client->request("GET", "/.well-known/openid-configuration");
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  ptree conf;
  read_json(result->content, conf);
  CPPUNIT_ASSERT_MESSAGE("issuer", conf.count("issuer") > 0);
  CPPUNIT_ASSERT_MESSAGE("jwks_uri", conf.count("jwks_uri") > 0);

  // goto JWKS URL, will use only "path part"
  URL jwks_uri(conf.get("jwks_uri", ""));

  string jwks_full_host = jwks_uri.host + ":" + jwks_uri.port;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same host", string(TOKEN_SERVER_HOST), jwks_full_host);

  result = client->request("GET", jwks_uri.path);
  code = result->status_code.substr(0, 3);
  auto expected_algo = context->getParam("token_provider.algo");
  if (expected_algo.empty()) expected_algo = "RS512";

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto content = result->content.string();
  // cout << content << endl;
  CPPUNIT_ASSERT_MESSAGE("Response should not be empty", !content.empty());
}

/**
 * Scenario: Send malformed request with invalid grant
 * Expected: 400
 **/
void ServerTest::testInvalidGrant() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto result =
      client->request("POST", "/oauth/token", password_wrong_request, header);

  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should fail with 400", code == "400");
  ptree respt;
  read_json(result->content, respt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Error code", string("auth_error"), respt.get("error", ""));
}

/**
 * Scenario: Happy path to get an access token
 * Expected: token is valid, has roles and nonce
 **/
void ServerTest::testGetToken() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);
  auto access_token = pt_token.get<string>("access_token", "");

  JWT tokenapi(*context->info);
  auto &ok = tokenapi.Verify(access_token, true);
  CPPUNIT_ASSERT_MESSAGE("access_token is valid", ok.isValid());
  CPPUNIT_ASSERT_MESSAGE("access_token contains nonce", ok.has_payload_claim("nonce"));

  auto auds = ok.get_audience();
  CPPUNIT_ASSERT_MESSAGE("should have 1 audience value", auds.size() == 1);
  auto it = auds.find("api.vxpro.com");
  CPPUNIT_ASSERT_MESSAGE("should have expected audience", it != auds.end());

  auto roles = ok.get_payload_set("roles");
  CPPUNIT_ASSERT_MESSAGE("should have 2 roles", roles.size() == 2);
  auto itr = roles.find(ROLE_1);
  CPPUNIT_ASSERT_MESSAGE("should have role1", itr != roles.end());
  itr = roles.find(ROLE_2);
  CPPUNIT_ASSERT_MESSAGE("should have role2", it != roles.end());

  auto userid = ok.get_payload_claim("userid");
  CPPUNIT_ASSERT_MESSAGE("should have userid claim", !userid.empty());

  auto nonce = (ok.has_payload_claim("nonce")) ? ok.get_payload_claim("nonce") : string("");
  CPPUNIT_ASSERT_MESSAGE("access_token contains nonce", !nonce.empty());
}

/**
 * Scenario: Send malformed request
 * Expected: 400
 **/
void ServerTest::testGetTokenBadClient() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", "invalid_client_id"});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto result =
      client->request("POST", "/oauth/token", password_request, header);

  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should fail with 401", code == "401");
  ptree respt;
  read_json(result->content, respt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Error code", string("invalid_client"), respt.get("error", ""));
}

/**
 * Scenario: Get token with username > 64 characters
 * Expected: 401
 **/
void ServerTest::testGetTokenBadUsername() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  ptree r;
  r.put("grant_type", "password");
  r.put("password", "password");
  r.put("username", "12345678901234567890123456789012345678901234567890123456789012345678901234567890");
  stringstream s;
  write_json(s, r);

  auto result =
      client->request("POST", "/oauth/token", s.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "400");

  ptree pt_token;
  read_json(result->content, pt_token);

  auto error = pt_token.get<string>("error", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, error);
  auto desc = pt_token.get<string>("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid user name"), desc);
}

/**
 * Scenario: Get token with password > 64 characters
 * Expected: 401
 **/
void ServerTest::testGetTokenBadPassword() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  ptree r;
  r.put("grant_type", "password");
  r.put("password", "12345678901234567890123456789012345678901234567890123456789012345678901234567890");
  r.put("username", "username");
  stringstream s;
  write_json(s, r);

  auto result =
      client->request("POST", "/oauth/token", s.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "400");

  ptree pt_token;
  read_json(result->content, pt_token);

  auto error = pt_token.get<string>("error", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, error);
  auto desc = pt_token.get<string>("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid password"), desc);
}

/**
 * @brief get tokens from muptiple threads. all should be valid
 *
 */
void ServerTest::testGetTokenMany() {
  int count = 40;
  int cycles = 20;
  std::atomic<int> err_count{0};
  cout << 'W' << flush;
  // cout << endl
  //      << "Speed test, running request from " << count << " threads" << endl;
  time_t t_start = ::time(NULL);
  vector<thread> threads;
  auto self = this;
  for (int i = 0; i < count; i++) {
    threads.push_back(thread([self, i, &err_count, cycles]() {
      for (auto t = 0; t < cycles; t++) {
        SimpleWeb::CaseInsensitiveMultimap header;
        ptree pt_token;
        try {
          self->getToken(header, pt_token);
          auto access_token = pt_token.get<string>("access_token", "");

          bool valid = false;

          {
            JWT tokenapi(*self->context->info, &self->context->mtx);
            auto &tok = tokenapi.Verify(access_token, true);
            valid = tok.isValid();
            if (!valid) {
              string sout = (boost::format("access_token should be valid, thread #%d -- %s") % i % tok.what()).str();
              cerr << sout << endl;
              err_count++;
              break;
            }
          }
        }
        catch (const std::exception &e) {
          std::cerr << e.what() << '\n';
          err_count++;
          break;
        }
      }
    }));
  }

  for (int i = 0; i < count; i++) {
    threads[i].join();
  }
  time_t t_tot = ::time(NULL) - t_start;
  // cout << ">>> time to get tokens from " << count << " threads: " << t_tot << " secs" << endl;
  auto speed = (t_tot == 0) ? 1000 : (count * cycles / t_tot) + 1;
  CPPUNIT_ASSERT_MESSAGE("Speed more than 5", speed > 5);
  // CPPUNIT_ASSERT_GREATEREQUAL(long(5), speed);
  int final_count = err_count;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have zero errors", (int)0, final_count);
}

/**
 * Scenario: Get token and refresh
 * Expected: Token is obtained and refresh is successful
 **/
void ServerTest::testGetTokenRefresh() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET token using refresh token  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "refresh_token");
  req.put("refresh_token", refresh_token);
  write_json(req_refresh_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  read_json(result->content, pt_token);
  auto access_token = pt_token.get<string>("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains access_token", !access_token.empty());

  JWT tokenapi(*context->info);
  auto &ok2 = tokenapi.Verify(access_token, true);
  CPPUNIT_ASSERT_MESSAGE("access_token is valid", ok2.isValid());
  auto nonce = (ok2.has_payload_claim("nonce")) ? ok2.get_payload_claim("nonce") : string("");
  CPPUNIT_ASSERT_MESSAGE("access_token contains nonce", !nonce.empty());
}

/**
 * Scenario: Get token and refresh with invalide refresh token
 * Expected: Token is obtained and refresh is failed with 401
 **/
void ServerTest::testGetTokenRefreshBad() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET token using refresh token  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "refresh_token");
  req.put("refresh_token", "bad_refresh_token");
  write_json(req_refresh_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 401", string("401"), code);

  read_json(result->content, pt_token);
  auto error = pt_token.get<string>("error", "");
  auto desc = pt_token.get<string>("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("contains error", string("invalid_token"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("contains error description", string("Invalid refresh token"), desc);
}

/**
 * Scenario: Get token and validate is it possible to refresh it with PIN
 * Expected: Token is obtained and refresh is possible
 **/
void ServerTest::testGetTokenRefreshPINOK() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- check we cah use PIN to refresh token  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "validate");
  write_json(req_refresh_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
}

/**
 * Scenario: Get token, refresh it, and then try to refresh with "old" refresh token
 * Expected: Token is obtained and second refresh is failed with 401
 **/
void ServerTest::testGetTokenRefreshStolen() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);
  auto access_token = pt_token.get<string>("access_token", "");
  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET new token using refresh token  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "refresh_token");
  req.put("refresh_token", refresh_token);
  write_json(req_refresh_json, req);
  string refresh_request = req_refresh_json.str();

  auto result =
      client->request("POST", "/oauth/token", refresh_request, header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  //--- GET new token trying to use old token + old refresh token (stolen), should fail
  result =
      client->request("POST", "/oauth/token", refresh_request, header);
  code = result->status_code.substr(0, 3);
  ptree pt_fail;
  read_json(result->content, pt_fail);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expected error", string("invalid_token"), pt_fail.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expected message", string("Invalid refresh token"), pt_fail.get("error_description", ""));
}

/**
 * Scenario: Get token and refresh it with PIN
 * Expected: Token is obtained and refresh is successful
 **/
void ServerTest::testGetTokenPin() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET token using pin  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "pin");
  req.put("pin", USER_PIN);
  write_json(req_refresh_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  read_json(result->content, pt_token);
  auto access_token = pt_token.get<string>("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains access_token", !access_token.empty());

  JWT tokenapi(*context->info);
  auto &ok2 = tokenapi.Verify(access_token, true);
  CPPUNIT_ASSERT_MESSAGE("access_token is valid", ok2.isValid());
  CPPUNIT_ASSERT_MESSAGE("access_token contains nonce", ok2.has_payload_claim("nonce"));
}

/**
 * Scenario: Get token, refresh with wrong PIN, try again with correct PIN
 * Expected: Token is obtained and second refresh is successful
 **/
void ServerTest::testGetTokenPinWrongOneTry() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET token using wrong pin  (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "pin");
  req.put("pin", "wrong");
  write_json(req_refresh_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);

  //--- GET token using correct pin  (+ bearer token in header)
  stringstream req_good_refresh_json;
  req.put("pin", USER_PIN);
  write_json(req_good_refresh_json, req);

  result =
      client->request("POST", "/oauth/token", req_good_refresh_json.str(), header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
}


/**
 * Scenario: Get token, try many times a wrong PIN. Then try with correct PIN.
 * Expected: Token is obtained. However it is not possible to refresh due too many errors.
 **/
void ServerTest::testGetTokenPinWrongMultiple() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //--- GET token using wrong pin multiple times (+ bearer token in header)
  ptree req;
  stringstream req_refresh_json;
  req.put("grant_type", "pin");
  req.put("pin", "wrong_pin");
  write_json(req_refresh_json, req);

  for (auto ii = 0; ii < 3; ii++) {
    auto result =
        client->request("POST", "/oauth/token", req_refresh_json.str(), header);
    auto code = result->status_code.substr(0, 3);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
  }

  // NOTE: testing only if it is not allowed to get a token after refresh token expiration
  auto pin_after_expiration = context->getParam("token_provider.pin_after_expiration");
  if (pin_after_expiration != "on") {
    //--- GET token using correct pin, should fail due too many failures before
    stringstream req_good_refresh_json;
    req.put("pin", USER_PIN);
    write_json(req_good_refresh_json, req);

    auto result =
        client->request("POST", "/oauth/token", req_good_refresh_json.str(), header);
    auto code = result->status_code.substr(0, 3);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);
  }
  else {
    CPPUNIT_FAIL("Token server should be configured with pin_after_expiration=off");
  }
}

/**
 * Scenario: Get token, wait enough to let refresh token to expire(in production that period is quite large)
 * Expected: Token is obtained. However it is not possible to refresh. Error: 401
 **/
void ServerTest::testGetTokenRefreshExpired() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getToken(header, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");

  //-- 4 wait >(DEBUG_TTL*2) seconds (current refresh token TTL) and try to refresh using very first
  // refresh_token
  cout << 'W' << flush;
  std::this_thread::sleep_for(std::chrono::seconds(DEBUG_TTL * 2 + 1));

  ptree req;
  stringstream req_refresh2_json;
  req.put("grant_type", "refresh_token");
  req.put("refresh_token", refresh_token);
  write_json(req_refresh2_json, req);

  auto result =
      client->request("POST", "/oauth/token", req_refresh2_json.str(), header);
  auto code = result->status_code.substr(0, 3);
  // cout << result->status_code << endl;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);
  ptree respt;
  read_json(result->content, respt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Error code", string("invalid_token"), respt.get("error", ""));
}

/**
 * Scenario: Get server info
 * Expected: Info contains some expected information
 **/
void ServerTest::testInfo() {
  // cout << "ServerTest::testInfo()" << endl;
  auto result = client->request("GET", "/info");
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  result = client->request("GET", "/");
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  result = client->request("GET", "/index.html");
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  result = client->request("GET", "/unknown_url");
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "404");
}

/**
 * Scenario: Get reset server info
 * Expected: Info contains some expected information
 **/
void ServerTest::testResetInfo() {
  auto result = client->request("GET", "/reset/serverinfo");
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");
  auto body = result->content.string();
  // got some HTML (no details, expect info like info handler data)
  CPPUNIT_ASSERT_MESSAGE("1", body.find("<h1>Request from") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("2", body.find("GET /reset/serverinfo") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("3", body.find("Query Fields") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("4", body.find("Header Fields") != string::npos);
}

/**
 * Scenario: Send malformed request
 * Expected: 400
 **/
void ServerTest::testBadRequest() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto result =
      client->request("POST", "/oauth/token", password_bad_request, header);

  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should fail with 400", code == "400");
  ptree respt;
  read_json(result->content, respt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Error code", string("auth_error"), respt.get("error", ""));
}


/**
 * Scenario: Send token request using using x-form-urlencoded request
 * Expected: Successful
 **/
void ServerTest::testGetTokenForm() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree pt_token;
  getTokenForm(header, pt_token);
  auto access_token = pt_token.get<string>("access_token", "");

  JWT tokenapi(*context->info);
  auto &ok = tokenapi.Verify(access_token, true);
  CPPUNIT_ASSERT_MESSAGE("access_token is valid", ok.isValid());
  CPPUNIT_ASSERT_MESSAGE("access_token contains nonce", ok.has_payload_claim("nonce"));

  auto auds = ok.get_audience();
  CPPUNIT_ASSERT_MESSAGE("should have 1 audience value", auds.size() == 1);
  auto it = auds.find("api.vxpro.com");
  CPPUNIT_ASSERT_MESSAGE("should have expected audience", it != auds.end());

  auto roles = ok.get_payload_set("roles");
  CPPUNIT_ASSERT_MESSAGE("should have 2 roles", roles.size() == 2);
  auto itr = roles.find(ROLE_1);
  CPPUNIT_ASSERT_MESSAGE("should have role1", itr != roles.end());
  itr = roles.find(ROLE_2);
  CPPUNIT_ASSERT_MESSAGE("should have role2", it != roles.end());

  auto userid = ok.get_payload_claim("userid");
  CPPUNIT_ASSERT_MESSAGE("should have userid claim", !userid.empty());
}

/**
 * Scenario: Get server certificate fingerprint
 * Expected: Returned JSON object
 **/
void ServerTest::testFingerprint() {
  auto result = client->request("GET", "/fingerprint");
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  ptree finger;
  read_json(result->content, finger);

  auto sh = finger.get<string>("sha1", "");
  CPPUNIT_ASSERT_MESSAGE("sha1", !sh.empty());

  sh = finger.get<string>("sha256", "");
  CPPUNIT_ASSERT_MESSAGE("sha256", !sh.empty());
}


void ServerTest::getToken(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token) {
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto result =
      client->request("POST", "/oauth/token", password_request, header);
  auto code = result->status_code.substr(0, 3);

  if (code != "200") {
    cout << "/oauh/token failed with code = " << code << " : " << result->content.string() << endl;
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  read_json(result->content, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains refresh_token", !refresh_token.empty());

  auto access_token = pt_token.get<string>("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains access_token", !access_token.empty());

  auto token_type = pt_token.get<string>("token_type", "");
  CPPUNIT_ASSERT_MESSAGE("contains token_type", token_type == "bearer");

  string bearer = string("bearer") + " " + access_token;
  header.insert({"Authorization", bearer});
}

void ServerTest::getTokenForm(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token) {
  // header.insert({"X-Client-Id", CLIENT_ID});
  // header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});

  auto result =
      client->request("POST", "/oauth/token", password_request_form, header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  read_json(result->content, pt_token);

  auto refresh_token = pt_token.get<string>("refresh_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains refresh_token", !refresh_token.empty());

  auto access_token = pt_token.get<string>("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("contains access_token", !access_token.empty());

  auto token_type = pt_token.get<string>("token_type", "");
  CPPUNIT_ASSERT_MESSAGE("contains token_type", token_type == "bearer");

  string bearer = string("bearer") + " " + access_token;
  header.insert({"Authorization", bearer});
}

void ServerTest::validateJWKS(string jwks) {
  auto expected_algo = context->getParam("token_provider.algo");
  if (expected_algo.empty()) expected_algo = "RS512";

  stringstream s(jwks);
  ptree pt;
  read_json(s, pt);
  CPPUNIT_ASSERT_MESSAGE("JWKS contains keys", pt.count("keys") > 0);

  ptree pkeys = pt.get_child("keys");
  for (auto &pkey : pkeys) {
    auto key = pkey.second;

    auto val = key.get<string>("kty", "");
    CPPUNIT_ASSERT_MESSAGE("contains kty", val == "RSA");

    val = key.get<string>("n", "");
    CPPUNIT_ASSERT_MESSAGE("contains n", !val.empty());

    val = key.get<string>("e", "");
    CPPUNIT_ASSERT_MESSAGE("contains e", val == "AQAB");

    val = key.get<string>("use", "");
    CPPUNIT_ASSERT_MESSAGE("contains use", val == "sig");

    val = key.get<string>("alg", "");
    CPPUNIT_ASSERT_MESSAGE("contains alg", val == expected_algo);

    auto kid = key.get<string>("kid", "");
    CPPUNIT_ASSERT_MESSAGE("contains kid", !kid.empty());

    if (!context->info->isUsingKeys()) {
      auto x5t = key.get<string>("x5t", "");
      CPPUNIT_ASSERT_MESSAGE("contains x5t", !x5t.empty());
      CPPUNIT_ASSERT_MESSAGE("x5t == kid", x5t == kid);

      CPPUNIT_ASSERT_MESSAGE("contains x5c", key.count("x5c") == 1);
      ptree certs = key.get_child("x5c");
      for (auto &cc : certs) {
        auto cval = cc.second.data();
        CPPUNIT_ASSERT_MESSAGE("cert is not empty", !cval.empty());
      }
    }
  }
}

/// @brief Using a token with admin role and hit server test endpoints
void ServerTest::testAdminRole() {
  provider->SetRoles(USER_NAME, {"admin"});

  SimpleWeb::CaseInsensitiveMultimap head;
  ptree pt_token;
  getToken(head, pt_token);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Authorization", string("Bearer ") + pt_token.get("access_token", "")});
  auto result = client->request("POST", "/test/admin", "{}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  result = client->request("POST", "/test/user", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");

  result = client->request("POST", "/test/other", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");
}

/// @brief Using a token with user role and hit server test endpoints
void ServerTest::testUserRole() {
  provider->SetRoles(USER_NAME, {"user"});

  SimpleWeb::CaseInsensitiveMultimap head;
  ptree pt_token;
  getToken(head, pt_token);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Authorization", string("Bearer ") + pt_token.get("access_token", "")});
  auto result = client->request("POST", "/test/admin", "{}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");

  result = client->request("POST", "/test/user", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  result = client->request("POST", "/test/other", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");
}

/// @brief Using a token with other role and hit server test endpoints
void ServerTest::testOtherRole() {
  provider->SetRoles(USER_NAME, {"other"});

  SimpleWeb::CaseInsensitiveMultimap head;
  ptree pt_token;
  getToken(head, pt_token);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Authorization", string("Bearer ") + pt_token.get("access_token", "")});
  auto result = client->request("POST", "/test/admin", "{}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "401");

  result = client->request("POST", "/test/user", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");

  result = client->request("POST", "/test/other", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");
}

/// @brief Using a token with different role and hit server test endpoints
void ServerTest::testDifferentRole() {
  provider->SetRoles(USER_NAME, {ROLE_1});

  SimpleWeb::CaseInsensitiveMultimap head;
  ptree pt_token;
  getToken(head, pt_token);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Authorization", string("Bearer ") + pt_token.get("access_token", "")});
  auto result = client->request("POST", "/test/admin", "{}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");

  result = client->request("POST", "/test/user", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");

  result = client->request("POST", "/test/other", "{}", header);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");
}

/// Using a token for the endpoint that does not require an authorization
void ServerTest::testRoleNotRequired() {
  provider->SetRoles(USER_NAME, {ROLE_1});

  SimpleWeb::CaseInsensitiveMultimap head;
  ptree pt_token;
  getToken(head, pt_token);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Authorization", string("Bearer ") + pt_token.get("access_token", "")});
  auto result = client->request("POST", "/test", "{}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successfull", code == "200");

  SimpleWeb::CaseInsensitiveMultimap header2;
  result = client->request("POST", "/test", "{}", header2);
  code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be not authorized", code == "401");
}

void ServerTest::testGetClientCredentialsToken() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree client_cred_pt;
  client_cred_pt.put("client_id", "aaa");
  client_cred_pt.put("client_secret", "111");
  client_cred_pt.put("scope", "api");
  client_cred_pt.put("grant_type", "client_credentials");

  stringstream ss;
  write_json(ss, client_cred_pt);

  auto client_cred = ss.str();

  auto result =
      client->request("POST", "/oauth/token", client_cred, header);
  auto code = result->status_code.substr(0, 3);
  ptree pt_token;
  read_json(result->content, pt_token);

  // write_json(cout, pt_token);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto access_token = pt_token.get("access_token","");
  CPPUNIT_ASSERT_MESSAGE("token", !access_token.empty());

  auto tok_result = context->validateToken(access_token);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok, scheme basic", string("basic"), tok_result.scheme);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string("aaa"), tok_result.data.get("client_id",""));
}

void ServerTest::testGetClientCredentialsTokenFail() {
  SimpleWeb::CaseInsensitiveMultimap header;
  ptree client_cred_pt;
  client_cred_pt.put("client_id", "invalid");
  client_cred_pt.put("client_secret", "invalid");
  client_cred_pt.put("scope", "api");
  client_cred_pt.put("grant_type", "client_credentials");

  stringstream ss;
  write_json(ss, client_cred_pt);

  auto client_cred = ss.str();

  auto result =
      client->request("POST", "/oauth/token", client_cred, header);
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);

  ptree pt_result;
  read_json(result->content, pt_result);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), pt_result.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), pt_result.get("error_description",""));
}

/// @brief check the pattern, using capture values like (?<a>) (?<b>), only 2 so far 
/// @param data input data to test
/// @param pattern expected pattern
/// @return index as -1 (not found), 0 - matched w/o capture, 1- "a" capture, 2 - "b" capture
int ServerTest::get_assert_pattern(const string& data, const string& pattern) {
  boost::regex reg(pattern);
  boost::smatch match;
  int idx = -1;
  if (boost::regex_search(data, match, reg)) {
    auto val_a = match["a"].str();
    auto val_b = match["b"].str();
    if (!val_a.empty())
      idx = 1;
    else if (!val_b.empty())
      idx = 2;
    else
      idx = 0;
  }
  return idx;
}


CPPUNIT_TEST_SUITE_REGISTRATION(ServerTest);
