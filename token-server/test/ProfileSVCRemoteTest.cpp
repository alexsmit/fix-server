#include "ProfileSVCRemoteTest.h"

using namespace vx::openid::test;

ProfileSVCRemoteTest::ProfileSVCRemoteTest()
    : ProfileSVCBaseTest("remote") {}

void ProfileSVCRemoteTest::setUp() { setUpInternal(); }
void ProfileSVCRemoteTest::tearDown() { tearDownInternal(); }

CPPUNIT_TEST_SUITE_REGISTRATION(ProfileSVCRemoteTest);
