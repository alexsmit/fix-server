#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /v1/token
      class AuthHandlerTestToken : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestToken);

        CPPUNIT_TEST(AuthTokenNotAuthorizedAPINoClientId);
        CPPUNIT_TEST(AuthTokenNotAuthorizedAPIWrongClientId);
        CPPUNIT_TEST(AuthTokenNotAuthorizedAPINoClientSecret);
        CPPUNIT_TEST(AuthTokenNotAuthorized);
        CPPUNIT_TEST(AuthTokenWrongGrantEmpty);
        CPPUNIT_TEST(AuthTokenWrongGrantWrong);
        CPPUNIT_TEST(AuthTokenWrongGrantPassword);
        CPPUNIT_TEST(AuthTokenWrongGrantPasswordNoScope);
        CPPUNIT_TEST(AuthTokenWrongGrantPasswordNoUsername);
        CPPUNIT_TEST(AuthTokenWrongGrantCode);
        CPPUNIT_TEST(AuthTokenWrongGrantCodeNoUri);
        CPPUNIT_TEST(AuthTokenWrongGrantRefreshToken);
        CPPUNIT_TEST(AuthTokenExpiredCode);
        CPPUNIT_TEST(AuthTokenWrongCode);
        CPPUNIT_TEST(AuthTokenCode);
        CPPUNIT_TEST(AuthTokenCodePKCENoVerifier);
        CPPUNIT_TEST(AuthTokenCodePKCEWrongVerifier);
        CPPUNIT_TEST(AuthTokenCodePKCE);
        CPPUNIT_TEST(AuthTokenCodePKCE512);
        CPPUNIT_TEST(AuthTokenWrongPassword);
        CPPUNIT_TEST(AuthTokenPassword);
        CPPUNIT_TEST(AuthTokenWrongRefresh);
        CPPUNIT_TEST(AuthTokenExpiredRefresh);
        CPPUNIT_TEST(AuthTokenRefresh);
        CPPUNIT_TEST(AuthTokenRefreshSameRefresh);
        CPPUNIT_TEST(AuthTokenRefreshSameRefreshReuse);
        CPPUNIT_TEST(AuthTokenOptions);
        CPPUNIT_TEST(AuthTokenPasswordNoRefresh);
        CPPUNIT_TEST(AuthTokenPasswordWithRefresh);
        CPPUNIT_TEST(AuthTokenRefreshSubsetScope);
        CPPUNIT_TEST(AuthTokenCodePKCESubsetScope);
        CPPUNIT_TEST(AuthTokenPKCEBasicNoSecret);
        CPPUNIT_TEST(AuthTokenPKCENoSecret);
        CPPUNIT_TEST(AuthTokenPasswordBasicNoSecret);
        CPPUNIT_TEST(AuthTokenPasswordNoSecret);
        CPPUNIT_TEST(AuthTokenRefreshBasicNoSecret);
        CPPUNIT_TEST(AuthTokenRefreshNoSecret);
        CPPUNIT_TEST(AuthTokenPKCERefreshNoSecret);
        CPPUNIT_TEST(AuthTokenPKCERefreshBasicNoSecret);

        CPPUNIT_TEST_SUITE_END();


      public:
        AuthHandlerTestToken();
        ~AuthHandlerTestToken();

        // using API

        void AuthTokenNotAuthorizedAPINoClientId();      //!< client_id is missing
        void AuthTokenNotAuthorizedAPIWrongClientId();   //!< client_id is wrong
        void AuthTokenNotAuthorizedAPINoClientSecret();  //!< client_secret is missing

        // using HTTP

        void AuthTokenOptions();                       //!< OPTIONS method
        void AuthTokenNotAuthorized();                 //!< client_id is missing or wrong
        void AuthTokenWrongGrantEmpty();               //!< grant_type is empty
        void AuthTokenWrongGrantWrong();               //!< grant_type is wrong
        void AuthTokenWrongGrantPassword();            //!< for password grant password is missing
        void AuthTokenWrongGrantPasswordNoScope();     //!< for password grant password is missing
        void AuthTokenWrongGrantPasswordNoUsername();  //!< for password grant password is missing
        void AuthTokenWrongGrantCode();                //!< for authorization_code grant code is missing
        void AuthTokenWrongGrantCodeNoUri();           //!< for authorization_code grant redirect_uri is missing
        void AuthTokenWrongGrantRefreshToken();        //!< for refresh_token token itself is missing
        void AuthTokenExpiredCode();                   //!< authorization code grant, expired code
        void AuthTokenWrongCode();                     //!< authorization code grant, wrong code
        void AuthTokenCode();                          //!< happy path
        void AuthTokenCodePKCENoVerifier();            //!< provide no code verifier
        void AuthTokenCodePKCEWrongVerifier();         //!< provide invalid code verifier
        void AuthTokenCodePKCE();                      //!< happy path
        void AuthTokenCodePKCE512();                   //!< happy path with sha512
        void AuthTokenWrongPassword();                 //!< password grant, wrong password
        void AuthTokenPassword();                      //!< happy path
        void AuthTokenWrongRefresh();                  //!< refresh token is wrong
        void AuthTokenExpiredRefresh();                //!< refresh token has expired
        void AuthTokenRefresh();                       //!< refresh token happy path
        void AuthTokenRefreshSameRefresh();            //!< refresh token happy path, configured to rotate refresh token
        void AuthTokenRefreshSameRefreshReuse();       //!< refresh token happy path, configured to keep refresh token (no rotate)
        void AuthTokenPasswordNoRefresh();             //!< without offline_access scope should not be refresh token
        void AuthTokenPasswordWithRefresh();           //!< without offline_access scope should not be refresh token
        void AuthTokenRefreshSubsetScope();            //!< refresh token happy path
        void AuthTokenCodePKCESubsetScope();           //!< happy path
        void AuthTokenPKCEBasicNoSecret();             //!< success, client_secret is not needed
        void AuthTokenPKCENoSecret();                  //!< success, client_secret is not needed
        void AuthTokenPasswordBasicNoSecret();         //!< fail, privileged operation
        void AuthTokenPasswordNoSecret();              //!< fail, privileged operation
        void AuthTokenRefreshBasicNoSecret();          //!< fail, privileged operation, if refresh token obtained not via PKCE, secret is required
        void AuthTokenRefreshNoSecret();               //!< fail, privileged operation, if refresh token obtained not via PKCE, secret is required
        void AuthTokenPKCERefreshNoSecret();           //!< success, refresh token obtained via PKCE can be used w/o client-secret
        void AuthTokenPKCERefreshBasicNoSecret();      //!< success, refresh token obtained via PKCE can be used w/o client-secret
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
