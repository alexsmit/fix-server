#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <vx/JWT.h>
#include <vx/PtreeUtil.h>
#include "../src/TokenProviderMySQL.h"
#include "../src/TokenProviderDB.h"
#include "../src/TokenServerContext.h"

using namespace std;

#define TTL 2

template <class T>
class TokenProviderBaseTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TokenProviderBaseTest<T>);

  CPPUNIT_TEST(testCreateToken);
  CPPUNIT_TEST(testCreateTokenWithTTL);
  CPPUNIT_TEST(testHit);
  CPPUNIT_TEST(testValidateToken);
  CPPUNIT_TEST(testValidateNonce);
  CPPUNIT_TEST(testClean);
  CPPUNIT_TEST(testSetTTL);
  CPPUNIT_TEST(testSetRefreshTTL);
  CPPUNIT_TEST(testNonce);

  CPPUNIT_TEST_SUITE_END();

private:
  shared_ptr<DataFactory> factory;

public:
  TokenProviderBaseTest() {}
  ~TokenProviderBaseTest() {}

  // POST /user/{username}
  void setUp() override {
    factory.reset(new DataFactory());
  }

  void tearDown() override {
    factory.reset();
  }

  void testCreateToken() {
    const char *idu = "_bad_nonce";
    auto token_provider = factory->Connect<T>();
    token_provider->SetTTL(TTL);
    // create new token
    auto id_db = token_provider->CreateToken("_user", "_agent", "");
    CPPUNIT_ASSERT_MESSAGE("id should be not empty", !id_db.refresh_token.empty());
    // refresh token (should still exists in db)
    auto id_db2 = token_provider->CreateToken("_user", "_agent", id_db.nonce);
    CPPUNIT_ASSERT_MESSAGE("id should be not empty", !id_db2.refresh_token.empty());
    CPPUNIT_ASSERT_MESSAGE("new refresh token should be different", id_db.refresh_token != id_db2.refresh_token);
    CPPUNIT_ASSERT_MESSAGE("new nonce should be different", id_db.nonce != id_db2.nonce);
    // try to refresh non existing token
    auto id_db3 = token_provider->CreateToken("_user", "_agent", idu);
    CPPUNIT_ASSERT_MESSAGE("id should be not empty", !id_db3.refresh_token.empty());
    CPPUNIT_ASSERT_MESSAGE("refresh token be different", id_db3.refresh_token != idu);
  }

  /// create token and set explicit TTL
  void testCreateTokenWithTTL() {
    auto token_provider = factory->Connect<T>();
    token_provider->SetTTL(TTL+2);
    auto id_db = token_provider->CreateToken("_user", "_agent", "", 1);
    auto cur_time_exp = ::time(NULL)+1;
    CPPUNIT_ASSERT_MESSAGE("should have expired property", id_db.expried <= cur_time_exp && id_db.expried != 0);

    TokenInfo tinfo = {.refresh_token = id_db.refresh_token, .nonce = id_db.nonce};
    auto result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should validate user", result);

    // wait
    cout << "W" << flush;
    std::this_thread::sleep_for(std::chrono::seconds(2));

    tinfo = {.refresh_token = id_db.refresh_token, .nonce = id_db.nonce};
    result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("db should not validate user", !result);
  }

  // Hit method records number of invalid attempts to refresh a token (for example wrong pin flow)
  void testHit() {
    auto token_provider = factory->Connect<T>();
    token_provider->SetTTL(TTL + 3);
    auto id_db = token_provider->CreateToken("_user", "_agent", "");

    TokenInfo tinfo = {.refresh_token = id_db.refresh_token, .nonce = id_db.nonce};
    auto result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should validate user", result);

    // record bad hit 3 times
    // 1
    token_provider->Hit(id_db.nonce, 3);
    result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should validate user", result);
    // 2
    token_provider->Hit(id_db.nonce, 3);
    result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should validate user", result);
    // 3 final
    token_provider->Hit(id_db.nonce, 3);
    result = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should not validate user (token deleted after 3 hits)", !result);
  }

  void testValidateNonce() {
    auto token_provider_db = factory->Connect<T>();
    token_provider_db->SetTTL(TTL);
    auto id_db = token_provider_db->CreateToken("_user", "_agent", "");
    TokenInfo tinfo = {.refresh_token = "", .nonce = id_db.nonce};
    auto result_db = token_provider_db->ValidateToken("_user", tinfo, true);
    CPPUNIT_ASSERT_MESSAGE("should validate user with nonce", result_db);

    tinfo = {.refresh_token = "", .nonce = "_bad_nonce"};
    result_db = token_provider_db->ValidateToken("_user", tinfo, true);
    CPPUNIT_ASSERT_MESSAGE("should not validate user with nonce", !result_db);
  }

  void testValidateToken() {
    auto token_provider = factory->Connect<T>();
    token_provider->SetTTL(1000);
    token_provider->SetRefreshTTL(TTL);
    auto id_db = token_provider->CreateToken("_user", "_agent", "");
    TokenInfo tinfo = {.refresh_token = id_db.refresh_token, .nonce = id_db.nonce};
    auto result_db = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should validate user", result_db);

    tinfo = {.refresh_token = id_db.refresh_token, .nonce = "_invalid_nonce"};
    result_db = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should not validate user", !result_db);

    tinfo = {.refresh_token = "_invalid_token", .nonce = "_invalid_nonce"};
    result_db = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("should not validate user", !result_db);

    // wait
    cout << "W" << flush;
    std::this_thread::sleep_for(std::chrono::seconds(TTL+1));

    tinfo = {.refresh_token = id_db.refresh_token, .nonce = id_db.nonce};
    result_db = token_provider->ValidateToken("_user", tinfo);
    CPPUNIT_ASSERT_MESSAGE("db should not validate user", !result_db);
  }

  void testClean() {
    auto token_provider = factory->Connect<T>();
    token_provider->SetTTL(1000);
    token_provider->SetRefreshTTL(TTL);
    auto id_db = token_provider->CreateToken("_user", "_agent", "");

    // wait and clean
    cout << "W" << flush;
    std::this_thread::sleep_for(std::chrono::seconds(TTL+1));
    token_provider->Clean();

    auto id_db2 = token_provider->CreateToken("_user", "_agent", id_db.refresh_token);
    CPPUNIT_ASSERT_MESSAGE("id should be not empty", !id_db2.refresh_token.empty());
    CPPUNIT_ASSERT_MESSAGE("last refresh token id should not be the same", id_db.refresh_token != id_db2.refresh_token);
  }

  void testSetTTL() {
    auto token_provider = factory->Connect<T>();
    CPPUNIT_ASSERT_MESSAGE("should have default TTL", token_provider->GetEffectiveTTL() == 300);
    token_provider->SetTTL(TTL);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("should have TTL", (unsigned long)TTL, token_provider->GetEffectiveTTL());
  }

  void testSetRefreshTTL() {
    auto token_provider = factory->Connect<T>();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("should have default refresh TTL", 
      (unsigned long)600,
      token_provider->GetEffectiveRefreshTTL());
    token_provider->SetRefreshTTL(TTL);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("should have refresh TTL", (unsigned long)TTL, token_provider->GetEffectiveRefreshTTL());
  }

  void testNonce() {
    auto token_provider_db = factory->Connect<T>();
    auto nonce = token_provider_db->CreateNonce();
    CPPUNIT_ASSERT_MESSAGE("nonce should have 12/3*4 chars", nonce.length() == 16);
  }
};
