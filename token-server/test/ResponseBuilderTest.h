#pragma once
/*
 * Copyright 2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// Token API tests for /oauth/token endoint. This is an extention over the resource owner flow.
      class ResponseBuilderTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ResponseBuilderTest);

        CPPUNIT_TEST(testInvalidGrant);
        CPPUNIT_TEST(testInvalidBearerPin);
        CPPUNIT_TEST(testInvalidBearerValidate);
        CPPUNIT_TEST(testWithPinAuthAudMissiing);
        CPPUNIT_TEST(testWithPinAuthAudWrong);
        CPPUNIT_TEST(testWithPinAuthNonceMissiing);
        CPPUNIT_TEST(testValidateNonceMissiing);
        CPPUNIT_TEST(testValidateNoUserid);
        CPPUNIT_TEST(testValidateExpired);
        CPPUNIT_TEST(testValidate);
        CPPUNIT_TEST(testAuthenticateScope);
        CPPUNIT_TEST(testAuthenticateNoRoles);
        CPPUNIT_TEST(testCreateClientCredentials);
        CPPUNIT_TEST(testCreateClientCredentialsFail);
        CPPUNIT_TEST(testCreateClientCredentialsInvalid);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<TokenServerContext> context;
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<TokenProvider> token_provider;

      public:
        ResponseBuilderTest();
        ~ResponseBuilderTest();

        /// @brief  create token
        /// @param nonce  nonce value, if empty will not be added to the token
        /// @param addUserid if true userid claim will be added to the token
        /// @param Audience if "-" uses default audience, if empty - no audience will be addee, non-empty will be used from this parameter
        /// @return
        string getToken(string nonce,
                        bool addUserid = false,
                        const string& Audience = "-");

        void setUp() override;                      //!< cppunit setup
        void tearDown() override;                   //!< ccpunit teardown
        void testInvalidGrant();                    //!< no grant passed
        void testInvalidBearerPin();                //!< bearer header is wrong for grant "pin"
        void testInvalidBearerValidate();           //!< bearer header is wrong for grant "validate"
        void testWithPinAuthAudMissiing();          //!< auth with pin, aud is not present in a token
        void testWithPinAuthAudWrong();             //!< auth with pin, aud is present in a token, but invalid
        void testWithPinAuthNonceMissiing();        //!< auth with pin, nonce is not present in a token
        void testValidateNonceMissiing();           //!< run validate request, nonce is missing
        void testValidateNoUserid();                //!< run validate request, userid is missing
        void testValidateExpired();                 //!< run validate request, expired token
        void testValidate();                        //!< run validate request
        void testAuthenticateScope();               //!< authenticate with scope
        void testAuthenticateNoRoles();             //!< fail to authenticate if user has no roles
        void testCreateClientCredentials();         //!< ok, create and validate token
        void testCreateClientCredentialsFail();     //!< fail, not exising client/secret
        void testCreateClientCredentialsInvalid();  //!< fail, invalid client/secret
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
