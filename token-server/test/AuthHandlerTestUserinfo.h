#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /v1/userinfo
      class AuthHandlerTestUserinfo : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestUserinfo);

        CPPUNIT_TEST(UserInfoNoAuth);
        CPPUNIT_TEST(UserInfoBadJWT);
        CPPUNIT_TEST(UserInfo);
        CPPUNIT_TEST(UserInfoPhone);
        CPPUNIT_TEST(UserInfoProfile);
        CPPUNIT_TEST(UserInfoEmail);
        CPPUNIT_TEST(UserInfoAddress);
        CPPUNIT_TEST(UserInfoRoles);
        CPPUNIT_TEST(UserInfoNonce);
        CPPUNIT_TEST(UserInfoProp);
        CPPUNIT_TEST(UserInfoOptions);

        CPPUNIT_TEST_SUITE_END();


      public:
        AuthHandlerTestUserinfo();
        ~AuthHandlerTestUserinfo();

        void UserInfoNoAuth();   //!< userinfo w/o bearer token
        void UserInfoBadJWT();   //!< userinfo with wrong bearer token
        void UserInfo();         //!< get openid
        void UserInfoPhone();    //!< get phone
        void UserInfoProfile();  //!< get profile
        void UserInfoEmail();    //!< get email
        void UserInfoAddress();  //!< get address
        void UserInfoRoles();    //!< get roles
        void UserInfoNonce();    //!< get nonce (skipped)
        void UserInfoProp();     //!< add existing prop, should be configured as valid scope in config.json
        void UserInfoOptions();  //!< check OPTIONS method
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
