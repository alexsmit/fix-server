/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "OAuthAllHandlersTest.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

/// fixture class to validate presense for all OAuth2 endpoints. NOTE: a functionality of those endpoints are not considered
/// in those tests.
class OAuthRequestFix {
public:
  shared_ptr<TokenServerContext> context;  //!< current server context, set by the constructor
  unsigned int code;                       //!< result HTTP code
  string url;                              //!< url to run request
  Request::RequestType type;               //!< current request type

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param url current URL
  /// @param type GET/POST/etc
  OAuthRequestFix(shared_ptr<TokenServerContext> context, string url, Request::RequestType type)
      : context(context), url(url), type(type) {
  }

  /// run request, parse result
  void Handle() {
    stringstream pt_out, sout;
    stringstream sin;

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = VxServer::Request::request_rmap[type];
    env.Handle();

    BeastParser<be::string_body> parser(sout);
    parser.Parse();
    code = parser.resp.result_int();
  }
};

OAuthAllHandlersTest::OAuthAllHandlersTest() {}

OAuthAllHandlersTest::~OAuthAllHandlersTest() {}

void OAuthAllHandlersTest::setUp() {
  AppConfig::resetInstance();

  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());

  context->section->setParam("server_id", "some-id");
}

void OAuthAllHandlersTest::tearDown() {}

void OAuthAllHandlersTest::testTokenServerContext() {
  CPPUNIT_ASSERT_EQUAL_MESSAGE("session token expiration parameter", (unsigned long)60, context->ttl_session_token);
}

void OAuthAllHandlersTest::testAuthn() {
  OAuthRequestFix fix(context, "/api/v1/authn", VxServer::Request::RequestType::POST);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testAuthorize() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/authorize", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);

  OAuthRequestFix fix2(context, "/oauth2/some-id/v1/authorize", VxServer::Request::RequestType::POST);
  fix2.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix2.code != 404 && fix2.code != 500);
}

void OAuthAllHandlersTest::testToken() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/token", VxServer::Request::RequestType::POST);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testIntrospect() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/introspect", VxServer::Request::RequestType::POST);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testRevoke() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/revoke", VxServer::Request::RequestType::POST);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testLogout() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/logout", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testKeys() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/keys", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testUserinfo() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/userinfo", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

void OAuthAllHandlersTest::testOpenid() {
  OAuthRequestFix fix(context, "/oauth2/some-id/.well-known/openid-configuration", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("/oauth2/some-id/.well-known/openid-configuration", fix.code != 404 && fix.code != 500);

  OAuthRequestFix fix2(context, "/.well-known/openid-configuration", VxServer::Request::RequestType::GET);
  fix2.Handle();
  CPPUNIT_ASSERT_MESSAGE("/.well-known/openid-configuration not accessible via new openid", fix2.code == 500);
}

void OAuthAllHandlersTest::testAuthServer() {
  OAuthRequestFix fix(context, "/oauth2/some-id/v1/.well-known/oauth-authorization-server", VxServer::Request::RequestType::GET);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("code", fix.code != 404 && fix.code != 500);
}

CPPUNIT_TEST_SUITE_REGISTRATION(OAuthAllHandlersTest);

#endif
