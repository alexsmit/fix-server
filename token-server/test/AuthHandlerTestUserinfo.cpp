/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestUserinfo.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestUserinfo::AuthHandlerTestUserinfo() {}
AuthHandlerTestUserinfo::~AuthHandlerTestUserinfo() {}

//-----------------------------------------

/// fixture class to validate /v1/userinfo endpoint
class UserInfoRequestFix : public AuthorizeFixBase {
public:
  string url;  //!< default of specific URL, set by the constructor

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param url default of specific URL
  UserInfoRequestFix(shared_ptr<TokenServerContext> context,
                     string url = "/oauth2/default/v1/userinfo") : AuthorizeFixBase(context), url(url) {
  }

  /// run request and parse result
  void Handle(const string& method = "GET") {
    stringstream sin;
    stringstream sout;

    header.emplace("Origin", "https://jwt.io");

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = method;
    env.no_req->header = header;
    if (method != "OPTIONS") {
      env.no_req->header.emplace("Content-Type", "application/json");
    }

    env.Handle();

    ParseResult(sout);
  }
};

void AuthHandlerTestUserinfo::UserInfoNoAuth() {
  UserInfoRequestFix fix(context);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("bearer token is not provided or invalid"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestUserinfo::UserInfoBadJWT() {
  UserInfoRequestFix fix(context);
  fix.SetHeader("Authorization", "Bearer some-wrong-jwt");

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("bearer token is not provided or invalid"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestUserinfo::UserInfo() {
  string token_scope = "openid";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  auto rec = provider->LoadUser(USER_NAME);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("preferred_username", string(USER_NAME), fix.pt_body.get("preferred_username", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", rec.userid, fix.pt_body.get("sub", ""));

  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoPhone() {
  auto user = provider->LoadUser(USER_NAME);

  UserRecord rec;
  rec.username = USER_NAME;
  rec.props.emplace("phone_number", "888-012-3456");
  rec.props.emplace("phone_number_verified", "true");

  provider->SaveUser(rec);

  string token_scope = "openid phone";
  UserInfoRequestFix fix(context);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));
  fix.Handle();

  // write_json(cout, fix.pt_body);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("preferred_username", string(USER_NAME), fix.pt_body.get("preferred_username", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("phone", string("888-012-3456"), fix.pt_body.get("phone_number", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("phone verified", string("true"), fix.pt_body.get("phone_number_verified", ""));
  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoProfile() {
  auto user = provider->LoadUser(USER_NAME);

  UserRecord rec;
  rec.username = USER_NAME;
  rec.props.emplace("name", "John B Doe");
  rec.props.emplace("given_name", "John");
  rec.props.emplace("family_name", "Doe");
  rec.props.emplace("middle_name", "Bob");
  rec.props.emplace("nickname", "Jonta");
  rec.props.emplace("picture", "https://vxpro.io/jonta.png");
  rec.props.emplace("website", "https://vxpro.io/jonta.html");
  rec.props.emplace("gender", "mushroom");
  rec.props.emplace("birthdate", "2000-01-21");
  provider->SaveUser(rec);

  string token_scope = "openid profile";
  UserInfoRequestFix fix(context);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);
  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));
  fix.Handle();

  // write_json(cout, fix.pt_body);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("preferred_username", string(USER_NAME), fix.pt_body.get("preferred_username", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("name", string("John B Doe"), fix.pt_body.get("name", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("given_name", string("John"), fix.pt_body.get("given_name", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("family_name", string("Doe"), fix.pt_body.get("family_name", ""));

  CPPUNIT_ASSERT_EQUAL_MESSAGE("middle_name", string("Bob"), fix.pt_body.get("middle_name", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("nickname", string("Jonta"), fix.pt_body.get("nickname", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("picture", string("https://vxpro.io/jonta.png"), fix.pt_body.get("picture", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("website", string("https://vxpro.io/jonta.html"), fix.pt_body.get("website", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("gender", string("mushroom"), fix.pt_body.get("gender", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("birthdate", string("2000-01-21"), fix.pt_body.get("birthdate", ""));

  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoEmail() {
  UserRecord rec;
  rec.username = USER_NAME;
  rec.props.emplace("email_verified", "true");
  provider->SaveUser(rec);

  string token_scope = "openid email";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), fix.pt_body.get("email", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email_verified", string("true"), fix.pt_body.get("email_verified", ""));

  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoAddress() {
  // auto user = provider->LoadUser(USER_NAME);
  UserRecord rec;
  rec.username = USER_NAME;
  rec.props.emplace("formatted", "800 Colton st, Colton, CA, 92324");
  rec.props.emplace("street_address", "800 Colton st");
  rec.props.emplace("locality", "Colton");
  rec.props.emplace("region", "CA");
  rec.props.emplace("postal_code", "92324");
  rec.props.emplace("country", "USA");
  provider->SaveUser(rec);

  string token_scope = "openid address";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("formatted", string("800 Colton st, Colton, CA, 92324"), fix.pt_body.get("address.formatted", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("street_address", string("800 Colton st"), fix.pt_body.get("address.street_address", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("locality", string("Colton"), fix.pt_body.get("address.locality", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("region", string("CA"), fix.pt_body.get("address.region", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("postal_code", string("92324"), fix.pt_body.get("address.postal_code", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("country", string("USA"), fix.pt_body.get("address.country", ""));

  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoRoles() {
  UserRecord rec;
  rec.username = USER_NAME;
  rec.roles = {"user", "admin", "other"};
  provider->SaveUser(rec);

  string token_scope = "openid roles";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  auto vroles = StringUtil::split(fix.pt_body.get("roles", ""), ' ');

  CPPUNIT_ASSERT_EQUAL_MESSAGE("has roles", (size_t)3, vroles.size());
  CPPUNIT_ASSERT_MESSAGE("has rolew", vectorContains(vroles, rec.roles, true));

  fix.assert_cors();
}

void AuthHandlerTestUserinfo::UserInfoNonce() {
  UserRecord rec;
  rec.username = USER_NAME;
  rec.roles = {"user", "admin", "other"};
  provider->SaveUser(rec);

  string token_scope = "openid nonce";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("has onlyt 2 props returned", (size_t)2, fix.pt_body.size());
  CPPUNIT_ASSERT_MESSAGE("has sub", !fix.pt_body.get("sub","").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("has preferred_username", string(USER_NAME), fix.pt_body.get("preferred_username",""));
}

void AuthHandlerTestUserinfo::UserInfoProp() {
  UserRecord rec;
  rec.username = USER_NAME;
  rec.roles = {"user", "admin", "other"};
  rec.props.emplace("foo","bar");
  provider->SaveUser(rec);

  string token_scope = "openid foo";

  UserInfoRequestFix fix(context);

  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  fix.SetHeader("Authorization", string("Bearer ") + ops.token_result.get("access_token", ""));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("has onlyt 2 props returned", (size_t)3, fix.pt_body.size());
  CPPUNIT_ASSERT_MESSAGE("has sub", !fix.pt_body.get("sub","").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("has preferred_username", string(USER_NAME), fix.pt_body.get("preferred_username",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("has foo", string("bar"), fix.pt_body.get("foo",""));
}


void AuthHandlerTestUserinfo::UserInfoOptions() {
  UserInfoRequestFix fix(context);
  fix.Handle("OPTIONS");

  fix.assert_cors();
  fix.assert_options("GET,OPTIONS");
  fix.assert_no_cache();
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestUserinfo);

#endif
