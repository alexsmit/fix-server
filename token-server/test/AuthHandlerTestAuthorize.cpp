/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <VxMessage.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestAuthorize.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <vx/StringUtil.h>
#include <VxMessage.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestAuthorize::AuthHandlerTestAuthorize() {}
AuthHandlerTestAuthorize::~AuthHandlerTestAuthorize() {}

//-----------------------------------------

/// fixture class to test /authorize endpoint
class AuthorizeRequestFix : public AuthorizeFixBase {
public:
  AuthorizeRequest& req;  //!< request object, set by the constructor
  string url;             //!< url, set by the constructor

  /// create fixture class
  /// @param context current server context, object created by setup should be used here
  /// @param req request object, set by the test
  /// @param url default url unless set by the test
  AuthorizeRequestFix(shared_ptr<TokenServerContext> context,
                      AuthorizeRequest& req,
                      string url = "/oauth2/default/v1/authorize") : AuthorizeFixBase(context), req(req), url(url) {
  }

  /// run request and parse result
  void Handle(const string& method = "GET") {
    string query;
    query << req;

    string slen = boost::lexical_cast<string>(query.size());

    stringstream sin;
    stringstream sout;

    if (method == "POST") {
      sin.str(query);
    }

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = method;
    env.no_req->header = header;
    if (method == "GET")
      env.no_req->query_string << req;
    else {
      env.no_req->header.emplace("Content-Type", "application/x-www-form-urlencoded");
      env.no_req->header.emplace("Content-Length", slen);
    }
    env.Handle();
    ParseResult(sout);
  }

  /// validate that session cookie is removed
  void assert_unset_cookie() {
    auto coo = pt_header.get("Set-Cookie", "");
    CPPUNIT_ASSERT_MESSAGE("Should have set-cookie header", !coo.empty());
    auto vcoo = StringUtil::split(coo, '=');
    CPPUNIT_ASSERT_EQUAL_MESSAGE("cookie name", string(SESSION_COOKIE), vcoo[0]);
    set<string> cookie_items;
    boost::split(cookie_items, coo, boost::is_any_of(";"));
    CPPUNIT_ASSERT_MESSAGE("Should have max-age=0", cookie_items.find("max-age=0") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have HttpOnly", cookie_items.find("HttpOnly") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have Path=/", cookie_items.find("Path=/") != cookie_items.end());
    CPPUNIT_ASSERT_MESSAGE("Should have SameSite=None", cookie_items.find("SameSite=None") != cookie_items.end());
  }
};

void AuthHandlerTestAuthorize::AuthorizeConfig() {
  CPPUNIT_ASSERT_MESSAGE("server_id", !context->section->getParam("server_id").empty());

  auto credirect = context->section->getTree("").get_child_optional("callback_uri");
  CPPUNIT_ASSERT_MESSAGE("callback_uri", !!credirect);

  auto svc = context->getService("token");
  TokenProvider* token_svc = dynamic_cast<TokenProvider*>(svc.get());
  if (token_svc == NULL) throw runtime_error("unable to get token_provider");
}

void AuthHandlerTestAuthorize::AuthorizeWrongClientId() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "wrong";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeMissingClientId() {
  stringstream sout;
  AuthorizeRequest req;
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeNotConfiguredCallback() {
  context->section->deleteParam("callback_uri");
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "wrong_url";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 500", (unsigned)500, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::server_error, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("callback_uri section is missing in the config"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeWrongCallback() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "wrong_url";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid callback_uri parameter"), fix.pt_body.get("error_description", ""));
}

// void AuthHandlerTestAuthorize::AuthorizeMissingNonce() {
//   stringstream sout;
//   AuthorizeRequest req;
//   req.client_id = "aaa";
//   req.redirect_uri = "http://localhost:8080/authorize_callback";
//   AuthorizeRequestFix fix(context, req);
//   fix.Handle();

//   CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
//   CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
//   CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("nonce is missing"), fix.pt_params.get("error_description", ""));
// }

//--------------------------------------
//--- response_type
//--------------------------------------

void AuthHandlerTestAuthorize::AuthorizeMissingResponseType() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("response_type is missing"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseType() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "unknown";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";
  req.sessionToken = "some_token";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_type"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeAllResponseType() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code token id_token";
  req.response_mode = "fragment";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";
  req.sessionToken = "some_token";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  auto desc = fix.pt_params.get("error_description", "");
  CPPUNIT_ASSERT_MESSAGE("error_description", string("invalid response_type") != desc);
  CPPUNIT_ASSERT_MESSAGE("error_description", string("invalid response_mode") != desc);
}

void AuthHandlerTestAuthorize::AuthorizeAllAndInvalidResponseType() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code token id_token wrong_type";
  req.response_mode = "fragment";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";
  req.sessionToken = "some_token";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  auto desc = fix.pt_params.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_type"), desc);
}

//--------------------------------------
//--- response_mode
//--------------------------------------

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseModeTokenQuery() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "token";
  req.response_mode = "query";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_mode"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseModeIdTokenQuery() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "id_token";
  req.response_mode = "query";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_mode"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseModeWrongValue() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.response_mode = "some_unknown";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_mode"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseModeOktaNoConfig() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.response_mode = "okta_post_message";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid response_mode"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidResponseModeOktaOk() {
  context->setParam("token_provider.compatibility", "OKTA");
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.response_mode = "okta_post_message";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_MESSAGE("error_description", string("invalid response_mode") != fix.pt_params.get("error_description", ""));
}

//--------------------------------------
//--- scope
//--------------------------------------

void AuthHandlerTestAuthorize::AuthorizeMissingScope() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("scope is missing"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeInvalidScope() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "some_scope";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_scope"), fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("scope is not supported"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeOkNoOpenIdScopes() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "email phone";  // good scopes but missing openid
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_scope"), fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("scope is not supported"), fix.pt_params.get("error_description", ""));
}

// will fail on the next validation, but not with the scope error
void AuthHandlerTestAuthorize::AuthorizeOkAndInvalidScopes() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid some_scope";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_MESSAGE("error", string("invalid_scope") != fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_MESSAGE("error_description", string("scope is not supported") != fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeAllOpenIDScopes() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid profile email address phone some_scope offline_access roles";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_MESSAGE("error", string("invalid_scope") != fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_MESSAGE("error_description", string("scope is not supported") != fix.pt_params.get("error_description", ""));
}

//--------------------------------------
//--- state
//--------------------------------------

void AuthHandlerTestAuthorize::AuthorizeMissingState() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("state is missing"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeWrongChallenge() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.code_challenge_method = "invalid_method";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("code_challenge should be S256"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizeNoChallengeMethod() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.code_challenge = "some_challenge";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("code_challenge_method is not provided"), fix.pt_params.get("error_description", ""));
}


void AuthHandlerTestAuthorize::AuthorizePromptInvalid() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "some_prompt";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or unsupported prompt value"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizePromptLogin() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "login";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or unsupported prompt value"), fix.pt_params.get("error_description", ""));
}

void AuthHandlerTestAuthorize::AuthorizePromptConsent() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "consent";
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or unsupported prompt value"), fix.pt_params.get("error_description", ""));
}

/*
https://127.0.0.1:8482/oauth2/default/v1/authorize?client_id=aaa&response_type=code&scope=openid&redirect_uri=http://localhost:8080/authorize_callback&state=some-state&nonce=some-nonce&sessionToken=6hvKfhSsPkNXc7ymjmIay+rQNxPAPg5c
*/
void AuthHandlerTestAuthorize::AuthorizeNoSessionToken() {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";  // currently only "none" is supported
  AuthorizeRequestFix fix(context, req);
  fix.Handle();
  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", !location.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("login_required"), fix.pt_params.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("prompt set to none, login required"), fix.pt_params.get("error_description", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), fix.pt_params.get("state", ""));
}

shared_ptr<AuthorizeFixBase> AuthHandlerTestAuthorize::AuthorizeWithSessionTokenBase(
    const string& method,
    const string& response_mode,
    const string& response_type,
    unsigned int response_code,
    const string& new_response_mode,
    map<string,string> headers) {
  stringstream sout;
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = response_type;
  req.response_mode = response_mode;
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";  // currently only "none" is supported

  auto token = token_provider->CreateToken(USER_NAME, "authn", "", 300);
  req.sessionToken = token.nonce;


  shared_ptr<AuthorizeFixBase> pfix(new AuthorizeRequestFix(context, req));
  AuthorizeRequestFix& fix = static_cast<AuthorizeRequestFix&>(*pfix);

  for(auto &kv: headers) {
    fix.header.emplace(kv.first,kv.second);
  }

  fix.Handle(method);

  // cout << ">>> params:";
  // write_json(cout, fix.pt_params);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", response_code, fix.code);

  if (response_code == 302) {
    int expected_cnt = 1;  // state
    auto response_types = StringUtil::split(response_type, ' ');
    map<string, string> expected_keys = {{"state", "state"}};

    for (auto& r : response_types) {
      // expected_keys.insert(r);
      if (r == "token") {
        expected_cnt += 2;  // token_type, access_token
        expected_keys.emplace("token_type", "Bearer");
        expected_keys.emplace("access_token", "");
      }
      else if (r == "code") {
        expected_cnt++;  // code
        expected_keys.emplace("code", "");
      }
      else if (r == "id_token") {
        expected_cnt++;  // code
        expected_keys.emplace("id_token", "");
      }
    }

    auto location = fix.pt_header.get("Location", "");
    CPPUNIT_ASSERT_MESSAGE("Should have Location header", !location.empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("number of parameters", (size_t)expected_cnt, fix.pt_params.size());

    for (auto& kv : expected_keys) {
      if (kv.second.empty())
        CPPUNIT_ASSERT_MESSAGE(kv.first, !fix.pt_params.get(kv.first, "").empty());
      else
        CPPUNIT_ASSERT_EQUAL_MESSAGE(kv.first, kv.second, fix.pt_params.get(kv.first, ""));
    }
  }

  // session token should be deleted
  auto user_token = token_provider->GetTokenByNonce(token.nonce);
  CPPUNIT_ASSERT_MESSAGE("session token should be deleted", user_token.nonce.empty());

  auto cur_response_mode = (new_response_mode.empty()) ? response_mode : new_response_mode;

  if (cur_response_mode == "query")
    CPPUNIT_ASSERT_MESSAGE("response in query", fix.in_query);
  else if (cur_response_mode == "fragment")
    CPPUNIT_ASSERT_MESSAGE("response in fragment", fix.in_fragment);

  return pfix;
}

void AuthHandlerTestAuthorize::AuthorizeWithSessionTokenGet() {
  AuthorizeWithSessionTokenBase("GET");
}

void AuthHandlerTestAuthorize::AuthorizeWithSessionTokenPost() {
  AuthorizeWithSessionTokenBase("POST");
}

void AuthHandlerTestAuthorize::AuthorizeAPIWithSessionToken() {
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";  // currently only "none" is supported

  auto token = token_provider->CreateToken(USER_NAME, "authn", "", 300);
  req.sessionToken = token.nonce;

  AuthorizeFlow flow(*context, req, "");
  flow.Execute();
  ptree result = flow.GetResult();
  auto head = flow.GetResultHeader();

  auto coo = head.find("Set-Cookie");
  CPPUNIT_ASSERT_MESSAGE("Should have set-cookie header", coo != head.end());
  auto vcoo = StringUtil::split(coo->second, '=');
  CPPUNIT_ASSERT_EQUAL_MESSAGE("cookie name", string(SESSION_COOKIE), vcoo[0]);

  // sid=1029C8B0nzxRjWFh9-mbb4kMw;Version=1;Path=/;Secure;HttpOnly;SameSite=None

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, (unsigned)flow.GetResultCode());
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", head.find("location") != head.end());
  CPPUNIT_ASSERT_MESSAGE("code", !result.get("code", "").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), result.get("state", ""));

  auto user_token = token_provider->GetTokenByNonce(token.nonce);
  CPPUNIT_ASSERT_MESSAGE("session token should be deleted", user_token.nonce.empty());
}

void AuthHandlerTestAuthorize::AuthorizeSessionCookie() {
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";  // currently only "none" is supported

  auto id_token = token_provider->CreateToken(USER_NAME, "id", "", 300);
  auto access_token = token_provider->CreateToken(USER_NAME, "access", "", 300);

  auto cookie_token = token_provider->CreateTokenMeta(USER_NAME, "cookie", "", 300, {{"id_token", id_token.nonce}, {"access_token", access_token.nonce}});

  AuthorizeRequestFix fix(context, req);

  auto cookie = (boost::format("%s=%s") % SESSION_COOKIE % SimpleWeb::Percent::encode(cookie_token.nonce)).str();
  fix.SetHeader("Cookie", cookie);
  fix.Handle();

  ptree result = fix.pt_body;
  auto head = fix.pt_header;

  auto coo = head.get("Set-Cookie", "");
  CPPUNIT_ASSERT_MESSAGE("Should have set-cookie header", !coo.empty());
  auto vcoo = StringUtil::split(coo, '=');
  CPPUNIT_ASSERT_EQUAL_MESSAGE("cookie name", string(SESSION_COOKIE), vcoo[0]);

  // sid=1029C8B0nzxRjWFh9-mbb4kMw;Version=1;Path=/;Secure;HttpOnly;SameSite=None

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, (unsigned)fix.code);

  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", !location.empty());
  auto vlocation = StringUtil::split(location, "?");
  auto qmap = SimpleWeb::QueryString::parse(vlocation[1]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Location header", req.redirect_uri, vlocation[0]);

  CPPUNIT_ASSERT_MESSAGE("code", !qmap.find("code")->second.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), qmap.find("state")->second);

  cookie_token = token_provider->GetTokenByNonce(cookie_token.nonce);
  CPPUNIT_ASSERT_MESSAGE("sample session cookie token should be deleted", cookie_token.nonce.empty());
  id_token = token_provider->GetTokenByNonce(id_token.nonce);
  CPPUNIT_ASSERT_MESSAGE("sample session cookie token should be deleted", id_token.nonce.empty());
  access_token = token_provider->GetTokenByNonce(access_token.nonce);
  CPPUNIT_ASSERT_MESSAGE("sample session cookie token should be deleted", access_token.nonce.empty());
}

void AuthHandlerTestAuthorize::AuthorizeWrongSessionCookie() {
  AuthorizeRequest req;
  req.client_id = "aaa";
  req.redirect_uri = "http://localhost:8080/authorize_callback";
  req.nonce = "nonce";
  req.response_type = "code";
  req.scope = "openid";
  req.state = "state";
  req.prompt = "none";  // currently only "none" is supported

  AuthorizeRequestFix fix(context, req);

  auto cookie = (boost::format("%s=%s") % SESSION_COOKIE % "somecookie").str();
  fix.SetHeader("Cookie", cookie);
  fix.Handle();

  ptree result = fix.pt_body;
  auto head = fix.pt_header;

  fix.assert_unset_cookie();
  // auto coo = head.get("Set-Cookie", "");
  // CPPUNIT_ASSERT_MESSAGE("Should have set-cookie header", !coo.empty());
  // auto vcoo = StringUtil::split(coo, '=');
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("cookie name", string(SESSION_COOKIE), vcoo[0]);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 302", (unsigned)302, (unsigned)fix.code);

  auto location = fix.pt_header.get("Location", "");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", !location.empty());
  auto vlocation = StringUtil::split(location, "?");
  auto qmap = SimpleWeb::QueryString::parse(vlocation[1]);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have Location header", req.redirect_uri, vlocation[0]);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("login_required"), qmap.find("error")->second);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("prompt set to none, login required"), qmap.find("error_description")->second);
}

//--------------------------------------
//--- fragment, id_token, token
//--------------------------------------

void AuthHandlerTestAuthorize::AuthorizeFragmentCode() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "fragment");
}

void AuthHandlerTestAuthorize::AuthorizeFragmentIdToken() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "fragment", "id_token");
}

void AuthHandlerTestAuthorize::AuthorizeFragmentToken() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "fragment", "token");
}

void AuthHandlerTestAuthorize::AuthorizeFragmentHybrid() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "fragment", "code token id_token");
}

void AuthHandlerTestAuthorize::AuthorizeFragmentNoResponseMode() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "", "token id_token",302, "fragment");
}

void AuthHandlerTestAuthorize::AuthorizeFormPost() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "form_post", "code", 200);

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("code", pfix->body.find("<input type=\"hidden\" name=\"code\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no id_token", pfix->body.find("<input type=\"hidden\" name=\"id_token\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("no access_token", pfix->body.find("<input type=\"hidden\" name=\"access_token\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("callback", pfix->body.find("<form method=\"post\" action=\"http://localhost:8080/authorize_callback\">") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find("<input type=\"hidden\" name=\"state\" value=\"state\"") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeFormPostIdToken() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "form_post", "id_token", 200);

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no code", pfix->body.find("<input type=\"hidden\" name=\"code\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("id_token", pfix->body.find("<input type=\"hidden\" name=\"id_token\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no access_token", pfix->body.find("<input type=\"hidden\" name=\"access_token\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("callback", pfix->body.find("<form method=\"post\" action=\"http://localhost:8080/authorize_callback\">") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find("<input type=\"hidden\" name=\"state\" value=\"state\"") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeFormPostToken() {
  auto pfix = AuthorizeWithSessionTokenBase("GET", "form_post", "token", 200);

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no code", pfix->body.find("<input type=\"hidden\" name=\"code\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("no id_token", pfix->body.find("<input type=\"hidden\" name=\"id_token\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("access_token", pfix->body.find("<input type=\"hidden\" name=\"access_token\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("callback", pfix->body.find("<form method=\"post\" action=\"http://localhost:8080/authorize_callback\">") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find("<input type=\"hidden\" name=\"state\" value=\"state\"") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeOktaFormPost() {
  context->setParam("token_provider.compatibility", "OKTA");
  auto pfix = AuthorizeWithSessionTokenBase("GET", "okta_post_message", "code", 200);

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("code", pfix->body.find(" code:\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no id_token", pfix->body.find(" id_token:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("no token", pfix->body.find(" access_token:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find(" state:\"state\"") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeOktaFormPostIdToken() {
  context->setParam("token_provider.compatibility", "OKTA");
  auto pfix = AuthorizeWithSessionTokenBase("GET", "okta_post_message", "id_token", 200);

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no code", pfix->body.find(" code:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("id_token", pfix->body.find(" id_token:\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no token", pfix->body.find(" access_token:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find(" state:\"state\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("token_type", pfix->body.find(" token_type:\"Bearer\"") == string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeOktaFormPostToken() {
  context->setParam("token_provider.compatibility", "OKTA");
  auto pfix = AuthorizeWithSessionTokenBase("GET", "okta_post_message", "token", 200);

  // uncomment to get a HTML form post
  // cout << pfix->body << '\n';

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no code", pfix->body.find(" code:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("no id_token", pfix->body.find(" id_token:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("access_token", pfix->body.find(" access_token:\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find(" state:\"state\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("token_type", pfix->body.find(" token_type:\"Bearer\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("post message", pfix->body.find("parentWindow.postMessage(postData, '*');") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

void AuthHandlerTestAuthorize::AuthorizeOktaFormPostTokenReferer() {
  context->setParam("token_provider.compatibility", "OKTA");
  auto pfix = AuthorizeWithSessionTokenBase("GET", "okta_post_message", "token", 200, "", {{"Referer","http://domain.tld"}});

  // uncomment to get a HTML form post
  // cout << pfix->body << '\n';

  CPPUNIT_ASSERT_MESSAGE("Result is HTML <html>", pfix->body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Result is HTML </html>", pfix->body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("no code", pfix->body.find(" code:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("no id_token", pfix->body.find(" id_token:\"") == string::npos);
  CPPUNIT_ASSERT_MESSAGE("access_token", pfix->body.find(" access_token:\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("state", pfix->body.find(" state:\"state\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("token_type", pfix->body.find(" token_type:\"Bearer\"") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("post message", pfix->body.find("parentWindow.postMessage(postData, 'http://domain.tld');") != string::npos);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have cache-control no-cache,no-store header", string("no-cache, no-store"), pfix->pt_header.get("cache-control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should have pragma no-cache header", string("no-cache"), pfix->pt_header.get("pragma", ""));
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestAuthorize);

#endif
