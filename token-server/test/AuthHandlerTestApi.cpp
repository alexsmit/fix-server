/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <set>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestApi.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <vx/StringUtil.h>
#include <vx/openid/flow/Flow.h>
#include <vx/EncodingUtil.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::openid::test;


AuthHandlerTestApi::AuthHandlerTestApi() {}
AuthHandlerTestApi::~AuthHandlerTestApi() {}

//-----------------------------------------

/// Fixture class to test token API
class ApiTestFix {
public:
  ptree pt;  //!< parsed JWT token payload

  /// create fixture object. @param token expected comlete JWT token
  ApiTestFix(const string& token) {
    auto vtoken = StringUtil::split(token, ".");
    random_util r;
    auto spayload = r.frombase64url(vtoken[1]);
    spayload = r.base64decode(spayload);
    stringstream s(spayload);
    read_json(s, pt);
  }
};

void AuthHandlerTestApi::ApiIdtokenOnly() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"id_token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("no access_token", pt.get("access_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("has id_token", !pt.get("id_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("no refresh_token", pt.get("refresh_token", "").empty());
}

void AuthHandlerTestApi::ApiAccessTokenOnly() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("no id_token", pt.get("id_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("no refresh_token", pt.get("refresh_token", "").empty());
}

void AuthHandlerTestApi::ApiIdAccessNoRefresh() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token", "id_token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("has id_token", !pt.get("id_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("no refresh_token", pt.get("refresh_token", "").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("type", string("Bearer"), pt.get("token_type", ""));
  auto sttl = boost::lexical_cast<string>(token_provider->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expires_in", sttl, pt.get("expires_in", ""));
}

void AuthHandlerTestApi::ApiAllTokens() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid offline_access", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("has id_token", !pt.get("id_token", "").empty());
  CPPUNIT_ASSERT_MESSAGE("has refresh_token", !pt.get("refresh_token", "").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("type", string("Bearer"), pt.get("token_type", ""));
  auto sttl = boost::lexical_cast<string>(token_provider->GetEffectiveTTL());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expires_in", sttl, pt.get("expires_in", ""));
}

void AuthHandlerTestApi::ApiAccessAddressClaim() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);

  UserRecord rec;
  rec.username = USER_NAME;
  rec.props.emplace("phone_number", "888-012-3456");
  rec.props.emplace("phone_number_verified", "true");
  rec.props.emplace("address_formatted", "800 Colton st, Colton, CA, 92324");
  rec.props.emplace("address_street_address", "800 Colton st");
  rec.props.emplace("address_locality", "Colton");
  rec.props.emplace("address_region", "CA");
  rec.props.emplace("address_postal_code", "92324");
  rec.props.emplace("address_country", "USA");
  provider->SaveUser(rec);

  api.Scope("openid address", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  ApiTestFix fix(pt.get("access_token", ""));

  auto ch = fix.pt.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("has scp claim", !!ch);
  bool found = true;
  for (auto& kv : *ch) {
    if (kv.second.data() == "address") {
      found = true;
      break;
    }
  }
  CPPUNIT_ASSERT_MESSAGE("has address scope", found);
}

void AuthHandlerTestApi::ApiAccessPhoneClaim() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid phone", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  ApiTestFix fix(pt.get("access_token", ""));

  auto ch = fix.pt.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("has scp claim", !!ch);
  bool found = false;
  for (auto& kv : *ch) {
    if (kv.second.data() == "phone") {
      found = true;
      break;
    }
  }
  CPPUNIT_ASSERT_MESSAGE("has phone scope", found);
}

void AuthHandlerTestApi::ApiAccessProfileClaim() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid profile", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  ApiTestFix fix(pt.get("access_token", ""));

  auto ch = fix.pt.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("has scp claim", !!ch);
  bool found = false;
  for (auto& kv : *ch) {
    if (kv.second.data() == "profile") {
      found = true;
      break;
    }
  }
  CPPUNIT_ASSERT_MESSAGE("has profile scope", found);
}

void AuthHandlerTestApi::ApiAccessUnknownClaim() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid unknown", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  // write_json(cout, pt);

  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());
  ApiTestFix fix(pt.get("access_token", ""));

  auto ch = fix.pt.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("has scp claim", !!ch);
  bool found = false;
  for (auto& kv : *ch) {
    if (kv.second.data() == "unknown") {
      found = true;
      break;
    }
  }
  CPPUNIT_ASSERT_MESSAGE("has not unknown scope", !found);
}

void AuthHandlerTestApi::ApiGetPropertyNE() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid", "openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto val = api.GetProperty(TokenAPIProperty::jwks_uri);
  CPPUNIT_ASSERT_MESSAGE("no jwks_uri prop yet", val.empty());
}

void AuthHandlerTestApi::ApiIgnoredNonce() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid nonce");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  ApiTestFix fix(pt.get("access_token", ""));
  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());

  auto ch = fix.pt.get_child_optional("scp");
  CPPUNIT_ASSERT_MESSAGE("has scp claim", !!ch);

  vector<string> scopes;
  for (auto& kv : *ch) {
    scopes.push_back(kv.second.data());
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE("only 1 scope", (size_t)1, scopes.size());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("only openid scope", string("openid"), scopes[0]);
}

void AuthHandlerTestApi::ApiGetError() {
  TokenAPI api(*context);

  auto user = provider->LoadUser(USER_NAME);
  api.Scope("openid");
  api.Nonce("nonce");
  api.Flow("PKCE");
  api.Tokens({"token"});
  api.CreateAccessToken(user.userid, CLIENT_ID, "");

  auto pt = api.Result();
  ApiTestFix fix(pt.get("access_token", ""));
  CPPUNIT_ASSERT_MESSAGE("has access_token", !pt.get("access_token", "").empty());

  std::this_thread::sleep_for(chrono::milliseconds(4500));

  TokenAPI apivalidate(*context);
  auto result = apivalidate.Validate(pt.get("access_token", ""));

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  // cout << apivalidate.GetError() << endl;
  // cout << apivalidate.GetErrorDescription() << endl;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), apivalidate.GetError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("token verification failed: token expired"), apivalidate.GetErrorDescription());
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestApi);

#endif
