/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "ResetTest.h"
#include "BaseTest.h"

using namespace std;
using namespace vx::openid::test;

ResetTest::ResetTest() { server_context = getContext(); }

ResetTest::~ResetTest() {}

void ResetTest::setUp() {
  client.reset(new HttpClient(DEBUG_HOST));
  // delete/create test user
  // profile_provider = factory->Connect<ProfileProviderMySQL>();
  TokenServerContext* context = dynamic_cast<TokenServerContext*>(server_context.get());
  token_provider = context->GetTokenProvider();

  context->setParam("profile_provider.type", "mysql");
  profile_provider = context->GetProfileProvider();
  profile_provider->DeleteUser(USER_NAME);
  profile_provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  profile_provider->SetRoles(USER_NAME, {"user"});

  if (!server_context) {
    cout << "FATAL ERROR. Context is not initialized." << endl;
    ::exit(1);
  }

  server_context->setParam("profile_provider.send_email", "off");
}

void ResetTest::tearDown() {
  profile_provider.reset();
  AppConfig::resetInstance();
}

string ResetTest::ResetUser(bool goodUser, bool useForm) {
  auto param = server_context->getParam("profile_provider.send_token", "not-set");
  if (param != "on") {
    cout << "WARNING!!! To run the test you should set profile_provider.send_token to \"on\"" << endl;
    cout << "Otherwise our test won't have an access to a token being sent over the email" << endl;
    CPPUNIT_FAIL("Invalid configuration");
  }

  ptree pt;
  string expected, reset_code, email;
  if (goodUser) {
    email = USER_EMAIL;
    expected = "200";
  }
  else {
    email = "some_invalid_email@vxpro.com";
    expected = "400";
  }

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  stringstream s;
  if (useForm) {
    header.insert({"Content-Type", "application/x-www-form-urlencoded"});
    s << "email=" << email;
  }
  else {
    header.insert({"Content-Type", "application/json"});
    pt.put("email", email);
    write_json(s, pt);
  }

  auto result = client->request("POST", "/reset", s.str(), header);

  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected code", expected, code);

  if (goodUser) {
    ptree pt_result;
    read_json(result->content, pt_result);
    reset_code = pt_result.get("reset_token", "");
    CPPUNIT_ASSERT_MESSAGE("Should get a reset token", !reset_code.empty());
  }

  return reset_code;
}

void ResetTest::testReset() {
  auto reset_code = ResetUser(true);
}

void ResetTest::testResetForm() {
  auto reset_code = ResetUser(true, true);
}

void ResetTest::testResetNotExist() {
  ResetUser(false);
}

void ResetTest::testResetNotExistForm() {
  ResetUser(false, true);
}

void ResetTest::testEmailConfirm() {
  auto reset_code = ResetUser(true);
  auto url = boost::format("/reset/confirm?code=%s") % reset_code;
  auto result = client->request("GET", url.str());
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
}

void ResetTest::testEmailConfirmBadCode() {
  auto result = client->request("GET", "/reset/confirm?code=some_bad_code");
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect", string("302"), code);

  string url = server_context->getParam("profile_provider.reset_failure_url");
  auto it = result->header.find("Location");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", it != result->header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect to configured page", url, it->second);
}

void ResetTest::testPasswordUpdate() {
  // 1 - reset
  auto reset_code = ResetUser(true);
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});
  header.insert({"Cookie", "_vxreset=my_only_cookie"});

  // 2 - update password via reset code
  auto body = boost::format("password=new&confirm_password=new&token=%s") % reset_code;
  auto result = client->request("POST", "/reset/update", body.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect", string("302"), code);

  string url = server_context->getParam("profile_provider.reset_success_url");
  auto it = result->header.find("Location");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", it != result->header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect to configured page", url, it->second);

  // 3 - try to login with new password
  header.clear();
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", "new");
  pt.put("username", USER_NAME);
  stringstream s;
  write_json(s, pt);

  ptree pt_token;
  result = client->request("POST", "/oauth/token", s.str(), header);
  code = result->status_code.substr(0, 3);
  // cout << "code: " << code << endl;
  // ::exit(1);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
}

void ResetTest::testConfiguration() {
  // deprecated parameter, should be removed from config
  auto param = server_context->getParam("profile_provider.reset_html", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should not have a configured reset link", param == "not-set");

  param = server_context->getParam("profile_provider.reset_success_url", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have a configured confirmation email title", param != "not-set");

  param = server_context->getParam("profile_provider.reset_failure_url", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have a send email parameter", param != "not-set");

  param = server_context->getParam("profile_provider.reset_email", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have a send email parameter", param != "not-set");

  param = server_context->getParam("profile_provider.reset_email_title", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have a send email parameter", param != "not-set");
}

void ResetTest::testResetVerify() {
  // 1 - reset
  auto reset_code = ResetUser(true);

  // 2 - verify reset code
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt, pt_result;
  pt.put("reset_token", reset_code);
  stringstream s;
  write_json(s, pt);

  auto result = client->request("POST", "/reset/verify", s.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected code", string("200"), code);
  read_json(result->content, pt_result);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected message", string("pending"), pt_result.get("status", ""));
}

void ResetTest::testResetVerifyBadToken() {
  ptree pt_result;
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("POST", "/reset/verify", "{\"reset_token\":\"bad_value\"}", header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected code", string("400"), code);
  read_json(result->content, pt_result);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected message", string("invalid \"token\""), pt_result.get("error_description", ""));
}

//------------------------------
//--- CHANGE PIN/PASSWORD ------
//------------------------------

string ResetTest::GetBearer() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", "aaa"});
  header.insert({"X-Client-Secret", "111"});
  header.insert({"Content-Type", "application/json"});

  ptree pt_auth;
  pt_auth.put("grant_type", "password");
  pt_auth.put("password", USER_PASSWORD);
  pt_auth.put("username", USER_NAME);
  stringstream s;
  write_json(s, pt_auth);

  auto result =
      client->request("POST", "/oauth/token", s.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  ptree pt_token;
  read_json(result->content, pt_token);

  string bearer = string("bearer ") + pt_token.get("access_token", "");
  return bearer;
}

void ResetTest::testChangePin() {
  auto rec = profile_provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("user should exist", rec.id != 0);

  string bearer = GetBearer();

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Authorization", bearer});
  header.insert({"Content-Type", "application/json"});

  ptree pt_change;
  pt_change.put("pin", "0000");
  pt_change.put("pin_verify", "0000");
  stringstream schange;
  write_json(schange, pt_change);

  auto result = client->request("POST", "/pin", schange.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  auto after_update = profile_provider->AuthenticatePin(USER_NAME, "0000");
  CPPUNIT_ASSERT_MESSAGE("Should authenticate new password", after_update);
}

void ResetTest::testChangePasswordBase(string oldPassword, string newPassword1, string newPassword2, string expectedCode) {
  auto rec = profile_provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("user should exist", rec.id != 0);

  string bearer = GetBearer();

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Authorization", bearer});
  header.insert({"Content-Type", "application/json"});

  ptree pt_change;
  pt_change.put("password_old", oldPassword);
  pt_change.put("password", newPassword1);
  pt_change.put("password_verify", newPassword2);
  stringstream schange;
  write_json(schange, pt_change);

  auto result = client->request("POST", "/password", schange.str(), header);
  auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Password update should result with code", expectedCode, code);
}

void ResetTest::testAuthenticate(string password, bool expect) {
  auto after_update = profile_provider->Authenticate(USER_NAME, password.c_str());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Authenticates status", expect, after_update);
}

void ResetTest::testChangePassword() {
  testChangePasswordBase(USER_PASSWORD, "123", "123", "200");
  testAuthenticate("123", true);
}

void ResetTest::testChangePasswordBadOld() {
  testChangePasswordBase("some_bad_old_password", "123", "123", "400");
  testAuthenticate("123", false);
}

void ResetTest::testChangePasswordBad() {
  testChangePasswordBase("some_bad_old_password", "", "123", "400");
  testChangePasswordBase("some_bad_old_password", "123", "", "400");
}

CPPUNIT_TEST_SUITE_REGISTRATION(ResetTest);
