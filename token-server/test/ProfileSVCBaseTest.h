#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// remote profile server response
      struct ProfileRemoteResponse {
        ptree data;                  //!< response data
        SimpleWeb::StatusCode code;  //!< response code
      };

      /// Base test for mysql and remote implementations
      class ProfileSVCBaseTest {
      protected:
        string provider_type;  //!< profile provider name, we should have mysql, remote and remotes configs to run a test
        // string host;
        // string remote_profile;
        // shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;    //!< current profile provider(mysql, remote, ...)
        shared_ptr<TokenServerContext> context;  //!< current server context

        /// result will be validated. should contain an expected profile data
        virtual void expectAuthenticate(string prefix, std::shared_ptr<ProfileData> result);

      public:
        /// test constructor
        ProfileSVCBaseTest(string provider_type);
        virtual ~ProfileSVCBaseTest();

        void setUpInternal();     //!< internal cppunit setup
        void tearDownInternal();  //!< internal cppunit teardown
        // void setUp();
        // void tearDown();
        virtual void testAuthenticate();         //!< authenticate with password
        virtual void testAuthenticateUUID();     //!< authenticate with password (uuid)
        virtual void testAuthenticatePin();      //!< authenticate with pin
        virtual void testAuthenticatePinUUID();  //!< authenticate with pin (uuid)
        virtual void testGetProfileData();       //!< load user profile
        virtual void testGetProfileDataUUID();   //!< load user profile by UUID

        virtual void testConfirmationEmailParam();  //!< validate config.json parameters

        virtual void testSendConfirmationEmailTemplate();   //!< validate template parameter
        virtual void testSendConfirmationEmailTitle();   //!< validate title parameter
        virtual void testSendConfirmationEmailFrom();   //!< validate from parameter
        virtual void testSendConfirmationEmailTo();   //!< validate email To parameter
        virtual void testSendConfirmationEmail();   //!< validate parameters and send confirm email (no actual email)
        virtual void testSendConfirmationEmailOff();   //!< sending emails is disabled
        virtual void testSendConfirmationEmailNoSVC();   //!< sending emails is enabled, but service is not registered
        virtual void testSendConfirmationEmailBadSVC();   //!< sending emails is enabled, but bad service is registered

        virtual void testRegisterUser();                 //!< execute user registration (see RegisterTest for details)
        virtual void testValidateRegistration();         //!< register, validate registration
        virtual void testValidatePendingRegistration();  //!< register, wait>cleanup, validate registration (only when profile server is running)
        virtual void testConfirmRegistration();  //!< register, confirm registration

        virtual void testReset();                //!< execute reset password
        virtual void testUpdatePassword();       //!< reset password, update password
        virtual void testUpdatePasswordBad();    //!< update password with wrong token
        virtual void testVerifyResetToken();     //!< reset, verify reset token
        virtual void testVerifyResetTokenBad();  //!< verify using wrong reset token

        virtual void testChangePassword();  //!< update password using UUID, check authentication
        virtual void testChangePin();       //!< update pin using UUID, check authentication
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
