/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include <BaseTest.h>

#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTest.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>
#include <vx/ServiceProfileFactory.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::openid::test;

//-----------------------------------------

AuthHandlerTest::AuthHandlerTest() {}

AuthHandlerTest::~AuthHandlerTest() {
}

void AuthHandlerTest::setUp() {
  AppConfig::resetInstance();

  // cache is disabled
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());

  provider = context->GetProfileProvider("mysql");
  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetRoles(USER_NAME, {"user"});

  token_provider = context->GetTokenProvider();

  context->setParam("token_provider.compatibility", "none");
  context->section->setParam("refresh_token_rotation", "on");
}

void AuthHandlerTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  context.reset();
  provider.reset();
  token_provider.reset();
  AppConfig::resetInstance();
}

void AuthHandlerTest::AuthStress() {
  auto rec = provider->LoadUser(USER_NAME, ProfileKey::username);

  cout << "W" << flush;

  for (auto i = 0; i < 1000; i++) {
    auto session_cookie = token_provider->CreateTokenMeta(
        rec.userid,
        "session",
        "",
        0,
        {{"access_token", "xxx"},
         {"id_token", "xxx"}});

    auto msg = (boost::format("should load iter = %d") % i).str();
    auto session_cookie_loaded = token_provider->GetTokenByNonce(session_cookie.nonce);
    CPPUNIT_ASSERT_MESSAGE(msg, !session_cookie_loaded.nonce.empty());

    token_provider->DeleteTokenByNonce(session_cookie.nonce);

    msg = (boost::format("should not load iter = %d") % i).str();
    session_cookie_loaded = token_provider->GetTokenByNonce(session_cookie.nonce);
    CPPUNIT_ASSERT_MESSAGE(msg, session_cookie_loaded.nonce.empty());
  }
}

void AuthHandlerTest::testAuthorizeResponse() {
  AuthorizeResponse resp{"state", "code"};
  ptree pt;
  pt << resp;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("size", (size_t)2, pt.size());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("state", string("state"), pt.get("state", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", string("code"), pt.get("code", ""));
}

/// fixture class to test /openid-configuration endpoint
class AuthHandlerRequestFix : public AuthorizeFixBase {
public:
  string error_output;
  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  AuthHandlerRequestFix(shared_ptr<TokenServerContext> context)
      : AuthorizeFixBase(context) {
  }

  void Handle(const string& method = "POST") {
  }

  /// run request, parse result
  void InitAuthHander(const string& method = "POST") {
    stringstream sin;
    stringstream sout;

    header.emplace("Origin", "https://jwt.io");

    vx::web::test::HandlerEnvironment env(sin, sout);

    stringstream out;
    std::streambuf* out_buf(std::cerr.rdbuf(out.rdbuf()));

    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());

    std::cerr.rdbuf(out_buf);
    error_output = out.str();
  }
};

void AuthHandlerTest::testAuthHandlerCerr() {
  AppConfig& conf = AppConfig::getInstance();
  conf["token_provider.key_rotation"] = "wrong_data";
  AuthHandlerRequestFix fix(context);
  fix.InitAuthHander();

  CPPUNIT_ASSERT_MESSAGE("bad key_rotation", fix.error_output.find("Invalid config parameter - key_rotation") != string::npos);
}


CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTest);

#endif
