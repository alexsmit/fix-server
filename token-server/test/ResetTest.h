/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _RESET_TEST_H
#define _RESET_TEST_H

#include <stdafx.h>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <TokenServerContext.h>

#include <BaseTest.h>

using namespace std;
using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace test {
      /// reset handler test
      class ResetTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ResetTest);

        CPPUNIT_TEST(testReset);
        CPPUNIT_TEST(testResetForm);

        CPPUNIT_TEST(testResetNotExist);
        CPPUNIT_TEST(testResetNotExistForm);

        CPPUNIT_TEST(testEmailConfirm);
        CPPUNIT_TEST(testEmailConfirmBadCode);
        CPPUNIT_TEST(testPasswordUpdate);
        CPPUNIT_TEST(testConfiguration);
        CPPUNIT_TEST(testResetVerify);
        CPPUNIT_TEST(testResetVerifyBadToken);

        CPPUNIT_TEST(testChangePin);
        CPPUNIT_TEST(testChangePassword);
        CPPUNIT_TEST(testChangePasswordBadOld);
        CPPUNIT_TEST(testChangePasswordBad);


        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ProfileProvider> profile_provider;
        shared_ptr<TokenProvider> token_provider;
        shared_ptr<HttpClient> client;
        shared_ptr<ServerContext> server_context;

        // string CreateUserJSON(bool goodUser = true);
        string ResetUser(bool goodUser = true, bool useForm = false);
        string GetBearer();
        void testChangePasswordBase(string oldPassword, string newPassword1, string newPassword2, string expectedCode);
        void testAuthenticate(string password, bool expect);

      public:
        ResetTest();
        ~ResetTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

        void testReset();      //!< happy path, reset
        void testResetForm();  //!< happy path, reset, using form post

        void testResetNotExist();      //!< reset using invalid email
        void testResetNotExistForm();  //!< reset using invalid email, form post

        void testEmailConfirm();         //!< reset, using reset token to confirm
        void testEmailConfirmBadCode();  //!< using invalid reset token to confirm
        void testPasswordUpdate();       //!< reset, update password, authenticate
        void testConfiguration();        //!< test config.json parameters
        void testResetVerify();          //!< reset, verify
        void testResetVerifyBadToken();  //!< verify using wrong token

        void testChangePin();             //!< update pin using bearer token, authenticate
        void testChangePassword();        //!< update password using bearer token, authenticte
        void testChangePasswordBadOld();  //!< change password, provide wrong old password
        void testChangePasswordBad();     //!< change password, wrong new password or wrong confirn password
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
