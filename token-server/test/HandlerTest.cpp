/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "HandlerTest.h"
#include <vx/HttpParser.h>
#include "AuthHandlerTest.h"
#include <TokenHandler.h>
#include <TokenServerInfoHandler.h>
#include <VxMessage.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceProfileFactory.h>

#include <HandlerTestFix.h>

using namespace std;
using namespace VxServer;
using namespace vx::web::test;
using namespace vx::openid::test;


namespace vx {
  namespace openid {
    namespace test {
      shared_ptr<VxServer::Handlers::VxHandler> setup_token_handler(std::shared_ptr<TokenServerContext> context, const vx::web::test::HandlerEnvironment& env) {
        shared_ptr<VxServer::Handlers::VxHandler> handler;
        handler.reset(new Handlers::TokenHandler(*(context.get()), *env.server.get(), *env.service.get()));
        return handler;
      }
    }  // namespace test
  }    // namespace openid
}  // namespace vx

//-----------------------------------------

HandlerTest::HandlerTest() {}

HandlerTest::~HandlerTest() {}

void HandlerTest::setUp() {
  AppConfig::resetInstance();

  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());

  auto sttl = boost::lexical_cast<string>(DEBUG_TTL);
  auto sttl_refresh = boost::lexical_cast<string>(DEBUG_TTL * 2);
  context->section->setParam("expiration", sttl);
  context->section->setParam("refresh_expiration", sttl_refresh);
  context->section->setParam("pin_after_expiration", "off");
  context->section->setParam("refresh_token_rotation", "on");

  factory.reset(new DataFactory());
#if defined(VX_USE_MYSQL)
  provider = factory->Connect<ProfileProviderMySQL>();
#elif defined(VX_USE_SOCI)
  provider = factory->Connect<ProfileProviderSOCI>();
#endif

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});

  token_provider = context->GetTokenProvider();
}

void HandlerTest::tearDown() {
  context.reset();
  token_provider.reset();
  provider.reset();
}

/// Validate all required parameters are provided in the config.json
void HandlerTest::testConfig() {
  // sample values are not being verified
  map<string, string> params = {
      {"pin_after_expiration", "off"},
      {"refresh_token_rotation", "on"},
      {"ttl_session_token", "60"},
      {"expiration", "30"},
      {"refresh_expiration", "600"}};

  for (auto& kv : params) {
    auto val = context->section->getParam(kv.first);
    auto msg = string("required parameter: ") + val;
    CPPUNIT_ASSERT_MESSAGE(msg, !val.empty());
  }
}

void HandlerTest::testCors() {
  stringstream sout;
  stringstream sin("test");

  CGIServer server;
  CGIService service(server);
  Handlers::TokenHandler tokenHandler(*(context.get()), server, service);

  shared_ptr<NoRequest> no_req(new NoRequest(sin));
  shared_ptr<NoResponse> no_res(new NoResponse(sout));
  shared_ptr<Request> request(new RequestSpec<shared_ptr<NoRequest>>(no_req, 0));
  shared_ptr<Response> response(new ResponseSpec<shared_ptr<NoResponse>>(no_res));

  request->header().emplace("Origin", "https://jwt.io");

  SimpleWeb::CaseInsensitiveMultimap header;
  tokenHandler.AddCORS(request, header);
  auto it = header.find("Access-Control-Allow-Origin");
  CPPUNIT_ASSERT_MESSAGE("Has cors header", it != header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should return cors header with https://jwt.io", string("https://jwt.io"), it->second);
}

void HandlerTest::testCorsAll() {
  stringstream sout;
  stringstream sin("test");
  HandlerEnvironment env(sin, sout);

  Handlers::TokenHandler tokenHandler(*(context.get()), *env.server.get(), *env.service.get());

  context->setParam("token_provider.cors_all", "true");
  env.request->header().emplace("Origin", "https://some.web.site");

  SimpleWeb::CaseInsensitiveMultimap header;
  tokenHandler.AddCORS(env.request, header);
  auto it = header.find("Access-Control-Allow-Origin");
  CPPUNIT_ASSERT_MESSAGE("Has cors header", it != header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should return cors header with *", string("*"), it->second);
}

void HandlerTest::testGetToken() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", USER_PASSWORD);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  fix.assert_simple_token(3);
}

void HandlerTest::testGetTokenBadClient() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", USER_PASSWORD);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", "invalid"});
  fix.header.insert({"X-Client-Secret", "invalid"});
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Unauthorized", (unsigned int)401, fix.code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("description", string("Wrong client id or secret"), fix.pt_body.get("error_description", ""));
}

void HandlerTest::testGetTokenWrongPassword() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", "wrong_password");
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid username or password"), fix.pt_body.get("error_description", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix.code);
}

void HandlerTest::testGetTokenWrongUsername() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", "some_wrong_user");
  pt.put("password", USER_PASSWORD);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid username or password"), fix.pt_body.get("error_description", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix.code);
}
// 123456789+123456789+123456789+123456789+123456789+123456789+123456789+123456789+
void HandlerTest::testGetTokenInvalidPassword() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", USER_NAME);
  pt.put("password", "123456789+123456789+123456789+123456789+123456789+123456789+123456789+123456789+");
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid password"), fix.pt_body.get("error_description", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with invalid password", (unsigned int)400, fix.code);
}

void HandlerTest::testGetTokenInvalidUsername() {
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("username", "123456789+123456789+123456789+123456789+123456789+123456789+123456789+123456789+");
  pt.put("password", USER_PASSWORD);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid user name"), fix.pt_body.get("error_description", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with invalid password", (unsigned int)400, fix.code);
}

void HandlerTest::testGetTokenRefresh() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();
  auto refresh_token1 = fix.pt_body.get("refresh_token", "");
  auto ref_record1 = token_provider->GetTokenByNonce(refresh_token1);

  ptree pt_refresh;
  pt_refresh.put("grant_type", "refresh_token");
  pt_refresh.put("refresh_token", refresh_token1);
  HandlerTestFix fix2(context, pt_refresh, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.Handle();
  auto refresh_token2 = fix2.pt_body.get("refresh_token", "");
  auto ref_record1after = token_provider->GetTokenByNonce(refresh_token1);

  fix2.assert_simple_token(100);
  CPPUNIT_ASSERT_MESSAGE("different refresh token", refresh_token1 != refresh_token2);
  CPPUNIT_ASSERT_MESSAGE("old refresh token is deleted", ref_record1after.nonce.empty());

  // refresh using old token should fail
  HandlerTestFix fix3(context, pt_refresh, setup_token_handler);
  fix3.header.insert({"X-Client-Id", CLIENT_ID});
  fix3.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix3.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Cannot use prevous refresh token", (unsigned int)401, fix3.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix3.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid refresh token"), fix3.pt_body.get("error_description", ""));
}

void HandlerTest::testGetTokenRefreshNoRotate() {
  context->ttl = 100;
  context->refresh_ttl = 200;
  context->section->setParam("refresh_token_rotation", "off");

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  ptree pt_refresh;
  pt_refresh.put("grant_type", "refresh_token");
  pt_refresh.put("refresh_token", fix.pt_body.get("refresh_token", ""));
  HandlerTestFix fix2(context, pt_refresh, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.Handle();

  fix2.assert_simple_token(100);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same refresh token", fix.pt_body.get("refresh_token", ""), fix2.pt_body.get("refresh_token", ""));
}

void HandlerTest::testGetTokenRefreshAfterExp() {
  context->ttl = 1;
  context->refresh_ttl = 2;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(3000));

  ptree pt_refresh;
  pt_refresh.put("grant_type", "refresh_token");
  pt_refresh.put("refresh_token", fix.pt_body.get("refresh_token", ""));
  HandlerTestFix fix2(context, pt_refresh, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized after expiration", (unsigned int)401, fix2.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix2.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Invalid refresh token"), fix2.pt_body.get("error_description", ""));
}

void HandlerTest::testGetTokenPin() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  fix2.assert_simple_token(100);
}

void HandlerTest::testGetTokenWrongPin() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", "wrong");
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix2.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);
}

void HandlerTest::testGetTokenInvalidPin() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", "some wrong pin");
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix2.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with invalid pin", (unsigned int)400, fix2.code);
}

void HandlerTest::testGetTokenPinAfterExp() {
  context->ttl = 1;
  context->refresh_ttl = 2;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(3000));

  ptree pt_refresh;
  pt_refresh.put("grant_type", "pin");
  pt_refresh.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_refresh, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix2.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized after expiration", (unsigned int)401, fix2.code);
}

void HandlerTest::testGetTokenPinAfterExpOK() {
  context->ttl = 1;
  context->refresh_ttl = 2;

  context->section->setParam("pin_after_expiration", "on");

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(3000));

  ptree pt_refresh;
  pt_refresh.put("grant_type", "pin");
  pt_refresh.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_refresh, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  fix2.assert_simple_token(1);
}

void HandlerTest::testGetTokenPin2Wrong() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  // 2 wrong pins
  for (auto i = 0; i < 2; i++) {
    ptree pt_pin;
    pt_pin.put("grant_type", "pin");
    pt_pin.put("pin", "wrong");
    HandlerTestFix fix2(context, pt_pin, setup_token_handler);
    fix2.header.insert({"X-Client-Id", CLIENT_ID});
    fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
    fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
    fix2.Handle();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);
  }

  // correct pin
  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  fix2.assert_simple_token(100);
}

void HandlerTest::testGetTokenPin3Wrong() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  // 3 wrong pins, should lock ability to get a token
  for (auto i = 0; i < 3; i++) {
    ptree pt_pin;
    pt_pin.put("grant_type", "pin");
    pt_pin.put("pin", "wrong");
    HandlerTestFix fix2(context, pt_pin, setup_token_handler);
    fix2.header.insert({"X-Client-Id", CLIENT_ID});
    fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
    fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
    fix2.Handle();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);
  }

  // correct pin
  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);

  // try to refresh
  ptree pt_refresh;
  pt_refresh.put("grant_type", "refresh_token");
  pt_refresh.put("refresh_token", fix.pt_body.get("refresh_token", ""));
  HandlerTestFix fix3(context, pt_refresh, setup_token_handler);
  fix3.header.insert({"X-Client-Id", CLIENT_ID});
  fix3.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix3.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_token"), fix2.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized after 3 pin failures", (unsigned int)401, fix2.code);
}

void HandlerTest::testGetTokenPin2Wrong2Wrong() {
  context->ttl = 100;
  context->refresh_ttl = 200;

  // get access token
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  HandlerTestFix fix(context, pt, setup_token_handler);
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle();

  // 2 wrong pins
  for (auto i = 0; i < 2; i++) {
    ptree pt_pin;
    pt_pin.put("grant_type", "pin");
    pt_pin.put("pin", "wrong");
    HandlerTestFix fix2(context, pt_pin, setup_token_handler);
    fix2.header.insert({"X-Client-Id", CLIENT_ID});
    fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
    fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
    fix2.Handle();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);
  }

  // correct pin
  ptree pt_pin;
  pt_pin.put("grant_type", "pin");
  pt_pin.put("pin", USER_PIN);
  HandlerTestFix fix2(context, pt_pin, setup_token_handler);
  fix2.header.insert({"X-Client-Id", CLIENT_ID});
  fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix2.Handle();
  fix2.assert_simple_token(100);

  // 2 wrong pins (again)
  for (auto i = 0; i < 2; i++) {
    ptree pt_pin;
    pt_pin.put("grant_type", "pin");
    pt_pin.put("pin", "wrong");
    HandlerTestFix fix2(context, pt_pin, setup_token_handler);
    fix2.header.insert({"X-Client-Id", CLIENT_ID});
    fix2.header.insert({"X-Client-Secret", CLIENT_SECRET});
    fix2.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
    fix2.Handle();
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Not authorized with wrong pin", (unsigned int)401, fix2.code);
  }

  // correct pin
  HandlerTestFix fix3(context, pt_pin, setup_token_handler);
  fix3.header.insert({"X-Client-Id", CLIENT_ID});
  fix3.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix3.header.insert({"Authorization", string("Bearer ") + fix.pt_body.get("access_token", "")});
  fix3.Handle();

  fix3.assert_simple_token(100);
}

void HandlerTest::testFingerPrint() {
  ptree pt;
  HandlerTestFix fix(context, pt, setup_token_handler, "/fingerprint");
  fix.header.insert({"X-Client-Id", CLIENT_ID});
  fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle("GET");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sha1", string("9F:F5:70:EC:05:EF:AC:6D:8B:AA:1B:2F:2A:BD:CF:45:24:53:E2:26"), fix.pt_body.get("sha1", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sha256", string("61:F0:F1:49:A0:9D:03:1A:0E:48:B6:D2:40:F3:C2:67:86:CE:EC:6B:32:D0:4C:32:23:AE:04:C7:E7:C1:D0:32"), fix.pt_body.get("sha256", ""));
}

CPPUNIT_TEST_SUITE_REGISTRATION(HandlerTest);

#endif
