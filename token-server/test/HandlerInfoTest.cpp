/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include <vx/HttpParser.h>
#include "AuthHandlerTest.h"
#include "HandlerInfoTest.h"
#include <TokenServerInfoHandler.h>
#include <VxMessage.h>

#include <BaseServiceTest.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceProfileFactory.h>
#include <vx/ServiceTokenFactory.h>

#ifndef PROJECT_GIT_TAG
#define PROJECT_GIT_TAG "DEBUG"
#endif

using namespace std;
using namespace VxServer;
using namespace vx::web::test;
using namespace vx::openid::test;

/// fixture class to test /oauth/token endpoint
class HandlerInfoTestFix : public AuthorizeFixBase {
public:
  ptree& pt_request;  //!< request object, set by the constructor
  string url;         //!< url, set by the constructor

  /// create fixture object
  /// @param context current server context, should use created by setup
  /// @param req request object as ptree
  /// @param url default or specific url for the test
  HandlerInfoTestFix(shared_ptr<TokenServerContext> context,
                 ptree& req,
                 string url = "/info") : AuthorizeFixBase(context), pt_request(req), url(url) {
  }

  /// run request and parse result
  void Handle(const string& method = "GET") {
    stringstream sout;
    stringstream sin;
    string slen;

    if (method == "POST") {
      // stringstream squery;
      // write_json(squery, pt_request);
      SimpleWeb::CaseInsensitiveMultimap mquery;
      for (auto& kv : pt_request) {
        mquery.emplace(kv.first, kv.second.data());
      }
      string query = SimpleWeb::QueryString::create(mquery);
      slen = boost::lexical_cast<string>(query.size());
      sin.str(query);
    }

    HandlerEnvironment env(sin, sout);
    Handlers::TokenServerInfoHandler tokenHandler(*(context.get()), *env.server.get(), *env.service.get(), 33);
    env.no_req->path = url;
    env.no_req->method = method;
    env.no_req->header = header;

    if (method == "POST") {
      env.no_req->header.emplace("Content-Type", "application/x-www-form-urlencoded");
      env.no_req->header.emplace("Content-Length", slen);
    }
    else {
      env.no_req->header.emplace("Origin", "https://jwt.io");
    }

    env.Handle();

    ParseResult(sout);
  }

};


//-----------------------------------------

HandlerInfoTest::HandlerInfoTest() {}

HandlerInfoTest::~HandlerInfoTest() {}

void HandlerInfoTest::setUp() {
  AppConfig::resetInstance();

  context.reset(new TokenServerContext(8082, 0, DEBUG_TTL, LogLevel::none, 5, DEBUG_TTL * 2, ""));
  context->Init("token_provider", "config.json");
}

void HandlerInfoTest::tearDown() {
  context.reset();
  token_provider.reset();
  provider.reset();
}

void HandlerInfoTest::testInfoNoTokenSvc() {
  ptree pt;

  context->AddProvider(make_shared<vx::ServiceProfileFactory>(10, "profile_provider"));
  // context->AddProvider(make_shared<vx::ServiceTokenFactory>(10));

  HandlerInfoTestFix fix(context, pt, "/info");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("code 500", (unsigned int)500, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("server_error"), fix.pt_body.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("description", string("Invalid service: token, type: "), fix.pt_body.get("error_description",""));
}

void HandlerInfoTest::testInfoBadTokenSvc() {
  ptree pt;

  context->AddProvider(make_shared<vx::ServiceProfileFactory>(10, "profile_provider"));
  context->AddProvider(make_shared<vx::ServiceDummyFactoryToken>(10));

  HandlerInfoTestFix fix(context, pt, "/info");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("code 500", (unsigned int)500, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("server_error"), fix.pt_body.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("description", string("Invalid token service type returned from the factory"), fix.pt_body.get("error_description",""));
}

void HandlerInfoTest::testInfo() {
  ptree pt;

  context->AddProvider(make_shared<vx::ServiceProfileFactory>(10, "profile_provider"));
  context->AddProvider(make_shared<vx::ServiceTokenFactory>(10));

  HandlerInfoTestFix fix(context, pt, "/info");
  fix.Handle("GET");

  // cout << fix.body << endl;
  // check for some keywords in that html (draft validation)
  CPPUNIT_ASSERT_MESSAGE("<html>", fix.body.find("<html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("</html>", fix.body.find("</html>") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Version", fix.body.find("Version") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Ports", fix.body.find("Ports") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Configured", fix.body.find("Configured") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Runtime", fix.body.find("Runtime") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Profile provider", fix.body.find("Profile provider") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Access token TTL", fix.body.find("Access token TTL") != string::npos);
  CPPUNIT_ASSERT_MESSAGE("Used memory", fix.body.find("Used memory") != string::npos);
}

void HandlerInfoTest::testVersion() {
  ptree pt;
  HandlerInfoTestFix fix(context, pt, "/version");
  // fix.header.insert({"X-Client-Id", CLIENT_ID});
  // fix.header.insert({"X-Client-Secret", CLIENT_SECRET});
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("code",(unsigned int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("version", string(PROJECT_GIT_TAG), fix.pt_body.get("version",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("service", string("token-server"), fix.pt_body.get("service",""));
}


CPPUNIT_TEST_SUITE_REGISTRATION(HandlerInfoTest);

#endif
