#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;

namespace vx {
  namespace openid {
    namespace test {

      /// testing endpoint /v1/authorize
      class AuthHandlerTestAuthorize : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestAuthorize);

        CPPUNIT_TEST(AuthorizeConfig);
        CPPUNIT_TEST(AuthorizeWrongClientId);
        CPPUNIT_TEST(AuthorizeMissingClientId);
        CPPUNIT_TEST(AuthorizeNotConfiguredCallback);
        CPPUNIT_TEST(AuthorizeWrongCallback);
        CPPUNIT_TEST(AuthorizeMissingResponseType);
        CPPUNIT_TEST(AuthorizeInvalidResponseType);
        CPPUNIT_TEST(AuthorizeAllResponseType);
        CPPUNIT_TEST(AuthorizeAllAndInvalidResponseType);
        CPPUNIT_TEST(AuthorizeInvalidResponseModeTokenQuery);
        CPPUNIT_TEST(AuthorizeInvalidResponseModeIdTokenQuery);
        CPPUNIT_TEST(AuthorizeInvalidResponseModeWrongValue);
        CPPUNIT_TEST(AuthorizeInvalidResponseModeOktaNoConfig);
        CPPUNIT_TEST(AuthorizeInvalidResponseModeOktaOk);
        CPPUNIT_TEST(AuthorizeMissingScope);
        CPPUNIT_TEST(AuthorizeOkNoOpenIdScopes);
        CPPUNIT_TEST(AuthorizeInvalidScope);
        CPPUNIT_TEST(AuthorizeOkAndInvalidScopes);
        CPPUNIT_TEST(AuthorizeAllOpenIDScopes);
        CPPUNIT_TEST(AuthorizeMissingState);
        CPPUNIT_TEST(AuthorizeWrongChallenge);
        CPPUNIT_TEST(AuthorizeNoChallengeMethod);
        CPPUNIT_TEST(AuthorizePromptInvalid);
        CPPUNIT_TEST(AuthorizePromptLogin);
        CPPUNIT_TEST(AuthorizePromptConsent);

        CPPUNIT_TEST(AuthorizeNoSessionToken);
        CPPUNIT_TEST(AuthorizeAPIWithSessionToken);
        CPPUNIT_TEST(AuthorizeSessionCookie);
        CPPUNIT_TEST(AuthorizeWrongSessionCookie);

#if BOOST_VERSION >= 107000
        CPPUNIT_TEST(AuthorizeWithSessionTokenGet);
        CPPUNIT_TEST(AuthorizeWithSessionTokenPost);
        CPPUNIT_TEST(AuthorizeFragmentCode);
        CPPUNIT_TEST(AuthorizeFragmentIdToken);
        CPPUNIT_TEST(AuthorizeFragmentToken);
        CPPUNIT_TEST(AuthorizeFragmentHybrid);
        CPPUNIT_TEST(AuthorizeFragmentNoResponseMode);
        CPPUNIT_TEST(AuthorizeFormPost);
        CPPUNIT_TEST(AuthorizeFormPostIdToken);
        CPPUNIT_TEST(AuthorizeFormPostToken);
        CPPUNIT_TEST(AuthorizeOktaFormPost);
        CPPUNIT_TEST(AuthorizeOktaFormPostIdToken);
        CPPUNIT_TEST(AuthorizeOktaFormPostToken);
        CPPUNIT_TEST(AuthorizeOktaFormPostTokenReferer);
#endif

        CPPUNIT_TEST_SUITE_END();

      public:
        AuthHandlerTestAuthorize();
        ~AuthHandlerTestAuthorize();

        void AuthorizeConfig();                 //!< make sure we have all necessary pieces in the config.json
        void AuthorizeWrongClientId();          //!< client id is not configured in config.json
        void AuthorizeMissingClientId();        //!< required: cleint_id
        void AuthorizeNotConfiguredCallback();  //!< required: callback_uri
        void AuthorizeWrongCallback();          //!< callback_uri is not configured in config.json

        void AuthorizeMissingResponseType();        //!< required: response_type
        void AuthorizeInvalidResponseType();        //!< required: response_type is invalid
        void AuthorizeAllResponseType();            //!< all possible response modes (space delimited)
        void AuthorizeAllAndInvalidResponseType();  //!< all possible response modes (space delimited)

        void AuthorizeInvalidResponseModeTokenQuery();    //!< query response type is invalid for response_type=token
        void AuthorizeInvalidResponseModeIdTokenQuery();  //!< query response type is invalid for response_type=id_token
        void AuthorizeInvalidResponseModeWrongValue();    //!< query response type is not in the list of supported
        void AuthorizeInvalidResponseModeOktaNoConfig();  //!< query response type is okta_post_message, but compatibility is not configured
        void AuthorizeInvalidResponseModeOktaOk();        //!< query response type is okta_post_message, but compatibility is configured

        void AuthorizeMissingScope();        //!< required: scope
        void AuthorizeInvalidScope();        //!< required: scope, invalid value provided
        void AuthorizeOkNoOpenIdScopes();    //!< required: scope, openid scope not provided
        void AuthorizeOkAndInvalidScopes();  //!< required: scope, invalid value provided and openid
        void AuthorizeAllOpenIDScopes();     //!< required: scope, all openid scopes, should not get a scope error

        void AuthorizeMissingState();       //!< required: state
        void AuthorizeWrongChallenge();     //!< valid only "S256" & "S512"
        void AuthorizeNoChallengeMethod();  //!< code_challenge provided without the method
        void AuthorizePromptInvalid();      //!< prompt set to invalid value
        void AuthorizePromptLogin();        //!< "login" currently not supported
        void AuthorizePromptConsent();      //!< "consent" currently not supported

        void AuthorizeNoSessionToken();  //!< session token should be defined if there is no session cookie set

        /// code flow with session token (base method)
        shared_ptr<AuthorizeFixBase> AuthorizeWithSessionTokenBase(
            const string& method,
            const string& response_mode = "query",
            const string& response_type = "code",
            unsigned int response_code = 302,
            const string& new_response_mode = "",
            map<string, string> headers = {});

        void AuthorizeWithSessionTokenGet();     //!< code flow GET
        void AuthorizeWithSessionTokenPost();    //!< code flow POST
        void AuthorizeAPIWithSessionToken();     //!< exec authorize using API
        void AuthorizeSessionCookie();           //!< session cookie set, correct
        void AuthorizeWrongSessionCookie();      //!< session cookie set, but invalid
        void AuthorizeFragmentCode();            //!< happy path, return = fragment
        void AuthorizeFragmentIdToken();         //!< get id token via redirect, return = fragment
        void AuthorizeFragmentToken();           //!< get token via redirect, return = fragment
        void AuthorizeFragmentHybrid();          //!< get both tokens and code via redirect, return = fragment
        void AuthorizeFragmentNoResponseMode();  //!< get both tokens and code via redirect, return = fragment, response mode is empty
        void AuthorizeFormPost();                //!< response_mode=form_post, code only
        void AuthorizeFormPostIdToken();         //!< response_mode=form_post, id_token only
        void AuthorizeFormPostToken();           //!< response_mode=form_post, token only

        void AuthorizeOktaFormPost();              //!< response_mode=okta_post_message with code only
        void AuthorizeOktaFormPostIdToken();       //!< response_mode=okta_post_message with id_token
        void AuthorizeOktaFormPostToken();         //!< response_mode=okta_post_message with token
        void AuthorizeOktaFormPostTokenReferer();  //!< response_mode=okta_post_message with token + referer
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
