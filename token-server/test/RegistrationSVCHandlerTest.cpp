/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <atomic>
#include <chrono>
#include <vector>

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/regex.hpp>

#include "BaseTest.h"
#include "RegistrationSVCHandlerTest.h"
#include <vx/URL.h>

#include <VxMessage.h>

#include <vx/ServiceProfileFactory.h>

#include <vx/web/VxHandler.h>
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTest.h"
#include "HandlerTestFix.h"
#include "RegistrationSVCHandler.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      shared_ptr<VxServer::Handlers::VxHandler> setup_registration_handler(std::shared_ptr<TokenServerContext> context, const vx::web::test::HandlerEnvironment& env) {
        shared_ptr<VxServer::Handlers::VxHandler> handler;
        handler.reset(new Handlers::RegistrationSVCHandler(*(context.get()), *env.server.get(), *env.service.get()));
        return handler;
      }

    }
  }  // namespace openid
}  // namespace vx

RegistrationSVCHandlerTest::RegistrationSVCHandlerTest() {
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext());
}

RegistrationSVCHandlerTest::~RegistrationSVCHandlerTest() {}

void RegistrationSVCHandlerTest::setUp() {
  client.reset(new HttpClient(DEBUG_HOST));

  context->setParam("profile_provider.type", "mysql");
  context->setParam("token_provider.type", "db");

  provider = context->GetProfileProvider();
  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  provider->DeleteUser(USER_NAME);
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});

  password_request =
      (boost::format("{\"grant_type\":\"password\",\"username\":\"%s\",\"password\":\"%s\"}") % USER_NAME % USER_PASSWORD).str();

  password_request_form =
      (boost::format("grant_type=password&username=%s&password=%s&client_id=aaa&client_secret=111") % USER_NAME % USER_PASSWORD).str();

  password_wrong_request =
      (boost::format("{\"grant_type\":\"unknown\",\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();

  password_bad_request =
      (boost::format("{\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();
}

void RegistrationSVCHandlerTest::tearDown() {
  if (provider) provider->DeleteUser(USER_NAME);
  // provider->DeleteRole(ROLE_1);
  // provider->DeleteRole(ROLE_2);
  provider.reset();
  // token_provider.reset();
}


/// @brief check the pattern, using capture values like (?<a>) (?<b>), only 2 so far
/// @param data input data to test
/// @param pattern expected pattern
/// @return index as -1 (not found), 0 - matched w/o capture, 1- "a" capture, 2 - "b" capture
int RegistrationSVCHandlerTest::get_assert_pattern(const string& data, const string& pattern) {
  boost::regex reg(pattern);
  boost::smatch match;
  int idx = -1;
  if (boost::regex_search(data, match, reg)) {
    auto val_a = match["a"].str();
    auto val_b = match["b"].str();
    if (!val_a.empty())
      idx = 1;
    else if (!val_b.empty())
      idx = 2;
    else
      idx = 0;
  }
  return idx;
}

/// @brief access /register/success
void RegistrationSVCHandlerTest::testRegisterSuccess() {
  SimpleWeb::CaseInsensitiveMultimap header;
  auto result =
      client->request("GET", "/register/success", "", header);
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", string("200"), code);

  auto data = result->content.string();
  auto idx = get_assert_pattern(data,
                                "<html>.*Registration is successful.*"
                                "You can login now to the Application on your phone.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /register/failure
void RegistrationSVCHandlerTest::testRegisterFailure() {
  SimpleWeb::CaseInsensitiveMultimap header;
  auto result =
      client->request("GET", "/register/failure", "", header);
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", string("200"), code);

  auto data = result->content.string();
  auto idx = get_assert_pattern(data,
                                "<html>.*Registration failed.*"
                                "Invalid registration code.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /register/success using handler
void RegistrationSVCHandlerTest::testRegisterSuccessHandler() {
  ptree pt;
  HandlerTestFix fix(context, pt, setup_registration_handler, "/register/success");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", (unsigned int)200, fix.code);

  auto idx = get_assert_pattern(fix.body,
                                "<html>.*Registration is successful.*"
                                "You can login now to the Application on your phone.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /register/failure using handler
void RegistrationSVCHandlerTest::testRegisterFailureHandler() {
  ptree pt;
  HandlerTestFix fix(context, pt, setup_registration_handler, "/register/failure");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", (unsigned int)200, fix.code);
  
  auto idx = get_assert_pattern(fix.body,
                                "<html>.*Registration failed.*"
                                "Invalid registration code.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /reset/success
void RegistrationSVCHandlerTest::testResetSuccess() {
  SimpleWeb::CaseInsensitiveMultimap header;
  auto result =
      client->request("GET", "/reset/success", "", header);
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", string("200"), code);

  auto data = result->content.string();
  auto idx = get_assert_pattern(data,
                                "<html>.*Password reset is successful.*"
                                "You can login now to the Application on your phone.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /reset/failure
void RegistrationSVCHandlerTest::testResetFailure() {
  SimpleWeb::CaseInsensitiveMultimap header;
  auto result =
      client->request("GET", "/reset/failure", "", header);
  auto code = result->status_code.substr(0, 3);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", string("200"), code);

  auto data = result->content.string();
  auto idx = get_assert_pattern(data,
                                "<html>.*Password Reset is not successful.*"
                                "Please try again.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /reset/success using handler
void RegistrationSVCHandlerTest::testResetSuccessHandler() {
  ptree pt;
  HandlerTestFix fix(context, pt, setup_registration_handler, "/reset/success");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", (unsigned int)200, fix.code);

  auto idx = get_assert_pattern(fix.body,
                                "<html>.*Password reset is successful.*"
                                "You can login now to the Application on your phone.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}

/// @brief access /reset/failure using handler
void RegistrationSVCHandlerTest::testResetFailureHandler() {
  ptree pt;
  HandlerTestFix fix(context, pt, setup_registration_handler, "/reset/failure");
  fix.Handle("GET");

  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", (unsigned int)200, fix.code);
  
  auto idx = get_assert_pattern(fix.body,
                                "<html>.*Password Reset is not successful.*"
                                "Please try again.*"
                                "</html>");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("html with message", (int)0, idx);
}


CPPUNIT_TEST_SUITE_REGISTRATION(RegistrationSVCHandlerTest);

#endif