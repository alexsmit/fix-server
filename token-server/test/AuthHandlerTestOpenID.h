#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing openid configuration endpoints
      class AuthHandlerTestOpenID : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestOpenID);

        CPPUNIT_TEST(OpenIDAuthServer);
        CPPUNIT_TEST(OpenIDAuthServerOpt);
        CPPUNIT_TEST(OpenIDBaseURL);
        CPPUNIT_TEST(OpenIDBaseURLOpt);

        CPPUNIT_TEST_SUITE_END();

      public:
        AuthHandlerTestOpenID();
        ~AuthHandlerTestOpenID();

        void OpenID(bool base = true, bool useGet = true);  //!< openid configuration base test.
        void OpenIDAuthServer();                            //!< openid configuration from authorization server full url
        void OpenIDAuthServerOpt();                         //!< OPTIONS openid configuration from authorization server full url
        void OpenIDBaseURL();                               //!< (resource owner) openid configuration from base issuer url
        void OpenIDBaseURLOpt();                            //!< (resource owner) OPTIONS openid configuration from base issuer url
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
