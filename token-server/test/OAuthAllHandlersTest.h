#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /**
       * @brief Testing all endpoints to make sure all of them are available and configuration is correct
       *
       */
      class OAuthAllHandlersTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(OAuthAllHandlersTest);

        CPPUNIT_TEST(testTokenServerContext);

        CPPUNIT_TEST(testAuthn);
        CPPUNIT_TEST(testAuthorize);
        CPPUNIT_TEST(testToken);
        CPPUNIT_TEST(testIntrospect);
        CPPUNIT_TEST(testRevoke);
        CPPUNIT_TEST(testLogout);
        CPPUNIT_TEST(testKeys);
        CPPUNIT_TEST(testUserinfo);
        CPPUNIT_TEST(testOpenid);

        // CPPUNIT_TEST(testAuthServer); --- not implemented

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<TokenServerContext> context;
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;

      public:
        OAuthAllHandlersTest();
        ~OAuthAllHandlersTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< ccpunit teardown

        void testTokenServerContext();  //!< Make sure it has a proper session token expriration property

        void testAuthn();       //!< verify enpoint is registered
        void testAuthorize();   //!< verify enpoint is registered
        void testToken();       //!< verify enpoint is registered
        void testIntrospect();  //!< verify enpoint is registered
        void testRevoke();      //!< verify enpoint is registered
        void testLogout();      //!< verify enpoint is registered
        void testKeys();        //!< verify enpoint is registered
        void testUserinfo();    //!< verify enpoint is registered
        void testAuthServer();  //!< verify enpoint is registered
        void testOpenid();      //!< verify enpoint is registered
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
