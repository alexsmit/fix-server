#pragma once
/*
 * Copyright 2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// Token API tests for /oauth/token endoint. This is an extention over the resource owner flow.
      class HandlerTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(HandlerTest);

        CPPUNIT_TEST(testConfig);
        CPPUNIT_TEST(testCors);
        CPPUNIT_TEST(testCorsAll);
        CPPUNIT_TEST(testGetToken);
        CPPUNIT_TEST(testGetTokenBadClient);
        CPPUNIT_TEST(testGetTokenWrongPassword);
        CPPUNIT_TEST(testGetTokenWrongUsername);
        CPPUNIT_TEST(testGetTokenInvalidPassword);
        CPPUNIT_TEST(testGetTokenInvalidUsername);
        CPPUNIT_TEST(testGetTokenRefresh);
        CPPUNIT_TEST(testGetTokenRefreshNoRotate);
        CPPUNIT_TEST(testGetTokenRefreshAfterExp);
        CPPUNIT_TEST(testGetTokenPin);
        CPPUNIT_TEST(testGetTokenWrongPin);
        CPPUNIT_TEST(testGetTokenInvalidPin);
        CPPUNIT_TEST(testGetTokenPinAfterExp);
        CPPUNIT_TEST(testGetTokenPinAfterExpOK);
        CPPUNIT_TEST(testGetTokenPin2Wrong);
        CPPUNIT_TEST(testGetTokenPin3Wrong);
        CPPUNIT_TEST(testGetTokenPin2Wrong2Wrong);
        CPPUNIT_TEST(testFingerPrint);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<TokenServerContext> context;
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<TokenProvider> token_provider;

      public:
        HandlerTest();
        ~HandlerTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< ccpunit teardown

        void testConfig();                   //!< validate that config.json has all required parameers
        void testCors();                     //!< cors headers for specific origin
        void testCorsAll();                  //!< cors headers for any origin
        void testGetToken();                 //!< get token
        void testGetTokenBadClient();        //!< get token, clientid/secret are wrong
        void testGetTokenWrongPassword();    //!< FAIL: not_authorized
        void testGetTokenWrongUsername();    //!< FAIL: not_authorized
        void testGetTokenInvalidPassword();  //!< FAIL: bad_request for some very long passwords
        void testGetTokenInvalidUsername();  //!< FAIL: bad_request for some very long user names
        void testGetTokenRefresh();          //!< get token, refresh
        void testGetTokenRefreshNoRotate();  //!< get token, refresh, rotation is disabled
        void testGetTokenRefreshAfterExp();  //!< FAIL: get token, try to refresh after expiration
        void testGetTokenPin();              //!< get token with PIN while refresh token is still good
        void testGetTokenWrongPin();         //!< FAIL: get token with wrong PIN
        void testGetTokenInvalidPin();       //!< FAIL: get token with invalid PIN (some long invalid value)
        void testGetTokenPinAfterExp();      //!< FAIL: get token with PIN after expiration
        void testGetTokenPinAfterExpOK();    //!< get token with PIN after expiration when it is allowed in config.json
        void testGetTokenPin2Wrong();        //!< 2 wrong pins, should succeed with correct pin after
        void testGetTokenPin3Wrong();        //!< 3 wrong pins, should fail with correct pin after, refresh should fail as well
        void testGetTokenPin2Wrong2Wrong();  //!< 2 wrong pins, correct pin, 2 wrong should succeed with correct pin after
        void testFingerPrint();              //!< /fingerprint request (using default prod/dev certs, subject to change)
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
