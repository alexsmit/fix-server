/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestOpenID.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

AuthHandlerTestOpenID::AuthHandlerTestOpenID() {}
AuthHandlerTestOpenID::~AuthHandlerTestOpenID() {}

/// fixture class to test /openid-configuration endpoint
class OpenIdRequestFix : public AuthorizeFixBase {
public:
  bool baseURL; //!< if true uses just url, if false - authorization server + url 
  bool useGet; //!< if true - run GET, OPTIONS if false
  string url; //!< openid well known url
  string urlAuthServer; //!< authorization server base URL

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param baseURL if true - testing "basic" openid configuration for simple resource-owner flow
  /// @param useGet if true - run GET, OPTIONS if false
  OpenIdRequestFix(shared_ptr<TokenServerContext> context,
                   bool baseURL = true,
                   bool useGet = true)
      : AuthorizeFixBase(context), baseURL(baseURL), useGet(useGet) {
    urlAuthServer = "/oauth2/default";
    url = "/.well-known/openid-configuration";
  }

  /// run request, parse result
  void Handle(const string& method = "POST") {
    stringstream sin;
    stringstream sout;

    header.emplace("Origin", "https://jwt.io");

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    Handlers::TokenHandler tokenHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = baseURL ? url : (urlAuthServer + url);
    env.no_req->method = (useGet) ? "GET" : "OPTIONS";
    env.no_req->header = header;
    env.Handle();

    ParseResult(sout);
  }
};


void AuthHandlerTestOpenID::OpenIDAuthServer() {
  OpenID(false, true);
}

void AuthHandlerTestOpenID::OpenIDAuthServerOpt() {
  OpenID(false, false);
}

void AuthHandlerTestOpenID::OpenIDBaseURL() {
  OpenID(true, true);
}
void AuthHandlerTestOpenID::OpenIDBaseURLOpt() {
  OpenID(true, false);
}

void assert_ptree_has_array(ptree& pt, const string key, set<string> arr) {
  auto ch = pt.get_child_optional(key);
  CPPUNIT_ASSERT_MESSAGE((string("contains a key ") + key), !!ch);
  size_t cnt = 0;
  for (auto& kv : *ch) {
    auto val = kv.second.data();
    CPPUNIT_ASSERT_MESSAGE((string("key is defined ") + val), arr.find(val) != arr.end());
    cnt++;
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Contains same number of elements", arr.size(), cnt);
}

/// @param base if true - testing "basic" openid configuration for simple resource-owner flow
/// @param useGet if true - running GET method, otherwise - POST
void AuthHandlerTestOpenID::OpenID(bool base, bool useGet) {
  OpenIdRequestFix fix(context, base, useGet);

  fix.Handle();
  // write_json(cout, fix.pt_header);
  // write_json(cout, fix.pt_body);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  if (!useGet) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("Allow", string("GET,OPTIONS"), fix.pt_header.get("Allow", ""));
  }
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Cache-Control", string("max-age=86400, must-revalidate"), fix.pt_header.get("Cache-Control", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Origin", string("https://jwt.io"), fix.pt_header.get("Access-Control-Allow-Origin", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Access-Control-Allow-Credentials", string("true"), fix.pt_header.get("Access-Control-Allow-Credentials", ""));

  if (!useGet) return;

  auto svc2 = context->getService("token");
  TokenProvider* token_svc = dynamic_cast<TokenProvider*>(svc2.get());
  if (token_svc == NULL) throw runtime_error("unable to get a service 'token'");
  // auto issuer = token_svc->getIssuer(*context);

  auto sec = context->config->Section("token_provider");
  auto algo = sec["algo"].str();
  auto server_id = sec["server_id"].str();
  if (server_id.empty()) server_id = "default";

  string issuer, jwksURL;

  if (base) {
    issuer = "http://token-server:8082/";  // per server context initialization
    jwksURL = issuer + ".well-known/jwks.json";
  }
  else {
    issuer = string("https://token-server:8482/oauth2/") + server_id;
    jwksURL = (boost::format("%s/.well-known/jwks.json") % issuer).str();
  }

  auto& pt = fix.pt_body;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("issuer", issuer, pt.get("issuer", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("jwks_uri", jwksURL, pt.get("jwks_uri", ""));

  if (base) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("number of elements", size_t(5), pt.size());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("token_endpoint", (issuer + "oauth/token"), pt.get("token_endpoint", ""));
    CPPUNIT_ASSERT_MESSAGE("token_endpoint_auth_methods_supported", pt.count("token_endpoint_auth_methods_supported") > 0);
    assert_ptree_has_array(pt, "token_endpoint_auth_methods_supported", {"client_secret_post", "client_secret_basic"});
    CPPUNIT_ASSERT_MESSAGE("id_token_signing_alg_values_supported", pt.count("id_token_signing_alg_values_supported") > 0);
    assert_ptree_has_array(pt, "id_token_signing_alg_values_supported", {"RS256", "RS384", "RS512"});
    return;
  }

  auto oauthPrefix = (boost::format("%s/v1") % issuer).str();
  CPPUNIT_ASSERT_MESSAGE("id_token_signing_alg_values_supported", pt.count("id_token_signing_alg_values_supported") > 0);
  CPPUNIT_ASSERT_MESSAGE("token_endpoint_auth_methods_supported", pt.count("token_endpoint_auth_methods_supported") > 0);
  CPPUNIT_ASSERT_MESSAGE("scopes_supported", pt.count("scopes_supported") > 0);
  CPPUNIT_ASSERT_MESSAGE("code_challenge_methods_supported", pt.count("code_challenge_methods_supported") > 0);
  assert_ptree_has_array(pt, "code_challenge_methods_supported", {"S256", "S512"});

  CPPUNIT_ASSERT_MESSAGE("response_types_supported", pt.count("response_types_supported") > 0);
  assert_ptree_has_array(pt, "response_types_supported", {"code", "token", "id_token"});

  CPPUNIT_ASSERT_MESSAGE("response_modes_supported", pt.count("response_modes_supported") > 0);
  assert_ptree_has_array(pt, "response_modes_supported", {"query", "form_post", "fragment"});

  CPPUNIT_ASSERT_EQUAL_MESSAGE("authorization_endpoint", (oauthPrefix + "/authorize"), pt.get("authorization_endpoint", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_endpoint", (oauthPrefix + "/token"), pt.get("token_endpoint", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("userinfo_endpoint", (oauthPrefix + "/userinfo"), pt.get("userinfo_endpoint", ""));
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestOpenID);

#endif
