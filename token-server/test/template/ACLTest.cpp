#ifdef TEMPLATE_DEF

#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <boost/lexical_cast.hpp>
#include <chrono>
#include <vector>
#include <vx/HttpParser.h>

#include "ACLTest.h"

using namespace std;
using namespace VxServer;

ACLTest::ACLTest() {}

ACLTest::~ACLTest() {}

void ACLTest::setUp() {
}

void ACLTest::tearDown() {
}

CPPUNIT_TEST_SUITE_REGISTRATION(ACLTest);
#endif