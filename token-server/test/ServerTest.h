/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _SERVER_TEST_H
#define _SERVER_TEST_H

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      /// all token server endpoints tests
      class ServerTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ServerTest);

        CPPUNIT_TEST(testJWKS);
        CPPUNIT_TEST(testOpenid);
        CPPUNIT_TEST(testInvalidGrant);
        CPPUNIT_TEST(testGetToken);
        CPPUNIT_TEST(testGetTokenBadClient);

        CPPUNIT_TEST(testGetTokenBadUsername);
        CPPUNIT_TEST(testGetTokenBadPassword);

        CPPUNIT_TEST(testGetTokenMany);
        CPPUNIT_TEST(testGetTokenRefresh);
        CPPUNIT_TEST(testGetTokenRefreshBad);
        CPPUNIT_TEST(testGetTokenRefreshPINOK);

        CPPUNIT_TEST(testGetTokenRefreshStolen);
        CPPUNIT_TEST(testGetTokenPin);
        CPPUNIT_TEST(testGetTokenPinWrongOneTry);
        CPPUNIT_TEST(testGetTokenPinWrongMultiple);
        CPPUNIT_TEST(testGetTokenRefreshExpired);
        CPPUNIT_TEST(testInfo);
        CPPUNIT_TEST(testResetInfo);
        CPPUNIT_TEST(testBadRequest);
        CPPUNIT_TEST(testFingerprint);
        CPPUNIT_TEST(testGetTokenForm);

        CPPUNIT_TEST(testAdminRole);
        CPPUNIT_TEST(testUserRole);
        CPPUNIT_TEST(testOtherRole);
        CPPUNIT_TEST(testDifferentRole);
        CPPUNIT_TEST(testRoleNotRequired);

        CPPUNIT_TEST(testGetClientCredentialsToken);
        CPPUNIT_TEST(testGetClientCredentialsTokenFail);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ProfileProvider> provider;
        shared_ptr<HttpClient> client;
        string password_request, password_wrong_request, password_bad_request;
        string password_request_form;
        shared_ptr<TokenServerContext> context;

        void getToken(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token);
        void getTokenForm(SimpleWeb::CaseInsensitiveMultimap &header, ptree &pt_token);
        void validateJWKS(string jwks);

      public:
        ServerTest();
        ~ServerTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

        int get_assert_pattern(const string& data, const string& pattern);

        void testJWKS();    //!< get JWKS from public endpoint and validate
        void testOpenid();  //!< get openid config, verify data

        void testInvalidGrant();  //!< send invalid grant request to /token endpoint

        void testGetToken();             //!< get token, validate all claims
        void testGetTokenBadClient();    //!< get token, using invalid client
        void testGetTokenBadUsername();  //!< try to get token using wrong username
        void testGetTokenBadPassword();  //!< try to get token using wrong password

        void testGetTokenMany();  //!< get token from multiple threads. all should be valid.
        void testGetTokenRefresh();
        void testGetTokenRefreshBad();
        void testGetTokenRefreshPINOK();
        void testGetTokenRefreshStolen();
        void testGetTokenPin();
        void testGetTokenPinWrongOneTry();
        void testGetTokenPinWrongMultiple();

        void testGetTokenRefreshExpired();

        void testInfo();
        void testResetInfo();
        void testBadRequest();
        void testFingerprint();

        void testGetTokenForm();

        // tests calling backend with different tokens

        void testAdminRole();
        void testUserRole();
        void testOtherRole();
        void testDifferentRole();
        void testRoleNotRequired();

        void testGetClientCredentialsToken();
        void testGetClientCredentialsTokenFail();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
