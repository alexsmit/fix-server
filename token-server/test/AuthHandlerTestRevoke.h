#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

#include "AuthHandlerTest.h"

using namespace std;
using namespace vx::openid::test;

namespace vx {
  namespace openid {
    namespace test {
      /// testing endpoint /v1/revoke
      class AuthHandlerTestRevoke : public AuthHandlerTest {
        CPPUNIT_TEST_SUITE(AuthHandlerTestRevoke);

        CPPUNIT_TEST(RevokeWrongClientId);
        CPPUNIT_TEST(RevokeNoToken);
        CPPUNIT_TEST(RevokeWrongTokenType);
        CPPUNIT_TEST(RevokeWrongRefreshToken);
        CPPUNIT_TEST(RevokeRefreshExpired);
        CPPUNIT_TEST(RevokeRefresh);
        CPPUNIT_TEST(RevokeRefreshBasicAuth);
        CPPUNIT_TEST(RevokeAccessInvalid);
        CPPUNIT_TEST(RevokeAccess);
        CPPUNIT_TEST(RevokeAccessBasicAuth);
        CPPUNIT_TEST(RevokeAccessExpired);
        CPPUNIT_TEST(RevokeId);
        CPPUNIT_TEST(RevokeIdBasicAuth);
        CPPUNIT_TEST(RevokeIdExpired);
        CPPUNIT_TEST(RevokeTwiceRefresh);
        CPPUNIT_TEST(RevokeTwiceAccess);
        CPPUNIT_TEST(RevokeRefreshAfterAccess);
        CPPUNIT_TEST(RevokeAccessAfterRefresh);
        CPPUNIT_TEST(RevokeOptions);
        CPPUNIT_TEST(RevokeRefreshNoClientSecret);
        CPPUNIT_TEST(RevokeRefreshBasicJustClientID);
        CPPUNIT_TEST(RevokeAccessNoClientSecret);
        CPPUNIT_TEST(RevokeAccessBasicJustClientID);
        CPPUNIT_TEST(RevokePKCERefreshNoClientSecret);
        CPPUNIT_TEST(RevokePKCERefreshBasicJustClientID);
        CPPUNIT_TEST(RevokePKCEAccessNoClientSecret);
        CPPUNIT_TEST(RevokePKCEAccessBasicJustClientID);

        CPPUNIT_TEST_SUITE_END();

      public:
        AuthHandlerTestRevoke();
        ~AuthHandlerTestRevoke();

        void RevokeWrongClientId();                 //!< wrong input parameters: wrong client_id
        void RevokeNoToken();                       //!< token parameter is missing
        void RevokeWrongTokenType();                //!< token_type_hint is wrong
        void RevokeWrongRefreshToken();             //!< using wrong refresh token
        void RevokeRefreshExpired();                //!< using expired refresh token
        void RevokeRefresh();                       //!< using valid refresh token
        void RevokeRefreshBasicAuth();              //!< using valid refresh token and Basic Auth
        void RevokeAccessInvalid();                 //!< using invalid access token
        void RevokeAccess();                        //!< using valid access token
        void RevokeAccessBasicAuth();               //!< using valid access token and Basic Auth
        void RevokeAccessExpired();                 //!< using expired access token
        void RevokeId();                            //!< using id token (error)
        void RevokeIdBasicAuth();                   //!< using id token and Basic authentication (error)
        void RevokeIdExpired();                     //!< using expired id token (error)
        void RevokeTwiceRefresh();                  //!< revoke refresh token second time (ok, error output only)
        void RevokeTwiceAccess();                   //!< revoke access token second time (ok, error output only)
        void RevokeRefreshAfterAccess();            //!< revoke refresh token after revoking access token (ok, error output only)
        void RevokeAccessAfterRefresh();            //!< revoke access token after revoking refresh token (ok, no error)
        void RevokeOptions();                       //!< OPTIONS
        void RevokeRefreshNoClientSecret();         //!< should fail if client_secret not provided
        void RevokeRefreshBasicJustClientID();      //!< should fail if client_secret not provided
        void RevokeAccessNoClientSecret();          //!< should fail if client_secret not provided
        void RevokeAccessBasicJustClientID();       //!< should fail if client_secret not provided
        void RevokePKCE(TokenOps& ops);             //!< run PKCE flow
        void RevokePKCERefreshNoClientSecret();     //!< should scucceed if client_secret not provided PKCE only
        void RevokePKCERefreshBasicJustClientID();  //!< should scucceed if client_secret not provided PKCE only
        void RevokePKCEAccessNoClientSecret();      //!< should scucceed if client_secret not provided PKCE only
        void RevokePKCEAccessBasicJustClientID();   //!< should scucceed if client_secret not provided PKCE only
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
