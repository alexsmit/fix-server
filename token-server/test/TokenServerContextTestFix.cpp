/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/web/NoRequest.h>
#include <vx/StringUtil.h>
#include "TokenServerContextTestFix.h"

using namespace std;
using namespace VxServer;
// using namespace vx::web::test;

TokenServerContextTestFix::TokenServerContextTestFix(
    TokenServerContext& context,
    const string& content,
    map<string, string> headers) : context(context) {
  // context.reset(new ServerContext(80, 0, 3));
  // context->Init(section, "config.json");
  sin.str(content);
  no_req.reset(new NoRequest(sin));
  no_res.reset(new NoResponse(sout));
  request.reset(new RequestSpec<shared_ptr<NoRequest>>(no_req, MAX_POST_SIZE));
  response.reset(new ResponseSpec<shared_ptr<NoResponse>>(no_res, false));

  has_exception = false;

  for (auto& kv : headers) {
    no_req->header.emplace(kv.first, kv.second);
  }
}

TokenServerContextTestFix::~TokenServerContextTestFix() {
  if (capture_started) StopCapture();
}

/// default constructor for capturing only
// TokenServerContextTestFix::TokenServerContextTestFix() {
//   context.reset(new ServerContext(80, 0, 3));
//   context->Init("test", "config.json");
// }

/// capture stderr & stdout
void TokenServerContextTestFix::StartCapture() {
  if (capture_started) return;
  capture_started = true;
  out.str("");
  out_err.str("");
  std::streambuf* out_buf(std::cout.rdbuf(out.rdbuf()));
  buf_output = out_buf;
  std::streambuf* out_err_buf(std::cerr.rdbuf(out_err.rdbuf()));
  buf_err_output = out_err_buf;
}

/// stop capture
void TokenServerContextTestFix::StopCapture() {
  if (!capture_started) return;
  capture_started = false;
  std::cout.rdbuf(buf_output);
  std_output = out.str();
  std::cerr.rdbuf(buf_err_output);
  std_err_output = out_err.str();
}

void TokenServerContextTestFix::Parse() {
  vector<string> v1;

  auto val = sout.str();
  code = "200";
  pt_body.clear();
  body = val;

  auto ix = val.find("\r\n\r\n");
  if (ix != string::npos) {
    body = val.substr(ix + 4);
    auto head = val.substr(0, ix);
    boost::algorithm::split(v1, head, boost::algorithm::is_any_of("\r\n"), boost::token_compress_on);
    for (auto& line : v1) {
      auto vline = StringUtil::split(line, ":", true);
      auto key = vline[0];
      auto val = vline[1];
      StringUtil::trim(key);
      StringUtil::trim(val);
      headers.emplace(key, val);
    }
    try {
      stringstream ss(body);
      read_json(ss, pt_body);
    }
    catch (const std::exception& e) {
    }

    auto it = headers.find("Status");
    if (it != headers.end()) {
      auto status = it->second;
      auto vstatus = StringUtil::split(status, " ");
      code = vstatus[0];
    }
  }
}

void TokenServerContextTestFix::Run(std::function<void()> fun) {
  has_exception = false;

  if (fun == nullptr) return;
  StartCapture();

  try
  {
    fun();
  }
  catch(const std::exception& e)
  {
    has_exception = true;
    exception_message = e.what();
  }
  
  StopCapture();
  Parse();
}