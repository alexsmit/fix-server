/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PROFILE_SVC_REMOTE_TEST_H
#define _PROFILE_SVC_REMOTE_TEST_H
#include "ProfileSVCBaseTest.h"

namespace vx {
  namespace openid {
    namespace test {
      /// profile provider test, remote implementation
      class ProfileSVCRemoteTest : public ProfileSVCBaseTest, public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ProfileSVCRemoteTest);

        CPPUNIT_TEST(testAuthenticate);
        CPPUNIT_TEST(testAuthenticateUUID);
        CPPUNIT_TEST(testAuthenticatePin);
        CPPUNIT_TEST(testAuthenticatePinUUID);
        CPPUNIT_TEST(testGetProfileData);
        CPPUNIT_TEST(testGetProfileDataUUID);

        CPPUNIT_TEST(testConfirmationEmailParam);

        CPPUNIT_TEST(testSendConfirmationEmailTemplate);
        CPPUNIT_TEST(testSendConfirmationEmailTitle);
        CPPUNIT_TEST(testSendConfirmationEmailFrom);
        CPPUNIT_TEST(testSendConfirmationEmailTo);
        CPPUNIT_TEST(testSendConfirmationEmail);
        CPPUNIT_TEST(testSendConfirmationEmailOff);
        CPPUNIT_TEST(testSendConfirmationEmailNoSVC);
        CPPUNIT_TEST(testSendConfirmationEmailBadSVC);

        CPPUNIT_TEST(testRegisterUser);
        CPPUNIT_TEST(testValidateRegistration);
        CPPUNIT_TEST(testValidatePendingRegistration);
        CPPUNIT_TEST(testConfirmRegistration);

        CPPUNIT_TEST(testReset);
        CPPUNIT_TEST(testUpdatePassword);
        CPPUNIT_TEST(testUpdatePasswordBad);
        CPPUNIT_TEST(testVerifyResetToken);
        CPPUNIT_TEST(testVerifyResetTokenBad);

        CPPUNIT_TEST(testVerifyResetToken);
        CPPUNIT_TEST(testVerifyResetTokenBad);

        CPPUNIT_TEST(testChangePin);
        CPPUNIT_TEST(testChangePassword);

        CPPUNIT_TEST_SUITE_END();

      public:
        ProfileSVCRemoteTest();
        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
