#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <boost/lexical_cast.hpp>
#include <chrono>
#include <vector>
#include <vx/HttpParser.h>

#include "VxIndexTest.h"
#include <vx/web/VxHandler.h>
#include <vx/Metadata.h>
#include <vx/openid/models/ModelsMetadata.h>

using namespace std;
using namespace VxServer;
using namespace vx::openid::test;

class MyVxIndex : public VxIndex {
public:
  std::string value;
  MyVxIndex() {}
  MyVxIndex(const string& value) : value(value) {}
  std::string operator[](const std::string& key) const override {
    return value;
  }
};

struct MyTestObject {
  int foo;  //!< foo property
  /// de-serialize from VxIndex objects
  friend MyTestObject& operator>>(VxServer::VxIndex& j, MyTestObject& request) {
    fromVxIndex(j, request);
    return request;
  }
  /// metadata for serialization
  constexpr static auto properties = std::make_tuple(
      property(&MyTestObject::foo, "foo"));
};

struct MyTestObjectBool {
  bool foo;  //!< foo property
  /// de-serialize from VxIndex objects
  friend MyTestObjectBool& operator>>(VxServer::VxIndex& j, MyTestObjectBool& request) {
    fromVxIndex(j, request);
    return request;
  }
  /// metadata for serialization
  constexpr static auto properties = std::make_tuple(
      property(&MyTestObjectBool::foo, "foo"));
};


VxIndexTest::VxIndexTest() {}

VxIndexTest::~VxIndexTest() {}

void VxIndexTest::setUp() {
}

void VxIndexTest::tearDown() {
}

void VxIndexTest::testEmpty() {
  MyVxIndex vx;
  MyTestObject obj;
  vx >> obj;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("zero", (int)0, obj.foo);
}

void VxIndexTest::testBadString() {
  MyVxIndex vx("some-non-integer");
  MyTestObject obj;
  vx >> obj;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("zero", (int)0, obj.foo);
}

void VxIndexTest::testGoodString() {
  MyVxIndex vx("123");
  MyTestObject obj;
  vx >> obj;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("123", (int)123, obj.foo);
}

//  auto bval = (str == "true" || str == "1" || str == "True" || str == "Yes" || str == "yes" || str == "Y" || str == "y");


void VxIndexTest::testTrue() {
  MyVxIndex vx("true");
  MyTestObjectBool obj;
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "1";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "True";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "Yes";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "yes";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "Y";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "y";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "TRUE";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "ON";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);

  vx.value = "on";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("ok", obj.foo);
}

void VxIndexTest::testFalse() {
  MyVxIndex vx("false");
  MyTestObjectBool obj;
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("false", !obj.foo);

  vx.value = "0";
  vx >> obj;
  CPPUNIT_ASSERT_MESSAGE("false", !obj.foo);
}


CPPUNIT_TEST_SUITE_REGISTRATION(VxIndexTest);
