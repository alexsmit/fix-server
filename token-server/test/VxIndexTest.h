#pragma once
/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <stdafx.h>


using namespace std;

namespace vx {
  namespace openid {
    namespace test {
      class VxIndexTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(VxIndexTest);

        CPPUNIT_TEST(testEmpty);
        CPPUNIT_TEST(testBadString);
        CPPUNIT_TEST(testGoodString);
        CPPUNIT_TEST(testTrue);
        CPPUNIT_TEST(testFalse);

        CPPUNIT_TEST_SUITE_END();

      private:
      public:
        VxIndexTest();
        ~VxIndexTest();

        void setUp() override;
        void tearDown() override;

        void testEmpty();       //!< empty string -> 0
        void testBadString();   //!< bad string  -> 0
        void testGoodString();  //!< converg fromn number string
        void testTrue();        //!< conversion of some values like 1,true,T,y,yes as "true"
        void testFalse();       //!< anything as non-true is false
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
