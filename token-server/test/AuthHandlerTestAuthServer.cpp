/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestAuthServer.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

AuthHandlerTestAuthServer::AuthHandlerTestAuthServer() {}
AuthHandlerTestAuthServer::~AuthHandlerTestAuthServer() {}

/// fixture class to test /openid-configuration endpoint
class AuthServerRequestFix : public AuthorizeFixBase {
public:
  bool baseURL; //!< if true uses just url, if false - authorization server + url 
  bool useGet; //!< if true - run GET, OPTIONS if false
  string url; //!< openid well known url
  string urlAuthServer; //!< authorization server base URL

  /// create fixture object
  /// @param context current server context, object created by setup should be used here
  /// @param baseURL if true - testing "basic" openid configuration for simple resource-owner flow
  /// @param useGet if true - run GET, OPTIONS if false
  AuthServerRequestFix(shared_ptr<TokenServerContext> context,
                   bool baseURL = true,
                   bool useGet = true)
      : AuthorizeFixBase(context), baseURL(baseURL), useGet(useGet) {
    urlAuthServer = "/oauth2/default";
    url = "/.well-known/oauth-authorization-server";
  }

  /// run request, parse result
  void Handle(const string& method = "POST") {
    stringstream sin;
    stringstream sout;

    header.emplace("Origin", "https://jwt.io");

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    Handlers::TokenHandler tokenHandler(*(context.get()), *env.server.get(), *env.service.get());
    string path = baseURL ? url : (urlAuthServer + url);
    env.no_req->path = path;
    env.no_req->method = (useGet) ? "GET" : "OPTIONS";
    env.no_req->header = header;
    env.Handle();

    ParseResult(sout);
  }
};

void AuthHandlerTestAuthServer::AuthServer() {
 AuthServerRequestFix  fix(context, false, true);

  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  auto& pt = fix.pt_body;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error",string("bad_request"), pt.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description",string("not implemented"), pt.get("error_description",""));
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestAuthServer);

#endif
