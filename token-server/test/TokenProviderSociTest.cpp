/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <stdlib.h>
#include <sys/stat.h>
#include <boost/lexical_cast.hpp>
#include <chrono>
#include <vector>

#include "TokenProviderSociTest.h"

using namespace vx::openid::test;

TokenProviderSociTest::TokenProviderSociTest() : TokenProviderBaseTest("soci") { provider_name = "token.mysql"; }
void TokenProviderSociTest::setUp() { setUpInternal(); }
void TokenProviderSociTest::tearDown() { tearDownInternal(); }

CPPUNIT_TEST_SUITE_REGISTRATION(TokenProviderSociTest);
