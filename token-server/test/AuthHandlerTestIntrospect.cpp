/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>
#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestIntrospect.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>

#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;

using namespace vx::web::test;
using namespace vx::openid::test;

AuthHandlerTestIntrospect::AuthHandlerTestIntrospect() {}
AuthHandlerTestIntrospect::~AuthHandlerTestIntrospect() {}

//-----------------------------------------

/// fixture class to test /authorize endpoint
class IntrospectRequestFix : public AuthorizeFixBase {
public:
  IntrospectRequest& req; //!< request object, set by the constructor
  string url;             //!< url, set by the constructor

  /// create fixture object
  /// @param context current server context, should use created by setup
  /// @param req request object, set by the test
  /// @param url default or specific url, set by the test
  IntrospectRequestFix(shared_ptr<TokenServerContext> context,
                       IntrospectRequest& req,
                       string url = "/oauth2/default/v1/introspect") : AuthorizeFixBase(context), req(req), url(url) {
  }

  /// run request and parse result
  void Handle(const string& method = "POST") {
    ptree pt;
    toPtree<IntrospectRequest>(req, pt);
    stringstream pt_out;
    write_json(pt_out, pt);

    string str_out = pt_out.str();
    string slen = boost::lexical_cast<string>(str_out.length());
    stringstream sin(pt_out.str());
    stringstream sout;

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());
    env.no_req->path = url;
    env.no_req->method = "POST";
    env.no_req->header = header;
    env.no_req->header.emplace("Content-Type", "application/json");
    env.no_req->header.emplace("Content-Length", slen);
    env.Handle();

    ParseResult(sout);
  }
};


void AuthHandlerTestIntrospect::IntrospectNoClientId() {
  IntrospectRequest r;
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectWrongClientId() {
  IntrospectRequest r;
  r.client_id = "wrong_client_id";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_client_id_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectNoToken() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("token is not provided"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectWrongTokenType() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = "token";
  r.token_type_hint = "wrong_hint";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid token type"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectRefreshWrongToken() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token_type_hint = "refresh_token";
  r.token = "wrong_refresh";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid refresh token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectRefresh() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};
  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  auto rec = provider->LoadUser(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("active", resp.active);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("Bearer"), resp.token_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", token_scope, resp.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string(CLIENT_ID), resp.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), resp.username);
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp >= (cur + 1));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", rec.email, resp.sub);
  CPPUNIT_ASSERT_MESSAGE("jti", !resp.jti.empty());
}

void AuthHandlerTestIntrospect::IntrospectRefreshExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("refresh_token", "");
  r.token_type_hint = "refresh_token";
  IntrospectRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};
  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  CPPUNIT_ASSERT_MESSAGE("active", !resp.active);
  CPPUNIT_ASSERT_MESSAGE("token_type", resp.token_type.empty());
  CPPUNIT_ASSERT_MESSAGE("scope", resp.scope.empty());
  CPPUNIT_ASSERT_MESSAGE("client_id", resp.client_id.empty());
  CPPUNIT_ASSERT_MESSAGE("username", resp.username.empty());
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp == 0);
  CPPUNIT_ASSERT_MESSAGE("sub", resp.sub.empty());
  CPPUNIT_ASSERT_MESSAGE("sub", resp.jti.empty());
}

void AuthHandlerTestIntrospect::IntrospectAccessInvalid() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = "wrong access token";
  r.token_type_hint = "access_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or expired access token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectAccess() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};

  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  auto rec = provider->LoadUser(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("active", resp.active);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("Bearer"), resp.token_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", token_scope, resp.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string(CLIENT_ID), resp.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), resp.username);
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp >= (cur + 1));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", rec.userid, resp.sub);
  CPPUNIT_ASSERT_MESSAGE("jti", !resp.jti.empty());
}

void AuthHandlerTestIntrospect::IntrospectAccessExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  context->ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  IntrospectRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or expired access token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectIdInvalid() {
  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = "wrong id token";
  r.token_type_hint = "id_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or expired id token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectId() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  context->ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "id_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};
  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  auto rec = provider->LoadUser(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("active", resp.active);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("token_type", string("Bearer"), resp.token_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("scope", token_scope, resp.scope);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("client_id", string(CLIENT_ID), resp.client_id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), resp.username);
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp >= (cur + 1));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sub", rec.userid, resp.sub);
  CPPUNIT_ASSERT_MESSAGE("jti", resp.jti.empty());
}

void AuthHandlerTestIntrospect::IntrospectIdExpired() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 1;
  context->ttl = 1;
  auto cur = ::time(NULL);
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "id_token";
  IntrospectRequestFix fix(context, r);

  cout << "W" << flush;
  std::this_thread::sleep_for(chrono::milliseconds(2000));

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid or expired id token"), fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestIntrospect::IntrospectAccessRevoked() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 100;
  context->ttl = 100;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  auto access_token = ops.token_metadata.get("access_token","");
  token_provider->DeleteTokenByNonce(access_token);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("access_token", "");
  r.token_type_hint = "access_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};
  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  CPPUNIT_ASSERT_MESSAGE("active", !resp.active);
  CPPUNIT_ASSERT_MESSAGE("token_type", resp.token_type.empty());
  CPPUNIT_ASSERT_MESSAGE("scope", resp.scope.empty());
  CPPUNIT_ASSERT_MESSAGE("client_id", resp.client_id.empty());
  CPPUNIT_ASSERT_MESSAGE("username", resp.username.empty());
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp == 0);
  CPPUNIT_ASSERT_MESSAGE("sub", resp.sub.empty());
  CPPUNIT_ASSERT_MESSAGE("jti", resp.jti.empty());
}

void AuthHandlerTestIntrospect::IntrospectIdRevoked() {
  string token_scope = "openid email offline_access";
  context->refresh_ttl = 100;
  context->ttl = 100;
  TokenOps ops(context);
  ops.token(USER_NAME, USER_PASSWORD, token_scope);

  auto id_token = ops.token_metadata.get("id_token","");
  token_provider->DeleteTokenByNonce(id_token);

  IntrospectRequest r;
  r.client_id = CLIENT_ID;
  r.token = ops.token_result.get("id_token", "");
  r.token_type_hint = "id_token";
  IntrospectRequestFix fix(context, r);

  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  IntrospectResponse resp = {};
  fromPtree<IntrospectResponse>(fix.pt_body, resp);

  CPPUNIT_ASSERT_MESSAGE("active", !resp.active);
  CPPUNIT_ASSERT_MESSAGE("token_type", resp.token_type.empty());
  CPPUNIT_ASSERT_MESSAGE("scope", resp.scope.empty());
  CPPUNIT_ASSERT_MESSAGE("client_id", resp.client_id.empty());
  CPPUNIT_ASSERT_MESSAGE("username", resp.username.empty());
  CPPUNIT_ASSERT_MESSAGE("exp", resp.exp == 0);
  CPPUNIT_ASSERT_MESSAGE("sub", resp.sub.empty());
  CPPUNIT_ASSERT_MESSAGE("jti", resp.jti.empty());
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestIntrospect);

#endif
