/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/web/WebServer.h>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;
using namespace boost::posix_time;
using namespace VxServer;

#include "../include/BaseTest.h"
#include <VxMessage.h>
#include <vx/web/HandlerEnvironment.h>
#include "AuthHandlerTestAuthn.h"
#include <vx/HttpParser.h>
#include <TokenHandler.h>
#include <AuthHandler.h>
#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>

using namespace std;
using namespace VxServer;
using namespace boost::property_tree;
using namespace vx::web::test;
using namespace vx::openid::test;

//-----------------------------------------

/// Implementation of a fixture class to test /authn endpoint operations
class AuthnRequestFix : public AuthorizeFixBase {
public:
  AuthRequest& req;  //!< request object, initialized in the constructor
  string url;        //!< url for the endpoint, initialized in the constructor

  /// create fixture object.
  /// @param context current server context, object created by setup should be used here
  /// @param req request object
  /// @param url endpoint if different from the default
  AuthnRequestFix(shared_ptr<TokenServerContext> context,
                  AuthRequest& req,
                  string url = "/api/v1/authn") : AuthorizeFixBase(context), req(req), url(url) {
  }

  /// Execute request and parse response
  void Handle(const string& method = "POST") {
    stringstream pt_out;
    stringstream sin;
    stringstream sout;

    if (method == "POST") {
      ptree pt;
      pt << req;
      write_json(pt_out, pt);
      sin.str(pt_out.str());
    }

    HandlerEnvironment env(sin, sout);
    Handlers::AuthHandler authHandler(*(context.get()), *env.server.get(), *env.service.get());

    env.no_req->header.emplace("Origin", "https://jwt.io");

    env.no_req->path = url;
    env.no_req->method = method;
    env.Handle();

    ParseResult(sout);
  }
};

AuthHandlerTestAuthn::AuthHandlerTestAuthn() {}

AuthHandlerTestAuthn::~AuthHandlerTestAuthn() {}

void AuthHandlerTestAuthn::testAuthn() {
  // set session token TTL
  context->ttl_session_token = 15;

  stringstream sout;
  AuthRequest r = {USER_NAME, USER_PASSWORD};
  AuthnRequestFix fix(context, r);
  fix.Handle();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, fix.code);

  AuthResponse resp{};
  fix.pt_body >> resp;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), resp.status);
  CPPUNIT_ASSERT_MESSAGE("sessionToken", !resp.sessionToken.empty());
  CPPUNIT_ASSERT_MESSAGE("expiresAt", !resp.expiresAt.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", string(USER_NAME), resp.profile.username);
  CPPUNIT_ASSERT_MESSAGE("profile.id", !resp.profile.id.empty());
  CPPUNIT_ASSERT_MESSAGE("profile.email", !resp.profile.email.empty());

  // check expiration time, should be less or equal to current time + 30 secs
  auto pd = from_iso_extended_string(resp.expiresAt);
  auto texp = boost::posix_time::to_time_t(pd);
  time_t tcurexp = ::time(NULL) + 15;
  CPPUNIT_ASSERT_MESSAGE("updated 15 sec session token expiration", texp <= tcurexp);
}

void AuthHandlerTestAuthn::testAuthnAPI() {
  // set session token TTL
  context->ttl_session_token = 15;
  context->setParam("token_provider.compatibility","");

  AuthRequest r = {USER_NAME, USER_PASSWORD};

  vx::openid::flow::AuthnFlow flow(*context, r);
  flow.Execute();
  auto result = flow.GetResult();

  AuthResponse resp{};
  result >> resp;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, (unsigned)flow.GetResultCode());

  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), resp.status);
  CPPUNIT_ASSERT_MESSAGE("sessionToken", !resp.sessionToken.empty());
  CPPUNIT_ASSERT_MESSAGE("expiresAt", !resp.expiresAt.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", string(USER_NAME), resp.profile.username);
  CPPUNIT_ASSERT_MESSAGE("profile.id", !resp.profile.id.empty());
  CPPUNIT_ASSERT_MESSAGE("profile.email", !resp.profile.email.empty());

  // check expiration time, should be less or equal to current time + 30 secs
  auto pd = from_iso_extended_string(resp.expiresAt);
  auto texp = boost::posix_time::to_time_t(pd);
  time_t tcurexp = ::time(NULL) + 15;
  CPPUNIT_ASSERT_MESSAGE("updated 15 sec session token expiration", texp <= tcurexp);
}

void AuthHandlerTestAuthn::testAuthnAPICompatibility() {
  // set session token TTL
  context->ttl_session_token = 15;
  context->setParam("token_provider.compatibility","OKTA");

  AuthRequest r = {USER_NAME, USER_PASSWORD};

  vx::openid::flow::AuthnFlow flow(*context, r);
  flow.Execute();
  auto result = flow.GetResult();

  AuthResponse resp{};
  result >> resp;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 200", (unsigned)200, (unsigned)flow.GetResultCode());

  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), resp.status);
  CPPUNIT_ASSERT_MESSAGE("sessionToken", !resp.sessionToken.empty());
  CPPUNIT_ASSERT_MESSAGE("expiresAt", !resp.expiresAt.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", string(USER_NAME), resp.profile.username);
  CPPUNIT_ASSERT_MESSAGE("profile.id", !resp.profile.id.empty());
  CPPUNIT_ASSERT_MESSAGE("profile.email", !resp.profile.email.empty());

  // check expiration time, should be less or equal to current time + 30 secs
  auto pd = from_iso_extended_string(resp.expiresAt);
  auto texp = boost::posix_time::to_time_t(pd);
  time_t tcurexp = ::time(NULL) + 15;
  CPPUNIT_ASSERT_MESSAGE("updated 15 sec session token expiration", texp <= tcurexp);
}

void AuthHandlerTestAuthn::testAuthnWrongCredentials() {
  stringstream sout;
  AuthRequest r = {USER_NAME, "wrong_password"};
  AuthnRequestFix fix(context, r);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 401", (unsigned)401, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_authorized"), fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_username_password_invalid, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthn::testAuthnBadUsername() {
  stringstream sout;
  AuthRequest r = {"", "password"};
  AuthnRequestFix fix(context, r);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_username_not_defined, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthn::testAuthnBadPassword() {
  AuthRequest r = {"username", ""};
  AuthnRequestFix fix(context, r);
  fix.Handle();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Response code 400", (unsigned)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", VxMessages::bad_request, fix.pt_body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", VxMessages::msg_password_not_defined, fix.pt_body.get("error_description", ""));
}

void AuthHandlerTestAuthn::testAuthnOptions() {
  AuthRequest r = {"username", ""};
  AuthnRequestFix fix(context, r);

  fix.Handle("OPTIONS");

  fix.assert_options("POST,OPTIONS");
  fix.assert_cors();
  fix.assert_no_cache();
}

void AuthHandlerTestAuthn::testAuthResponseToCompat() {
  AuthResponse resp{};

  auto cur = ::time(NULL);

  auto pexpired = boost::posix_time::from_time_t(cur);
  auto sexprired = to_iso_extended_string(pexpired);

  AuthResponse aresp = {
      sexprired,
      "SUCCESS",
      "token",
      {"userid",
       "username",
       "email",
       "firstName",
       "lastName"}};

  ptree pt, j;

  pt = aresp.toOKTA();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", string("SUCCESS"), pt.get("status", ""));
  string expectedExpired = sexprired + ".000Z";
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expiresAt", expectedExpired, pt.get("expiresAt", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", string("token"), pt.get("sessionToken", ""));

  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.id", ""), string("userid"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.passwordChanged", ""), string("2021-08-24T23:18:34.000Z"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.profile.login", ""), string("username"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.profile.firstName", ""), string("firstName"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.profile.lastName", ""), string("lastName"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.profile.locale", ""), string("en"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_embedded.user.profile.timeZone", ""), string("America/Los_Angeles"));
  CPPUNIT_ASSERT_EQUAL(pt.get("_links.cancel.href", ""), string(""));
}

void AuthHandlerTestAuthn::testAuthResponseFromCompat() {
  AuthResponse resp{};

  auto cur = ::time(NULL);

  auto pexpired = boost::posix_time::from_time_t(cur);
  auto sexprired = to_iso_extended_string(pexpired);

  AuthResponse aresp = {
      sexprired,
      "SUCCESS",
      "token",
      {"userid",
       "username",
       "email",
       "firstName",
       "lastName"}};

  ptree pt;
  pt = aresp.toOKTA();

  AuthResponse arespParsed;
  pt >> arespParsed;

  CPPUNIT_ASSERT_EQUAL_MESSAGE("expiresAt", aresp.expiresAt, arespParsed.expiresAt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", aresp.status, arespParsed.status);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", aresp.sessionToken, arespParsed.sessionToken);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.id", aresp.profile.id, arespParsed.profile.id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", aresp.profile.username, arespParsed.profile.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.email", aresp.profile.email, arespParsed.profile.email);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.firstName", aresp.profile.firstName, arespParsed.profile.firstName);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.lastName", aresp.profile.lastName, arespParsed.profile.lastName);
}

void AuthHandlerTestAuthn::testAuthResponseFromNoExp() {
  AuthResponse resp{};

  auto cur = ::time(NULL);

  auto pexpired = boost::posix_time::from_time_t(cur);
  auto sexprired = to_iso_extended_string(pexpired);

  AuthResponse aresp = {
      "",
      "SUCCESS",
      "token",
      {"userid",
       "username",
       "email",
       "firstName",
       "lastName"}};

  ptree pt;
  pt = aresp.toOKTA();

  AuthResponse arespParsed;
  pt >> arespParsed;

  CPPUNIT_ASSERT_MESSAGE("expiresAt", arespParsed.expiresAt.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("status", aresp.status, arespParsed.status);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("sessionToken", aresp.sessionToken, arespParsed.sessionToken);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.id", aresp.profile.id, arespParsed.profile.id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.username", aresp.profile.username, arespParsed.profile.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.email", aresp.profile.email, arespParsed.profile.email);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.firstName", aresp.profile.firstName, arespParsed.profile.firstName);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("profile.lastName", aresp.profile.lastName, arespParsed.profile.lastName);
}


CPPUNIT_TEST_SUITE_REGISTRATION(AuthHandlerTestAuthn);

#endif
