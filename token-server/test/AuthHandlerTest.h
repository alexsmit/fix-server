/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef _AUTH_SERVER_HANDLER_TEST_H
#define _AUTH_SERVER_HANDLER_TEST_H

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {

      /// base fixture class to handle Oauth2 endpoints. Allows to parse results, get the code, response payload, etc.
      class AuthorizeFixBase {
      public:
        shared_ptr<TokenServerContext> context;  //!< current server context, should use initialized in the setup method
        string body;                             //!< complete HTTP body output without headers
        ptree pt_body,                           //!< parsed JSON rsponse
            pt_header,                           //!< all header elements
            pt_params;                           //!< all query parameters
        bool in_query,                           //!< URL parameters are part of the query
            in_fragment;                         //!< URL parameters are part of the fragment
        unsigned int code;                       //!< HTTP result code

        SimpleWeb::CaseInsensitiveMultimap header;  //!< request header, set by the caller

        AuthorizeFixBase(shared_ptr<TokenServerContext> context);  //!< fixture ctor. @param context current server context, context created by setup should be used here.
        virtual ~AuthorizeFixBase();

        /// helper function to set request header parameter
        void virtual SetHeader(const string& key, const string& val);
        /// run request and parse result, virtual placeholder
        void virtual Handle(const string& method = "POST") = 0;

        void ParseResult(stringstream& sout);                 //!< process handler response, will set all local variables like pt_body, pt_header. See the description for each variable.
        void assert_cors();                                   //!< verify CORS headers in the response
        void assert_options(const string& accepted_methods);  //!< verify expected OPTIONS response
        void assert_no_cache();                               //!< vefiry that no-cache header series has been set in the response
      };

      /// Utility API to manipulate tokens using Token API and run some validations.
      class TokenOps {
      private:
        shared_ptr<TokenServerContext> context;

      public:
        /// create utilty object
        /// @param context current server context, object created by setup should be used here
        TokenOps(shared_ptr<TokenServerContext> context);
        /// run /authn
        AuthResponse Authn();
        /// run /authorize with different parameters
        AuthorizeResponse Authorize(AuthResponse& authn_response, const string& token_scope, const string& code_verifier, const string& code_challenge_method = "S256");
        /// create token for pkce
        void token(AuthorizeResponse& authorize_response, const string& token_scope, const string& code_verifier);
        /// create token for password grant
        void token(const string& user, const string& password, const string& token_scope);
        /// create token using refresh token grant
        void token(const string& refresh_token, const string& token_scope);
        /// validate token to have scope and nonce
        void assert_token_success(const string& token_scope, const string& token_nonce = "nonce");
        /// validate token to have nonce
        void assert_token(const string& token, const string& token_nonce);
        /// validate errors
        /// @return error error description index 1 for a-match, 2 - for b-match, if there are no named capture matches returns 0, -1 if no match
        int assert_token_error(unsigned code, const string& error, const string& error_description);
        /// validate list of scopes
        void assert_token_scope(const string& token, set<string> scopes);
        /// validate scope vs expected (accepts a list)
        void assert_scope(const string& expected, const string& actual);

        shared_ptr<vx::openid::flow::TokenFlow> token_flow;  //!< inner flow object
        ptree token_result;                                  //!< result
        ptree token_metadata;                                //!< loaded token metadata
      };

      /// Base test class for OAuth2 series of endpoints. All other test classes has to be derived from this base class.
      /// Provides initializations for all consequet tests (delete/create test user data)
      class AuthHandlerTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(AuthHandlerTest);

        CPPUNIT_TEST(AuthStress);
        CPPUNIT_TEST(testAuthorizeResponse);
        CPPUNIT_TEST(testAuthHandlerCerr);

        CPPUNIT_TEST_SUITE_END();

      protected:
        shared_ptr<TokenServerContext> context;    //!< setup: initialize server context
        shared_ptr<DataFactory> factory;           //!< setup: created a data factory to be able to create data factory based services
        shared_ptr<ProfileProvider> provider;      //!< setup: loaded profie provider instance
        shared_ptr<TokenProvider> token_provider;  //!< setup: loaded token provider instance

      public:
        AuthHandlerTest();
        ~AuthHandlerTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

        // integration test

        void AuthStress();             //!< integration test: run multiple token operations to validate a series of the requests
        void testAuthorizeResponse();  //!< test serialize ops
        void testAuthHandlerCerr();    //!< cerr output for certain config problems
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx

#endif
#endif
