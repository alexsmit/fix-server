/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#if BOOST_VERSION >= 106600

#include <stdlib.h>
#include <sys/stat.h>
#include <chrono>
#include <vector>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <boost/asio.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/vector_body.hpp>

namespace be = boost::beast::http;
namespace bb = boost::beast;
namespace asio = boost::asio;

#include "../include/BaseTest.h"
#include <vx/web/HandlerEnvironment.h>
#include "HandlerTest.h"
#include <vx/HttpParser.h>
#include "AuthHandlerTest.h"
#include <TokenHandler.h>
#include <TokenServerInfoHandler.h>
#include <VxMessage.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceProfileFactory.h>

#include "HandlerTestFix.h"

using namespace std;
using namespace VxServer;
using namespace vx::web::test;
using namespace vx::openid::test;


HandlerTestFix::HandlerTestFix(shared_ptr<TokenServerContext> context,
                               ptree& req,
                               SetupHandlerFun fun,
                               string url
                               ) : AuthorizeFixBase(context), pt_request(req), url(url), fun(fun) {
}

void HandlerTestFix::Handle(const string& method) {
  stringstream sout;
  stringstream sin;
  string slen;

  if (method == "POST") {
    // stringstream squery;
    // write_json(squery, pt_request);
    SimpleWeb::CaseInsensitiveMultimap mquery;
    for (auto& kv : pt_request) {
      mquery.emplace(kv.first, kv.second.data());
    }
    string query = SimpleWeb::QueryString::create(mquery);
    slen = boost::lexical_cast<string>(query.size());
    sin.str(query);
  }

  HandlerEnvironment env(sin, sout);

  shared_ptr<VxServer::Handlers::VxHandler> cur_handler;
  if (fun != nullptr) cur_handler = fun(context, env);

  env.no_req->path = url;
  env.no_req->method = method;
  env.no_req->header = header;

  if (method == "POST") {
    env.no_req->header.emplace("Content-Type", "application/x-www-form-urlencoded");
    env.no_req->header.emplace("Content-Length", slen);
  }
  else {
    env.no_req->header.emplace("Origin", "https://jwt.io");
  }

  env.Handle();

  ParseResult(sout);
}

/// vefify a token data is provided in the response, i.e. access, refresh, expires
void HandlerTestFix::assert_simple_token(int exp) {
  // write_json(cout, pt_body);
  auto val = pt_body.get("access_token", "");
  CPPUNIT_ASSERT_MESSAGE("access_token", val.length() != 0);
  val = pt_body.get("token_type", "");
  CPPUNIT_ASSERT_MESSAGE("token_type", val.length() != 0);
  val = pt_body.get("refresh_token", "");
  CPPUNIT_ASSERT_MESSAGE("refresh_token", val.length() != 0);
  val = pt_body.get("expires_in", "");
  string sexp = boost::lexical_cast<string>(exp);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("expires_in", sexp, val);
}

#endif