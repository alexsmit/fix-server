#pragma once
/*
 * Copyright 2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#if BOOST_VERSION >= 106600

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>
#include <vx/web/FormParser.h>
#include <vx/web/NoRequest.h>
#include <vx/web/CGIServer.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <TokenServerContext.h>

using namespace std;

namespace vx {
  namespace openid {
    namespace test {

      using SetupHandlerFun = std::function<std::shared_ptr<VxServer::Handlers::VxHandler>(std::shared_ptr<TokenServerContext>, const vx::web::test::HandlerEnvironment& env)>;

      /// fixture class to test /oauth/token endpoint
      class HandlerTestFix : public AuthorizeFixBase {
      public:
        ptree& pt_request;    //!< request object, set by the constructor
        string url;           //!< url, set by the constructor
        SetupHandlerFun fun;  //!< setup necessary handlers

        /// create fixture object
        /// @param context current server context, should use created by setup
        /// @param req request object as ptree
        /// @param url default or specific url for the test
        HandlerTestFix(
            shared_ptr<TokenServerContext> context,
            ptree& req,
            SetupHandlerFun fun = nullptr,
            string url = "/oauth/token");

        void Handle(
            const string& method = "POST") override;

        /// vefify a token data is provided in the response, i.e. access, refresh, expires
        void assert_simple_token(
            int exp);
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
