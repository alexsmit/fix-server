/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#include <TokenServerContext.h>
#include <ProfileProviderRemote.h>

#include <BaseTest.h>
#include "ProfileSVCBaseTest.h"
#include <vx/URL.h>

#include <vx/ServiceProfileFactory.h>

#include <vx/ServiceEmailFactory.h>

using namespace std;
using namespace vx::openid::test;


//----------------------------------------------------------

namespace vx {
  /// @brief Dummy service to inject instead of real EmailService
  class EmailServiceDummy : public EmailService {
  public:
    static string
        template_name,   //!< record last passed value
        email_from,      //!< last email from value
        email_to,        //!< last email to value
        title,           //!< last title used by service
        url;             //!< last url used to generate a email body
    static bool result;  //!< last result (i.e. success)

    /// @brief Dummy/Mock implementation of EmailService
    EmailServiceDummy(JsonConfig& conf) : EmailService(conf) {}

    /// @brief mock for the functino to send an email
    /// @param template_name temlate file name to generate email body
    /// @param email_from email from
    /// @param email_to email to
    /// @param title title for email
    /// @param url url to replace in the template
    /// @return
    bool SendCodeURL(const std::string& template_name,
                     const std::string& email_from,
                     const std::string& email_to,
                     const std::string& title,
                     const std::string& url) override {
      EmailServiceDummy::template_name = template_name;
      EmailServiceDummy::email_from = email_from;
      EmailServiceDummy::email_to = email_to;
      EmailServiceDummy::title = title;
      EmailServiceDummy::url = url;
      EmailServiceDummy::result = true;
      return true;
    }
  };

  string EmailServiceDummy::template_name;
  string EmailServiceDummy::email_from;
  string EmailServiceDummy::email_to;
  string EmailServiceDummy::title;
  string EmailServiceDummy::url;
  bool EmailServiceDummy::result = false;

  /// @brief Factory to create mock EmailService for testing
  class ServiceEmailFactoryDummy : public ServiceEmailFactory {
  private:
    bool dummy;

  public:
    /// @brief mock email factory, will create a subclass of EmailService or just Service
    /// @param dummy if true will return generic Service object (which is invalid to use)
    ServiceEmailFactoryDummy(bool dummy = false) : ServiceEmailFactory(), dummy(dummy) {}
    
    /// @brief Create mock instance
    /// @param type this parameter is ignored
    /// @return pointer to a service mock implementation of EmailService or just Service
    std::shared_ptr<Service> createService(const std::string& type) override {
      if (dummy)
        return make_shared<Service>();
      return make_shared<EmailServiceDummy>(*section);
    }
  };


};  // namespace vx


//----------------------------------------------------------

ProfileSVCBaseTest::ProfileSVCBaseTest(string provider_type)
    : provider_type(provider_type) {
}

const string POST_METHOD = "POST";
const string GET_METHOD = "GET";

ProfileSVCBaseTest::~ProfileSVCBaseTest() {}

void ProfileSVCBaseTest::setUpInternal() {
  AppConfig::resetInstance();
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("", true));

  context->setParam("profile_provider.type", provider_type.c_str());
  // if (!host.empty() && !remote_profile.empty()) {
  //   string key = remote_profile + string(".profile_host");
  //   context->setParam(key.c_str(), host.c_str());
  //   key = string("profile_provider.config.")  + provider_type;
  //   context->setParam(key.c_str(), remote_profile.c_str());
  // }

  // context->setParam("profile_provider.send_email", "off");
  context->AddProvider(make_shared<vx::ServiceProfileFactory>(0, "profile_provider"));

  provider = context->GetProfileProvider("mysql");
  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  // user for test
  UserRecord rec1 = {0, USER_EMAIL3, USER_NAME3, "password", "1234"};
  rec1.roles.push_back(ROLE_1);
  rec1.roles.push_back(ROLE_2);
  provider->DeleteUser(USER_NAME3);
  provider->SaveUser(rec1);

  // user for registration
  provider->DeleteUser(USER_NAME);

  // user for reset password
  UserRecord rec2 = {0, USER_EMAIL2, USER_NAME2, USER_PASSWORD, USER_PIN};
  rec2.roles.push_back(ROLE_1);
  rec2.roles.push_back(ROLE_2);
  provider->DeleteUser(USER_NAME2);
  provider->SaveUser(rec2);

  // disable emails
  context->setParam("profile_provider.send_email", "off");
}

void ProfileSVCBaseTest::tearDownInternal() {
  provider->DeleteUser(USER_NAME);
  provider->DeleteUser(USER_NAME2);
  provider->DeleteUser(USER_NAME3);

  // provider->CleanRegistrations(0);

  provider.reset();
  // factory.reset();
}

void ProfileSVCBaseTest::expectAuthenticate(string prefix, std::shared_ptr<ProfileData> result) {
  string message = provider_type + ":" + prefix + ": should have userid";
  CPPUNIT_ASSERT_MESSAGE(message, !result->userid.empty());

  message = provider_type + ":" + prefix + ": should have 2 roles";
  CPPUNIT_ASSERT_MESSAGE(message, result->roles.size() == 2);

  auto roles = result->roles;

  auto it = std::find(roles.begin(), roles.end(), ROLE_1);
  message = provider_type + ":" + prefix + ": should have role1";
  CPPUNIT_ASSERT_MESSAGE(message, it != roles.end());

  it = std::find(roles.begin(), roles.end(), ROLE_2);
  message = provider_type + ":" + prefix + ": should have role2";
  CPPUNIT_ASSERT_MESSAGE(message, it != roles.end());
}

void ProfileSVCBaseTest::testAuthenticate() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto result = service->Authenticate(USER_NAME3, "password");
  expectAuthenticate("Authenticate", result);
}

void ProfileSVCBaseTest::testAuthenticateUUID() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto result = service->Authenticate(USER_NAME3, "password");
  auto userid = result->userid;
  CPPUNIT_ASSERT_MESSAGE("should have userid", !userid.empty());
  result = service->Authenticate(userid.c_str(), "password", true);
  expectAuthenticate("Authenticate UUID", result);
}

void ProfileSVCBaseTest::testAuthenticatePin() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto result = service->AuthenticatePin(USER_NAME3, "1234");
  expectAuthenticate("Pin", result);
}

void ProfileSVCBaseTest::testAuthenticatePinUUID() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto result = service->Authenticate(USER_NAME3, "password");
  auto userid = result->userid;
  CPPUNIT_ASSERT_MESSAGE("should have userid", !userid.empty());
  result = service->AuthenticatePin(userid.c_str(), "1234", true);
  expectAuthenticate("Pin UUID", result);
}

void ProfileSVCBaseTest::testGetProfileData() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto profile = service->GetProfileData(USER_NAME3);
  expectAuthenticate("Profile", profile);
}

void ProfileSVCBaseTest::testGetProfileDataUUID() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto profile = service->GetProfileData(USER_NAME3);
  auto userid = profile->userid;

  auto profile1 = service->GetProfileData(userid.c_str(), true);
  expectAuthenticate("ProfileUUID", profile1);
}

void ProfileSVCBaseTest::testRegisterUser() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  UserRecord rec = {0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};

  auto reg = service->RegisterUser(rec, true);
  CPPUNIT_ASSERT_MESSAGE("should get a registration code", !reg.registration_code.empty());
  CPPUNIT_ASSERT_MESSAGE("should get a reset token", !reg.reset_token.empty());
}

void ProfileSVCBaseTest::testValidateRegistration() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  UserRecord rec = {0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto reg = service->RegisterUser(rec, false);

  auto result = service->ValidateRegistration(reg.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("registration should be pending", RegistrationStatus::pending, result);

  auto result_bad = service->ValidateRegistration("invalid_token");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("registration should be invalid (wrong token)", RegistrationStatus::invalid, result_bad);
  result_bad = service->ValidateRegistration("");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("registration should be invalid (empty token)", RegistrationStatus::invalid, result_bad);

  // CPPUNIT_ASSERT_THROW_MESSAGE("should fail, wrong token",
  //                              ,
  //                              std::runtime_error);
}

void ProfileSVCBaseTest::testValidatePendingRegistration() {
  // 1) register a user
  auto service = context->GetProfileSVC(provider_type.c_str());
  UserRecord rec = {0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto reg = service->RegisterUser(rec, false);

  auto result = service->ValidateRegistration(reg.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("registration should be pending", RegistrationStatus::pending, result);

  // 2) wait 15 seconds to make sure a clenup thread will delete a registration
  cout << "W" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(17));

  // 3) after cleanup a registration should be no longer valid
  result = service->ValidateRegistration(reg.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("registration should be invalid", RegistrationStatus::invalid, result);
}

void ProfileSVCBaseTest::testConfirmRegistration() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  UserRecord rec = {0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto reg = service->RegisterUser(rec, false);

  auto result = service->ConfirmRegistration(reg.reset_token);
  CPPUNIT_ASSERT_MESSAGE("registration should be successful", result);

  auto reg_result = service->ConfirmRegistration("invalid_token");
  CPPUNIT_ASSERT_MESSAGE("should fail, wrong token", !reg_result);

  reg_result = service->ConfirmRegistration("");
  CPPUNIT_ASSERT_MESSAGE("should fail, empty token", !reg_result);
}

void ProfileSVCBaseTest::testConfirmationEmailParam() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  auto param = context->getParam("profile_provider.confirmation_email");
  CPPUNIT_ASSERT_MESSAGE("should have a configured confirmation email", !param.empty());

  param = context->getParam("profile_provider.confirmation_email_title");
  CPPUNIT_ASSERT_MESSAGE("should have a configured confirmation email title", !param.empty());

  param = context->getParam("profile_provider.send_email", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have a send email parameter", param != "not-set");

  param = context->getParam("profile_provider.confirmation_failure_url", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have confirmation_failure_url parameter", param != "not-set");

  param = context->getParam("profile_provider.confirmation_success_url", "not-set");
  CPPUNIT_ASSERT_MESSAGE("should have confirmation_success_url parameter", param != "not-set");
}

void ProfileSVCBaseTest::testSendConfirmationEmailTemplate() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "off");

  context->setParam("profile_provider.confirmation_email", "");
  bool result = service->SendConfirmationEmail("", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail when email template is not defined", !result);
}

void ProfileSVCBaseTest::testSendConfirmationEmailTitle() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.confirmation_email_title", "");
  auto result = service->SendConfirmationEmail("", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail when email title is not defined", !result);
}

void ProfileSVCBaseTest::testSendConfirmationEmailFrom() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "off");

  context->setParam("profile_provider.email_from", "");
  auto result = service->SendConfirmationEmail("", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail when 'from' email is not defined", !result);
}

void ProfileSVCBaseTest::testSendConfirmationEmailTo() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  auto result = service->SendConfirmationEmail("", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail when email empty", !result);
}

void ProfileSVCBaseTest::testSendConfirmationEmail() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "on");
  context->AddProvider(make_shared<vx::ServiceEmailFactoryDummy>());

  auto result = service->SendConfirmationEmail("alex@replaytrader.com", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should succeed all validation is successful", result);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string("alex@replaytrader.com"), EmailServiceDummy::email_to);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("template", string("email_confirm"), EmailServiceDummy::template_name);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("title", string("Registration confirmation @ VxPRO"), EmailServiceDummy::title);
}

void ProfileSVCBaseTest::testSendConfirmationEmailOff() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "off");

  auto result = service->SendConfirmationEmail("alex@replaytrader.com", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail to send when it is OFF", !result);
}

void ProfileSVCBaseTest::testSendConfirmationEmailNoSVC() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "on");

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "should fail to send when service is not registered", {
        auto result = service->SendConfirmationEmail("alex@replaytrader.com", "token", "confirmation_email", "register/confirm");
      },
      invalid_argument);
}

void ProfileSVCBaseTest::testSendConfirmationEmailBadSVC() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  context->setParam("profile_provider.send_email", "on");
  context->AddProvider(make_shared<vx::ServiceEmailFactoryDummy>(true));

  auto result = service->SendConfirmationEmail("alex@replaytrader.com", "token", "confirmation_email", "register/confirm");
  CPPUNIT_ASSERT_MESSAGE("should fail to send when it is OFF", !result);
}


void ProfileSVCBaseTest::testReset() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  string reset_code = service->Reset(USER_EMAIL2);
  CPPUNIT_ASSERT_MESSAGE("should get a reset code", !reset_code.empty());

  auto result = service->UpdatePassword(reset_code.c_str(), "pass");
  CPPUNIT_ASSERT_MESSAGE("should succeed", result);

  auto result_auth = service->Authenticate(USER_NAME2, "pass");
  expectAuthenticate("Authenticate", result_auth);
}

void ProfileSVCBaseTest::testUpdatePasswordBad() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto result = service->UpdatePassword("wrong_reset_code", "password");
  CPPUNIT_ASSERT_MESSAGE("should fail", !result);
}

void ProfileSVCBaseTest::testUpdatePassword() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  string reset_code = service->Reset(USER_EMAIL2);
  auto result = service->UpdatePassword(reset_code.c_str(), "pwd");
  CPPUNIT_ASSERT_MESSAGE("should succeed", result);

  auto rec = service->Authenticate(USER_NAME2, "pwd");
  expectAuthenticate("Update password", rec);
}

void ProfileSVCBaseTest::testVerifyResetToken() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  string reset_code = service->Reset(USER_EMAIL2);
  auto result = service->ResetVerify(reset_code.c_str());
  CPPUNIT_ASSERT_MESSAGE("should succeed", result);
}

void ProfileSVCBaseTest::testVerifyResetTokenBad() {
  auto service = context->GetProfileSVC(provider_type.c_str());

  auto result = service->ResetVerify("bad_token");
  CPPUNIT_ASSERT_MESSAGE("should fail", !result);

  result = service->ResetVerify("");
  CPPUNIT_ASSERT_MESSAGE("should fail", !result);
}

void ProfileSVCBaseTest::testChangePassword() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto profile = service->GetProfileData(USER_NAME2);
  auto result = service->ChangePassword(profile->userid.c_str(), "pwd2");
  if (!result) {
    cout << "change password is unsuccessful" << endl;
  }
  CPPUNIT_ASSERT_MESSAGE("should succeed", result);

  auto rec = service->Authenticate(USER_NAME2, "pwd2");
  expectAuthenticate("Change password", rec);
}

void ProfileSVCBaseTest::testChangePin() {
  auto service = context->GetProfileSVC(provider_type.c_str());
  auto profile = service->GetProfileData(USER_NAME2);
  auto result = service->ChangePin(profile->userid.c_str(), "7777");
  if (!result) {
    cout << "change pin is unsuccessful" << endl;
  }
  CPPUNIT_ASSERT_MESSAGE("should succeed", result);

  auto rec = service->AuthenticatePin(USER_NAME2, "7777");
  expectAuthenticate("Change pin", rec);
}
