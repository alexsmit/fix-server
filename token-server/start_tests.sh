#!/bin/bash

. ./start_stress_common.sh

pushd build > /dev/null

./token-server-test -

stop_server localhost 9997
stop_server localhost 9998

popd >/dev/null
