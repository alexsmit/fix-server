#!/bin/bash

. ./start_stress_common.sh

threads=35
count=50
ans=
trystart=1
host='localhost'
isssl=
port=9998
shutdown=600

verify_stress
if [ $? -ne 0 ] ; then
    return 1 2>/dev/null || exit 1
fi

stressParams=()
params=()
cont=0
while [ ! -z "$1" ] ; do
    case $1 in
        --help|help)
            cat <<HHH
Usage:   ./start_stress.sh [valgrind] [-a shutdown] [-h host] [-v] [--ssl]
        valgrind    : run valgrind
        -a          : shutdown interval
        -h host     : remote host
        -v          : verbose
        --ssl       : using SSL
        -i count    : number of iterations
        -t threads  : number of threads

        Default for stress:
            -i $count
            -t $threads
HHH
            cont=1
        ;;
        valgrind)
            params+=(valgrind refresh 60 ttl 30)
            threads=200
            count=5
        ;;
        -a|shutdown|--shutdown)
            shift
            shutdown=$1
        ;;
        -h)
            shift
            trystart=0
            host=$1
        ;;
        --v|-v)
            stressParams+=('-v')
        ;;
        --vv|-vv)
            stressParams+=('-vv')
        ;;
        --ssl)
            isssl='--ssl'
            params+=('--ssl')
        ;;
        -t)
            shift
            threads=$1
        ;;
        -i)
            shift
            count=$1
        ;;
        *)
            echo Invalid parameter - $1
        ;;
    esac
    shift
done

if [ $cont -ne 0 ] ; then
    return 0 2>/dev/null || exit 0
fi

pid=
if [ $trystart -ne 0 ] ; then
    cnt=$(ps -ef | grep token-server | wc -l)
    if [ $cnt -lt 2 ] ; then
        echo -n 'Token server is not running. Start? [Y/n] (y):'
        read ans
        params+=(shutdown $shutdown)
        if [ -z "$ans" ] || [[ $ans =~ [Yy] ]] ; then
            ./start_for_test_token.sh ${params[@]}&
            pid=$?
            soeep 5
            #return 1 2>/dev/null || exit 1
        fi
    fi
fi

pushd build > /dev/null
#  ./token-server-stress --no_start -t $threads -i $count --progress --ssl $port
./token-server-stress --no_start -h $host -t $threads -i $count -p $port $isssl --stress-all ${stressParams[@]}
popd > /dev/null

if [ ! -z "$pid" ] ; then
    stop_server localhost 9498 1
fi
