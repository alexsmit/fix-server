/*
 * Copyright 2019-21 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "stdafx.h"
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <iostream>
#include <set>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

//  #define NO_BOOST_PROCESS

#ifdef NO_BOOST_PROCESS
#error "Boost process is not defined"
#endif

#include <boost/process.hpp>
#include <boost/process/posix.hpp>
namespace bp = boost::process;

#include <vx/URL.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <ProfileProviderRemote.h>

#include "test/TokenProviderDBTest.h"
#include "test/TokenProviderMysqlTest.h"
#include "test/TokenProviderSociTest.h"
#include "test/ProfileSVCMysqlTest.h"
#include "test/ProfileSVCRemoteTest.h"
#include "test/ServerTest.h"
#include "test/RegisterTest.h"
#include "test/WebTest.h"
#include "test/ResetTest.h"
#include "test/ProfileSVCRemoteHttpsTest.h"
#include "test/AuthModelsTest.h"

#if BOOST_VERSION >= 106600
#include "test/HandlerTest.h"
#include "test/AuthHandlerTest.h"
#include "test/AuthHandlerTestAuthn.h"
#include "test/AuthHandlerTestLogout.h"
#include "test/AuthHandlerTestAuthorize.h"
#include "test/AuthHandlerTestUserinfo.h"
#include "test/AuthHandlerTestIntrospect.h"
#include "test/AuthHandlerTestApi.h"
#include "test/AuthHandlerTestOpenID.h"
#include "test/AuthHandlerTestKeys.h"
#include "test/AuthHandlerTestRevoke.h"
#include "test/AuthHandlerTestToken.h"
#include "test/AuthHandlerTestLogin.h"

#include "test/NotImplementedTest.h"
// #include "test/HandlerEmailTest.h"
#include "test/OAuthAllHandlersTest.h"
#include "test/ResponseBuilderTest.h"
#include "test/HandlerInfoTest.h"
#include "test/AuthHandlerTestAuthServer.h"
#include "test/RegistrationSVCHandlerTest.h"
#endif  // BOOST >= 106600

#include "test/ProfileRemoteTest.h"
#include "test/ProfileRemoteFailTest.h"
#include "test/TokenServerContextTest.h"
#include "test/VxIndexTest.h"

#include <BaseTest.h>
#include <TokenServerContext.h>

#include <vx/web/Util.h>

#include <InitProfileTest.h>
#include <vx/ServiceProfileFactory.h>
#include <vx/ServiceTokenFactory.h>

using namespace std;
using namespace vx::openid::test;
using namespace vx::providers;

namespace fis = boost::filesystem;

// clang-format off
//                         port    ports  ttl  debug  no/https ttl/reg    no/mail auto-off     ttl/ref_token cleanup interval
#define TOKEN_ARGS         "9998", "0",   "3", "0",   "-n",    "-r", "5", "-e",   "-a", "300", "-f", "6",    "-l", "10"
#define TOKEN_ARGS_DEBUG   "9998", "0",   "3", "3",   "-n",    "-r", "5", "-e",   "-a", "300", "-f", "6",    "-l", "10"

//                   port    ports  ttl  auto-off
#define PROFILE_ARGS "9997", "0",   "3", "300"
// clang-format on

#define USE_SHUTDOWN_SIGNAL

// Note: we don't need to include RegisteredTest

static std::shared_ptr<ServerContext> server_context;

string profile_name = "profile.mysql";
string backend = "mysql";
string ssl_port = "0";
JsonConfig conf;

std::shared_ptr<ServerContext> getContext(const string& param, bool useDefault, int cache_ttl) {
  // if (!server_context) {
  if (useDefault)
    server_context.reset(new TokenServerContext());
  else
    server_context.reset(new TokenServerContext(8082, 0, DEBUG_TTL, LogLevel::none, 5, DEBUG_TTL * 2, param));
  server_context->Init("token_provider", "config.json");

  auto context = std::dynamic_pointer_cast<TokenServerContext>(server_context);
  context->provider_ttl = cache_ttl;
  context->AddProvider(make_shared<vx::ServiceProfileFactory>(cache_ttl, "profile_provider"));
  context->AddProvider(make_shared<vx::ServiceTokenFactory>(cache_ttl));
  // }
  return server_context;
}

#define FOCUS(x)                                                                    \
  if (boost::algorithm::ends_with(x::suite()->getName(), (string("::") + focus))) { \
    if (tests.find(#x) == tests.end()) {                                            \
      start_tests = true;                                                           \
      cout << x::suite()->getName() << endl;                                        \
      runner.addTest(x::suite());                                                   \
      tests.insert(#x);                                                             \
    }                                                                               \
  }

#define FOCUSW(x)                                                                   \
  if (boost::algorithm::ends_with(x::suite()->getName(), (string("::") + focus))) { \
    if (tests.find(#x) == tests.end()) {                                            \
      start_profile_server = true;                                                  \
      start_token_server = true;                                                    \
      cout << x::suite()->getName() << endl;                                        \
      runner_web.addTest(x::suite());                                               \
      tests.insert(#x);                                                             \
    }                                                                               \
  }

#define FOCUSP(x)                                                                   \
  if (boost::algorithm::ends_with(x::suite()->getName(), (string("::") + focus))) { \
    if (tests.find(#x) == tests.end()) {                                            \
      start_profile_server = true;                                                  \
      start_profile_tests = true;                                                   \
      cout << x::suite()->getName() << endl;                                        \
      runner_profile.addTest(x::suite());                                           \
      tests.insert(#x);                                                             \
    }                                                                               \
  }

#define FOCUS2(x, y, z)                                  \
  if (focus == #x) {                                     \
    cout << #x << " -> " << #y << " and " << #z << endl; \
    vfocus.push_back(#y);                                \
    vfocus.push_back(#z);                                \
  }

#define FOCUS3(x, y, z, w)                                                \
  if (focus == #x) {                                                      \
    cout << #x << " -> " << #y << " and " << #z << " and " << #w << endl; \
    vfocus.push_back(#y);                                                 \
    vfocus.push_back(#z);                                                 \
    vfocus.push_back(#w);                                                 \
  }

DebugEnvironment DebugEnvironment::env = {};

void DebugEnvironment::init() {
  string hos;
  hos.resize(128);
  auto res = ::gethostname(&hos[0], hos.length());
  string myhost(hos.data());

  DBIOEnvironment::SetNative(true);

  env.DEBUG_HOST = (boost::format("%s:9998") % myhost).str();
  env.DEBUG_HOST_PROFILE = (boost::format("%s:9997") % myhost).str();
  // env.DEBUG_HOST_PROFILE2 = (boost::format("%s:9996") % myhost).str();
  env.TOKEN_SERVER_HOST = "token-server:9998";
}

int main(int argc, char** argv) {
  int debug = 0;
  vector<string> token_args{TOKEN_ARGS};
  vector<string> profile_args{PROFILE_ARGS};

  DebugEnvironment::env.init();
  std::cout << "token-server test\n"
            << "Host         : " << DebugEnvironment::env.DEBUG_HOST << endl
            << "Profile      : " << DebugEnvironment::env.DEBUG_HOST_PROFILE << endl
            // << "Profile2     : " << DebugEnvironment::env.DEBUG_HOST_PROFILE2 << endl
            << "Token server : " << DebugEnvironment::env.TOKEN_SERVER_HOST << endl;

  fis::path prof("../../profile-server/build/profile-server");
  fis::path profd("../../profile-server/build");
  auto exp = fis::exists(prof);
  // LCOV_EXCL_START
  if (!exp) {
    prof = "../profile-server/profile-server";
    profd = "../profile-server";
    exp = fis::exists(prof);

    if (!exp) {
      cout << "ERROR: profile server binary does nto exist" << endl;
      cout << "Build profile-server to run this test" << endl;
    }
  }
  // LCOV_EXCL_STOP

  // LCOV_EXCL_START
  fis::path tokf("./token-server");
  auto ext = fis::exists(tokf);
  if (!ext) {
    cout << "ERROR: token server binary does nto exist" << endl;
    cout << "Build token-server to run this test" << endl;
  }
  // LCOV_EXCL_STOP

  if (!exp || !ext) return 1;

  vector<string> vfocus;
  set<string> tests;
  bool start_servers = true;
  bool start_tests = false;
  bool start_token_server = false;
  bool start_profile_server = false;
  bool start_profile_tests = false;

  for (auto i = 1; i < argc; i++) {
    auto val = argv[i];
    if (string("-") == val) {
      start_servers = false;
      continue;
    }

    // LCOV_EXCL_START
    if (string("--") == val) break;
    if (string("--debug") == val) {
      token_args = {TOKEN_ARGS_DEBUG};
      continue;
    }
    vfocus.push_back(val);
    // LCOV_EXCL_STOP
  }

  shared_ptr<bp::child> server_profile;
  shared_ptr<bp::child> server_token;

  CppUnit::TextUi::TestRunner runner;
  CppUnit::TextUi::TestRunner runner_profile;
  CppUnit::TextUi::TestRunner runner_web;


  // test from the registry

  vector<string> deffocus = {
    "RegisterTest",
    "ResetTest",
    "ServerTest",
    "WebTest",

    "ProfileSVC",  // -- 2 tests: ProfileSVCMysqlTest & ProfileSVCRemoteTest
                   // "ProfileSVCMysqlTest",
                   // "ProfileSVCRemoteTest",
                   // "ProfileSVCRemoteHttpsTest", // -- need to setup common server to run this test

#if BOOST_VERSION >= 106600
    "AuthHandlerTest",
    "AuthHandlerTestApi",
    "AuthHandlerTestAuthn",
    "AuthHandlerTestAuthorize",
    "AuthHandlerTestIntrospect",
    "AuthHandlerTestKeys",
    "AuthHandlerTestLogin",
    "AuthHandlerTestLogout",
    "AuthHandlerTestOpenID",
    "AuthHandlerTestRevoke",
    "AuthHandlerTestToken",
    "AuthHandlerTestUserinfo",
    "AuthModelsTest",
    "HandlerTest",
    // "NotImplementedTest",
    "OAuthAllHandlersTest",
    "ResponseBuilderTest",
    "HandlerInfoTest",
    "RegistrationSVCHandlerTest",
    "AuthHandlerTestAuthServer",
#endif
    "ProfileRemoteTest",
    "ProfileRemoteFailTest",
    "TestConstants",

    "TokenProvider",  // -- actually 3 tests: DB & mysql & soci
    //"TokenProviderDBTest",
    //"TokenProviderMysqlTest",
    //"TokenProviderSociTest"
    "TokenServerContextTest",
    "VxIndexTest"
  };

  if (vfocus.empty()) {
    vfocus = deffocus;
    // if (vfocus.empty()) {
    //   CppUnit::TestFactoryRegistry& registry =
    //       CppUnit::TestFactoryRegistry::getRegistry();
    //   runner.addTest(registry.makeTest());
    // }
  }

  //------------------------------------------------------------------------

  for (auto i = 0; i < vfocus.size(); i++) {
    auto focus = vfocus[i];

    FOCUS(ProfileSVCMysqlTest)
    FOCUS(VxIndexTest)
#if BOOST_VERSION >= 106600
    FOCUS(HandlerTest)
    FOCUS(AuthHandlerTest)

    FOCUS(AuthHandlerTestAuthn)
    FOCUS(AuthHandlerTestLogout)
    FOCUS(AuthHandlerTestAuthorize)
    FOCUS(AuthHandlerTestUserinfo)
    FOCUS(AuthHandlerTestIntrospect)
    FOCUS(AuthHandlerTestApi)
    FOCUS(AuthHandlerTestOpenID)
    FOCUS(AuthHandlerTestKeys)
    FOCUS(AuthHandlerTestRevoke)
   
    FOCUSW(AuthHandlerTestToken) // require token server to run
   
    FOCUS(AuthHandlerTestLogin)

    FOCUS(NotImplementedTest)
    FOCUS(AuthModelsTest)
    // FOCUS(HandlerEmailTest)
    FOCUS(OAuthAllHandlersTest)
    FOCUS(ResponseBuilderTest)
    FOCUS(HandlerInfoTest)

    FOCUSW(RegistrationSVCHandlerTest)
    FOCUS(AuthHandlerTestAuthServer)
#endif

    FOCUS(TokenProviderDBTest)
#if defined(VX_USE_MYSQL)
    FOCUS(TokenProviderMysqlTest)
#endif
#if defined(VX_USE_SOCI)
    FOCUS(TokenProviderSociTest)
#endif

    FOCUS2(ProfileSVC, ProfileSVCMysqlTest, ProfileSVCRemoteTest)
    FOCUS3(TokenProvider, TokenProviderDBTest, TokenProviderMysqlTest, TokenProviderSociTest)

    FOCUS(TokenServerContextTest)
  }

  ConnectProfileProvider = [](std::shared_ptr<DataFactory> factory, const char* p, const string& backend) {
    std::shared_ptr<ProfileProvider> provider;
    if (p != NULL) profile_name = p;

#if defined(VX_USE_MYSQL)
    if (backend == "mysql")
      provider = factory->Connect<ProfileProviderMySQL>(profile_name.c_str());
#endif

#if defined(VX_USE_SOCI)
    if (!provider) {
      if (backend == "soci" || backend == "mysql")
        provider = factory->Connect<ProfileProviderSOCI>(profile_name.c_str());
    }
#endif

    // LCOV_EXCL_START
    if (backend == "remote") {
      AppConfig app;
      JsonConfig& conf_src = app.Section(profile_name);

      string host = conf_src["profile_host"];

      URL u(host);
      if (ssl_port != "0") {
        u.port = ssl_port;
        u.protocol = "https";
        host = u.getHost();
      }

      conf[profile_name + string(".profile_host")] = host;
      conf[profile_name + string(".client_id")] = conf_src["client_id"].str();
      conf[profile_name + string(".client_secret")] = conf_src["client_secret"].str();

      JsonConfig& c = conf.getSection(profile_name);

      provider = factory->Connect<ProfileProviderRemote>(c);
    }
    // LCOV_EXCL_STOP

    if (!provider)
      throw runtime_error("Invalid backend");  // LCOV_EXCL_LINE

    return provider;
  };  // LCOV_EXCL_LINE

  int result = 0;
  if (start_tests) {
    result = (runner.run("",
                         /*wait*/ false,
                         /*printResult*/ true,
                         /*printProgress*/ true))
                 ? 0
                 : 1;
  }

  //------------------------------------------------------------------------

  for (auto i = 0; i < vfocus.size(); i++) {
    auto focus = vfocus[i];

    FOCUSP(ProfileRemoteTest)
    FOCUSP(ProfileRemoteFailTest)
    FOCUSP(ProfileSVCMysqlTest)
    FOCUSP(ProfileSVCRemoteTest)
    FOCUSP(ProfileSVCRemoteHttpsTest)

    FOCUSW(RegisterTest)
    FOCUSW(ResetTest)
    FOCUSW(ServerTest)
    FOCUSW(WebTest)
  }

  //------------------------------------------------------------------------

  //+++ start profile server
  if (start_servers && start_profile_server) {
    // LCOV_EXCL_START
    cout << "\nStarting profile server: " << prof << endl;
    server_profile.reset(new bp::child(
        bp::start_dir(profd.string()),
        prof,
        profile_args));
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    // LCOV_EXCL_STOP
  }
  else {
    if (start_profile_server) {
      cout << "\nUsing profile server with  parameters:";
      for (auto& s : profile_args) cout << " " << s;
      cout << "\n\n";
    }
  }

  int result_profile = 0;
  if (start_profile_tests) {
    result_profile = (runner_profile.run("",
                                         /*wait*/ false,
                                         /*printResult*/ true,
                                         /*printProgress*/ true))
                         ? 0
                         : 1;
  }

  //------------------------------------------------------------------------

  if (start_servers && start_token_server) {
    // LCOV_EXCL_START
    cout << "Starting token server: " << tokf << endl;
    server_token.reset(new bp::child(tokf, bp::args(token_args)));
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    // LCOV_EXCL_STOP
  }
  else {
    if (start_token_server) {
      cout << "\nUsing token server with  parameters:";
      for (auto& s : token_args) cout << " " << s;
      cout << "\n\n";
    }
  }

  int result_token = 0;
  if (start_token_server) {
    result_token = (runner_web.run("",
                                   /*wait*/ false,
                                   /*printResult*/ true,
                                   /*printProgress*/ true))
                       ? 0
                       : 1;
  }

  // +++ terminate both servers

#ifdef USE_SHUTDOWN_SIGNAL
  bool shutdown_signal = true;
#else
  bool shutdown_signal = false;
#endif

  auto stop_server = [&shutdown_signal](const string& msg, shared_ptr<bp::child> serv, string& host) {
    std::cout << "Stopping " << msg << "..." << endl;
    // LCOV_EXCL_START
    bool do_signal = (shutdown_signal && !!serv);
    if (do_signal) {
      ::kill(serv->id(), SIGHUP);
      long max_timeout = 6000;
      while (serv->running() && max_timeout > 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        max_timeout -= 300;
      }
    }
    else {
      SimpleWeb::CaseInsensitiveMultimap header;
      header.insert({"X-Client-Id", "aaa"});
      header.insert({"X-Client-Secret", "111"});
      header.insert({"Content-Type", "application/json"});
      string payload = "{\"magic\":\"hello\"}";

      boost::regex pat("^Connection refused");
      boost::smatch match;
      try {
        shared_ptr<HttpClient> client;
        client.reset(new HttpClient(DEBUG_HOST));
        client->request("POST", "/stop", payload, header);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
      }
      catch (const std::exception& e) {
        string mes = e.what();
        if (!boost::regex_search(mes, match, pat)) {
          std::cerr << "Stopping server: " << e.what() << '\n';
        }
      }
    }
    // LCOV_EXCL_STOP
  };

  thread a([&start_token_server, &server_token, &stop_server]() {  // LCOV_EXCL_LINE
    if (!start_token_server) return;
    stop_server("token server", server_token, DEBUG_HOST);
  });

  thread b([&start_profile_server, &server_profile, &stop_server]() {  // LCOV_EXCL_LINE
    if (!start_profile_server) return;
    stop_server("profile server", server_profile, DEBUG_HOST_PROFILE);
  });

  a.join();
  b.join();

  /// --- terminate

  cout << "Test result (combined): " << (result + result_profile + result_token) << endl;

  return (result + result_token);
}
