#!/bin/bash
# usage: start_for_test_teken.sh [valgrind] ttl shutdown

. ./start_stress_common.sh

maxcycles=100
params=()
doclean=
nostart='-'

while [ ! -z "$1" ] ; do
    case $1 in
        --nostart|--no_start|nostart)
            nostart="-"
        ;;
        --start|start)
            nostart=
        ;;
        -c)
            doclean="-c"
        ;;
        --run|run)
            if [ ! -z "$doclean" ] ; then
                pushd .. >/dev/null
                . ./cleanup.sh
                popd >/dev/null
            fi
            . ./prepare_build.sh run
            pushd ../profile-server >/dev/null
            . ./prepare_build.sh run
            popd >/dev/null
        ;;
        --pull|pull)
            git pull
            git submodule update --init
        ;;
        *)
            params+=( $1 )
        ;;
    esac
    shift
done

if [ ! -e build/token-server-test ] ; then
    echo Token server test binary does not exist
    return 1 2>/dev/null || exit 1
fi

if [ "$nostart" == "-" ] ; then
    ./start_for_test_profile.sh -d
    ./start_for_test_token.sh -d
fi

err=0
ts=$(date +'%s')

pushd build > /dev/null
./token-server-test ${nostart} ${params[@]}
err=$?
popd > /dev/null

tsend=$(date +'%s')
((tstot=$tsend-$ts))

if [ "$nostart" == "-" ] ; then
    stop_server localhost 9998
    stop_server localhost 9997

    token_pid=$(cat /var/run/user/$EUID/token-server.pid)
    profile_pid=$(cat /var/run/user/$EUID/profile-server.pid)
    for((i=0;i<15;i++)); do
        kill -s 0 $token_pid 2>/dev/null
        ret=$?
        kill -s 0 $profile_pid 2>/dev/null
        ret1=$?
        if [ $ret -ne 0 ] && [ $ret1 -ne 0 ] ; then
            break
        fi
        sleep 2
    done

    #-----------------------------------------------

    echo "*** start token server and stop with signal"
    ./start_for_test_token.sh -d
    token_pid=$(cat /var/run/user/$EUID/token-server.pid)
    sleep 10

    kill -HUP $token_pid

    wait_for_exit $token_pid
    ret=$?
    let err=$err+$ret

    #-----------------------------------------------

    echo "*** start token server and check automatic shutdown"
    ./start_for_test_token.sh -d -a 10
    token_pid=$(cat /var/run/user/$EUID/token-server.pid)
    sleep 10
    wait_for_exit $token_pid
    ret=$?
    let err=$err+$ret

    #-----------------------------------------------

    echo "*** start token server without http/https ports"
    ./start_for_test_token.sh -d http 0
    token_pid=$(cat /var/run/user/$EUID/token-server.pid)
    wait_for_exit $token_pid
    ret=$?
    let err=$err+$ret

fi

echo "--- eelapsed    : $tstot seconds"
echo "--- return code : $err"
