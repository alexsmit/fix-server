#!/bin/bash
# usage: start_for_test_teken.sh [valgrind] ttl shutdown

dovalgrind=
ttl=3
refttl=6
shut=300
threads=4
doclean=""
clean_timeout=10
sslport=0
httpport=9998
nossl="-n"
reg_timeout=5
asdaemon=0

cont=1
while [ ! -z "$1" ] ; do
    cont=1
    case $1 in
        --help|help)
            cat <<HHH
Usage:   ./start_for_test_token.sh [valgrind] [reg x] [http x] [ssl] [ssl-port x] [ttl x] [refresh x] [shutdown x] [threads x] [clean] [clean_timout x]

Default (if no paramers are provided): 
	http port   : $httpport (http x)
        ssl port    : $sslport (ssl-port x)
        ttl         : $ttl (ttl x)
        refresh ttl : $refttl (refresh x)
        shutdown    : $shut (shutdown x)
        threads     : $threads (threads x -- not used)
        clean       : $doclean (clean) to enable registration cleanup
        reg timeout : $reg_timeout (reg x)
        clean_timout: $clean_timeout (clean_timout x)

        Similar to running the server with following parameters    

        ./token-server $httpport $sslport $ttl 0 $nossl -r $reg_timeout -e -a $shut -f $refttl -l $clean_timeout $noclean
HHH
            cont=2
        ;;
        valgrind)
            dovalgrind="valgrind --leak-check=full -s --progress-interval=30 --show-leak-kinds=all"
            refttl=60
            ttl=30
            shut=600
        ;;
        -t|--ttl|ttl)
            shift
            ttl=$1
        ;;
        --refresh|-f|refresh)
            shift
            refttl=$1
        ;;
        -a|--shutdown|shutdown)
            shift
            shut=$1
        ;;
        threads)
            shift
            threads=$1
        ;;
        clean)
            shift
            doclean="--clean"
        ;;
        --ssl|ssl)
            sslport=9498
            nossl=""
        ;;
        -s|--ports|ssl-port)
    	    shift
    	    sslport=$1
    	    nossl=""
    	;;
    	-r|--reg|reg)
    	    shift
    	    reg_timeout=$1
    	;;
        -d)
    	    asdaemon=1
        ;;
        http)
    	    shift
    	    httpport=$1
    	;;
        *)
            cont=0
        ;;
    esac
    if [ $cont -ne 1 ] ; then
        break
    fi
    shift
done

if [ $cont -eq 2 ] ; then
    return 0 2>/dev/null || exit 0
fi

echo starting with ttl: $ttl  refttl: $refttl

killall -9 token-server
pushd build > /dev/null

# echo Starting: $dovalgrind ./token-server 9998 $sslport $ttl 0 -n -r 5 -e -a $shut -f $refttl $doclean $usessl $@

if [ $asdaemon -ne 0 ] ; then
    ./token-server $httpport $sslport $ttl 0 $nossl -r $reg_timeout -e -a $shut -f $refttl -l $clean_timeout $doclean $usessl &
    token_pid=$!
    echo $token_pid > /var/run/user/$EUID/token-server.pid
else
    $dovalgrind ./token-server $httpport $sslport $ttl 0 $nossl -r $reg_timeout -e -a $shut -f $refttl -l $clean_timeout $doclean $usessl
fi

popd > /dev/null
