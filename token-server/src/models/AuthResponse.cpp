/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/algorithm/string.hpp>
#include <vx/openid/models/AuthResponse.h>

namespace vx {
  namespace openid {
    namespace models {

      string get_one(ptree& j, const string& key1, const string& key2) {
        auto tmp = j.get(key1, "");
        if (tmp.empty()) tmp = j.get(key2, "");
        return tmp;
      }  // LCOV_EXCL_LINE

      ptree& operator<<(ptree& j, const AuthResponse& response) {
        j.put("expiresAt", response.expiresAt);
        j.put("status", response.status);
        j.put("sessionToken", response.sessionToken);
        j.put("profile.id", response.profile.id);
        j.put("profile.username", response.profile.username);
        j.put("profile.email", response.profile.email);
        return j;
      }

      AuthResponse& operator>>(ptree& j, AuthResponse& response) {
        auto tmp = j.get("expiresAt", "");
        if (!tmp.empty()) {
          if (boost::ends_with(tmp, ".000Z")) {
            tmp = tmp.substr(0, tmp.length() - 5);
          }
          response.expiresAt = tmp;
        }
        else
          response.expiresAt.clear();

        response.status = j.get("status", "");
        response.sessionToken = j.get("sessionToken", "");

        response.profile.id = get_one(j, "profile.id", "_embedded.user.id");
        response.profile.username = get_one(j, "profile.username", "_embedded.user.profile.login");
        response.profile.email = get_one(j, "profile.email", "_embedded.user.profile.email");
        response.profile.firstName = get_one(j, "profile.firstName", "_embedded.user.profile.firstName");
        response.profile.lastName = get_one(j, "profile.lastName", "_embedded.user.profile.lastName");

        return response;
      }

      ptree AuthResponse::toOKTA() {
        ptree j;
        j.put("expiresAt", (!expiresAt.empty()) ? (expiresAt + ".000Z") : "");
        j.put("status", status);
        if (status == "SUCCESS") {
          j.put("sessionToken", sessionToken);
          j.put("_embedded.user.id", profile.id);
          j.put("_embedded.user.passwordChanged", "2021-08-24T23:18:34.000Z");
          j.put("_embedded.user.profile.login", profile.username);
          j.put("_embedded.user.profile.firstName", profile.firstName);
          j.put("_embedded.user.profile.lastName", profile.lastName);
          j.put("_embedded.user.profile.email", profile.email);
          j.put("_embedded.user.profile.locale", "en");
          j.put("_embedded.user.profile.timeZone", "America/Los_Angeles");

          j.put("_links.cancel.href", "");
        }
        return j;
      }  // LCOV_EXCL_LINE

    }  // namespace models
  }    // namespace openid
}  // namespace vx