/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/openid/models/Models.h>

namespace vx {
  namespace openid {
    namespace models {

      ptree& operator<<(ptree& j, const AuthRequest& request) {
        j.put("username", request.username);
        j.put("password", request.password);
        if (request.options.multiOptionalFactorEnroll || request.options.warnBeforePasswordExpired) {
          j.put("options.multiOptionalFactorEnroll", request.options.multiOptionalFactorEnroll);
          j.put("options.warnBeforePasswordExpired", request.options.warnBeforePasswordExpired);
        }
        return j;
      }

      AuthRequest& operator>>(ptree& j, AuthRequest& request) {
        request.username = j.get("username", "");
        request.password = j.get("password", "");
        auto ch = j.get_child_optional("options");
        if (!!ch) {
          request.options.multiOptionalFactorEnroll = (j.get("options.multiOptionalFactorEnroll", "false") == "true");
          request.options.warnBeforePasswordExpired = (j.get("options.warnBeforePasswordExpired", "false") == "true");
        }
        return request;
      }

      AuthRequest& operator>>(VxServer::VxIndex& j, AuthRequest& request) {
        request.username = j["username"];
        request.password = j["password"];
        request.options.multiOptionalFactorEnroll = (j["options.multiOptionalFactorEnroll", "false"] == "true");
        request.options.warnBeforePasswordExpired = (j["options.warnBeforePasswordExpired", "false"] == "true");
        return request;
      }

    }  // namespace models
  }    // namespace openid
}  // namespace vx