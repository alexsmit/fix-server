/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <vx/openid/models/Models.h>

namespace vx {
  namespace openid {
    namespace models {

      TokenRequest& operator>>(VxServer::VxIndex& j, TokenRequest& request) {
        fromVxIndex(j, request);
        return request;
      }

      std::string& operator<<(std::string& s, TokenRequest& j) {
        ptree pt;
        SimpleWeb::CaseInsensitiveMultimap m;
        toPtree(j, pt);

        for (auto& kv : pt) {
          auto val = kv.second.data();
          if (!val.empty()) m.emplace(kv.first, val);
        }

        s += SimpleWeb::QueryString::create(m);
        return s;
      }

    }  // namespace models
  }    // namespace openid
}  // namespace vx