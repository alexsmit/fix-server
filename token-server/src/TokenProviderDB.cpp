/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <TokenProviderDB.h>
#include <vx/PasswordUtil.h>

#include <iostream>
#include <time.h>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <vx/Auth.h>

using namespace std;
using namespace boost::property_tree;
namespace bf = boost::filesystem;

TokenProviderDB::TokenProviderDB() {}
TokenProviderDB::~TokenProviderDB() {
  DBIOEnvironment::ReleaseDatabase(dbio);
  // if (dbio) {
  //   // cout << ">>> TokenProviderDB::~TokenProviderDB(). close db" << endl;
  //   dbio.reset();
  // }
}

void TokenProviderDB::Connect(JsonConfig& _config) {
  config = _config;

  auto db_name = config["database"].str();
  if (db_name.empty())
    throw auth::auth_exception("Database name [database] is not defined");

  auto db_type = config["type"].str();
  DBIOType type = DBIOType::db4;
  if (!db_type.empty() && db_type == "gdbm") type = DBIOType::gdbm;
  if (type == DBIOType::gdbm) {
    bf::path p(db_name);
    p.replace_extension("gdbm");
    db_name = p.string();
  }

  // cout << "Using database " << db_name << " for token provider" << endl;

  auto env_name = config["environment"].str();
  if (env_name.empty())
    throw auth::auth_exception(
        "Database environment [environment] is not defined");

  DBIOEnvironment::OpenEnvironment(env_name.c_str(), type);
  dbio = DBIOEnvironment::GetDatabase(db_name.c_str());

  SetDefaults(config);
}

TokenInfo TokenProviderDB::CreateToken(
    string userName,
    string agent,
    string old_nonce,  //!< old refresh token, if exists and createRefreshRecord is false - we will re-create that record
    unsigned long ttl,
    const string& metadata,
    bool createRefreshRecord) {

  if (!old_nonce.empty() && dbio->ContainsKey(old_nonce.c_str())) {
    if (createRefreshRecord) {
      DeleteTokenByNonce(old_nonce);
      old_nonce = "";
    }
    else {
      createRefreshRecord = true;
    }
  }
  else
    old_nonce = "";

  long iexp = GetEffectiveRefreshTTL();
  if (ttl != 0) iexp = ttl;
  time_t tim = ::time(NULL);
  time_t tim_expire = tim + iexp;

  TokenInfo info;
  if (createRefreshRecord) {
    info.token = (old_nonce.empty()) ? CreateNonce(24) : old_nonce;
  }
  info.nonce = CreateNonce(24);
  info.expried = tim_expire;

  ptree pt;
  pt.put("username", userName);
  pt.put("created", tim);
  pt.put("expires", tim_expire);
  pt.put("user_agent", agent);
  pt.put("token", info.token);  // reference to refresh token
  stringstream stream;
  write_json(stream, pt, false);

  dbio->Put(info.nonce.c_str(), stream.str().c_str());

  if (!metadata.empty()) {
    string noncekey = info.nonce + ":metadata";
    dbio->Put(noncekey.c_str(), metadata.c_str());
  }

  if (createRefreshRecord) {
    ptree pt_ref;
    pt_ref.put("username", userName);
    pt_ref.put("created", tim);
    pt_ref.put("expires", tim_expire);
    pt_ref.put("user_agent", agent);
    pt_ref.put("token", info.nonce);  // refernece to access token
    pt_ref.put("type","refresh");
    stringstream s_ref;
    write_json(s_ref, pt_ref, false);
    dbio->Put(info.token.c_str(), s_ref.str().c_str());
    if (!metadata.empty()) {
      string noncekey = info.token + ":metadata";
      dbio->Put(noncekey.c_str(), metadata.c_str());
    }
  }

  return info;
}

bool TokenProviderDB::ValidateToken(string userName, TokenInfo info, bool nonceOnly) {
  if (!dbio->ContainsKey(info.nonce.c_str())) return false;

  auto str = dbio->Get(info.nonce.c_str());
  ptree pt;
  stringstream s(str);
  read_json(s, pt);

  if (!nonceOnly) {
    auto old_token = pt.get("token", "");
    if (old_token != info.token) return false;
  }

  auto token_username = pt.get("username", "");
  if (token_username.empty() || token_username != userName) return false;

  auto created = pt.get<long>("created", 0);
  if (created == 0) return false;

  time_t tim = ::time(NULL);
  auto expires = pt.get<long>("expires", 0);
  return (expires != 0 && expires >= tim);
}

void TokenProviderDB::Clean() {
  time_t tim = ::time(NULL);
  vector<string> keys;
  dbio->select([&keys](string k, string v) {
    auto ix = k.find(':');
    if (ix != string::npos) return false;

    try {
      stringstream s(v);
      ptree pt;
      read_json(s, pt);
      time_t tim = ::time(NULL);
      auto expires = pt.get<long>("expires", 0);
      if (expires < tim) {
        keys.push_back(k);
      }
    }
    catch (const std::exception& e) {
      keys.push_back(k);
    }

    return false;
  });

  for (auto& k : keys) {
    string noncekey = k + ":metadata";
    if (dbio->ContainsKey(noncekey.c_str())) {
      dbio->Del(noncekey.c_str());
    }
    dbio->Del(k.c_str());
  }
}

// Record an invalid attempt to refresh a token. Used in PIN flow
void TokenProviderDB::Hit(std::string nonce, int max) {
  if (!dbio->ContainsKey(nonce.c_str())) return;

  auto str = dbio->Get(nonce.c_str());
  ptree pt;
  stringstream s(str);
  read_json(s, pt);

  auto scount = pt.get("hits", "0");
  int hits = 0;
  try {
    hits = boost::lexical_cast<int>(scount);
  }
  catch (const std::exception& e) {
  }
  hits++;

  if (max >0 && hits >= max) {
    dbio->Del(nonce.c_str());
  }
  else {
    if (max == -1) hits = 0;
    pt.put("hits", std::to_string(hits));
    stringstream s;
    write_json(s, pt, false);
    dbio->Put(nonce.c_str(), s.str().c_str());
  }
}

string TokenProviderDB::GetMetadata(std::string nonce) {
  string noncekey = nonce + ":metadata";
  if (dbio->ContainsKey(noncekey.c_str())) {
    return dbio->Get(noncekey.c_str());
  }
  return "";
}

bool TokenProviderDB::SetMetadata(std::string nonce, const std::string& medatada) {
  if (dbio->ContainsKey(nonce.c_str())) {
    string noncekey = nonce + ":metadata";
    dbio->Put(noncekey.c_str(), medatada.c_str());
    return true;
  }
  return false;
}

UserToken TokenProviderDB::GetTokenByNonce(std::string nonce) {
  UserToken tok;
  if (dbio->ContainsKey(nonce.c_str())) {
    auto str = dbio->Get(nonce.c_str());
    ptree pt;
    stringstream s(str);
    read_json(s, pt);
    tok.agent = pt.get("user_agent", "");
    tok.expried = pt.get<long>("expires", 0);
    tok.nonce = nonce;
    tok.token = pt.get("token", "");
    tok.username = pt.get("username", "");

    auto key = nonce + ":metadata";
    if (dbio->ContainsKey(key.c_str())) {
      tok.metadata = dbio->Get(key.c_str());
    }
  }

  return tok;
} // LCOV_EXCL_LINE

UserToken TokenProviderDB::GetTokenByRefresh(std::string refresh_token) {
  UserToken tok;
  UserToken tok_refresh = GetTokenByNonce(refresh_token);
  if (!tok_refresh.nonce.empty() && !tok_refresh.token.empty()) {
    return GetTokenByNonce(tok_refresh.token);
  }

  return tok;
}

void TokenProviderDB::DeleteTokenByNonce(std::string nonce) {
  auto rec = GetTokenByNonce(nonce);
  if (rec.nonce.empty()) return;

  if (!rec.token.empty()) {
    if (dbio->ContainsKey(rec.token.c_str())) {
      auto str = dbio->Get(rec.token.c_str());
      ptree pt;
      stringstream s(str);
      read_json(s, pt);
      pt.put("token", "");

      stringstream s_ref;
      write_json(s_ref, pt, false);
      dbio->Put(rec.token.c_str(), s_ref.str().c_str());
    }
  }

  if (dbio->ContainsKey(nonce.c_str())) {
    dbio->Del(nonce.c_str());
  }
  auto key = nonce + ":metadata";
  if (dbio->ContainsKey(key.c_str())) {
    dbio->Del(key.c_str());
  }
}
