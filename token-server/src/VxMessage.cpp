/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <VxMessage.h>

string VxMessages::bad_request = "bad_request";
string VxMessages::not_authorized = "not_authorized";
string VxMessages::invalid_request = "invalid_request";
string VxMessages::invalid_client = "invalid_client";
string VxMessages::invalid_grant = "invalid_grant";
string VxMessages::invalid_token = "invalid_token";
string VxMessages::server_error = "server_error";

string VxMessages::msg_username_password_invalid = "username or password is not valid";
string VxMessages::msg_password_not_defined = "password is not defined";
string VxMessages::msg_username_not_defined = "username is not defined";
string VxMessages::msg_client_id_invalid = "client_id is invalid or not defined";

string VxMessages::msg_svc_unable_to_get_token = "unable to get a service 'token'";
string VxMessages::msg_svc_unable_to_get_profile = "unable to get a service 'profile_service'";
