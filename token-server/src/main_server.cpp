/*
 * Copyright 2022 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define OPENSSL_SUPPRESS_DEPRECATED

#include <stdafx.h>


// #include <vx/JWT.h>
// #include <vx/PtreeUtil.h>
// #include <RSAKeyInfo.h>
// #include <RSACertInfo.h>
#include <vx/web/WebServer.h>

#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/atomic.hpp>
#include <boost/format.hpp>

#include <vx/web/ServerContext.h>
#include <vx/web/Util.h>

#include <vx/dbm/DBIO.h>

#include "TokenServerContext.h"
#include "TokenHandler.h"
#include "RegistrationSVCHandler.h"
#include "DebugHandler.h"
#include "StopHandler.h"
#include "TokenServerInfoHandler.h"
#include "TestHandler.h"
#include <vx/web/EmailHandler.h>
#include "AuthHandler.h"
#include "LoginHandler.h"
#include "HelpHandler.h"

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#elif defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceProfileFactory.h>
#include <vx/ServiceTokenFactory.h>
#include <vx/ServiceEmailFactory.h>

#ifndef VXPROD
#define CLEAN_THREAD_SLEEP 60
#define DRAIN_TIMEOUT 5
#else
#define CLEAN_THREAD_SLEEP 1800
#define DRAIN_TIMEOUT 15
#endif

#ifndef PROJECT_GIT_TAG
#define PROJECT_GIT_TAG "DEBUG"
#endif

using namespace std;
using namespace boost::property_tree;

using namespace VxServer::Handlers;

static bool server_done = false;

void drain(VxServer::WebServer &server, ServerContext &context, vector<thread> &ths) {
  while (!server_done) {
    this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT * 2));
    if (!server.running()) server_done = true;
  }

  context.Logger(LogLevel::debug, "Drain service threads");
  for (auto i = 0; i < ths.size(); i++) {
    if (ths[i].joinable()) {
      ths[i].join();
    }
  }
}

void run(RunParams params) {
  int debug = params.debug;

  int _port = params.port,
      _ports = (params.no_https) ? 0 : params.ports;

  unsigned int clean_timeout = params.clean_timeout;
  if (clean_timeout == 0) clean_timeout = CLEAN_THREAD_SLEEP;

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGHUP);   // 1 Hangup detected on controlling terminal or death of controlling process
  sigaddset(&set, SIGINT);   // 2 Interrupt from keyboard (Ctrl+C)
  sigaddset(&set, SIGQUIT);  // 3 Quit from keyboard
  sigaddset(&set, SIGABRT);  // 6 Abort signal from abort(3)
  sigaddset(&set, SIGTERM);  // 15 Termination request
  sigaddset(&set, SIGTSTP);  // 20 Stop typed at terminal (Ctrl+Z)

  auto psig = pthread_sigmask(SIG_BLOCK, &set, NULL);

  DBIOEnvironment::SetNative(true);

  TokenServerContext context(_port, _ports, params.ttl, (LogLevel)params.debug, params.reg_ttl, params.refresh_ttl, params.profile_provider);
  context.Init("token_provider", "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  auto token_service = make_shared<vx::ServiceTokenFactory>(60);

  context.AddProvider(profile_service);
  context.AddProvider(token_service);
  context.AddProvider(make_shared<vx::ServiceEmailFactory>());

  // threads

  auto sthreads = context.getParam("threads", "");
  if (!sthreads.empty()) {
    unsigned int threads = params.threads;
    try {
      threads = boost::lexical_cast<unsigned int>(sthreads);
      if (threads > 0) params.threads = threads;
    }
    // LCOV_EXCL_START
    catch (const std::exception &e) {
    }
    // LCOV_EXCL_STOP
  }
  if (params.threads < 1) params.threads = POOL_SIZE;

  cout << "Identity server  : " << PROJECT_GIT_TAG
#ifndef NDEBUG
       << " (debug build)"
#endif
       << endl
       << "debug level      : " << params.debug << endl
       << "threads          : " << params.threads << endl
       << "ports            : " << context.port << "," << context.ports << endl
       << "ttl              : " << context.ttl << endl
       << "refresh ttl      : " << context.refresh_ttl << endl
       << "registration ttl : " << context.reg_ttl << endl
       << "cleanup interval : " << clean_timeout << ((!params.doclean) ? " (registration cleanup disabled)" : "") << endl;
  if (params.no_mail) {
    cout << "Send emails      : no" << endl;
  }
#ifndef VXPROD
  if (params.shutdown != 0) {
    cout << "Shutdown after   : " << params.shutdown << " seconds" << endl;
  }
#endif

  if (_port == 0 && _ports == 0) {
    cout << "No listening ports (all disabled)" << endl;
    return;
  }

  if (params.debug == 0) {
    auto sdebug = context.getParam("loglevel");
    if (!sdebug.empty()) {
      debug = boost::lexical_cast<int>(sdebug);
    }

    context.SetLoggerLevel((LogLevel)debug);
  }

  if (params.no_mail) {
    context.setParam("profile_provider.send_token", "on");
    context.setParam("profile_provider.send_email", "off");
  }

  WebServer server(params.threads);

  shared_ptr<Instance> https_server;
  shared_ptr<Instance> http_server;

  if (context.port > 0) {
#ifndef VXPROD
    cout << "Starting HTTP listener on port " << context.port << endl;
#endif
    http_server = server.CreateHttpListener(context.port);
  }

  string server_cert = context.getParam("http_server_certificate");
  string server_key = context.getParam("http_server_key");
  if (!server_cert.empty() && !server_key.empty() && context.ports > 0) {
#ifndef VXPROD
    cout << "Starting HTTPS listener on port " << context.ports << endl;
#endif
    https_server = server.CreateHttpsListener(context.ports, server_cert, server_key);
  }

  // LCOV_EXCL_START
  if (debug > 0) {
    // print all requests
    server.all([&context](std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
      auto addr = req->remote_endpoint_address();
      auto metd = req->method();
      auto path = req->path();
      context.Logger(LogLevel::info, (boost::format("%s - %s %s") % addr % metd % path).str());
    });
  }
  // LCOV_EXCL_STOP

  // secure_server instance will be used to handle all API requests. If SSL is enabled that will be
  // a https instance, otherwise - a default server
  Instance &secure_server = *((!https_server) ? http_server : https_server).get();

  TokenHandler tokenHandler(context, server, secure_server);
  RegistrationSVCHandler registrationSVCHandler(context, server, secure_server);
  DebugHandler debugHandler(context, server, secure_server);
  StopHandler stopHandler(context, server, secure_server);
  TokenServerInfoHandler TokenServerInfoHandler(context, server, secure_server, params.threads);
  TestHandler testHandler(context, server, secure_server);
  EmailHandler emailHandler(context, server, secure_server);
  AuthHandler authHandler(context, server, secure_server);
  LoginHandler loginHandler(context, server, secure_server);
  HelpHandler helpHandler(context, server, secure_server);

  prettyPtree = false;

  auto ValidateClientIDHandler = [&](shared_ptr<Response> response, shared_ptr<Request> request) {
    context.validateRequest(*response, *request);
  };

  // secure_server
  //     .with(ValidateClientIDHandler)
  //     .use("^/authorize$", Request::RequestType::POST, [&context, &server](std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  //       //if (!context.validateRequest(*res, *req)) return;
  //       res->write(SimpleWeb::StatusCode::server_error_not_implemented, "Not implemented");
  //     })

  //     .with(ValidateClientIDHandler)
  //     .use("^/userinfo$", Request::RequestType::POST, [&context, &server](std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  //       // if (!context.validateRequest(*res, *req)) return;
  //       res->write(SimpleWeb::StatusCode::server_error_not_implemented, "Not implemented");
  //     });

  vector<thread> service_threads;

  thread sig_handler([&]() { // LCOV_EXCL_LINE
    int sig;
    sigwait(&set, &sig);
    context.Logger(LogLevel::none, (boost::format("Server terminated with signal: %d") % sig).str());
    server_done = true;
  });
  sig_handler.detach();

  // registration cleanup (normally should be handled by profile server)
  // LCOV_EXCL_START
  if (params.doclean) {
    context.Logger(LogLevel::debug, "Setup service thread: clean registrations");
    service_threads.push_back(thread([&context, &clean_timeout]() {
      while (!server_done) {
        try {
          // run cleanup only if "not-busy" at the moment
          if (!context.mtx.try_lock()) {
            this_thread::sleep_for(chrono::milliseconds(1000));
            continue;
          }

          if (context.getParam("profile_provider.type") == "mysql") {
            context.Logger(LogLevel::trace, "Cleanup registrations");
            shared_ptr<ProfileProvider> profile_provider;
#if defined(VX_USE_MYSQL)
            profile_provider = context.factory.Connect<ProfileProviderMySQL>();
#elif defined(VX_USE_SOCI)
            profile_provider = context.factory.Connect<ProfileProviderSOCI>();
#endif
            profile_provider->CleanRegistrations(context.reg_ttl);
          }
        }
        catch (const std::exception &e) {
          std::cerr << "Token server exception: [clean] " << e.what() << endl;
        }

        context.mtx.unlock();
        // sleep and wake up to exit
        int countdown = clean_timeout;
        while (!server_done && countdown > 0) {
          this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT));
          countdown -= DRAIN_TIMEOUT;
        }
      }
      context.Logger(LogLevel::none, "Registration cleanup thread stopped");
    }));
  }
  // LCOV_EXCL_STOP

  // token cleanup
  service_threads.push_back(thread([&context, &clean_timeout]() {
    while (!server_done) {
      try {
        // run cleanup only if "not-busy" at the moment
        // LCOV_EXCL_START
        if (!context.mtx.try_lock()) {
          this_thread::sleep_for(chrono::milliseconds(1000));
          continue;
        }
        // LCOV_EXCL_STOP

        // cout << " token_cleanup " << flush;
        shared_ptr<TokenProvider> provider = std::move(context.GetTokenProvider());
        provider->Clean();
      }
      catch (const std::exception &e) {
        std::cerr << "Token server exception: [clean] " << e.what() << endl;
      }

      context.mtx.unlock();
      // sleep and wake up cleanup next
      int countdown = clean_timeout;
      while (!server_done && countdown >= 0) {
        this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT));
        countdown -= DRAIN_TIMEOUT;
      }
    }
    context.Logger(LogLevel::none, "Token cleanup thread stopped");
  }));

  service_threads.push_back(thread([&context, &server, &params] {
    auto timestamp_end = ::time(NULL) + params.shutdown;

    while (!server_done) {
      this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT * 2));
      if (params.shutdown != 0) {
        auto timestamp_cur = ::time(NULL);
        if (timestamp_cur > timestamp_end) {
          context.Logger(LogLevel::none, "Automatic server shutdown");
          break;
        }
      }
    }

    if (!server_done || server.running()) {
      std::raise(SIGINT);
      server_done = true;
      this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT * 2));
      server.stop();
    }
    context.Logger(LogLevel::none, "Shutdown thread stopped");
  }));

  server.listen(true);  // run sync listeners

  drain(server, context, service_threads);

#ifndef VXPROD
  {
    auto msg = (boost::format("Token DB close count = %d") % DBIOBase::closed_count).str();
    context.Logger(LogLevel::none, msg);
  }
#endif

  context.Logger(LogLevel::none, "Token Server shutdown");
}
