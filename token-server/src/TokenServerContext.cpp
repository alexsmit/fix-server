/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/variant.hpp>

#include "TokenServerContext.h"
#include <vx/PtreeUtil.h>
#include <vx/StringUtil.h>
#include <ProfileSVC.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/sql/DataProviderMySQL.h>
#endif

#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#include <vx/sql/DataProviderSoci.h>
#endif

#include <ProfileProviderRemote.h>

// #include <vx/sql/DataProviderSoci.h>

#include <vx/ServiceFactory.h>

#include <TokenProviderDB.h>
#include <TokenProviderMySQLBase.h>

#include <boost/lexical_cast.hpp>

TokenServerContext::TokenServerContext(int port,
                                       int ports,
                                       unsigned long ttl,
                                       LogLevel debugLevel,
                                       unsigned long reg_ttl,
                                       unsigned long refresh_ttl,
                                       const string& provider_profile_type)
    : ServerContext(port, ports, ttl, debugLevel),
      reg_ttl(reg_ttl),
      refresh_ttl(refresh_ttl),
      ttl_session_token(60),
      provider_profile_type(provider_profile_type) {
}

TokenServerContext::~TokenServerContext() {
  token_providers.clear();
  profile_providers.clear();
}

void TokenServerContext::Init(const char* prefix, const string& configFileName) {
  ServerContext::Init(prefix, configFileName);

  auto getSectionParam = [&](const string& key, unsigned long& current) {
    if (current != 0) return;
    auto str = section->getParam(key, "");
    if (str.empty()) return;
    current = boost::lexical_cast<unsigned long>(str);
  };

  getSectionParam("ttl_session_token", ttl_session_token);
  getSectionParam("expiration", ttl);
  getSectionParam("refresh_expiration", refresh_ttl);
}

shared_ptr<TokenProvider> TokenServerContext::GetTokenProvider(const string& _type) {
  auto svc = ServerContext::getService("token",_type);
  shared_ptr<TokenProvider> provider = std::dynamic_pointer_cast<TokenProvider>(svc);
  if (!!provider) {
    provider->SetTTL(ttl);
    provider->SetRefreshTTL(refresh_ttl);
    return provider;
  }
  throw invalid_argument(
      "Invalid token service type returned from the factory");
}

shared_ptr<ProfileProvider> TokenServerContext::GetProfileProvider(const string& _type) {
  auto effective_type = (!_type.empty()) ? _type : provider_profile_type;
  auto svc = ServerContext::getService("profile", effective_type);
  shared_ptr<ProfileProvider> provider = std::dynamic_pointer_cast<ProfileProvider>(svc);
  if (!!provider) return provider;

  throw invalid_argument(
      "Invalid profile service type returned from the factory");
}

shared_ptr<ProfileSVC> TokenServerContext::GetProfileSVC(const string& _type) {
  shared_ptr<ProfileSVC> profile_svc;
  profile_svc.reset(new ProfileSVC(*this, GetProfileProvider(_type)));
  return profile_svc;
}

shared_ptr<Service> TokenServerContext::getService(const string& name, const string& type, time_t ttl) {
  if (name == "profile_service") return GetProfileSVC();
  if (name == "token") return GetTokenProvider();
  return ServerContext::getService(name, type, ttl);
}
