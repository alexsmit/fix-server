/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <boost/variant.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <TokenProviderDB.h>
#include <TokenProviderMySQLBase.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/DataProviderMySQL.h>
#endif

#include <vx/sql/DataProviderSoci.h>

#include <vx/ServiceTokenFactory.h>

using namespace vx;
using namespace vx::sql;

ServiceTokenFactory::ServiceTokenFactory(time_t cache_ttl, const string& _conf) {
  provider_ttl = cache_ttl;
  string conf = (_conf.empty()) ? "token_provider" : _conf;
  auto app = AppConfig::getInstancePtr();
  section = app->SectionPtr(conf);
}

std::string ServiceTokenFactory::id() const {
  return "token";
}

std::shared_ptr<Service> ServiceTokenFactory::createService(const std::string& _type) {
  std::lock_guard<std::recursive_mutex> lock{mtx};

  shared_ptr<TokenProvider> token_provider;

  // write_json(cout, section->getTree(""));

  string type = _type;
  if (type.empty()) type = section->getParam("type");

  DataProviderConfig providerConfig;
  string key = string("token.") + type;
  AppConfig& conf = AppConfig::getInstance();
  conf.Section(key).getTree("") >> providerConfig;

  // DB provider
  if (providerConfig.db_type == "db4" || providerConfig.db_type == "db") {
    token_provider = factory.Connect<TokenProviderDB>(key.c_str());
  }
  // MYSQL provider (supported mysql & soci backends)
  else if (providerConfig.db_type == "mysql") {
    string backend = providerConfig.db_backend;
    if (backend.empty()) backend = "mysql";

    shared_ptr<DataProviderMySQLBase> mysql_provider;
#if defined(VX_USE_MYSQL)
    if (backend == "mysql") {
      mysql_provider = factory.Connect<DataProviderMySQL>(key.c_str());
    }
#endif

#if defined(VX_USE_SOCI)
    if (backend == "soci" || backend == "mysql") {
      mysql_provider = factory.Connect<DataProviderSoci>(key.c_str());
    }
#endif
    if (!mysql_provider) {
      throw invalid_argument("Invalid mysql provider");
    }
    token_provider.reset(new TokenProviderMySQLBase(mysql_provider));
  }

  if (!token_provider)
    throw invalid_argument(
        "Invalid token provider type [token_provider]=mysql|db");

  return token_provider;
}
