/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <stdafx.h>

#include <boost/property_tree/json_parser.hpp>
#include <TokenProvider.h>
#include <vx/EncodingUtil.h>

string TokenProvider::id = "token_provider";

string TokenProvider::getIssuer(ServerContext& context) {
  return context.http_host(true);
}

string TokenProvider::CreateNonce(int size) {
  // string buf_nonce;
  // buf_nonce.resize(size);
  // ifstream myrand("/dev/urandom", ios::binary);
  // myrand.read((char*)buf_nonce.data(), buf_nonce.length());
  // string nonce = jwt::base::encode<jwt::alphabet::base64>(buf_nonce);
  // return nonce;
  return random_util().unix_random_alphanum(size);
}

ptree UserToken::parse_metadata() {
  ptree pt;
  if (!metadata.empty()) {
    try {
      stringstream s(metadata);
      read_json(s, pt);
    }
    catch (const std::exception& e) {
      std::cerr << "UserToken metadata error: " << e.what() << '\n';
    }
  }
  return pt;
} // LCOV_EXCL_LINE
