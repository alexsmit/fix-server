/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

RevokeFlow::RevokeFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  req->body >> ar;
  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&RevokeFlow::Verify, this)},
      {VerificationState::done, std::bind(&RevokeFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}

RevokeFlow::~RevokeFlow() {}

BaseFlow& RevokeFlow::Verify() {
  auto client_id = req->auth["basic"].get("client_id", "");

  if (client_id.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (!context.validate(client_id.c_str(), "", true)) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (ar.token.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "token is not provided");
  }

  if (ar.token_type_hint == "refresh_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&RevokeFlow::ExecRefresh, this));
  }
  else if (ar.token_type_hint == "access_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&RevokeFlow::ExecAccess, this));
  }
  else {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid token type");
  }

  verification_state = VerificationState::verified;

  return *this;
}

BaseFlow& RevokeFlow::ExecRefresh() {
  auto token_svc = getTokenProvider();

  auto token = token_svc->GetTokenByRefresh(ar.token);
  if (token.nonce.empty()) {
    cerr << "Revoking wrong refresh token" << endl;
    return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
  }

  auto meta = token.parse_metadata();
  if (meta.get("flow", "") != "PKCE") {
    auto client_id = req->auth["basic"].get("client_id", "");
    auto client_secret = req->auth["basic"].get("client_secret", "");
    if (!context.validate(client_id.c_str(), client_secret.c_str(), false)) {
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
    }
  }

  auto cur = ::time(NULL);
  bool isActive = (token.expried >= cur);
  if (!isActive) {
    cerr << "Revoking expired refresh token" << endl;
  }

  token_svc->DeleteTokenByNonce(ar.token);

  return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
}

BaseFlow& RevokeFlow::ExecAccess() {
  TokenAPI api((TokenServerContext&)context);
  if (!api.Validate(ar.token)) {
    cerr << "Revoking an invalid or expired token\n";
    return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
  }

  auto jti = api.Result().get("jti", "");
  if (jti.empty()) {
    cerr << "Revoking an invalid or expired token\n";
    return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
  }

  auto token_svc = getTokenProvider();

  auto token = token_svc->GetTokenByNonce(jti);
  if (token.nonce.empty()) {
    cerr << "Revoking an invalid or expired token\n";
    return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
  }

  auto meta = token.parse_metadata();
  if (meta.get("type", "") == "id") {
    cerr << "Using ID token for revoke\n";
    return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
  }

  if (meta.get("flow", "") != "PKCE") {
    auto client_id = req->auth["basic"].get("client_id", "");
    auto client_secret = req->auth["basic"].get("client_secret", "");
    if (!context.validate(client_id.c_str(), client_secret.c_str(), false)) {
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
    }
  }

  // delete access token
  token_svc->DeleteTokenByNonce(jti);

  return Empty({}, SimpleWeb::StatusCode::success_ok, {}, true);
}
