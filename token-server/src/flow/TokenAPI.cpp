/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <stdafx.h>

#include <time.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>

#include <vx/StringUtil.h>
#include <vx/JWT.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/EncodingUtil.h>


using namespace boost::posix_time;

TokenAPI::TokenApiContext::TokenApiContext(
    TokenServerContext& context,
    TokenAPI& api,
    const std::string& userid,
    const std::string& audience,
    const std::string& cookie) : context(context), api(api), cookie(cookie) {
  profile_svc = context.getService("profile_service");
  ProfileSVC* psvc = dynamic_cast<ProfileSVC*>(profile_svc.get());
  if (psvc == NULL) throw runtime_error("unable to get a service 'profile_provider'");
  profile_provider = psvc->GetProvider();

  token_svc = context.getService("token");
  token_provider = dynamic_pointer_cast<TokenProvider>(token_svc);
  if (!token_svc) throw runtime_error("unable to get a service 'token'");

  auto sec = context.config->Section("token_provider");

  refresh_token_rotation = (context.section->getParam("refresh_token_rotation", "on") == "on");
  issuer = GetIssuer();
  algo = sec["algo"].str();

  auto it = std::find(api.scopes.begin(), api.scopes.end(), "offline_access");
  add_refresh = (it != api.scopes.end());
  if (api.tokens.size() != 0) add_refresh = false;

  aud = audience;
  if (aud.empty()) aud = sec["audience"].str();
  if (aud.empty()) aud = "api";

  rec = profile_provider->LoadUser(userid.c_str(), ProfileKey::userid);
}

string TokenAPI::TokenApiContext::GetIssuer() {
  auto sec = context.config->Section("token_provider");
  auto server_id = sec["server_id"].str();
  if (server_id.empty()) server_id = "default";
  string issuer = (boost::format("%soauth2/%s") % sec["issuer"].str() % server_id).str();
  return issuer;
}

TokenAPI::TokenAPI(TokenServerContext& context, std::function<void(const std::string& key, const std::string& val)> meta_callback)
    : context(context), meta_callback(meta_callback) {
}

TokenAPI::~TokenAPI() {}

void TokenAPI::Nonce(const std::string& nonce) {
  if (!nonce.empty()) this->nonce = nonce;
}

void TokenAPI::WriteTokenMetadata(
    shared_ptr<TokenProvider> token_svc,
    const string& token_nonce,
    map<string, string> meta,
    const string& type) {
  ptree pt_meta;
  for (auto& kv : meta) {
    pt_meta.put(kv.first, kv.second);
  }
  WriteTokenMetadata(token_svc, token_nonce, pt_meta, type);
}

void TokenAPI::WriteTokenMetadata(
    shared_ptr<TokenProvider> token_svc,
    const string& token_nonce,
    ptree& pt_meta,
    const string& type) {
  pt_meta.put("type", type);
  stringstream smeta;
  write_json(smeta, pt_meta);
  token_svc->SetMetadata(token_nonce, smeta.str());
}

string TokenAPI::GetProperty(TokenAPIProperty type) {
  auto sec = context.config->Section("token_provider");

  switch (type) {
    case TokenAPIProperty::issuer: {
      auto server_id = sec["server_id"].str();
      if (server_id.empty()) server_id = "default";
      string issuer = (boost::format("%soauth2/%s") % sec["issuer"].str() % server_id).str();
      return issuer;
    } break;

    default:
      break;
  }
  return "";
}

TokenInfo TokenAPI::create_token(JWT& access_token, TokenAPI::TokenApiContext& apicontext) {
  auto refresh_ttl = apicontext.token_provider->GetEffectiveRefreshTTL();

  map<string, string> params_access_token;

  set<string> sroles;
  for (auto& r : apicontext.rec.roles) {
    sroles.insert(r);
  }

  access_token.setIssuer(apicontext.issuer);
  access_token.setAlgo(apicontext.algo);
  access_token.setAudience(apicontext.aud);
  access_token.setClaimArray("scp", scopes);

  // create refresh token and JTI for access token
  TokenInfo access_token_info = apicontext.token_provider->CreateToken(
      apicontext.rec.userid.c_str(),
      "web",
      refresh_token_hint,
      refresh_ttl,
      "",
      apicontext.add_refresh && (apicontext.refresh_token_rotation || refresh_token_hint.empty()));

  WriteTokenMetadata(
      apicontext.token_provider,
      access_token_info.nonce,
      {{"nonce", nonce},
       {"scope", scope},
       {"jti", access_token_info.nonce},
       {"cookie", apicontext.cookie},
       {"flow", flow}},
      "access");

  params_access_token.emplace(make_pair("nonce", nonce));
  params_access_token.emplace(make_pair("jti", access_token_info.nonce));

  for (auto& s : scopes) {
    if (s == "openid") {
      access_token.setCliam("sub", apicontext.rec.userid /*rec.email*/);
      continue;
    }
    else if (s == "profile") {  // TODO
      continue;
    }
    else if (s == "email") {
      access_token.setCliam("email", apicontext.rec.email);
    }
    else if (s == "address") {  // part of /userinfo
      continue;
    }
    else if (s == "phone") {  // part of /userinfo
      continue;
    }
    else if (s == "offline_access") {  // ignore offline_access (a refresh token will be created separately)
      continue;
    }
    else if (s == "roles") {
      access_token.setRoles(sroles);
    }
    // else if (s == "nonce") {  // ignore nonce
    //   continue;
    // }
    // all other scopes maps to props
    else {
      auto it = apicontext.rec.props.find(s);
      if (it != apicontext.rec.props.end()) {
        access_token.setCliam(s, it->second);
      }
    }
  }

  jwt_access_token = access_token.Sign(params_access_token, ttl);

  return access_token_info;
}

TokenInfo TokenAPI::create_id_token(JWT& id_token, TokenAPI::TokenApiContext& apicontext, const string& access_token) {
  auto id_ttl = apicontext.token_provider->GetEffectiveRefreshTTL();

  id_token.setIssuer(apicontext.issuer);
  id_token.setAlgo(apicontext.algo);
  id_token.setAudience(apicontext.aud);
  // create id token and JTI for id token
  TokenInfo id_token_info = apicontext.token_provider->CreateToken(apicontext.rec.userid.c_str(), "web", "", ttl);
  WriteTokenMetadata(
      apicontext.token_provider,
      id_token_info.nonce,
      {{"nonce", nonce},
       {"scope", scope},
       {"access_token", access_token},
       {"cookie", apicontext.cookie}},
      "id");

  map<string, string> params_id_token;

  id_token.setCliam("sub", apicontext.rec.userid /*rec.email*/);
  params_id_token.emplace(make_pair("nonce", nonce));
  params_id_token.emplace(make_pair("jti", id_token_info.nonce));

  jwt_id_token = id_token.Sign(params_id_token, id_ttl);
  return id_token_info;
}

void TokenAPI::CreateAccessToken(const std::string& userid, const std::string& audience, const string& cookie) {
  result.clear();

  TokenApiContext apicontext(context, *this, userid, audience, cookie);

  auto sec = context.config->Section("token_provider");

  JWT access_token(*context.info, &context.mtx),
      id_token(*context.info, &context.mtx);
  TokenInfo access_token_info, id_token_info;

  // nonce
  if (nonce.empty()) {
    nonce = apicontext.token_provider->CreateNonce(12);
  }

  // ttl & refresh ttl
  ttl = apicontext.token_provider->GetEffectiveTTL();

  if (tokens.size() == 0 || tokens.find("token") != tokens.end()) {
    access_token_info = create_token(access_token, apicontext);
  }

  if (tokens.size() == 0 || tokens.find("id_token") != tokens.end()) {
    id_token_info = create_id_token(id_token, apicontext, access_token_info.nonce);
  }

  refresh_token = access_token_info.token;

  // if cookie is passed in - update metadata if cookie token exists
  if (!cookie.empty()) {
    auto cookie_token = apicontext.token_provider->GetTokenByNonce(cookie);
    if (!cookie_token.nonce.empty()) {
      ptree meta = cookie_token.parse_metadata();
      meta.put("access_token", access_token_info.nonce);
      meta.put("id_token", id_token_info.nonce);
      WriteTokenMetadata(apicontext.token_provider, cookie, meta, "cookie");
    }
  }

  if (meta_callback != nullptr) {
    if (!access_token_info.nonce.empty())
      meta_callback("access_token", access_token_info.nonce);
    if (!access_token_info.token.empty())
      meta_callback("refresh_token", access_token_info.token);
    if (!id_token_info.nonce.empty())
      meta_callback("id_token", id_token_info.nonce);
  }

  // put to result
  string expires = boost::lexical_cast<string>(ttl);
  if (!jwt_access_token.empty()) {
    result.put("access_token", jwt_access_token);
    string scope = boost::algorithm::join(scopes, " ");
    result.put("scope", scope);
  }
  if (!jwt_id_token.empty()) {
    result.put("id_token", jwt_id_token);
  }
  if (!jwt_id_token.empty() || !jwt_access_token.empty()) {
    result.put("token_type", "Bearer");
    result.put("expires_in", expires);
  }

  if (apicontext.add_refresh) {
    result.put("refresh_token", refresh_token);
  }

  // write_json(cout, result);
}

bool TokenAPI::Scope(const std::string& scope, const std::string& standard_scopes) {
  return Scope(scope);
}

bool TokenAPI::Scope(const std::string& _scope) {
  vector<string> vscopes;
  vector<string> goodvscopes;
  boost::algorithm::split(vscopes, _scope, boost::algorithm::is_any_of(", "), boost::algorithm::token_compress_mode_type::token_compress_on);
  bool invalid = false;
  bool hasOpenid = false;
  for (auto& s : vscopes) {
    if (s == "openid") hasOpenid = true;
    if (s == "nonce") continue;  // ignore scope
    auto v = context.section->getParam(string("scopes.") + s, "n/a");
    if (v != "n/a") goodvscopes.push_back(s);
  }
  if (goodvscopes.size() == 0 || !hasOpenid) return false;

  scopes = goodvscopes;
  scope = boost::algorithm::join(goodvscopes, " ");
  return true;
}

void TokenAPI::Flow(const string& _flow) {
  flow = _flow;
}

void TokenAPI::Tokens(std::set<std::string> tokens) {
  this->tokens = tokens;
}

void TokenAPI::Refresh(const string& refresh_token) {
  refresh_token_hint = refresh_token;
}

ptree TokenAPI::Result() {
  return result;
}

std::string TokenAPI::GetError() {
  return error;
}

std::string TokenAPI::GetErrorDescription() {
  return error_description;
}

bool TokenAPI::HasErrors() {
  return !error.empty();
}

bool TokenAPI::Validate(const string& token) {
  auto svc2 = context.getService("token");
  TokenProvider* token_svc = dynamic_cast<TokenProvider*>(svc2.get());
  if (token_svc == NULL) throw runtime_error("unable to get a service 'token'");

  auto sec = context.config->Section("token_provider");
  auto issuer = GetProperty(TokenAPIProperty::issuer);
  auto algo = sec["algo"].str();
  auto audience = sec["audience"].str();
  if (audience.empty()) audience = "api";

  JWT tokenapi(*context.info, &context.mtx);
  tokenapi.setIssuer(issuer);
  try {
    auto& decoded_token = tokenapi.Verify(token, true);
    if (!decoded_token.isValid()) {
      error = "invalid_token";
      error_description = decoded_token.what();
      return false;
    }
    result.clear();
    result.put("sub", decoded_token.get_payload_claim("sub"));
    result.put("iss", decoded_token.get_issuer());
    if (decoded_token.has_payload_claim("jti"))
      result.put("jti", decoded_token.get_payload_claim("jti"));
    result.put("exp", decoded_token.get_expires_at());
    result.put("iat", decoded_token.get_issued_at());
    if (decoded_token.has_not_before())
      result.put("nbf", decoded_token.get_not_before());

    if (decoded_token.has_payload_claim("scp")) {
      auto arr = decoded_token.get_payload_set("scp");
      scopes.clear();
      std::copy(arr.begin(), arr.end(), std::back_inserter(scopes));
      // for(auto &a: arr) {
      //   scopes.push_back(a);
      // }
      scope = boost::algorithm::join(scopes, " ");
    }

    // if (decoded_token.has_payload_claim("userid"))
    //   result.put("uid", decoded_token.get_payload_claim("userid").as_string());
  }
  catch (const std::exception& e) {
    error = "invalid_token";
    error_description = e.what();
    return false;
  }

  return true;
}
