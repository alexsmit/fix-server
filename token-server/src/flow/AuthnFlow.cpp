/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <time.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

AuthnFlow::AuthnFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  req->body >> ar;

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&AuthnFlow::Verify, this)},
      {VerificationState::verified, std::bind(&AuthnFlow::ExecuteAuthn, this)},
      // LCOV_EXCL_STOP
  };
}

AuthnFlow::AuthnFlow(ServerContext& context, AuthRequest _req)
    : BaseFlow(context), ar(_req) {
  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&AuthnFlow::Verify, this)},
      {VerificationState::verified, std::bind(&AuthnFlow::ExecuteAuthn, this)},
      // LCOV_EXCL_STOP
  };
}

AuthnFlow::~AuthnFlow() {}

BaseFlow& AuthnFlow::Verify() {
  if (ar.username.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::bad_request, VxMessages::msg_username_not_defined);

  if (ar.password.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::bad_request, VxMessages::msg_password_not_defined);

  verification_state = VerificationState::verified;
  return *this;
}

BaseFlow& AuthnFlow::ExecuteAuthn() {

  auto profile_provider = getProfileProvider();
  auto token_svc = getTokenProvider();

  bool isAuthenticated = profile_provider->Authenticate(ar.username.c_str(), ar.password.c_str());
  if (!isAuthenticated)
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, VxMessages::msg_username_password_invalid);

  auto user_profile = profile_provider->LoadUser(ar.username.c_str());

  // nonce => {refresh_token, username, user_agent, created, (expired in 30 sec or as configured)}
  auto token_info = token_svc->CreateToken(
      user_profile.userid,
      "authn",
      "", ((TokenServerContext&)context).ttl_session_token);

  auto pexpired = boost::posix_time::from_time_t(token_info.expried);
  auto sexprired = to_iso_extended_string(pexpired);

  AuthResponse aresp = {
      sexprired,
      "SUCCESS",
      token_info.nonce,
      {user_profile.userid,
       user_profile.username,
       user_profile.email,
       user_profile.props["firstName"],
       user_profile.props["lastName"]}};

  ptree pt;

  if (context.section->getParam("compatibility") == "OKTA") {
    pt = aresp.toOKTA();
  }
  else {
    pt << aresp;
  }

  Result(pt,
         SimpleWeb::StatusCode::success_ok,
         {{"Cache-Control", "no-store"}, {"Pragma", "no-cache"}},
         true);

  return *this;
}
