/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

LogoutFlow::LogoutFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  req->params >> ar;

  vxcookie = req->cookies[SESSION_COOKIE];

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&LogoutFlow::Verify, this)},
      {VerificationState::verified, std::bind(&LogoutFlow::Exec, this)},
      // LCOV_EXCL_STOP
  };
}

LogoutFlow::~LogoutFlow() {}

BaseFlow& LogoutFlow::Verify() {
  //--- verify URL

  auto stree = context.section.get()->getTree("");
  auto vuri = stree.get_child_optional("logout_uri");
  if (!vuri) {
    return Error(SimpleWeb::StatusCode::server_error_internal_server_error, "server_error", "logout_uri section is missing in the config");
  }
  bool redirect_ok = false;
  for (auto& kv : *vuri) {
    auto u = kv.second.data();
    if (u == ar.post_logout_redirect_uri) {
      redirect_ok = true;
    }
  }
  if (!redirect_ok) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid logout url");
  }

  //--- verify session cookie

  if (vxcookie.empty()) {
    map<string, string> res;
    if (!ar.state.empty()) res.emplace("state", ar.state);

    redirect_uri = ar.post_logout_redirect_uri;

    string secure = (context.isSecure()) ? "Secure;" : "";
    string cookie = (boost::format("%s=%s;Path=/;%s;HttpOnly;SameSite=None;max-age=0") % SESSION_COOKIE % vxcookie % secure).str();

    return Empty(res, SimpleWeb::StatusCode::redirection_found, {{"Set-Cookie", cookie}});
  }

  //--- verify token

  if (ar.id_token_hint.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "token not provided");
  }

  TokenAPI api((TokenServerContext&)context);
  if (!api.Validate(ar.id_token_hint)) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid token");
  }

  auto token_svc = getTokenProvider();

  id_token = token_svc->GetTokenByNonce(api.Result().get("jti", ""));
  if (id_token.nonce.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid token");
  }

  auto meta = id_token.parse_metadata();
  if (meta.get("type", "") != "id") {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid token");
  }

  verification_state = VerificationState::verified;
  return *this;
}

BaseFlow& LogoutFlow::Exec() {
  auto token_provider = getTokenProvider();
  auto cookie_token = token_provider->GetTokenByNonce(vxcookie);

  // delete session cookie, id_token and access_token
  if (!cookie_token.nonce.empty()) {
    auto metadata = cookie_token.parse_metadata();
    auto id_token_ref = metadata.get("id_token", "");
    if (id_token_ref != id_token.nonce) {
      cerr << "Logout: cookie is referencing different id token" << endl;
    }
    auto access_token_ref = metadata.get("access_token", "");
    token_provider->DeleteTokenByNonce(id_token_ref);
    token_provider->DeleteTokenByNonce(access_token_ref);
    token_provider->DeleteTokenByNonce(vxcookie);
  }

  string secure = (context.isSecure()) ? "Secure;" : "";
  string cookie = (boost::format("%s=%s;Path=/;%s;HttpOnly;SameSite=None;max-age=0") % SESSION_COOKIE % vxcookie % secure).str();

  map<string, string> res;
  if (!ar.state.empty()) res.emplace("state", ar.state);

  redirect_uri = ar.post_logout_redirect_uri;

  return Result(
      res,
      SimpleWeb::StatusCode::redirection_found,
      {{"Set-Cookie", cookie}});
}
