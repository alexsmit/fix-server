/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <AuthHandler.h>
#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

// #include <vx/inja/vx_inja.h>
#include <vx/inja/vx_environment.h>
#include <VxMessage.h>


using namespace boost::posix_time;
using namespace vx::inja;

const vector<string> allowed_response_modes = {"query", "fragment", "form_post"};
const vector<string> allowed_prompt_modes = {"login", "consent", "none", "select_account"};
const set<string> allowed_response_types = {"code", "token", "id_token"};

// class FlowBuilder {
//   public:
//     AuthorizeFlow& flow;
//     FlowBuilder(AuthorizeFlow& flow) : flow(flow) {

//     }
// };

AuthorizeFlow::AuthorizeFlow(ServerContext& context, AuthorizeRequest _req, const std::string& session_cookie)
    : BaseFlow(context), ar(_req), vxcookie(session_cookie) {
  redirect_uri = ar.redirect_uri;
  authorize_return_code = SimpleWeb::StatusCode::redirection_found;

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&AuthorizeFlow::Verify, this)},
      {VerificationState::verified, std::bind(&AuthorizeFlow::ExecuteVerified, this)},
      {VerificationState::done, std::bind(&AuthorizeFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}

AuthorizeFlow::AuthorizeFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  if (req->method() == "POST") {
    req->body >> ar;
  }
  else {
    req->params >> ar;
  }

  redirect_uri = ar.redirect_uri;
  authorize_return_code = SimpleWeb::StatusCode::redirection_found;

  vxcookie = req->cookies[SESSION_COOKIE];

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&AuthorizeFlow::Verify, this)},
      {VerificationState::verified, std::bind(&AuthorizeFlow::ExecuteVerified, this)},
      {VerificationState::done, std::bind(&AuthorizeFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}

AuthorizeFlow::~AuthorizeFlow() {}

BaseFlow& AuthorizeFlow::Verify() {
  if (ar.client_id.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::bad_request, VxMessages::msg_client_id_invalid);
  }

  if (!context.validate(ar.client_id.c_str(), "", true)) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, VxMessages::msg_client_id_invalid);
  }

  auto stree = context.section.get()->getTree("");
  auto vuri = stree.get_child_optional("callback_uri");
  if (!vuri) {
    return Error(SimpleWeb::StatusCode::server_error_internal_server_error, VxMessages::server_error, "callback_uri section is missing in the config");
  }
  bool redirect_ok = false;
  for (auto& kv : *vuri) {
    auto u = kv.second.data();
    if (u == ar.redirect_uri) {
      redirect_ok = true;
    }
  }
  if (!redirect_ok) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::bad_request, "invalid callback_uri parameter");
  }

  // errors via redirect

  if (ar.response_type.empty()) {
    return Error(authorize_return_code, VxMessages::bad_request, "response_type is missing");
  }
  else {
    auto mode = boost::algorithm::token_compress_mode_type::token_compress_on;
    boost::algorithm::split(response_types, ar.response_type, boost::algorithm::is_any_of(" "), mode);
    bool found_all_supported = std::all_of(
        response_types.begin(),
        response_types.end(),
        [&](string val) { return allowed_response_types.find(val) != allowed_response_types.end(); });
    if (!found_all_supported) {
      return Error(authorize_return_code, VxMessages::bad_request, "invalid response_type");
    }
  }

  if (!ar.response_mode.empty()) {
    if (ar.response_mode == "query") {
      if (response_types.find("token") != response_types.end() ||
          response_types.find("id_token") != response_types.end()) {
        return Error(authorize_return_code, VxMessages::bad_request, "invalid response_mode");
      }
    }
    else {
      bool found = std::any_of(allowed_response_modes.begin(), allowed_response_modes.end(), [&](string val) { return val == ar.response_mode; });
      if (!found) {
        found |= (context.section->getParam("compatibility") == "OKTA" && ar.response_mode == "okta_post_message");
        if (found) {
          authorize_return_code = SimpleWeb::StatusCode::success_ok;
        }
      }

      if (!found)
        return Error(authorize_return_code, VxMessages::bad_request, "invalid response_mode");

      if (ar.response_mode == "form_post")
        authorize_return_code = SimpleWeb::StatusCode::success_ok;
    }
  }
  else {
    ar.response_mode = "query";
    if (response_types.find("token") != response_types.end() ||
        response_types.find("id_token") != response_types.end())
      ar.response_mode = "fragment";
  }

  if (ar.scope.empty()) {
    return Error(authorize_return_code, VxMessages::bad_request, "scope is missing");
  }
  else {
    TokenAPI api((TokenServerContext&)context);
    if (!api.Scope(ar.scope))
      return Error(authorize_return_code, "invalid_scope", "scope is not supported");
  }

  if (ar.state.empty()) {
    return Error(authorize_return_code, VxMessages::bad_request, "state is missing");
  }

  if (!ar.code_challenge_method.empty() && (ar.code_challenge_method != "S256" && ar.code_challenge_method != "S512")) {
    return Error(authorize_return_code, VxMessages::bad_request, "code_challenge should be S256");
  }

  if (!ar.code_challenge.empty() && ar.code_challenge_method.empty()) {
    return Error(authorize_return_code, VxMessages::bad_request, "code_challenge_method is not provided");
  }

  if (!ar.prompt.empty()) {
    bool valid = std::any_of(allowed_prompt_modes.begin(), allowed_prompt_modes.end(), [&](string val) { return val == ar.prompt; });
    if (!valid || ar.prompt != "none")
      return Error(authorize_return_code, VxMessages::bad_request, "invalid or unsupported prompt value");
  }

  if (ar.sessionToken.empty()) {
    if (!vxcookie.empty()) {
      verification_state = VerificationState::verified;
      return *this;
    }

    return LoginRequired();
  }
  else {
    verification_state = VerificationState::verified;
    return *this;
  }
}

void AuthorizeFlow::DeleteSessionCookie() {
  if (!vxcookie.empty()) {
    auto token_svc = getTokenProvider();
    auto user_token = token_svc->GetTokenByNonce(vxcookie);
    if (!user_token.nonce.empty()) {
      auto meta = user_token.parse_metadata();
      token_svc->DeleteTokenByNonce(meta.get("id_token", ""));
      token_svc->DeleteTokenByNonce(meta.get("access_token", ""));
    }
  }
  vxcookie.clear();
}

BaseFlow& AuthorizeFlow::ProcessSession() {
  if (verification_state != VerificationState::verified) return *this;
  auto token_svc = getTokenProvider();

  // 1) if session cookie is not empty, load it, session cookie will be deleted
  if (!ar.sessionToken.empty()) {
    user_token = token_svc->GetTokenByNonce(ar.sessionToken);
    if (!user_token.nonce.empty()) DeleteSessionCookie();
  }

  // 2) token is still empty, check session cookie
  if (user_token.nonce.empty()) {
    if (!vxcookie.empty()) {
      user_token = token_svc->GetTokenByNonce(vxcookie);
      if (!user_token.nonce.empty()) {
        time_t cur = ::time(NULL);
        auto meta = user_token.parse_metadata();
        token_svc->DeleteTokenByNonce(meta.get("id_token", ""));
        token_svc->DeleteTokenByNonce(meta.get("access_token", ""));
        // session cookie actually expired, cannot use it anymore
        if (cur > user_token.expried) user_token.nonce.clear();
      }
    }
  }

  // delete original session token or session cookie
  if (!user_token.nonce.empty()) {
    token_svc->DeleteTokenByNonce(user_token.nonce);
  }

  if (user_token.nonce.empty()) {
    return LoginRequired();
  }

  return *this;
}

BaseFlow& AuthorizeFlow::CreateCookie() {
  if (verification_state != VerificationState::verified) return *this;
  auto token_svc = getTokenProvider();

  // create session cookie
  cookie_token = token_svc->CreateTokenMeta(
      user_token.username,
      "session",
      "",
      ((TokenServerContext&)context).refresh_ttl,
      {{"scope", ar.scope}});

  // sid=1029C8B0nzxRjWFh9-mbb4kMw;Version=1;Path=/;Secure;HttpOnly;SameSite=None
  string secure = (context.isSecure()) ? "Secure;" : "";
  string cookie = (boost::format("%s=%s;Path=/;%s;HttpOnly;SameSite=None") % SESSION_COOKIE % cookie_token.nonce % secure).str();
  authorize_header_map.emplace("Set-Cookie", cookie);

  return *this;
}

BaseFlow& AuthorizeFlow::ExecuteCode() {
  if (verification_state != VerificationState::verified) return *this;
  auto token_svc = getTokenProvider();

  // create second token for code processing
  code_token = token_svc->CreateTokenMeta(
      user_token.username,
      "code",
      "",
      ((TokenServerContext&)context).ttl_session_token,
      {{"nonce", ar.nonce},
       {"scope", ar.scope},
       {"code_challenge", ar.code_challenge},
       {"code_challenge_method", ar.code_challenge_method},
       {"cookie", cookie_token.nonce}});

  authorize_return_map.emplace("code", code_token.nonce);

  return *this;
}

BaseFlow& AuthorizeFlow::ExecuteTokens() {
  if (verification_state != VerificationState::verified) return *this;

  TokenAPI api((TokenServerContext&)context);
  api.Scope(ar.scope, "openid");
  api.Nonce(ar.nonce);
  if (!ar.code_challenge.empty()) api.Flow("PKCE");
  api.Tokens(response_types);
  api.CreateAccessToken(user_token.username, ar.client_id, cookie_token.nonce);

  auto result = api.Result();
  auto token = result.get("id_token", "");
  if (!token.empty()) authorize_return_map.emplace("id_token", token);
  token = result.get("access_token", "");
  if (!token.empty()) {
    authorize_return_map.emplace("access_token", token);
    authorize_return_map.emplace("token_type", result.get("token_type", ""));
  }

  return *this;
}

BaseFlow& AuthorizeFlow::ExecuteVerified() {
  if (ar.response_mode == "fragment") returnQuery = false;

  ProcessSession();
  if (verification_state != VerificationState::verified) return *this;

  CreateCookie();
  if (verification_state != VerificationState::verified) return *this;

  if (response_types.find("code") != response_types.end()) ExecuteCode();
  if (verification_state != VerificationState::verified) return *this;

  authorize_return_map.emplace("state", ar.state);

  if (response_types.find("token") != response_types.end() ||
      response_types.find("id_token") != response_types.end()) ExecuteTokens();
  if (verification_state != VerificationState::verified) return *this;

  return Result(
      authorize_return_map,
      authorize_return_code,
      authorize_header_map);
}

BaseFlow& AuthorizeFlow::LoginRequired() {
  string secure = (context.isSecure()) ? "Secure;" : "";
  string cookie = (boost::format("%s=%s;Path=/;%s;HttpOnly;SameSite=None;max-age=0") % SESSION_COOKIE % SimpleWeb::Percent::encode(vxcookie) % secure).str();

  returnHeader.emplace("Set-Cookie", cookie);
  return Error(
      authorize_return_code,
      "login_required",
      "prompt set to none, login required",
      "",
      ar.state);
}

void AuthorizeFlow::SendResponseFormPost(std::shared_ptr<Response> res, ptree& pt) {
  string template_path = "./templates/post/";
  VxEnvironment env(template_path);

  json data;

  vector<string> keys;
  vector<string> values;
  for (auto& kv : pt) {
    keys.push_back(kv.first);
    values.push_back(kv.second.data());
  }

  data["keys"] = keys;
  data["values"] = values;
  data["title"] = "Autorizing...";
  data["callback"] = ar.redirect_uri;

  VxMultipart multi(env);
  auto result = multi.render_file("form_post", data, VxFileType::html);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-Type", "text/html");
  header.emplace("cache-control", "no-cache, no-store");
  header.emplace("pragma", "no-cache");

  res->write(SimpleWeb::StatusCode::success_ok, result, header);
}

void AuthorizeFlow::SendResponseOkta(std::shared_ptr<Response> res, ptree& pt) {
  string template_path = "./templates/post/";
  VxEnvironment env(template_path);

  json data;

  vector<string> keys;
  vector<string> values;
  for (auto& kv : pt) {
    keys.push_back(kv.first);
    values.push_back(kv.second.data());
  }

  data["keys"] = keys;
  data["values"] = values;

  auto& h = req->header();
  auto itref = h.find("Referer");
  if (itref != h.end()) {
    data["referer"] = itref->second;
  } else {
    data["referer"] = "*";
  }

  VxMultipart multi(env);
  auto result = multi.render_file("okta_post_message", data, VxFileType::html);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-Type", "text/html");
  header.emplace("cache-control", "no-cache, no-store");
  header.emplace("pragma", "no-cache");

  res->write(SimpleWeb::StatusCode::success_ok, result, header);
}

void AuthorizeFlow::SendResponse(std::shared_ptr<Response> res, ptree& pt,
                                 SimpleWeb::CaseInsensitiveMultimap& header, SimpleWeb::StatusCode code) {
  if (context.section->getParam("compatibility") == "OKTA" && ar.response_mode == "okta_post_message") {
    SendResponseOkta(res, pt);
  }
  else if (ar.response_mode == "form_post") {
    SendResponseFormPost(res, pt);
  }
  else {
    BaseFlow::SendResponse(res, pt, header, code);
  }
}
