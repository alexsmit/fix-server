/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>

#include <vx/StringUtil.h>
#include <vx/EncodingUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

TokenFlow::TokenFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  if (req->method() == "POST") {
    req->body >> ar;
  }
  else {
    req->params >> ar;
  }

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&TokenFlow::Verify, this)},
      // {VerificationState::verified, std::bind(&TokenFlow::ExecuteVerified, this)},
      {VerificationState::done, std::bind(&TokenFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}

TokenFlow::TokenFlow(ServerContext& context, TokenRequest _req)
    : BaseFlow(context), ar(_req) {
  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&TokenFlow::Verify, this)},
      // {VerificationState::verified, std::bind(&TokenFlow::ExecuteVerified, this)},
      {VerificationState::done, std::bind(&TokenFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}


TokenFlow::~TokenFlow() {}

BaseFlow& TokenFlow::Verify() {
  if (!!req) {
    ar.client_id = req->auth["basic"].get("client_id", "");
    ar.client_secret = req->auth["basic"].get("client_secret", "");
  }

  if (ar.client_id.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (!context.validate(ar.client_id.c_str(), "", true)) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (ar.grant_type == "refresh_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&TokenFlow::ExecuteRefreshToken, this));
  }
  else if (ar.grant_type == "authorization_code") {
    step_methods.emplace(VerificationState::verified, std::bind(&TokenFlow::ExecuteCode, this));
  }
  else if (ar.grant_type == "password") {
    step_methods.emplace(VerificationState::verified, std::bind(&TokenFlow::ExecutePassword, this));
  }
  else {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, "unsupported_grant_type", "invalid grant_type");
  }

  verification_state = VerificationState::verified;
  return *this;
}

string TokenFlow::MergeScopes(const string& old_scopes, const string& new_scopes) {
  if (!new_scopes.empty()) {
    auto vold = StringUtil::split(old_scopes, ", ");
    auto vnew = StringUtil::split(new_scopes, ", ");
    set<string> snew, sold;
    std::copy(vnew.begin(), vnew.end(), std::inserter(snew, snew.begin()));
    std::copy(vold.begin(), vold.end(), std::inserter(sold, sold.begin()));

    vector<string> vmerged;
    for (auto& v : sold) {
      if (snew.find(v) != snew.end()) vmerged.push_back(v);
    }
    return boost::algorithm::join(vmerged, " ");
  }
  return old_scopes;
}

BaseFlow& TokenFlow::ExecuteCode() {
  if (ar.code.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "invalid grant_type");
  if (ar.redirect_uri.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "redirect_uri is not defined");

  auto token_svc = getTokenProvider();

  UserToken code_token = token_svc->GetTokenByNonce(ar.code);
  time_t cur = ::time(NULL);
  if (code_token.nonce.empty() || cur > code_token.expried) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "invalid or expired code");
  }

  auto metadata = code_token.parse_metadata();
  auto standard_scope = metadata.get("scope", "");
  auto scope = MergeScopes(standard_scope, ar.scope);

  // PKCE validate
  auto code_challenge = metadata.get("code_challenge", "");
  auto code_challenge_method = metadata.get("code_challenge_method", "");
  if (!code_challenge.empty()) {
    if (ar.code_verifier.empty())
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "code verifier is not provided");
    string algo;
    if (code_challenge_method == "S256")
      algo = "sha256";
    else if (code_challenge_method == "S512")
      algo = "sha512";
    else {
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "code challenge method is not valid");
    }

    // note: incoming hash may be in base64url format or contains no '=' characters. we will normalize before compare
    code_challenge = random_util().frombase64url(code_challenge);

    // our has always has '=' trailing
    string hash = RSAKeyInfo::getHash(ar.code_verifier, algo);

    if (hash.compare(0, hash.length(), code_challenge) != 0)
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "code verifier is not valid");
  }

  // generate token
  TokenAPI api((TokenServerContext&)context, [&](const string& key, const string& val) {
    metadata.put(key, val);
  });
  api.Scope(scope, standard_scope);
  auto nonce = metadata.get("nonce", "");
  api.Nonce(nonce);
  api.Flow("PKCE");
  api.CreateAccessToken(code_token.username, ar.client_id, metadata.get("cookie", ""));

  if (api.HasErrors()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, api.GetError(), api.GetErrorDescription());
  }

  return Result(
      api.Result(),
      SimpleWeb::StatusCode::success_ok,
      {{"Cache-Control", "no-store"}, {"Pragma", "no-cache"}},
      true);
}

BaseFlow& TokenFlow::ExecuteRefreshToken() {
  if (ar.refresh_token.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "invalid grant_type");

  auto token_svc = getTokenProvider();

  auto token = token_svc->GetTokenByRefresh(ar.refresh_token);
  if (token.nonce.empty())
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "invalid refresh token");

  auto cur = ::time(NULL);
  if (cur > token.expried)
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "expired refresh token");

  ptree metadata = token.parse_metadata();
  auto flow = metadata.get("flow", "");

  if (flow != "PKCE") {
    if (!context.validate(ar.client_id.c_str(), ar.client_secret.c_str()))
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  auto standard_scope = metadata.get("scope", "");  //!< scopes, associated with refresh token
  auto nonce = metadata.get("nonce", "");

  // processing requested scopes, should be a subset of existing list of scopes
  auto scope = MergeScopes(standard_scope, ar.scope);

  TokenAPI api((TokenServerContext&)context, [&](const string& key, const string& val) {
    metadata.put(key, val);
  });
  api.Scope(scope, standard_scope);
  api.Nonce(nonce);
  api.Refresh(ar.refresh_token);
  api.CreateAccessToken(token.username);

  if (api.HasErrors()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, api.GetError(), api.GetErrorDescription());
  }

  return Result(
      api.Result(),
      SimpleWeb::StatusCode::success_ok,
      {{"Cache-Control", "no-store"}, {"Pragma", "no-cache"}},
      true);
}

BaseFlow& TokenFlow::ExecutePassword() {
  if (!context.validate(ar.client_id.c_str(), ar.client_secret.c_str()))
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  if (ar.password.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "invalid grant_type");
  if (ar.scope.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "scope is missing");
  if (ar.username.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_grant, "username is missing");

  auto profile_svc = getProfileService();

  shared_ptr<ProfileData> profile;
  try {
    profile = profile_svc->Authenticate(ar.username.c_str(), ar.password.c_str(), false);
  }
  catch (const std::exception& e) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::not_authorized, "wrong username or password");
  }

  TokenAPI api((TokenServerContext&)context, [&](const string& key, const string& val) {
    metadata.put(key, val);
  });
  api.Scope(ar.scope);
  api.CreateAccessToken(profile->userid);

  if (api.HasErrors()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, api.GetError(), api.GetErrorDescription());
  }

  return Result(
      api.Result(),
      SimpleWeb::StatusCode::success_ok,
      {{"Cache-Control", "no-store"}, {"Pragma", "no-cache"}},
      true);
}
