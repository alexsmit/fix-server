/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <AuthHandler.h>
#include <vx/StringUtil.h>
#include <ResponseBuilder.h>
#include <vx/openid/models/Models.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;
using namespace vx::openid::flow;

BaseFlow::BaseFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : context(context),
      res(res),
      req(req),
      returnCode(SimpleWeb::StatusCode::success_ok),
      returnCORS(false),
      verification_state(VerificationState::none),
      returnEmpty(false),
      returnQuery(true) {
}

BaseFlow::BaseFlow(ServerContext& context)
    : context(context),
      returnCode(SimpleWeb::StatusCode::success_ok),
      returnCORS(false),
      verification_state(VerificationState::none),
      returnEmpty(false),
      returnQuery(true) {
}

BaseFlow::~BaseFlow() {
  token_service.reset();
  profile_service.reset();
}

BaseFlow& BaseFlow::Error(
    SimpleWeb::StatusCode code,
    const std::string& error,
    const std::string& error_description,
    const std::string& error_url,
    const std::string& state,
    bool corsHeader) {
  verification_state = VerificationState::done;
  returnCode = code;
  returnCORS = corsHeader;
  returnDataTree.put("error", error);
  returnDataTree.put("error_description", error_description);
  if (!error_url.empty()) {
    returnDataTree.put("error_url", error_url);
  }
  if (!state.empty()) {
    returnDataTree.put("state", state);
  }
  return *this;
}

BaseFlow& BaseFlow::Result(ptree result,
                           SimpleWeb::StatusCode code,
                           map<string, string> extra_header,
                           bool corsHeader) {
  verification_state = VerificationState::done;
  returnCode = code;
  returnCORS = corsHeader;
  returnDataTree = result;

  for (auto& kv : extra_header) {
    returnHeader.emplace(kv.first, kv.second);
  }

  return *this;
}

BaseFlow& BaseFlow::Result(std::map<string, string> result,
                           SimpleWeb::StatusCode code,
                           map<string, string> extra_header,
                           bool corsHeader) {
  ptree pt;
  for (auto& kv : result) {
    pt.put(kv.first, kv.second);
  }
  return Result(pt, code, extra_header, corsHeader);
}

BaseFlow& BaseFlow::Empty(
    std::map<string, string> result,
    SimpleWeb::StatusCode code, map<string, string> extra_header,
    bool corsHeader) {
  verification_state = VerificationState::done;
  returnEmpty = true;
  returnCode = code;
  returnCORS = corsHeader;
  returnDataTree.clear();
  for (auto& kv : result) {
    returnDataTree.put(kv.first, kv.second);
  }
  for (auto& kv : extra_header) {
    returnHeader.emplace(kv.first, kv.second);
  }

  return *this;
}

ptree BaseFlow::GetResult() {
  return returnDataTree;
}

ptree BaseFlow::GetMetadata() {
  return metadata;
}

SimpleWeb::StatusCode BaseFlow::GetResultCode() {
  return returnCode;
}

SimpleWeb::CaseInsensitiveMultimap BaseFlow::GetResultHeader() {
  return returnHeader;
}

void BaseFlow::SendResponse(std::shared_ptr<Response> res, ptree& pt,
                            SimpleWeb::CaseInsensitiveMultimap& header, SimpleWeb::StatusCode code) {
  sendJSON(res, pt, header, code);
}

BaseFlow& BaseFlow::Send() {
  if (verification_state != VerificationState::done) return *this;
  verification_state = VerificationState::sent;

  switch (returnCode) {
    case SimpleWeb::StatusCode::redirection_found: {
      SimpleWeb::CaseInsensitiveMultimap query_map;
      for (auto& kv : returnDataTree) {
        query_map.emplace(kv.first, kv.second.data());
      }

      string sep = (returnQuery) ? "?" : "#";
      string query = SimpleWeb::QueryString::create(query_map);
      if (query.empty())
        returnHeader.emplace("Location", redirect_uri);
      else
        returnHeader.emplace("Location", redirect_uri + sep + query);

      if (!!res)
        res->write(SimpleWeb::StatusCode::redirection_found, "", returnHeader);
    } break;

    case SimpleWeb::StatusCode::server_error_internal_server_error:
    case SimpleWeb::StatusCode::client_error_unauthorized:
    case SimpleWeb::StatusCode::client_error_bad_request: {
      ptree pt_error;
      static vector<string> fields = {"error", "error_description", "state", "error_url"};
      for (auto& f : fields) {
        auto v = returnDataTree.get(f, "");
        if (!v.empty()) pt_error.put(f, v);
      }

      if (!!res) {
        if (!!req && returnCORS) {
          ((TokenServerContext&)context).AddCORS(req, returnHeader);
        }
        if (returnEmpty)
          res->write(returnCode, "", returnHeader);
        else
          SendResponse(res, pt_error, returnHeader, returnCode);
      }
    } break;

    default: {
      if (!!res) {
        if (!!req && returnCORS) {
          ((TokenServerContext&)context).AddCORS(req, returnHeader);
        }

        if (returnEmpty)
          res->write(returnCode, "", returnHeader);
        else
          SendResponse(res, returnDataTree, returnHeader, returnCode);
      }
    } break;
  }

  return *this;
}

void BaseFlow::Execute() {
  if (step_methods.size() == 0) return;

  metadata.clear();

  while (verification_state != VerificationState::sent) {
    auto old_state = verification_state;

    auto fit = step_methods.find(verification_state);
    if (fit == step_methods.end()) break;
    fit->second();

    if (verification_state == old_state) {
      verification_state = VerificationState::done;
      break;
    }
  }

  if (verification_state != VerificationState::sent) Send();
}

TokenProvider* BaseFlow::getTokenProvider() {
  if (!token_service) {
    token_service = context.getService("token");
  }
  auto token_svc = dynamic_cast<TokenProvider*>(token_service.get());
  if (token_svc == NULL) throw WebException(VxMessages::msg_svc_unable_to_get_token);
  return token_svc;
}

std::shared_ptr<ProfileProvider> BaseFlow::getProfileProvider() {
  ProfileSVC* profile_svc = getProfileService();
  if (profile_svc == NULL) throw WebException(VxMessages::msg_svc_unable_to_get_profile);
  auto profile_provider = profile_svc->GetProvider();
  return profile_provider;
}

ProfileSVC* BaseFlow::getProfileService() {
  if (!profile_service) {
    profile_service = context.getService("profile_service");
  }
  ProfileSVC* profile_svc = dynamic_cast<ProfileSVC*>(profile_service.get());
  if (profile_svc == NULL) throw WebException(VxMessages::msg_svc_unable_to_get_profile);
  return profile_svc;
}
