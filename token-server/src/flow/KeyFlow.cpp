/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

KeyFlow::KeyFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  fromVxIndex(req->params, ar);

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&KeyFlow::Verify, this)},
      {VerificationState::verified, std::bind(&KeyFlow::ExecKeys, this)},
      // LCOV_EXCL_STOP
  };
}

KeyFlow::~KeyFlow() {}

BaseFlow& KeyFlow::Verify() {
  if (!ar.client_id.empty()) {
    if (!context.validate(ar.client_id.c_str(), "", true)) {
      return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, "client_id is invalid");
    }
  }

  verification_state = VerificationState::verified;
  return *this;
}

BaseFlow& KeyFlow::ExecKeys() {
  string algo = context.section->getParam("algo");
  // returnHeader.emplace("Cache-Control", "max-age=3600");

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  auto pt = context.info->toJWKStree(algo.c_str());
  return Result(
      pt,
      SimpleWeb::StatusCode::success_ok,
      {{"Cache-Control", string("max-age=2592000, must-revalidate")}},
      true);
}
