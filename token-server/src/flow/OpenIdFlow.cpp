/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>



using namespace boost::posix_time;

void AddArray(ptree& pt, const string& key, vector<string> values);
ptree GenerateOpenIdConfiguration(ServerContext& context);

OpenIdFlow::OpenIdFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  step_methods = {
      {VerificationState::none, std::bind(&OpenIdFlow::Exec, this)}};
}

OpenIdFlow::~OpenIdFlow() {}

BaseFlow& OpenIdFlow::Exec() {
  return Result(
      GenerateOpenIdConfiguration(context),
      SimpleWeb::StatusCode::success_ok,
      {{"Cache-Control", "max-age=86400, must-revalidate"}},
      true);
}

void AddArray(ptree& pt, const string& key, vector<string> values) {
  ptree pt_values;
  vector2ptree(values, pt_values);
  pt.add_child(key, pt_values);
}

void AddPtreeArray(ptree& pt, const string& key, ptree& values, const string& values_key, bool fromKey = false) {
  if (values.count(values_key) > 0) {
    auto pt_child = values.get_child(values_key);
    vector<string> values_vector;
    for (auto& s : pt_child) {
      if (fromKey)
        values_vector.push_back(s.first);
      else
        values_vector.push_back(s.second.data());
    }
    AddArray(pt, key, values_vector);
  }
}

ptree GenerateOpenIdConfiguration(ServerContext& context) {
  ptree pt;
  string issuer = context.getParam("token_provider.issuer");
  string server_id = context.getParam("token_provider.server_id");
  string oauth = (boost::format("oauth2/%s/v1/") % server_id).str();
  TokenAPI api((TokenServerContext&)context);
  if (!issuer.empty()) {
    string real_issuer = api.GetProperty(TokenAPIProperty::issuer);  // TokenProvider::getIssuer(context);
    ptree& pt_token_provider = context.section->getTree("");

    pt.put("issuer", real_issuer);
    pt.put("jwks_uri", real_issuer + "/.well-known/jwks.json");

    AddPtreeArray(pt, "id_token_signing_alg_values_supported", pt_token_provider, "algos");

    pt.put("authorization_endpoint", real_issuer + "/v1/authorize");
    pt.put("token_endpoint", real_issuer + "/v1/token");
    pt.put("userinfo_endpoint", real_issuer + "/v1/userinfo");

    AddArray(pt, "token_endpoint_auth_methods_supported", {"client_secret_basic", "client_secret_post"});
    AddPtreeArray(pt, "scopes_supported", pt_token_provider, "scopes", true);
    AddArray(pt, "code_challenge_methods_supported", {"S256", "S512"});

    AddArray(pt, "response_types_supported", {"code", "token", "id_token"});
    AddArray(pt, "response_modes_supported", {"query", "fragment", "form_post"});

    // ptree pt_subject_types_supported;
    // vector<string> subject_types_supported = {"public"};
    // vector2ptree(subject_types_supported, pt_subject_types_supported);
    // pt.add_child("subject_types_supported", pt_subject_types_supported);
  }

  return pt;
}
