/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

LoginFlow::LoginFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&LoginFlow::Verify, this)},
      {VerificationState::verified, std::bind(&LoginFlow::Exec, this)},
      // LCOV_EXCL_STOP
  };
}

LoginFlow::~LoginFlow() {}

BaseFlow& LoginFlow::Verify() {
  return Error(SimpleWeb::StatusCode::server_error_internal_server_error, "server_error", "not implemented");
}

BaseFlow& LoginFlow::Exec() {
  return Error(SimpleWeb::StatusCode::server_error_internal_server_error, "server_error", "not implemented");
}
