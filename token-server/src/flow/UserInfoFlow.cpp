/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <map>
#include <time.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/algorithm/string.hpp>

#include <vx/StringUtil.h>
#include <vx/JWT.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

using namespace std;

/*
{
    "email": "email",
    "password": "password",
    "username": "test",
    "pin": "1234",
    "logins": "11",
    "last_login": "11111",
    "userid": "abcf3b8a-2862-11ec-a952-a85e453782da",
    "updated_at": "9999",
    "roles": [
        "admin"
    ],
    "properties": {
        "prop": "value"
    }
}
*/

UserInfoFlow::UserInfoFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&UserInfoFlow::Verify, this)},
      {VerificationState::verified, std::bind(&UserInfoFlow::Exec, this)},
      // LCOV_EXCL_STOP
  };
}

UserInfoFlow::~UserInfoFlow() {}

BaseFlow& UserInfoFlow::Verify() {
  auto auth = req->auth["authorization"].get("Bearer", "");
  if (auth.empty())
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_token, "bearer token is not provided or invalid");

  TokenAPI api((TokenServerContext&)context);
  if (!api.Validate(auth))
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_token, "bearer token is not provided or invalid");

  scope = api.GetScope();
  scopes = api.GetScopes();

  // write_json(cout, api.Result());

  userid = api.Result().get("sub", "");
  verification_state = VerificationState::verified;
  return *this;
}

// void

BaseFlow& UserInfoFlow::Exec() {
  ptree pt;

  auto profile_provider = getProfileProvider();

  auto rec = profile_provider->LoadUser(userid.c_str(), ProfileKey::userid);
  auto prec = rec.toPtree();

  // write_json(cout, prec);

  pt.put("sub", rec.userid);
  for (auto& s : scopes) {
    auto scopeKey = string("scopes.") + s;
    auto scopeMeta = context.section->getParam(scopeKey);
    if (!scopeMeta.empty()) {
      auto vprops = StringUtil::split(scopeMeta, ",");
      for (auto& p : vprops) {
        string infoProp = p;
        auto vpp = StringUtil::split(p, ":");
        if (vpp.size() > 1) {  // map our name -> userinfo name
          p = vpp[0];
          infoProp = vpp[1];
        }
        else {  // using only last part of the property as userinfo name
          auto vp = StringUtil::split(p, ".");
          if (vp.size() > 1) {
            infoProp = vp[vp.size() - 1];
          }
        }
        auto pval = prec.get(p, "");
        if (!pval.empty()) {
          // cout << infoProp << " = " << pval << endl;;
          pt.put(infoProp, pval);
        }
      }
      continue;
    }

    if (s == "offline_access") {
    }
    else if (s == "roles") {
      pt.put("roles", boost::algorithm::join(rec.roles, " "));
    }
    else if (s == "nonce") {
    }
    else {
      auto val = rec[s];
      if (!val.empty()) pt.put(s, val);
    }
  }

  return Result(pt, SimpleWeb::StatusCode::success_ok, {}, true);
}
