/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <time.h>
#include <boost/property_tree/json_parser.hpp>

#include <vx/StringUtil.h>
#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/flow/Flow.h>
#include <VxMessage.h>


using namespace boost::posix_time;

IntrospectFlow::IntrospectFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req)
    : BaseFlow(context, res, req) {
  req->body >> ar;

  step_methods = {
      // LCOV_EXCL_START
      {VerificationState::none, std::bind(&IntrospectFlow::Verify, this)},
      {VerificationState::done, std::bind(&IntrospectFlow::Send, this)},
      // LCOV_EXCL_STOP
  };
}

IntrospectFlow::IntrospectFlow(ServerContext& context, IntrospectRequest _req)
    : BaseFlow(context), ar(_req) {
  step_methods = {
      {VerificationState::none, std::bind(&IntrospectFlow::Verify, this)},
      {VerificationState::done, std::bind(&IntrospectFlow::Send, this)},
  };
}


IntrospectFlow::~IntrospectFlow() {}

BaseFlow& IntrospectFlow::Verify() {
  if (ar.client_id.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (!context.validate(ar.client_id.c_str(), ar.client_secret.c_str(), true)) {
    return Error(SimpleWeb::StatusCode::client_error_unauthorized, VxMessages::invalid_client, VxMessages::msg_client_id_invalid);
  }

  if (ar.token.empty()) {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "token is not provided");
  }

  if (ar.token_type_hint == "refresh_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&IntrospectFlow::ExecRefresh, this));
  }
  else if (ar.token_type_hint == "id_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&IntrospectFlow::ExecId, this));
  }
  else if (ar.token_type_hint == "access_token") {
    step_methods.emplace(VerificationState::verified, std::bind(&IntrospectFlow::ExecAccess, this));
  }
  else {
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid token type");
  }

  verification_state = VerificationState::verified;

  return *this;
}

BaseFlow& IntrospectFlow::ExecRefresh() {
  auto token_svc = getTokenProvider();
  auto profile_provider = getProfileProvider();

  auto token = token_svc->GetTokenByRefresh(ar.token);
  if (token.nonce.empty())
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, "invalid refresh token");

  auto cur = ::time(NULL);

  ptree pt;
  bool isActive = (token.expried >= cur);
  pt.put("active", isActive);

  if (isActive) {
    auto meta = token.parse_metadata();
    auto profile = profile_provider->LoadUser(token.username.c_str(), ProfileKey::userid);
    pt.put("token_type", "Bearer");
    pt.put("client_id", ar.client_id);
    pt.put("uid", token.username);
    pt.put("exp", token.expried);
    pt.put("scope", meta.get("scope", ""));
    pt.put("sub", profile.email);
    pt.put("username", profile.username);
    pt.put("jti", meta.get("jti", ""));
  }

  return Result(pt);
}

BaseFlow& IntrospectFlow::ExecId() {
  return ExecToken("id");
}

BaseFlow& IntrospectFlow::ExecAccess() {
  return ExecToken("access");
}

BaseFlow& IntrospectFlow::ExecToken(const string& type) {
  TokenAPI api((TokenServerContext&)context);
  if (!api.Validate(ar.token)) {
    auto msg = boost::format("invalid or expired %s token") % type;
    return Error(SimpleWeb::StatusCode::client_error_bad_request, VxMessages::invalid_request, msg.str());
  }

  auto token_svc = getTokenProvider();
  auto profile_provider = getProfileProvider();

  auto api_result = api.Result();
  auto jti = api_result.get("jti", "");

  auto access_token = token_svc->GetTokenByNonce(jti);
  // cout << "meta: " << access_token.metadata << endl;
  bool active = (!access_token.nonce.empty());

  ptree pt;
  pt.put("active", active);
  if (active) {
    auto meta = access_token.parse_metadata();
    auto profile = profile_provider->LoadUser(api_result.get("sub", "").c_str(), ProfileKey::userid);
    pt.put("token_type", "Bearer");
    pt.put("client_id", ar.client_id);
    // pt.put("uid", api_result.get("uid", ""));
    pt.put("exp", api_result.get("exp", ""));
    pt.put("iat", api_result.get("iat", ""));
    auto v = api_result.get("nbf", "");
    if (!v.empty()) pt.put("nbf", v);

    pt.put("scope", meta.get("scope", ""));

    pt.put("sub", api_result.get("sub", ""));
    pt.put("username", profile.username);

    if (type == "access") {
      pt.put("jti", api_result.get("jti", ""));
    }
  }

  return Result(pt);
}
