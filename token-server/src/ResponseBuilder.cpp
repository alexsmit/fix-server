/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define OPENSSL_SUPPRESS_DEPRECATED

#include <stdafx.h>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include "ResponseBuilder.h"
#include <VxMessage.h>

using namespace auth;

inline ResponseBuilder::ResponseBuilderStep operator|(ResponseBuilder::ResponseBuilderStep lhs, ResponseBuilder::ResponseBuilderStep rhs) {
  return static_cast<ResponseBuilder::ResponseBuilderStep>(
      static_cast<std::underlying_type<ResponseBuilder::ResponseBuilderStep>::type>(lhs) |
      static_cast<std::underlying_type<ResponseBuilder::ResponseBuilderStep>::type>(rhs));
}

ResponseBuilder::ResponseBuilder(std::shared_ptr<Response> response, std::shared_ptr<Request> request,
                                 ServerContext& context)
    : response(response), request(request), context(context) {
  auto sec = context.config->Section("token_provider");

  issuer = sec["issuer"].str();
  algo = sec["algo"].str();
  step = ResponseBuilderStep::Initial;
  audience = sec["audience"].str();
  if (audience.empty()) audience = "api";
  pin_after_expiration = sec["pin_after_expiration"] == "on";
}

string ResponseBuilder::GetUserId() {
  return userid;
}

ProfileSVC* ResponseBuilder::GetProfileService() {
  if (!profile_service)
    profile_service = context.getService("profile_service");
  auto svc = dynamic_cast<ProfileSVC*>(profile_service.get());
  if (svc == NULL) {
    throw auth_exception("profile service is not available");  // LCOV_EXCL_LINE
  }
  return svc;
}

TokenProvider* ResponseBuilder::GetTokenService() {
  if (!token_service)
    token_service = context.getService("token");
  auto svc = dynamic_cast<TokenProvider*>(token_service.get());
  if (svc == NULL) {
    throw auth_exception("token provider is not available");  // LCOV_EXCL_LINE
  }
  return svc;
}

void ResponseBuilder::LoadGrant() {
  grant_type = request->body["grant_type"];
  grant_value = request->body[grant_type];
}

void ResponseBuilder::Failed(const string& message, int code) {
  string msg = (boost::format("Authorization failed. %s.") % message).str();
  throw auth_exception(code, msg.c_str());
}

ResponseBuilder& ResponseBuilder::Bearer(bool bWithExpiration) {
  auto header = request->header();
  auto auth = header.find("Authorization");
  if (auth == header.end())
    throw auth_exception("Authorization header missing");

  vector<string> vauth = StringUtil::split(auth->second, ' ');

  string errmsg{"Invalid Authorization header"};
  if (vauth.size() == 2) {
    auto authkey = boost::algorithm::to_lower_copy(vauth[0]);
    if (authkey != "bearer")
      throw auth_exception(errmsg);
  }
  else {
    throw auth_exception(errmsg);
  }

  JWT tokenapi(*context.info, &context.mtx);
  tokenapi.setIssuer(issuer);

  try {
    auto& decoded_token = tokenapi.Verify(vauth[1], bWithExpiration);
    if (!decoded_token.isValid()) {
      Failed("Invalid token");
    }

    if (decoded_token.has_payload_claim("userid"))
      userid = decoded_token.get_payload_claim("userid");

    if (!decoded_token.has_payload_claim("aud")) {
      Failed("Invalid audience");
    }

    auto aud = decoded_token.get_audience();
    if (aud.find(audience) == aud.end())
      Failed("Invalid audience");

    if (decoded_token.has_payload_claim("roles")) {
      auto sroles = decoded_token.get_payload_set("roles");
      roles.clear();
      for (auto& r : sroles) {
        roles.push_back(r);
      }
    }

    nonce = (decoded_token.has_payload_claim("nonce"))
                ? decoded_token.get_payload_claim("nonce")
                : "";

    step = step | ResponseBuilderStep::Identity;

    return *this;
  }
  catch (const auth::auth_exception& ae) {
    throw;
  }
  catch (const std::exception& e) {
    throw auth_exception("Unable to validate token");
  }
}

bool ResponseBuilder::HasRole(const string role) {
  auto it = std::find_if(roles.begin(), roles.end(), [&](const string r) { return r == role; });
  return it != roles.end();
}

ResponseBuilder& ResponseBuilder::WithAuthenticate() {
  // LCOV_EXCL_START
  if (grant_type != "password")
    throw auth_exception("Invalid auth request. Wrong grant");
  // LCOV_EXCL_STOP

  user = request->body["username"];
  auto len = user.length();
  if (len == 0 || len > 64) {
    throw auth_exception(400, "Invalid user name", VxMessages::bad_request.c_str());
  }

  len = grant_value.length();
  if (len == 0 || len > 64) {
    throw auth_exception(400, "Invalid password", VxMessages::bad_request.c_str());
  }

  auto tmpscope = request->body["scope"];
  if (!tmpscope.empty()) {
    scope = tmpscope;
  }

  try {
    auto profile_service = GetProfileService();
    auto response = profile_service->Authenticate(user.c_str(), grant_value.c_str());
    roles = response->roles;
    userid = response->userid;
  }
  catch (const auth_exception& aex) {
    throw;
  }
  // LCOV_EXCL_START
  catch (const std::exception& e) {
    cerr << e.what();
    Failed("Wrong username or password");
  }
  // LCOV_EXCL_STOP

  step = ResponseBuilderStep::FullyAuthenticated;

  return *this;
}

ResponseBuilder& ResponseBuilder::WithRefreshToken() {
  // LCOV_EXCL_START
  if (grant_type != "refresh_token")
    throw auth_exception("Invalid auth request. Wrong grant");
  // LCOV_EXCL_STOP

  auto token_service = GetTokenService();
  UserToken tok_refresh = token_service->GetTokenByNonce(grant_value);
  auto result = token_service->ValidateToken("", tok_refresh);
  if (!result) {
    throw auth_exception(401, "Invalid refresh token", "invalid_token");
  }
  UserToken tok_access = token_service->GetTokenByNonce(tok_refresh.token);
  if (tok_access.nonce.empty()) {
    throw auth_exception(401, "Invalid refresh token", "invalid_token");
  }

  userid = tok_refresh.username;
  nonce = tok_refresh.token;
  refresh_nonce = tok_refresh.nonce;

  auto profile_service = GetProfileService();
  auto response = profile_service->GetProfileData(userid.c_str(), true);
  roles = response->roles;

  step = step | ResponseBuilderStep::FullyAuthenticated;

  return *this;
}

ResponseBuilder& ResponseBuilder::WithPINAuthenticate() {
  if (nonce.empty())
    throw auth_exception("Nonce is empty");

  auto token_service = GetTokenService();

  try {
    // LCOV_EXCL_START
    if (grant_type != "pin")
      throw auth_exception(400, "Invalid auth request. Wrong grant", VxMessages::bad_request.c_str());
    // LCOV_EXCL_STOP

    if (grant_value.empty() || grant_value.length() > 6)
      throw auth_exception(400, "Invalid auth request. Wrong PIN", VxMessages::bad_request.c_str());

    UserToken tok_access = token_service->GetTokenByNonce(nonce);
    UserToken tok_refresh;

    if (!pin_after_expiration) {
      bool nonceOK = token_service->ValidateToken(userid.c_str(), tok_access);
      if (!nonceOK) throw auth_exception(401, "Invalid nonce", "invalid_token");
    }
    else {
      token_service->DeleteTokenByNonce(nonce);
      token_service->DeleteTokenByNonce(tok_access.token);
    }

    refresh_nonce = tok_access.token;

    auto profile_service = GetProfileService();
    auto response = profile_service->AuthenticatePin(userid.c_str(), grant_value.c_str(), true);
    roles = response->roles;

    token_service->Hit(nonce, -1);  // reset pin count
  }
  catch (const auth_exception& aex) {
    token_service->Hit(nonce);
    throw;
  }
  // LCOV_EXCL_START
  catch (const std::exception& e) {
    string msg = string("PIN verification failed: ") + e.what();
    token_service->Hit(nonce);
    throw auth_exception(401, msg.c_str(), "not_authorized");
  }
  // LCOV_EXCL_STOP

  step = step | ResponseBuilderStep::Authenticated;

  return *this;
}

ResponseBuilder& ResponseBuilder::CreateAccessToken() {
  // LCOV_EXCL_START
  if (step != ResponseBuilderStep::Completed) {
    throw auth_exception("Logic error. Access denied.");
  }
  // LCOV_EXCL_STOP

  context.Logger(LogLevel::debug, "ResponseBuilder::CreateAccessToken()");

  set<string> sroles;
  for (auto& f : roles) {
    sroles.insert(f);
  }

  // 1.1) if a user has no roles -- deny access
  if (sroles.size() == 0) {
    Failed("Access denied", 403);
  }

  // 2) set claims
  map<string, string> params;
  params.emplace(make_pair("userid", userid));

  JWT tokenapi(*context.info, &context.mtx);
  tokenapi.setIssuer(issuer);
  tokenapi.setAlgo(algo);
  tokenapi.setRoles(sroles);
  tokenapi.setAudience(audience);

  if (!scope.empty())
    tokenapi.setCliam("scp", scope);

  params.emplace(make_pair("nonce", nonce));

  auto token_service = GetTokenService();
  access_token = tokenapi.Sign(params, token_service->GetEffectiveTTL());

  step = ResponseBuilderStep::Completed;

  return *this;
}

ResponseBuilder& ResponseBuilder::CreateRefreshToken() {
  // LCOV_EXCL_START
  if (step != ResponseBuilderStep::FullyAuthenticated) {
    throw auth_exception("Logic error. Access denied.");
  }
  // LCOV_EXCL_STOP

  context.Logger(LogLevel::debug, "ResponseBuilder::CreateRefreshToken()");

  auto token_rotation = context.section->getParam("refresh_token_rotation", "on") == "on";
  TokenInfo info;
  auto token_service = GetTokenService();
  info = token_service->CreateToken(userid.c_str(), "web", refresh_nonce, 0, "", refresh_nonce.empty() || token_rotation);

  refresh_token = info.token;
  nonce = info.nonce;

  step = step | ResponseBuilderStep::Refresh;

  // cout << "builder: refresh_token: " << refresh_token << '\n';

  return *this;
}

ResponseBuilder& ResponseBuilder::WithValidate() {
  // LCOV_EXCL_START
  if (grant_type != "validate")
    throw auth_exception("Invalid auth request. Wrong grant");
  // LCOV_EXCL_STOP

  if (nonce.empty())
    throw auth_exception("Nonce is empty");

  auto token_service = GetTokenService();
  try {
    if (!pin_after_expiration) {
      TokenInfo tinfo = {.token = "", .nonce = nonce};
      bool nonceOK = false;
      nonceOK = token_service->ValidateToken(userid.c_str(), tinfo, true);
      if (!nonceOK) Failed("Invalid token");
    }
  }
  catch (const auth_exception& au) {
    token_service->Hit(nonce);
    throw;
  }
  // LCOV_EXCL_START
  catch (const std::exception& e) {
    token_service->Hit(nonce);
    Failed(e.what());
  }
  // LCOV_EXCL_STOP

  return *this;
}

ResponseBuilder& ResponseBuilder::SendOKResponse() {
  response->write(SimpleWeb::StatusCode::success_ok, "");
  return *this;
}

ResponseBuilder& ResponseBuilder::SendResponse() {
  auto token_service = GetTokenService();
  ptree pt_token;
  pt_token.put("access_token", access_token);
  pt_token.put("token_type", "bearer");
  if (!refresh_token.empty()) pt_token.put("refresh_token", refresh_token);
  string sexp = boost::lexical_cast<string>(token_service->GetEffectiveTTL());
  pt_token.put("expires_in", sexp);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace(make_pair("Cache-Control", "no-store"));
  header.emplace(make_pair("Pragma", "no-cache"));
  sendJSON(response, pt_token, header, SimpleWeb::StatusCode::success_ok);
  return *this;
}

ResponseBuilder& ResponseBuilder::WithClientCredentials() {

  client_id = request->body["client_id"];
  client_secret = request->body["client_secret"];
  scope = request->body["scope"];

  auto acl = context.validateACL(client_id, client_secret);

  auto token_service = GetTokenService();
  access_token = acl.token(*context.info, token_service->GetEffectiveTTL());

  step = ResponseBuilderStep::Authenticated;
  return *this;
}

ResponseBuilder& ResponseBuilder::SendClientCredentialsResponse() {
  ptree pt_token;
  pt_token.put("access_token", access_token);
  pt_token.put("token_type", "bearer");

  auto token_service = GetTokenService();
  if (!refresh_token.empty()) pt_token.put("refresh_token", refresh_token);
  string sexp = boost::lexical_cast<string>(token_service->GetEffectiveTTL());
  pt_token.put("expires_in", sexp);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace(make_pair("Cache-Control", "no-store"));
  header.emplace(make_pair("Pragma", "no-cache"));
  sendJSON(response, pt_token, header, SimpleWeb::StatusCode::success_ok);
  return *this;
}

void ResponseBuilder::Exec() {
  LoadGrant();

  map_grants["password"] = [&]() {
    WithAuthenticate()
        .CreateRefreshToken()
        .CreateAccessToken()
        .SendResponse();
  };
  map_grants["pin"] = [&]() {
    Bearer(false)
        .WithPINAuthenticate()
        .CreateRefreshToken()
        .CreateAccessToken()
        .SendResponse();
  };
  map_grants["refresh_token"] = [&]() {
    WithRefreshToken()
        .CreateRefreshToken()
        .CreateAccessToken()
        .SendResponse();
  };
  map_grants["validate"] = [&]() {
    Bearer(false)
        .WithValidate()
        .SendOKResponse();
  };

  map_grants["client_credentials"] = [&]() {
    WithClientCredentials()
        .SendClientCredentialsResponse();
  };

  auto it = map_grants.find(GetGrantType());
  if (it == map_grants.end()) {
    throw auth_exception("Invalid or unsupported grant type");
  }

  it->second();
}
