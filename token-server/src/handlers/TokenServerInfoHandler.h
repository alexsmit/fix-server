/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _INFO_HANDLER_H
#define _INFO_HANDLER_H

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>

namespace VxServer {
  namespace Handlers {

    /// information handler
    class TokenServerInfoHandler {
    private:
      ServerContext& context;
      WebServer& server;
      Instance& secure_server;
      int pool_size;

    public:
      /// create info handler
      TokenServerInfoHandler(ServerContext& context, WebServer& server, Instance& secure_server, int pool_size);
      /// destructor. noop
      ~TokenServerInfoHandler();

      /// handler
      void Info(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// render standard version JSON
      void Version(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer

#endif