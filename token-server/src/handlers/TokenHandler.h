#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>
#include <vx/web/WebServer.h>
#include <vx/web/VxHandler.h>
#include <vx/web/ServerContext.h>
#include <TokenServerContext.h>

namespace VxServer {
  namespace Handlers {

    /// access token operations
    class TokenHandler : public VxHandler {
    private:
      TokenServerContext& token_context;

      string OpenIdConfiguration();

      void AddArray(ptree& pt, const string& key, vector<string> values);

    public:
      /// crete token handler object
      TokenHandler(TokenServerContext& context, WebServer& server, Instance& secure_server);
      /// destructor. noop
      ~TokenHandler();

      /// Validate if the Origin is enabled and add cross-origin header
      void AddCORS(std::shared_ptr<Request> req, SimpleWeb::CaseInsensitiveMultimap &header);
      
      // Handlers
      /// /.well-known/openid-configuration
      void GetOpenidConfig(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      
      /// get token endpoint
      void GetToken(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// get key/cert fingerprint
      void GetFingerprint(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer
