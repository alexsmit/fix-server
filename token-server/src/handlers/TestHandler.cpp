/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define OPENSSL_SUPPRESS_DEPRECATED

#include <boost/variant.hpp>

#include "TestHandler.h"
#include <vx/StringUtil.h>
#include <vx/web/Util.h>
#include "ResponseBuilder.h"

using namespace VxServer::Handlers;

TestHandler::TestHandler(ServerContext& context, WebServer& server, Instance& secure_server)
    : context(context), server(server), secure_server(secure_server) {
  secure_server
      .use("^/test$", Request::RequestType::POST,
           std::bind(&TestHandler::Post, *this, std::placeholders::_1, std::placeholders::_2, string("")))
      .use("^/test/admin$", Request::RequestType::POST,
           std::bind(&TestHandler::Post, *this, std::placeholders::_1, std::placeholders::_2, string("admin")))
      .use("^/test/user$", Request::RequestType::POST,
           std::bind(&TestHandler::Post, *this, std::placeholders::_1, std::placeholders::_2, string("user")))
      .use("^/test/other$", Request::RequestType::POST,
           std::bind(&TestHandler::Post, *this, std::placeholders::_1, std::placeholders::_2, string("other")))

      ;
}

TestHandler::~TestHandler() {}

void TestHandler::Post(std::shared_ptr<Response> res, std::shared_ptr<Request> req, string role) {
  try {
    auth::ResponseBuilder builder(res, req, context);
    builder.Bearer();

    if (!role.empty() && !builder.HasRole(role)) {
      // cerr << "/test: invalid role: " << role << '\n';
      sendError(res, "auth_error", "invalid role", SimpleWeb::StatusCode::client_error_unauthorized);
    }
    else {
      sendJSON(res, req->body.value());
    }
  }
  catch (const std::exception& e) {
    // cerr << "/test: " << e.what() << '\n';
    sendError(res, "auth_error", e.what(), SimpleWeb::StatusCode::client_error_unauthorized);
  }
}
