/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _AUTH_HANDLER_H
#define _AUTH_HANDLER_H

#include <stdafx.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/web/VxHandler.h>
#include <TokenServerContext.h>

namespace VxServer {
  namespace Handlers {

    /// Oauth and openid connect authorization handler
    class AuthHandler : public VxHandler{
    private:
      TokenServerContext& token_context;
      
    public:
      /// create authorization handler object. All hanlders are using secure_server iinstance.
      AuthHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      /// destructior. noop
      ~AuthHandler();

      /// /api/v1/authn
      void GetAuthN(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/authorize
      void Authorize(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/token
      void Token(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/introspect
      void Introspect(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/revoke
      void Revoke(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/logout
      void Logout(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/keys
      void Keys(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/userinfo
      void Userinfo(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/.well-known/oauth-authorization-server
      void AuthServer(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /oauth2/{server_id}/v1/.well-known/openid-configuration
      void OpenIDConfig(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// /callback dummy endpoint
      void Callback(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

    };

  }  // namespace Handlers
}  // namespace VxServer
#endif