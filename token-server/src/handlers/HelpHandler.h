/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _HELP_HANDLER_H
#define _HELP_HANDLER_H

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>

namespace VxServer {
  namespace Handlers {

    /// all registration operations
    class HelpHandler {
    private:
      ServerContext& context;
      WebServer& server;
      Instance& secure_server;

      // string LoadTemplate(string filename, map<string, string> params);

    public:
      /// create registration handler object, uses secure_server
      HelpHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      /// destructor. noop
      ~HelpHandler();
      
      /// Help endpoint. All help files are loaded from /template/help
      void Help(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer

#endif