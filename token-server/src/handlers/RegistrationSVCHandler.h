/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _REGISTRATION_HANDLER_H
#define _REGISTRATION_HANDLER_H

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/VxHandler.h>
#include <vx/web/ServerContext.h>

#include <ProfileSVC.h>

namespace VxServer {
  namespace Handlers {

    /// all registration operations
    class RegistrationSVCHandler : public VxHandler {
    private:
      // string LoadTemplate(string filename, map<string, string> params);
      
      /// @brief load profile service (wrapper for profile provider)
      /// May throw exception if profile service is not available
      /// @return ProfileSVC*
      shared_ptr<ProfileSVC> GetProfileSVC();

    public:
      /// create registration handler object
      RegistrationSVCHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      /// destructor. noop
      ~RegistrationSVCHandler();

      // User registration

      /// register
      void Register(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// validate pending registration
      void RegisterValidate(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// confirm registration with token
      void RegisterEmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      // /confirmation_success
      
      /// endpoint to render successful message
      void RegisterSuccess(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// endpoint to render failure message
      void RegisterFailure(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      // Reset password

      /// reset password
      void Reset(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// confirm and update a password
      void ResetEmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// endpoint to render update
      void ResetUpdate(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// verify pending reset operation
      void ResetVerify(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// endpoint to render a success message
      void ResetSuccess(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// endpoint to render a failure message
      void ResetFailure(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// Update pin/password path is a credential name
      void UpdateCredential(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer

#endif