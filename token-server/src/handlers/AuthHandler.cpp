/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <stdafx.h>

#include <time.h>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <AuthHandler.h>
#include <vx/StringUtil.h>
#include <vx/web/Util.h>
#include <ResponseBuilder.h>
#include <vx/openid/models/Models.h>

#include <TokenServerContext.h>
#include <vx/openid/flow/Flow.h>

using namespace VxServer::Handlers;

using namespace boost::posix_time;

AuthHandler::AuthHandler(ServerContext& context, WebServer& server, Instance& secure_server)
    : VxHandler(context, server, secure_server), token_context(static_cast<TokenServerContext&>(context)) {
  //TokenServerContext& ctx = static_cast<TokenServerContext&>(context);

  //auto tp = ctx.GetTokenProvider();
  //auto iss = tp->getIssuer(ctx);
  auto server_id = token_context.section->getParam("server_id");
  if (server_id.empty()) server_id = "default";
  auto oauthBase = (boost::format("^/oauth2/%s/") % server_id).str();
  auto key_rotatioin = token_context.section->getParam("key_rotation","2592000");
  int ikey_rotation = 2592000;
  try
  {
       ikey_rotation = boost::lexical_cast<int>(key_rotatioin);
  }
  catch(const std::exception& e)
  {
       std::cerr << "Invalid config parameter - key_rotation: " <<  e.what() << '\n';
  }
  

  // #ifndef NDEBUG
  //   cout << "OAuth2 base URL: " << oauthBase << endl;
  // #endif
  secure_server

      /**
      * @brief dummy callback endpoint to support PKCE
      * 
      */
      .use("^/callback$", Request::RequestType::GET,
           std::bind(&AuthHandler::Callback, *this, std::placeholders::_1, std::placeholders::_2))

      /**
       * @brief Public autthentication API.The resulting response on success will contain a session token could 
       * be used to make a call to /authorize endpoint.
       */
      .use("^/api/v1/authn$", Request::RequestType::POST,
           std::bind(&AuthHandler::GetAuthN, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/api/v1/authn$", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"POST",true,0))

      /**
       * @brief /authorize endpoint. Part of authorization code or PKCE flow.
       * 
       */
      .use(oauthBase + "v1/authorize", Request::RequestType::GET,
           std::bind(&AuthHandler::Authorize, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + "v1/authorize", Request::RequestType::POST,
           std::bind(&AuthHandler::Authorize, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/oauth2/v1/authorize", Request::RequestType::GET,
           std::bind(&AuthHandler::Authorize, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/oauth2/v1/authorize", Request::RequestType::POST,
           std::bind(&AuthHandler::Authorize, *this, std::placeholders::_1, std::placeholders::_2))

      /**
       * @brief /token endpoint. Allows to excnage a code to a token for authorization code or 
       * PKCE flows.
       */
      .use(oauthBase + "v1/token", Request::RequestType::POST,
           std::bind(&AuthHandler::Token, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + "v1/token", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2, "POST",true,0))

      /**
       * @brief /introspect api used to validate a token.
       * 
       */
      .use(oauthBase + "v1/introspect", Request::RequestType::POST,
           std::bind(&AuthHandler::Introspect, *this, std::placeholders::_1, std::placeholders::_2))


      /**
       * @brief /revoke revoke a refresh token
       * 
       */
      .use(oauthBase + "v1/revoke", Request::RequestType::POST,
           std::bind(&AuthHandler::Revoke, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + "v1/revoke", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"POST",true,0))


      /**
       * @brief /invalidate session token on token server domain
       * 
       */
      .use(oauthBase + "v1/logout", Request::RequestType::GET,
           std::bind(&AuthHandler::Logout, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + "v1/logout", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"GET",true,0))


      /**
       * @brief /keys retrieve signing keys
       * 
       */
      .use(oauthBase + "v1/keys", Request::RequestType::GET,
           std::bind(&AuthHandler::Keys, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + ".well-known/jwks.json", Request::RequestType::GET,
           std::bind(&AuthHandler::Keys, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/.well-known/jwks.json", Request::RequestType::GET,
           std::bind(&AuthHandler::Keys, *this, std::placeholders::_1, std::placeholders::_2))
      
      .use(oauthBase + "v1/keys", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"GET",true,ikey_rotation))
      .use(oauthBase + ".well-known/jwks.json", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2, "GET",true,ikey_rotation))
      .use("^/.well-known/jwks.json", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"GET",true,ikey_rotation))


      /**
       * @brief /userinfo
       * 
       */
      .use(oauthBase + "v1/userinfo", Request::RequestType::GET,
           std::bind(&AuthHandler::Userinfo, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + "v1/userinfo", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2, "GET", true, 0))


      /**
       * @brief /.well-known/oauth-authorization-server return information about the authorization server
       * 
       */
      .use(oauthBase + ".well-known/oauth-authorization-server", Request::RequestType::GET,
           std::bind(&AuthHandler::AuthServer, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + ".well-known/oauth-authorization-server", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2, "GET", true, 0))
     //  .use("^/.well-known/oauth-authorization-server", Request::RequestType::GET,
     //       std::bind(&AuthHandler::AuthServer, *this, std::placeholders::_1, std::placeholders::_2))

      /**
       * @brief /.well-known/openid-configuration return openid connect information
       * 
       */
      .use(oauthBase + ".well-known/openid-configuration", Request::RequestType::GET,
           std::bind(&AuthHandler::OpenIDConfig, *this, std::placeholders::_1, std::placeholders::_2))
      .use(oauthBase + ".well-known/openid-configuration", Request::RequestType::OPTIONS,
           std::bind(&AuthHandler::Options, *this, std::placeholders::_1, std::placeholders::_2,"GET",true,86400))

     //  .use("^/.well-known/openid-configuration", Request::RequestType::GET,
     //       std::bind(&AuthHandler::OpenIDConfig, *this, std::placeholders::_1, std::placeholders::_2))
     //  .use("^/.well-known/openid-configuration", Request::RequestType::OPTIONS,
     //       std::bind(&AuthHandler::OpenIDConfig, *this, std::placeholders::_1, std::placeholders::_2))


      ;
}

AuthHandler::~AuthHandler() {}

// /api/v1/authn
void AuthHandler::GetAuthN(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  AuthnFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/authorize
void AuthHandler::Authorize(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
     // cout << req->query_string() << endl;
  AuthorizeFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/token
void AuthHandler::Token(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  LoadClient(res, req);
  TokenFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/introspect
void AuthHandler::Introspect(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  IntrospectFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/revoke
void AuthHandler::Revoke(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  LoadClient(res, req);
  RevokeFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/logout
void AuthHandler::Logout(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  LogoutFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/keys
void AuthHandler::Keys(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  KeyFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/userinfo
void AuthHandler::Userinfo(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  LoadAuthorization(res, req);
  UserInfoFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/.well-known/oauth-authorization-server
void AuthHandler::AuthServer(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  AuthServerFlow(context, res, req).Execute();
}

// /oauth2/{server_id}/v1/.well-known/openid-configuration
void AuthHandler::OpenIDConfig(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  OpenIdFlow(context, res, req).Execute();
}

// /callback
// LCOV_EXCL_START
void AuthHandler::Callback(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  res->write("");
}
// LCOV_EXCL_STOP
