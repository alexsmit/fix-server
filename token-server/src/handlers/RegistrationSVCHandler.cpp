/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <nlohmann/json.hpp>

#include "RegistrationSVCHandler.h"
#include <vx/web/Util.h>
#include "ResponseBuilder.h"
#include <vx/EncodingUtil.h>
#include <vx/inja/vx_inja.h>
#include <vx/inja/vx_inja_page.h>
#include <vx/web/VxHandler.h>

#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

#define NEW_TEMPLATE

using namespace VxServer::Handlers;

using namespace vx::inja;

RegistrationSVCHandler::RegistrationSVCHandler(
    ServerContext& context, WebServer& server, Instance& secure_server)
    : VxHandler(context, server, secure_server) {
  secure_server
      // send user registration request
      .use("^/register$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::Register, *this, std::placeholders::_1, std::placeholders::_2))
      // check whether a registration is still pending
      .use("^/register/validate$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::RegisterValidate, *this, std::placeholders::_1, std::placeholders::_2))
      // confirm a registration from the email link
      .use("^/register/confirm$", Request::RequestType::GET,
           std::bind(&RegistrationSVCHandler::RegisterEmailConfirm, *this, std::placeholders::_1, std::placeholders::_2))

      // generate reset token and send email
      .use("^/reset$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::Reset, *this, std::placeholders::_1, std::placeholders::_2))
      // send a reset password HTML form
      .use("^/reset/confirm$", Request::RequestType::GET,
           std::bind(&RegistrationSVCHandler::ResetEmailConfirm, *this, std::placeholders::_1, std::placeholders::_2))
      // post from password reset form
      .use("^/reset/update$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::ResetUpdate, *this, std::placeholders::_1, std::placeholders::_2))

      // update pin/password
      .use("^/password$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::UpdateCredential, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/pin$", Request::RequestType::POST,
           std::bind(&RegistrationSVCHandler::UpdateCredential, *this, std::placeholders::_1, std::placeholders::_2))

      .use("/reset/serverinfo$", Request::RequestType::ALL, [&context](std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
        DefaultInfoHandler().get(response, request);
      })

      // WEB pages
      .use("^/register/success$", Request::RequestType::GET, std::bind(&RegistrationSVCHandler::RegisterSuccess, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/register/failure$", Request::RequestType::GET, std::bind(&RegistrationSVCHandler::RegisterFailure, *this, std::placeholders::_1, std::placeholders::_2))

      .use("^/reset/success$", Request::RequestType::GET, std::bind(&RegistrationSVCHandler::ResetSuccess, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/reset/failure$", Request::RequestType::GET, std::bind(&RegistrationSVCHandler::ResetFailure, *this, std::placeholders::_1, std::placeholders::_2))

      ;

  auto send_reset_token = context.section->getParam("send_token");
  if (send_reset_token == "on") {
    secure_server
        // verify reset token (may be used internally)
        .use("^/reset/verify$", Request::RequestType::POST,
             std::bind(&RegistrationSVCHandler::ResetVerify, *this, std::placeholders::_1, std::placeholders::_2));
  }
}

RegistrationSVCHandler::~RegistrationSVCHandler() {}

// string RegistrationSVCHandler::LoadTemplate(string filename, map<string, string> params) {
//   ifstream mf(filename);
//   string message((istreambuf_iterator<char>(mf)), istreambuf_iterator<char>());

//   for (auto& kv : params) {
//     auto key_pattern = boost::format("\\[\\[%s\\]\\]") % kv.first;
//     boost::regex re(key_pattern.str());
//     message = boost::regex_replace(message, re, kv.second);
//   }
//   return message;
// }

shared_ptr<ProfileSVC> RegistrationSVCHandler::GetProfileSVC() {
  auto svc = context.getService("profile_service");
  shared_ptr<ProfileSVC> p = dynamic_pointer_cast<ProfileSVC>(svc);
  if (!p) throw WebException("unable to get a service");
  return p;
}

void RegistrationSVCHandler::Register(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  auto profile_svc = GetProfileSVC();

  UserRecord rec = {0};
  rec.username = req->body["username"];
  rec.password = req->body["password"];
  rec.email = req->body["email"];
  rec.pin = req->body["pin"];

  RegistrationResult reg;
  try {
    reg = profile_svc->RegisterUser(rec);

    auto param = context.section->getParam("send_token");
    ptree pt_code;
    pt_code.put("registration_code", reg.registration_code);
    if (param == "on") pt_code.put("reset_token", reg.reset_token);
    sendJSON(res, pt_code);

    // Send confirmation email
    profile_svc->SendConfirmationEmail(rec.email, reg.reset_token, "confirmation_email", "register/confirm");
  }
  catch (const std::runtime_error& e) {
    throw WebException(e.what());
  }
}

void RegistrationSVCHandler::RegisterValidate(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  auto profile_svc = GetProfileSVC();

  auto token = req->body["registration_code"];
  if (token.empty()) throw WebException("invalid_registration", "token is missing or invalid");

  auto reg_status = profile_svc->ValidateRegistration(token);

  string status;
  if (reg_status == RegistrationStatus::pending) status = "pending";
  if (reg_status == RegistrationStatus::completed) status = "completed";
  if (status.empty())
    throw WebException("invalid_registration", "registration does not exist");

  ptree pt_response;
  pt_response.put("status", status);
  sendJSON(res, pt_response);
}

void RegistrationSVCHandler::RegisterSuccess(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  VxPage page("");
  page
      .Title("VxPRO | Registration Success")
      .PageTitle("VxPRO")
      .Message("Registration",
               "<div class=\"alert alert-success\" role=\"alert\">Registration is successful.</div>"
               "<p>You can login now to the Application on your phone.</p>",
               {}, false, false)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  string result = page.render();
  res->write(result, header);
}

void RegistrationSVCHandler::RegisterFailure(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  VxPage page("");
  page
      .Title("VxPRO | Registration Failure")
      .PageTitle("VxPRO")
      .Message("Registration",
               "<div class=\"alert alert-danger\" role=\"alert\">Registration failed.</div>"
               "<p>Invalid registration code</p>",
               {}, false, false)
      .CSS({"/css/all.css", "/css/style.css"});


  ;

  string result = page.render();
  res->write(result, header);
}

// /register/confirm
void RegistrationSVCHandler::RegisterEmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto token = req->params["code"];
  bool result = false;

  auto profile_svc = GetProfileSVC();

  string url = profile_svc->context.getParam("profile_provider.confirmation_failure_url");

  if (!token.empty()) {
    try {
      result = profile_svc->ConfirmRegistration(token);
      if (result)
        url = profile_svc->context.getParam("profile_provider.confirmation_success_url");
    }
    catch (const std::exception& e) {
      TokenServerContext& token_context = static_cast<TokenServerContext&>(context);
      token_context.Logger(LogLevel::error, string("RegisterEmailConfirm") + string(":") + e.what());
      result = false;
    }
  }

  if (url.empty())
    throw WebException("invalid_config", "url is not configured");

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Location", url});
  res->write(SimpleWeb::StatusCode::redirection_found, "", header);
}

// POST /reset
void RegistrationSVCHandler::Reset(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  string email = req->body["email"];
  if (email.empty()) throw WebException("parameter is missing");

  auto profile_svc = GetProfileSVC();

  auto token = profile_svc->Reset(email.c_str());
  if (token.empty()) throw WebException("reset operation failed");

  auto param = context.getParam("profile_provider.send_token");
  ptree pt_code;
  if (param == "on") pt_code.put("reset_token", token);
  sendJSON(res, pt_code);

  // Send resest password email
  try {
    profile_svc->SendConfirmationEmail(email, token, "reset_email", "reset/confirm");
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
  }
}

// GET /reset/confirm
// Load password reset form
void RegistrationSVCHandler::ResetEmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  string code = req->params["code"];
  if (code.empty())
    throw WebException("invalid_registration", "registration code missing");

  auto profile_svc = GetProfileSVC();

  auto form_verify = context.getParam("profile_provider.form_verify_reset_token");
  if (form_verify != "off") {
    if (!profile_svc->ResetVerify(code)) {
      string failure_url = profile_svc->context.getParam("profile_provider.reset_failure_url");
      SimpleWeb::CaseInsensitiveMultimap header;
      header.insert({"Location", failure_url});
      res->write(SimpleWeb::StatusCode::redirection_found, "", header);
      return;
    }
  }

  SimpleWeb::CaseInsensitiveMultimap header;

  header.emplace("Content-type", "text/html");

  VxPage page("");

  vector<VxButtonItem> buttons = {{"Update", "update", "submit"}};

  page
      .Title("VxPro | Change password")
      .PageTitle("VxPRO")
      .Message("Change passwod",
               "<small><p>Recommended: minimum length is 6.<br>Use upper case, special characters and numbers.</p></small>",
               buttons,
               false);

  vector<VxPageFormField> fields = {
      {"password", "Password", "", VxPageFormFiledType::password, "Enter a password"},
      {"confirm_password", "Confirm password", "", VxPageFormFiledType::password, "Please confirm a password"},
      {"token", "", code, VxPageFormFiledType::hidden}};

  page
      .Form("/reset/update", "vx_form", fields, true)
      .Scripts({"{{base}}/scripts/validate.js", "{{base}}/scripts/password.js"})
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  string html = page.render();

  auto session = random_util().unix_random_alphanum(32);
  header.insert({"Set-Cookie", string("_vxreset=") + session + string("; HttpOnly; Max-Age=600; SameSite=Strict")});
  res->write(html, header);
}

// POST /reset/update { "reset_token": "{{token}}", "password": "{{password}}"}
void RegistrationSVCHandler::ResetUpdate(std::shared_ptr<Response> res, std::shared_ptr<Request> request) {
  string password = request->body["password"];
  string confirm = request->body["confirm_password"];
  string token = request->body["token"];
  auto vxreset = request->cookies["_vxreset"];

  if (password.empty() || password != confirm || token.empty() || vxreset.empty()) {
    string failure_url = context.getParam("profile_provider.reset_failure_url");
    SimpleWeb::CaseInsensitiveMultimap header;
    header.insert({"Location", failure_url});
    res->write(SimpleWeb::StatusCode::redirection_found, "", header);
    return;
  }

  auto profile_svc = GetProfileSVC();

  bool result = false;
  result = profile_svc->UpdatePassword(token.c_str(), password.c_str());

  string url = (result)
                   ? profile_svc->context.getParam("profile_provider.reset_success_url")
                   : profile_svc->context.getParam("profile_provider.reset_failure_url");

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Location", url});
  res->write(SimpleWeb::StatusCode::redirection_found, "", header);
}

void RegistrationSVCHandler::ResetVerify(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  auto reset_token = req->body["reset_token"];
  if (reset_token.empty())
    throw WebException("invalid \"token\"");

  auto profile_svc = GetProfileSVC();

  if (!profile_svc->ResetVerify(reset_token))
    throw WebException("invalid \"token\"");

  ptree pt;
  pt.put("status", "pending");
  sendJSON(res, pt);
}

// /reset/success
void RegistrationSVCHandler::ResetSuccess(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  VxPage page("");
  page
      .Title("VxPRO | Password Reset Success")
      .PageTitle("VxPRO")
      .Message("Password Reset",
               "<div class=\"alert alert-success\" role=\"alert\">Password reset is successful.</div>"
               "<p>You can login now to the Application on your phone.</p>",
               {}, false, false)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  string result = page.render();
  res->write(result, header);
}

// /reset/failure
void RegistrationSVCHandler::ResetFailure(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  VxPage page("");
  page
      .Title("VxPRO | Password Reset Failure")
      .PageTitle("VxPRO")
      .Message("Password Reset",
               "<div class=\"alert alert-danger\" role=\"alert\">Password Reset is not successful.</div>"
               "<div><p>Please try again.</p></div>",
               {}, false, false)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  string result = page.render();
  res->write(result, header);
}

// Bearer token is required
// /pin
// /password
void RegistrationSVCHandler::UpdateCredential(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  try {
    auth::ResponseBuilder builder(res, req, context);
    builder.Bearer();
    string cred = req->path().substr(1);  // pin or password

    string val = req->body[cred];
    string val_old = req->body[cred + "_old"];
    string val_verify = req->body[cred + "_verify"];
    if (val.empty()) throw runtime_error("missing parameter");

    string uuid = builder.GetUserId();
    if (uuid.empty()) throw runtime_error("invalid credentials");

    auto profile_svc = GetProfileSVC();

    if (cred == "pin") {
      if (val_verify.empty() || val_verify != val) throw runtime_error("pins don't match");
      profile_svc->ChangePin(uuid.c_str(), val.c_str());
    }
    else if (cred == "password") {
      if (val_old.empty()) throw runtime_error("no old password");
      if (val_verify.empty() || val_verify != val) throw runtime_error("passwords don't match");

      auto rec = profile_svc->Authenticate(uuid.c_str(), val_old.c_str(), true);

      profile_svc->ChangePassword(rec->userid.c_str(), val.c_str());
    }
    else {
      throw runtime_error("invalid credential type");
    }
    ptree pt_result;
    sendJSON(res, pt_result);
  }
  catch (const std::runtime_error& e) {
    sendError(res, "service_error", e.what());
  }
}
