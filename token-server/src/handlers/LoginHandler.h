/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _LOGIN_HANDLER_H
#define _LOGIN_HANDLER_H

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>

namespace VxServer {
  namespace Handlers {

    /// all registration operations
    class LoginHandler {
    private:
      ServerContext& context;
      WebServer& server;
      Instance& secure_server;

      // string LoadTemplate(string filename, map<string, string> params);

    public:
      /// create registration handler object
      LoginHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      /// destructor. noop
      ~LoginHandler();

      /// GET /login. Render login page to login with password (demo for now).
      /// TODO: add configuration for the main app URL that will run authorize call from.
      void Login(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

      /// POST /login. 
      /// 1)Helper method to use username/email to login instead of using /api/v1/authn
      /// 2) Part of OAuth2 flow, when authorize prompt is not set and session is not active on the 
      /// authorization server. If user is authenticated OAuth2 flow will redirect to the provided callback URI
      void LoginPost(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      
#ifndef NDEBUG
      /// Debug okta_post_message compatibility
      void Okta(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
#endif
    };

  }  // namespace Handlers
}  // namespace VxServer

#endif