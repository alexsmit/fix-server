/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <nlohmann/json.hpp>

#include "HelpHandler.h"
#include <vx/web/Util.h>
#include "ResponseBuilder.h"
#include <vx/EncodingUtil.h>

#include <vx/inja/vx_environment.h>
#include <vx/inja/vx_inja_page.h>

#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

using namespace VxServer::Handlers;
using namespace vx::inja;

HelpHandler::HelpHandler(
    ServerContext& context, WebServer& server, Instance& secure_server)
    : context(context), server(server), secure_server(secure_server) {
  secure_server

      .use("^/help$", Request::RequestType::GET, std::bind(&HelpHandler::Help, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/help/([\\w]+)$", Request::RequestType::GET, std::bind(&HelpHandler::Help, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

HelpHandler::~HelpHandler() {}

void HelpHandler::Help(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  string template_path = "./templates/help/";
  VxEnvironment env(template_path);
  auto path_match = req->Match();
  string filename = "help";
  string page_title = "Help";
  if (path_match.size() > 1) {
    page_title = string("<a href=\"/help\">Help</a> > {{") +  path_match[1].str() + string("}}");
    filename += string("_") + path_match[1].str();
    boost::filesystem::path base_path = template_path;
    boost::filesystem::path path = base_path / (filename + ".html");
    if (!boost::filesystem::exists(path)) {
      filename = "help";
    }
  }

  json data;
  env.load_variables("help", data);

  VxMultipart multi(env);

  string content = multi.render_file(filename, data, VxFileType::html);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-Type","text/html");
  header.emplace("Cache-Control","no-cache");

  VxPage page("");
  page
      .Title("VxPRO | Help")
      .PageTitle(page_title)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  string result = page.render(data, false, content);
  res->write(result, header);
}
