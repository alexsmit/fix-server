/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <nlohmann/json.hpp>

#include "LoginHandler.h"
#include <vx/web/Util.h>
#include "ResponseBuilder.h"
#include <vx/EncodingUtil.h>

#include <vx/inja/vx_inja.h>
#include <vx/inja/vx_environment.h>
#include <vx/inja/vx_inja_menu.h>
#include <vx/inja/vx_inja_page.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

#define NEW_TEMPLATE

using namespace VxServer::Handlers;
using namespace vx::inja;
using namespace vx::openid::flow;

LoginHandler::LoginHandler(
    ServerContext& context, WebServer& server, Instance& secure_server)
    : context(context), server(server), secure_server(secure_server) {
  secure_server

      // Web login
      .use("^/login$", Request::RequestType::GET, std::bind(&LoginHandler::Login, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/login$", Request::RequestType::POST, std::bind(&LoginHandler::LoginPost, *this, std::placeholders::_1, std::placeholders::_2))

#ifndef NDEBUG
      .use("^/okta$", Request::RequestType::GET, std::bind(&LoginHandler::Okta, *this, std::placeholders::_1, std::placeholders::_2))
#endif
      ;
}

LoginHandler::~LoginHandler() {}

#define SPEED_CUT 2

void LoginHandler::LoginPost(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto token_service = context.getService("token");
  auto token_svc = dynamic_cast<TokenProvider*>(token_service.get());
  if (token_svc == NULL) throw WebException("unable to get a service 'token'");

  auto cur = ::time(NULL);
  long nbf = cur + SPEED_CUT;

  auto csrf = req->body["requestId"];
  auto csrfok = true;
  if (csrf.empty()) {
    csrfok = false;
  }
  if (csrfok) {
    auto csrf_token = token_svc->GetTokenByNonce(csrf);
    if (csrf_token.nonce.empty() || cur > csrf_token.expried) {
      csrfok = false;
    }
    else {
      ptree meta = csrf_token.parse_metadata();
      nbf = boost::lexical_cast<long>(meta.get("nbf", "0"));
      if (cur < nbf) {
        // prevent fast requests, extend for another X seconds
        // in this case a "scripting" will continue extending a time when
        // a password will be accepted
        auto snbf = boost::lexical_cast<string>(nbf + SPEED_CUT);
        stringstream s_meta;
        meta.put("nbf", snbf);
        write_json(s_meta, meta);
        token_svc->SetMetadata(csrf, s_meta.str());
        csrfok = false;
      }
    }
  }

  if (!csrfok) {
    ptree pt;
    pt.put("error_description", "We are sorry, request failed. Please try again.");
    sendJSON(res, pt, SimpleWeb::StatusCode::client_error_unauthorized);
    return;
  }

  auto username = req->body["username"];
  auto password = req->body["password"];
  bool userok = true;
  if (username.empty() || password.empty()) {
    userok = false;
  }

  if (userok) {
    auto profile_service = context.getService("profile_service");
    ProfileSVC* profile_svc = dynamic_cast<ProfileSVC*>(profile_service.get());
    if (profile_svc == NULL) throw WebException("unable to get a service 'profile_service'");
    auto profile_provider = profile_svc->GetProvider();

    auto rec = profile_provider->LoadUser(username.c_str(), ProfileKey::username);
    if (rec.id == 0) {
      rec = profile_provider->LoadUser(username.c_str(), ProfileKey::email);
    }
    if (rec.id == 0) {
      userok = false;
    }
    else {
      username = rec.username;
    }
  }

  if (!userok) {
    ptree pt;
    pt.put("error_description", "Invalid username or password");
    sendJSON(res, pt, SimpleWeb::StatusCode::client_error_unauthorized);
    return;
  }

  AuthRequest auth;
  auth.username = username;
  auth.password = password;

  AuthnFlow flow(context, auth);
  flow.Execute();
  auto result = flow.GetResult();
  auto header = flow.GetResultHeader();
  auto code = flow.GetResultCode();

  sendJSON(res, result, header, code);
}

void LoginHandler::Login(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto token_service = context.getService("token");
  auto token_svc = dynamic_cast<TokenProvider*>(token_service.get());
  if (token_svc == NULL) throw WebException("unable to get a service 'token'");

  auto cur = ::time(NULL);
  long nbf = cur + SPEED_CUT;

  string server_id = (*context.section)["server_id"];
  if (server_id.empty()) server_id = "default";
  string client_id = context.getParam("login.client_id");
  string scope = context.getParam("login.scope");

  auto snbf = boost::lexical_cast<string>(nbf);
  auto csrf_token = token_svc->CreateTokenMeta("_login", "web", "", 180, {{"nbf", snbf}});

  SimpleWeb::CaseInsensitiveMultimap header;

  header.emplace("Content-type", "text/html");

  VxPage page("");

  vector<VxButtonItem> buttons = {{"Login", "login", "onLogin(event)"}};

  page
      .Title("VxPro | Login")
      .PageTitle("VxPRO")
      .Message("Login",
               "<div><small><p>You can use your username or email<br>"
               "Username is case sensitive.<br>"
               "Minimum password length is 6</div>"
               "</p></small>"
               "<div id=\"login_error\" class=\"alert alert-danger\" role=\"alert\" style=\"display:none\">...</div>",
               buttons,
               false,
               true,
               "vx-login");

  vector<VxPageFormField> fields = {
      {"username", "Username", "", VxPageFormFiledType::input, "Please enter username"},
      {"password", "Password", "", VxPageFormFiledType::password, "Enter a password"},
      {"requestId", "", csrf_token.nonce, VxPageFormFiledType::hidden},
      {"serverId", "", server_id, VxPageFormFiledType::hidden},
      {"clientId", "", client_id, VxPageFormFiledType::hidden},
      {"scope", "", scope, VxPageFormFiledType::hidden}

  };

  page
      .Form("/login", "vx-login-form", fields, false)
      .Scripts({"{{base}}/scripts/validate.js", "{{base}}/scripts/login.js"})
      .CSS({"css/login.css", "css/style.css", "css/all.css"});

  shared_ptr<VxMenu> menu;
  page.AddMenu(menu);

  string html = page.render();

  auto session = random_util().unix_random_alphanum(32);
  header.insert({"Set-Cookie", string("_vxlogin=") + session + string("; HttpOnly; Max-Age=600; SameSite=Strict")});
  res->write(html, header);
}

#ifndef NDEBUG

void LoginHandler::Okta(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  string template_path = "./templates/compatibility/";
  VxEnvironment env(template_path);

  json data;

  vector<string> keys = {"error","error_description"};
  vector<string> values = {"some_error","some whatever error"};

  data["keys"] = keys;
  data["values"] = values;

  VxMultipart multi(env);
  auto result = multi.render_file("okta_post_message",data, VxFileType::html);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-Type","text/html");
  header.emplace("Cache-Control","no-cache");

  res->write(SimpleWeb::StatusCode::success_ok,result, header );
}

#endif