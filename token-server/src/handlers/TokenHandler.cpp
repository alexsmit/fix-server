/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS
#define OPENSSL_SUPPRESS_DEPRECATED

#include <boost/variant.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/web/ServerContext.h>
#include "TokenHandler.h"
#include <vx/StringUtil.h>
#include <vx/web/Util.h>
#include "ResponseBuilder.h"

using namespace VxServer::Handlers;

TokenHandler::TokenHandler(TokenServerContext& context, WebServer& server, Instance& secure_server)
    : VxHandler(context, server, secure_server), token_context(context) {

  auto server_id = context.section->getParam("server_id");
  if (server_id.empty()) server_id = "default";
  auto oauthPrefix = (boost::format("^/oauth2/%s/") % server_id).str();

  secure_server

      .use("^/.well-known/openid-configuration$", Request::RequestType::GET,
           std::bind(&TokenHandler::GetOpenidConfig, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/.well-known/openid-configuration$", Request::RequestType::OPTIONS,
           std::bind(&TokenHandler::Options, this, std::placeholders::_1, std::placeholders::_2, "GET", true, 86400))

      .use("^/oauth/token$", Request::RequestType::POST,
           std::bind(&TokenHandler::GetToken, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/fingerprint$", Request::RequestType::GET,
           std::bind(&TokenHandler::GetFingerprint, this, std::placeholders::_1, std::placeholders::_2))

      // /** -- removed
      //  * @brief
      //  *
      //  */
      // .use("^/api/v1/authn$", Request::RequestType::POST,
      //      std::bind(&TokenHandler::GetAuthN, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

TokenHandler::~TokenHandler() {}

void TokenHandler::GetOpenidConfig(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace(make_pair("Content-Type", "application/json"));
  header.emplace(make_pair("Cache-Control", "max-age=86400, must-revalidate"));
  AddCORS(req, header);
  res->write(SimpleWeb::StatusCode::success_ok, OpenIdConfiguration(), header);
}

void TokenHandler::GetToken(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) {
    token_context.Logger(LogLevel::debug, "/oauth/token: client, client/secret not validated");
    return;
  }

  try {
    auth::ResponseBuilder builder(res, req, context);
    builder.Exec();
  }
  catch (const auth::auth_exception& ea) {
    SimpleWeb::StatusCode code = SimpleWeb::StatusCode::client_error_bad_request;
    if (ea.code != 0) {
      code = (SimpleWeb::StatusCode)ea.code;
    }
    token_context.Logger(LogLevel::debug, string("/oauth/token: auth, ") + ea.what());
    sendError(res, ea.error_code, ea.what(), code);
  }
  // LCOV_EXCL_START
  catch (const std::exception& e) {
    token_context.Logger(LogLevel::debug, string("/oauth/token: error, ") + e.what());
    sendError(res, "auth_error", e.what(), SimpleWeb::StatusCode::client_error_bad_request);
  }
  // LCOV_EXCL_STOP
}

void TokenHandler::GetFingerprint(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  ptree finger;
  if (context.useCertificates) {
    auto info = dynamic_cast<RSACertInfo*>(context.info.get());
    finger.put("sha1", StringUtil::toHexChars(info->Fingerprint()));
    finger.put("sha256", StringUtil::toHexChars(info->Fingerprint256()));
  }
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace(make_pair("Content-Type", "application/json"));
  sendJSON(res, finger);
}

void TokenHandler::AddArray(ptree& pt, const string& key, vector<string> values) {
    ptree pt_values;
    vector2ptree(values, pt_values);
    pt.add_child(key, pt_values);
}

/// Returns just absolutely necessary elements for OpenID Connect auto-discovery
string TokenHandler::OpenIdConfiguration() {
  string result;

  ptree pt;
  string issuer = context.getParam("token_provider.issuer");
  string server_id = context.getParam("token_provider.server_id");
  // string oauth = (boost::format("oauth2/%s/v1/") % server_id).str();
  if (!issuer.empty()) {
    string real_issuer = TokenProvider::getIssuer(context);
    pt.put("issuer", real_issuer);
    // pt.put("authorization_endpoint", issuer + "authorize");
    // pt.put("token_endpoint", issuer + "oauth/token");
    // pt.put("userinfo_endpoint", issuer + "userinfo");
    pt.put("jwks_uri", real_issuer + ".well-known/jwks.json");

    //pt.put("authorization_endpoint", real_issuer + oauth + "authorize");
    pt.put("token_endpoint", real_issuer + "oauth/token");
    
    AddArray(pt, "token_endpoint_auth_methods_supported",{"client_secret_basic","client_secret_post"});

// 
    ptree& pt_token_provider = context.section->getTree("");

    // if (pt_options.count("token_provider") > 0) {
    // auto pt_token_provider = pt_options.get_child("token_provider");

    // if (pt_token_provider.count("scopes") > 0) {
    //   auto pt_scopes = pt_token_provider.get_child("scopes");
    //   vector<string> scopes_supported;
    //   for (auto& s : pt_scopes) {
    //     scopes_supported.push_back(s.first);
    //   }
    //   AddArray(pt, "scopes_supported", scopes_supported);
    // }

    // AddArray(pt, "response_types_supported", {"code"});

    // AddArray(pt, "code_challenge_methods_supported", {"S256","S512"});

    // ptree pt_response_modes_supported;
    // vector<string> response_modes_supported = {"query", "form_post"};
    // vector2ptree(response_modes_supported, pt_response_modes_supported);
    // pt.add_child("response_modes_supported", pt_response_modes_supported);

    // ptree pt_subject_types_supported;
    // vector<string> subject_types_supported = {"public"};
    // vector2ptree(subject_types_supported, pt_subject_types_supported);
    // pt.add_child("subject_types_supported", pt_subject_types_supported);

    ptree pt_id_token_signing_alg_values_supported;
    vector<string> id_token_signing_alg_values_supported = {"RS512"};
    if (pt_token_provider.count("algos") > 0) {
      id_token_signing_alg_values_supported.clear();
      auto ptv = pt_token_provider.get_child("algos");
      for (auto& s : ptv) {
        id_token_signing_alg_values_supported.push_back(s.second.data());
      }
    }
    AddArray(pt, "id_token_signing_alg_values_supported", id_token_signing_alg_values_supported);

    // ptree pt_token_endpoint_auth_methods_supported;
    // vector<string> token_endpoint_auth_methods_supported = {"client_secret_post"};
    // vector2ptree(token_endpoint_auth_methods_supported, pt_token_endpoint_auth_methods_supported);
    // pt.add_child("token_endpoint_auth_methods_supported", pt_token_endpoint_auth_methods_supported);
    // }
  }

  stringstream so;
  write_json(so, pt);
  return so.str();
}

void TokenHandler::AddCORS(std::shared_ptr<Request> req, SimpleWeb::CaseInsensitiveMultimap& header) {
  context.AddCORS(req,header);
}
