/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/algorithm/string.hpp>
#include "TokenServerInfoHandler.h"
#include <vx/web/Util.h>
#include "TokenProvider.h"
#include <vx/inja/vx_inja.h>
#include <vx/inja/vx_environment.h>
#include <vx/inja/vx_inja_page.h>

#ifndef PROJECT_GIT_TAG
#define PROJECT_GIT_TAG "DEBUG"
#endif

using namespace VxServer::Handlers;
using namespace vx::inja;

TokenServerInfoHandler::TokenServerInfoHandler(ServerContext &context, WebServer &server, Instance &secure_server, int pool_size)
    : context(context), server(server), secure_server(secure_server), pool_size(pool_size)

{
  secure_server

      .use("^/info$", Request::RequestType::GET,
           std::bind(&TokenServerInfoHandler::Info, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/version$", Request::RequestType::GET,
           std::bind(&TokenServerInfoHandler::Version, this, std::placeholders::_1, std::placeholders::_2))

      ;
}

TokenServerInfoHandler::~TokenServerInfoHandler() {}

string get_used_mem() {
  string m;
  auto pid = ::getpid();
  auto file = (boost::format("/proc/%d/status") % pid ).str();
  std::ifstream ifs(file);
  string line;
  while(std::getline(ifs,line) ) {
    if (boost::algorithm::starts_with(line,"VmRSS")) {
      auto ix = line.find(' ');
      if (ix != string::npos) {
        auto ixm = line.find_first_not_of(' ', ix);
        m = line.substr(ixm);
        break;
      }
    }
  }
  ifs.close();
  return m;
}


void TokenServerInfoHandler::Info(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto svc = context.getService("token");
  auto token_provider = dynamic_cast<TokenProvider *>(svc.get());

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  VxPage page("");
  page
      .Title("VxPRO | Server Info")
      .PageTitle("VxPRO Identity server")
      // .Message("Registration",
      //          "<div class=\"alert alert-success\" role=\"alert\">Registration is successful.</div>"
      //          "<p>You can login now to the Application on your phone.</p>",
      //          {}, false, true)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  VxEnvironment env("./templates/");
  json data;
  data["version"] = PROJECT_GIT_TAG;
  data["threads"] = pool_size;
  data["httpc"] = context.getParam("port");
  data["httpsc"] = context.getParam("port_https");
  data["httpr"] = context.port;
  data["httpsr"] = context.ports;
  data["token"] = context.getParam("token_provider.type");
  data["profile"] = context.getParam("profile_provider.type");
  data["ttl"] = token_provider->GetEffectiveTTL();
  data["refresh_ttl"] = token_provider->GetEffectiveRefreshTTL();

  data["alloc"] = get_used_mem();

  VxMultipart multi(env);
  string cont = multi.render_file("info", data, VxFileType::html);

  string result = page.render(false, cont);
  res->write(result, header);
}

void TokenServerInfoHandler::Version(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  ptree pt;
  pt.put("version", PROJECT_GIT_TAG);
  pt.put("service", "token-server");
  sendJSON(res, pt, SimpleWeb::StatusCode::success_ok);
}
