/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include "ProfileSVCBase.h"
#include <vx/SimpleMail.h>
#include <boost/format.hpp>
#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include <nlohmann/json.hpp>
#include <vx/inja/vx_environment.h>
#include <vx/ServiceEmailFactory.h>

using json = nlohmann::json;
using namespace vx::inja;

ProfileSVCBase::ProfileSVCBase(ServerContext &context) : context(context) {}

ProfileSVCBase::~ProfileSVCBase(){};

bool ProfileSVCBase::SendConfirmationEmail(const string& email, const string& token, const string& key, const string& path) {
  if (token.empty()) return false;

  auto section_profile_provider = context.config->Section("profile_provider");
  auto section_token_provider = context.config->Section("token_provider");

  string email_template_key = key;
  string email_template_title_key = key + "_title";

  auto email_template = section_profile_provider[key].str();
  if (email_template.empty()) return false;
  auto email_template_title = section_profile_provider[key + "_title"].str();
  if (email_template_title.empty()) return false;

  auto email_from = section_profile_provider["email_from"].str();
  if (email_from.empty()) return false;

  if (email.empty()) return false;

  string issuer = section_token_provider.getParam("issuer");
  string url = (boost::format("%s%s?code=%s") % issuer % path % token).str();

  SimpleMail sm(email_from, email/*, true, true*/);

  auto param = section_profile_provider["send_email"].str();
  if (param != "on") {
    return false;
  }

  ///////////////////////////////////////////////////

  auto svc = std::dynamic_pointer_cast<vx::EmailService>(context.getService("email"));
  if (!!svc) {
    return svc->SendCodeURL(email_template, email_from, email, email_template_title, url);
  }
  return false;

  // VxEnvironment env("data/");
  // json data;
  // data["url"] = url;

  // VxMultipart multi(env);

  // string message = multi.render_file(email_template, data, vx::inja::VxFileType::multipart);

  // if (env.GetErrorsCount() > 0) {
  //   cerr << "email rendering errors: " << env.GetErrorsCount() << endl;
  //   auto cnt = 1;
  //   for(auto &e: env.GetErrorsMessages()) {
  //     cerr << "error " << cnt++ << ": " << e << endl;
  //   }
  // }

  // // cout << email_template_title << '\n' << message << '\n';

  // return sm.Send(email_template_title, message);
}
