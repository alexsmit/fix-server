/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>
#include <vx/PasswordUtil.h>
#include <TokenProviderMySQLBase.h>

TokenProviderMySQLBase::TokenProviderMySQLBase(std::shared_ptr<DataProviderMySQLBase> prov) : prov(prov) {
  SetDefaults(prov->config);
  config = prov->config;
}
TokenProviderMySQLBase::~TokenProviderMySQLBase() {}

TokenInfo TokenProviderMySQLBase::CreateToken(
    string userName,
    string agent,
    string old_nonce,
    unsigned long ttl,
    const string& metadata,
    bool createRefreshRecord) {
  if (!old_nonce.empty()) {
    auto old_refresh = GetTokenByNonce(old_nonce);
    if (!old_refresh.nonce.empty()) {
      DeleteTokenByNonce(old_nonce);
      if (createRefreshRecord) {
        old_nonce = "";
      }
      else {
        createRefreshRecord = true;
      }
    }
    else {
      old_nonce = "";
    }
  }

  random_util r;

  string id = r.getGUID();

  unsigned long iexp = GetEffectiveRefreshTTL();
  if (ttl != 0) iexp = ttl;
  time_t tim = ::time(NULL);
  time_t tim_expire = tim + iexp;

  TokenInfo info;
  if (createRefreshRecord) {
    info.token = (old_nonce.empty()) ? CreateNonce(24) : old_nonce;
  }
  info.nonce = CreateNonce(24);
  info.expried = tim_expire;

  auto sql = (metadata.empty())
                 ? "insert into user_tokens (id, user_name, type, created, "
                   "expires, user_agent, nonce, token) values(?,?,?,?,?,?,?,?) "
                   "on duplicate key update expires=?,nonce=?,token=?"
                 : "insert into user_tokens (id, user_name, type, created, "
                   "expires, user_agent, nonce, token, metadata) values(?,?,?,?,?,?,?,?,?) "
                   "on duplicate key update expires=?,nonce=?,token=?";

  if (metadata.empty()) {
    prov->Exec(sql, {id, userName, "id", tim, tim_expire, agent, info.nonce, info.token,
                     tim_expire, info.nonce, info.token});
  }
  else {
    prov->Exec(sql, {id, userName, "id", tim, tim_expire, agent, info.nonce, info.token,
                     metadata,
                     tim_expire, info.nonce, info.token});
  }


  if (createRefreshRecord) {
    id = r.getGUID();  // new id for refresh token record
    if (metadata.empty()) {
      prov->Exec(sql, {id, userName, "refresh", tim, tim_expire, agent, info.token, info.nonce,
                       tim_expire, info.token, info.nonce});
    }
    else {
      prov->Exec(sql, {id, userName, "refresh", tim, tim_expire, agent, info.token, info.nonce,
                       metadata,
                       tim_expire, info.token, info.nonce});
    }
  }

  return info;
}

bool TokenProviderMySQLBase::ValidateToken(string userName, TokenInfo info, bool nonceOnly) {
  time_t tim = ::time(NULL);
  time_t expires;
  if (!nonceOnly) {
    expires = (time_t)prov->SelectInt("select expires from user_tokens where user_name=? and nonce=? and token=?",
                                      {userName, info.nonce, info.token}, 0);
  }
  else {
    expires = (time_t)prov->SelectInt("select expires from user_tokens where user_name=? and nonce=?",
                                      {userName, info.nonce}, 0);
  }

  return expires > tim;
}


void TokenProviderMySQLBase::Clean() {
  time_t tim = ::time(NULL);

  prov->Exec("delete from user_tokens where expires < ?", {tim});
}

void TokenProviderMySQLBase::Hit(std::string nonce, int max) {
  if (max == -1) {
    prov->Exec("update user_tokens set hits=0 where nonce=?", {nonce});
  }
  else {
    int cnt = prov->Exec("update user_tokens set hits=coalesce(hits,0)+1 where nonce=?", {nonce});
    if (cnt != 0) {
      prov->Exec("delete from user_tokens where hits>=? and nonce=?", {max, nonce});
    }
  }
}

string TokenProviderMySQLBase::GetMetadata(std::string nonce) {
  auto vresult = prov->Select("select metadata from user_tokens where nonce=?", {nonce});
  if (vresult.size() > 0) return vresult[0];

  return "";
}

bool TokenProviderMySQLBase::SetMetadata(std::string nonce, const std::string& medatada) {
  auto res = prov->Exec("update user_tokens set metadata=? where nonce=?", {medatada, nonce});
  return res == 1;
}

UserToken TokenProviderMySQLBase::GetTokenByNonce(std::string nonce) {
  UserToken tok;

  auto rows = prov->SelectRows("select * from user_tokens where nonce=?", {nonce});
  for (auto& r : *rows) {
    tok.agent = r.get_string(5);
    tok.expried = r.get_long(4);
    tok.metadata = r.get_string(9);
    tok.nonce = r.get_string(8);
    tok.token = r.get_string(7);
    tok.username = r.get_string(1);
    break;
  }

  return tok;
}

UserToken TokenProviderMySQLBase::GetTokenByRefresh(std::string refresh_token) {
  UserToken tok;

  auto rows = prov->SelectRows("select * from user_tokens where token=?", {refresh_token});
  for (auto& r : *rows) {
    tok.agent = r.get_string(5);
    tok.expried = r.get_long(4);
    tok.metadata = r.get_string(9);
    tok.nonce = r.get_string(8);
    tok.token = r.get_string(7);
    tok.username = r.get_string(1);
    break;
  }

  return tok;
}

void TokenProviderMySQLBase::DeleteTokenByNonce(std::string nonce) {
  if (nonce.empty()) return;
  // update referenced record, set token to empty
  auto res0 = prov->Exec("update user_tokens set token='' where token=?", {nonce});
  // delete token, finally
  auto res1 = prov->Exec("delete from user_tokens where nonce = ?", {nonce});
}
