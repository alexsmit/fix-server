/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <vx/ServiceEmailFactory.h>
#include <vx/inja/vx_environment.h>
#include <vx/SimpleMail.h>

using namespace vx;
using namespace vx::inja;

EmailService::EmailService(JsonConfig& _conf, const std::string& _template_dir) {
  config = _conf;
  template_dir = _template_dir;
}

string EmailService::id() {
  return "email";
}

bool EmailService::SendCodeURL(const std::string& template_name,
                               const std::string& email_from,
                               const std::string& email_to,
                               const std::string& title, const std::string& url) {
  SimpleMail sm(email_from, email_to /*, true, true*/);

  VxEnvironment env(template_dir);
  json data;
  data["url"] = url;

  VxMultipart multi(env);

  string message = multi.render_file(template_name, data, vx::inja::VxFileType::multipart);

  // if (env.GetErrorsCount() > 0) {
  //   cerr << "email rendering errors: " << env.GetErrorsCount() << endl;
  //   auto cnt = 1;
  //   for (auto& e : env.GetErrorsMessages()) {
  //     cerr << "error " << cnt++ << ": " << e << endl;
  //   }
  // }

  bool result = sm.Send(title, message);
  int code = sm.ExitCode();

  return result && (code == 0);
}

//---------------------------------------

ServiceEmailFactory::ServiceEmailFactory(time_t cache_ttl, const string& conf) {
  provider_ttl = cache_ttl;
  auto app = AppConfig::getInstancePtr();
  section = app->SectionPtr(conf);
}

std::string ServiceEmailFactory::id() const {
  return "email";
}

std::shared_ptr<Service> ServiceEmailFactory::createService(const std::string& type) {
  // return
  return make_shared<EmailService>(*section);
}