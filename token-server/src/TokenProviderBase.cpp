/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include "TokenProviderBase.h"
#include <vx/PasswordUtil.h>

#include <time.h>
#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <vx/Auth.h>

using namespace std;
using namespace boost::property_tree;

unsigned long TokenProviderBase::GetEffectiveTTL() {
  unsigned long iexp = 0;
  if (ttl == 0)
    iexp = configured_ttl;
  else
    iexp = ttl;
  if (iexp == 0) iexp = DEFAULT_TTL;
  return iexp;
}

unsigned long TokenProviderBase::GetEffectiveRefreshTTL() {
  unsigned long iexp = 0;
  if (refresh_ttl == 0)
    iexp = configured_refresh_ttl;
  else
    iexp = refresh_ttl;
  if (iexp == 0) iexp = DEFAULT_TTL_REFRESH;
  return iexp;
}

void TokenProviderBase::SetDefaults(JsonConfig& config) {
  auto sexp = config["expiration"].str();
  if (!sexp.empty()) {
    try {
      configured_ttl = boost::lexical_cast<unsigned long>(sexp);
    }
    catch (const std::exception& e) {
      configured_ttl = DEFAULT_TTL;
    }
  }

  sexp = config["refresh_expiration"].str();
  if (!sexp.empty()) {
    try {
      configured_refresh_ttl = boost::lexical_cast<unsigned long>(sexp);
    }
    catch (const std::exception& e) {
      configured_refresh_ttl = DEFAULT_TTL_REFRESH;
    }
  }
}

bool TokenProviderBase::ValidateToken(const string& user_name, UserToken& user_token, bool nonceOnly) {
  if (user_token.nonce.empty()) return false;
  if (user_token.username.empty()) return false;
  if (!user_name.empty() && user_name != user_token.username) return false;
  if (nonceOnly) return true;
  time_t tim = ::time(NULL);
  return (user_token.expried != 0 && user_token.expried >= tim);
}

TokenInfo TokenProviderBase::CreateTokenMeta(std::string userName,
                                             std::string agent,
                                             std::string old_nonce,
                                             unsigned long ttl,
                                             std::map<std::string, std::string> metadata,
                                             bool createRefreshRecord) {
  ptree pt;
  stringstream spt;
  for (auto& kv : metadata) {
    pt.put(kv.first, kv.second);
  }
  write_json(spt, pt);
  string smeta = spt.str();

  return CreateToken(userName, agent, old_nonce, ttl, smeta, createRefreshRecord);
}
