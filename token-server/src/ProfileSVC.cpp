/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/format.hpp>

#include <vx/web/ServerContext.h>
#include "ProfileSVCBase.h"
#include <ProfileSVC.h>
#include <vx/Auth.h>

ProfileSVC::ProfileSVC(ServerContext &context, std::shared_ptr<ProfileProvider> provider)
    : ProfileSVCBase(context), provider(provider) {}

ProfileSVC::~ProfileSVC(){};

std::shared_ptr<ProfileData> ProfileSVC::Authenticate(
    const string &userName, const string &password, bool uuid) {
  std::shared_ptr<ProfileData> data;
  auto provider = GetProvider();

  string user = userName;
  if (uuid) {
    user = provider->GetUserName(user);
  }

  auto result = provider->Authenticate(user, password);
  if (result) {
    data.reset(new ProfileData());
    data->userid = provider->GetProperty(user, "userid");
    data->roles = provider->Roles(user);
    return data;
  }

#ifndef NDEBUG
  {
    auto rec = provider->LoadUser(user);
    string rec_json = "{}";
    if (rec.id) rec_json = rec.toJSON();
    string msg = (boost::format("Authenticate failed. user: %s profile: %s") % user % rec_json).str();
    context.Logger(LogLevel::debug, msg);
  }
#endif

  throw auth::auth_exception(
      (int)SimpleWeb::StatusCode::client_error_unauthorized,
      "Invalid username or password",
      "not_authorized");
}

std::shared_ptr<ProfileData> ProfileSVC::AuthenticatePin(const string &userName,
                                                         const string &pin, bool uuid) {
  std::shared_ptr<ProfileData> data;

  auto provider = GetProvider();

  string user = userName;
  if (uuid) {
    user = provider->GetUserName(userName);
  }

  auto result = provider->AuthenticatePin(user, pin);
  if (result) {
    data.reset(new ProfileData);
    data->userid = provider->GetProperty(user, "userid");
    data->roles = provider->Roles(user);
    return data;
  }

  throw auth::auth_exception(
      (int)SimpleWeb::StatusCode::client_error_unauthorized,
      "Not authorized", "not_authorized");
}

std::shared_ptr<ProfileData> ProfileSVC::GetProfileData(
    const string &userName, bool uuid) {
  std::shared_ptr<ProfileData> data(new ProfileData);

  auto provider = GetProvider();

  if (uuid) {
    auto user = provider->GetUserName(userName);
    data->roles = provider->Roles(user);
    data->userid = userName;
  }
  else {
    data->roles = provider->Roles(userName);
    data->userid = provider->GetProperty(userName, "userid");
  }

  return data;
};

RegistrationResult ProfileSVC::RegisterUser(UserRecord &record, bool sendEmail) {
  auto provider = GetProvider();
  return provider->RegisterUser(record);
}

RegistrationStatus ProfileSVC::ValidateRegistration(const string& token) {
  auto provider = GetProvider();
  return provider->VerifyPendingRegistration(token);
}

bool ProfileSVC::ConfirmRegistration(const string& token) {
  auto provider = GetProvider();
  return provider->ConfirmRegistration(token);
}

//-----------------------------------
//--- RESET PASSWORD ----------------
//-----------------------------------

string ProfileSVC::Reset(const string& email) {
  auto provider = GetProvider();
  string token = provider->GetResetTokenByEmail(email);

  return token;
}

bool ProfileSVC::UpdatePassword(const string& reset_code, const string& password) {
  auto provider = GetProvider();
  return provider->UpdatePassword(reset_code, password);
}

bool ProfileSVC::ResetVerify(const string& reset_code) {
  auto provider = GetProvider();
  return provider->VerifyResetToken(reset_code);
}

//-----------------------------------
//--- CHANGE PIN/PASSWORD -----------
//-----------------------------------

bool ProfileSVC::ChangePin(const string &uuid, const string &pin) {
  auto provider = GetProvider();
  auto userName = provider->GetUserName(uuid);
  if (userName.empty()) return false;
  return provider->SetPin(userName, pin);
}

bool ProfileSVC::ChangePassword(const string &uuid, const string &password) {
  auto provider = GetProvider();
  auto userName = provider->GetUserName(uuid);
  if (userName.empty()) return false;
  return provider->SetPassword(userName, password);
}
