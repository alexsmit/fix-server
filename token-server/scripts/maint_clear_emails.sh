#!/bin/bash
#postsuper -d ALL
#Also, to remove all mails in the deferred queue, the command is as follows:
#postsuper -d ALL deferred
#Similarly, we can remove a particular mail from the queue using the command,
#postsuper -d mail_id
#And, we get the mail queue ID on running the mailq command.
#To remove messages from a particular domain.
#mailq | grep domain.com | awk {'print $1'} | xargs -I{} postsuper -d {}
