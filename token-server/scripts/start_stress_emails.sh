#!/bin/bash

##. ./start_for_debug_ssl.sh 3 300 &

to="support@vxpro.io"
count=100
server=localhost
if [ ! -z "$1" ] ; then
    to=$1
fi
if [ ! -z "$2" ] ; then
    count=$2
fi
if [ ! -z "$3" ] ; then
    server=$3
fi

for((i=1;i<=$count;i++)) ; do
curl --location --request POST 'https://'${server}':8482/email/sendasync' \
--insecure \
--header 'X-Client-Id: aaa' \
--header 'X-Client-Secret: 111' \
--header 'Content-Type: application/json' \
--data-raw '{
    "from": "support@vxpro.com",
    "to": "'${to}'",
    "subject": "test email via API",
    "template": "test",
    "data": {
        "key": "value",
        "key1": "value1",
        "titles": ["item", "price"],
        "col1": ["aaa","bbb","ccc"],
        "col2": ["1.00","12.30","122.99"],
        "base": "aGVsbG8gaW5qYQo"
    },
    "attachments": {
        "smile@here": {
          "content-type": "image/gif"
        },
        "frown@here": {
          "content-type": "image/gif"
        },
        "file": {
          "content-type": "text/plain",
          "filename": "test.txt",
          "data": "aGVsbG8gaW5qYQo="
        }
      }
}'
echo " - $i"
done
