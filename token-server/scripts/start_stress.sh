#!/bin/bash
cnt=$(ps -ef | grep token-server | wc -l)

params=
paramss=
if [ "$1" == "valgrind" ] ; then
    params="valgrind 300"
fi

if [ "$1" == "prod" ] ; then
    paramss="--port 8082 --ssl"
fi

if [ $cnt -lt 2 ] ; then
  echo -n 'Token server is not running. Start? [Y/n] :'
  read ans
  if [ -z "$ans" ] || [ $ans =~ [Yy] ] ; then
    . ./start_for_test_token.sh $params&
  else
    return 1 2>/dev/null || exit 1
  fi
fi

pushd build > /dev/null
  ./token-server-stress --no_start -t 100 -i 50 --progress $paramss
popd > /dev/null
