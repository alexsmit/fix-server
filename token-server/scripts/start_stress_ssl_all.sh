#!/bin/bash
cnt=$(ps -ef | grep token-server | wc -l)

params=
if [ "$1" == "valgrind" ] ; then
    params="valgrind 300"
    shift
fi
sslport=
if [ ! -z "$1" ] ; then
    sslport="--port $1"
fi


if [ $cnt -lt 2 ] ; then
  echo -n 'Token server is not running. Start? [Y/n] :'
  read ans
  if [ -z "$ans" ] || [ $ans =~ [Yy] ] ; then
    . ./start_for_test_token_ssl.sh $params&
  else
    return 1 2>/dev/null || exit 1
  fi
fi

pushd build > /dev/null
  ./token-server-stress --no_start -t 200 -i 30 --progress --ssl --stress-pin on --stress-refresh on --stress-web on --stress-openid on $sslport
popd > /dev/null
