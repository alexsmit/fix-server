#!/bin/bash
# usage: start_for_test_teken.sh [valgrind] ttl shutdown

dovalgrind=
ttl=3
refttl=30
shut=9000

if [ "$1" == "valgrind" ] ; then
    dovalgrind="valgrind --leak-check=full -s --progress-interval=30 --show-leak-kinds=all"
    ttl=30
    refttl=60
    shift
fi

if [ ! -z "$1" ] ; then
    ttl=$1
    let refttl=$1*2
fi

if [ ! -z "$2" ] ; then
    shut=$2
fi

echo starting with ttl: $ttl  refttl: $refttl
#return 1 2>/dev/null || exit 1

killall -9 token-server
pushd build > /dev/null
    $dovalgrind ./token-server 0 0 $ttl 3 -r 5 -a $shut -f $refttl --threads 2 $3 $4 $5 $6 $7
popd > /dev/null
