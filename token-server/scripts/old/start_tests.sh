#!/bin/bash
# usage: start_for_test_teken.sh [valgrind] ttl shutdown

maxcycles=20

forever=0
if [ "$1" == "loop" ] ; then
    forever=1
fi

if [ ! -e build/token-server-test ] ; then
    echo Token server test binary does not exist
    return 1 2>/dev/null || exit 1
fi

err=0
cnt=0
ts=$(date +'%s')
while [ $err -eq 0 ]
do 
pushd build > /dev/null
    while read tst
    do
	echo Running test $tst
	./token-server-test $tst
	err=$?
	if [ $err -ne 0 ] ; then
	    break
	fi
	echo ""
	echo "---- test $tst completed"
	echo ""
    done < <(cat ../tests.txt)
popd > /dev/null

((cnt=$cnt+1))

if [ $forever -eq 0 ] || [ $err -ne 0 ] ; then
    break
fi

echo "--- cycle $cnt completed"


if [ $cnt -ge $maxcycles ] ; then
    echo "enough cycles!!!"
    break
fi

done

tsend=$(date +'%s')
((tstot=$tsend-$ts))


echo "--- executed cycles: $cnt elapsed: $tstot seconds"

