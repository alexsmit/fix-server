#!/bin/bash
# usage: start_for_test_teken.sh [valgrind] ttl shutdown

echo "start_for_test_token_ssl.sh : " $@

dovalgrind=
ttl=3
refttl=30
shut=864000

if [ "$1" == "valgrind" ] ; then
    dovalgrind="valgrind --leak-check=full -s --progress-interval=30"
    ttl=30
    refttl=60
    shift
fi

if [ ! -z "$1" ] ; then
    ttl=$1
    let refttl=$1*2
fi

if [ ! -z "$2" ] ; then
    shut=$2
fi

echo starting with ttl: $ttl  refttl: $refttl

killall -9 token-server
pushd build > /dev/null
    $dovalgrind ./token-server 9998 9498 $ttl -r 5 -e -a $shut -f $refttl --threads 32 $4 $5 $6
popd > /dev/null
