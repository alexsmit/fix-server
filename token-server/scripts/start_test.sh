#!/bin/bash
cnt=$(ps -ef | grep token-server | wc -l)

if [ $cnt -lt 2 ] ; then
  echo -n 'Token server is not running. Start? [Y/n] :'
  read ans
  if [ -z "$ans" ] || [ $ans =~ [Yy] ] ; then
    . ./start_for_test_token.sh&
    . ./start_for_test_profile.sh&
  else
    return 1 2>/dev/null || exit 1
  fi
fi

pushd build > /dev/null
  ./token-server-test -
popd > /dev/null
