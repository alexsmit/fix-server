#!/bin/bash
if [ -z "$1" ] ; then
  echo -n 'Will connect to vxpro.com. Start? [Y/n] :'
  read ans
  if [ ! -z "$ans" ] && [ $ans =~ [Nn] ] ; then
      return 1 2>/dev/null || exit 1
  fi
fi

pushd build > /dev/null
  ./token-server-stress --no_start -t 250 -i 20 --progress --ssl --host vxpro.com --port 8082 --remote-profile
popd > /dev/null
