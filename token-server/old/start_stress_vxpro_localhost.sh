#!/bin/bash
ho=localhost
if [ -z "$1" ] ; then
  echo -n 'Will connect to service @localhost. Start? [Y/n] :'
  read ans
  if [ ! -z "$ans" ] && [ $ans =~ [Nn] ] ; then
      return 1 2>/dev/null || exit 1
  fi
else
    ho=$1
fi

pushd build > /dev/null
  ./token-server-stress --no_start -t 200 -i 100 --progress --ssl --host $ho --port 8082 --remote-profile
popd > /dev/null
