#include "stdafx.h"
#include <unistd.h>

#include <boost/program_options.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

using namespace std;
namespace po = boost::program_options;

void run(RunParams params);

#ifndef NDEBUG
const int def_shutdown = 300;
#else
const int def_shutdown = 00;
#endif

int main(int argc, char** argv) {
#ifndef NDEBUG
  for (int i = 1; i < argc; i++) {
    cout << "[" << i << "] " << argv[i] << endl;
  }
#endif

  po::options_description settings{"Options"};
  // clang-format off
  settings.add_options()
      ("help,h", "Help")
     
      // 4 positional parameters
      ("port,p", po::value<int>()->default_value(DEFAULT_HTTP_PORT), "Port HTTP")                                                // 1
      ("ports,s", po::value<int>()->default_value(DEFAULT_HTTPS_PORT), "Port HTTPS")                                             // 2
      ("ttl,t", po::value<unsigned long>()->default_value(0), "TTL for jwt token, default - 5 minutes")                          // 3
      ("debug,d", po::value<int>()->default_value(0), "Debug level")                                                               // 4
     
      ("refresh,f", po::value<unsigned long>()->default_value(0), "TTL for refresh token, default - 30 days")                    //
      ("reg,r", po::value<unsigned long>()->default_value(DEFAULT_TTL_REGISTRATION), "TTL for registration, default -1 day")     //
      ("no_https,n", po::value<bool>()->implicit_value(false), "Do not run on HTTPS")                                            //
      ("no_mail,e", po::value<bool>()->implicit_value(false), "Do not send email")                                               //
      ("threads", po::value<unsigned int>()->default_value(0), "Number of threads. Default is 4")                                //
      ("clean_timeout,l", po::value<unsigned int>()->default_value(0), "Interval between cleanup thread operations in seconds. Default is 1800 (30 minutes)")
      ("profile", po::value<string>()->default_value(""), "Profile provider name")

      ("shutdown,a", po::value<unsigned int>()->default_value(def_shutdown), "Automatically shutdown after provided timeout in seconds (debug only)")

// #ifndef VXPROD
      ("test", po::value<unsigned int>()->default_value(0), "Test case. Default - 0(none). (debug only)")                        //
      ("clean", "Run registration cleanup thread")                                            //
// #endif
      
      ;

  // clang-format on

  po::positional_options_description pdesc;
  pdesc.add("port", 1);
  pdesc.add("ports", 1);
  pdesc.add("ttl", 1);
  pdesc.add("debug", 1);

  po::command_line_parser parser{argc, argv};
  parser
      .options(settings)
      .positional(pdesc)
      .allow_unregistered()
      .style(po::command_line_style::default_style | po::command_line_style::allow_slash_for_short);

  po::variables_map vars;
  try {
    po::parsed_options parsed_options = parser.run();
    po::store(parsed_options, vars);
  }
  // LCOV_EXCL_START
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    exit(1);
  }
  // LCOV_EXCL_STOP

  po::notify(vars);

  // LCOV_EXCL_START
  if (vars.count("help")) {
    cout << settings << endl;
    return 0;
  }
  // LCOV_EXCL_STOP

  RunParams params{
      vars["port"].as<int>(),
      vars["ports"].as<int>(),
      vars["ttl"].as<unsigned long>(),
      vars["debug"].as<int>(),
      (vars.count("no_https")) ? true : false,
      vars["reg"].as<unsigned long>(),
      (vars.count("no_mail")) ? true : false,
      (vars.count("shutdown")) ? vars["shutdown"].as<unsigned int>() : 0,
      vars["refresh"].as<unsigned long>(),
      vars["threads"].as<unsigned int>(),
      vars["profile"].as<string>(),
      (vars.count("test")) ? vars["test"].as<unsigned int>() : 0,
      (vars.count("clean") ? true : false),
      vars["clean_timeout"].as<unsigned int>(),
  };
  run(params);
}
