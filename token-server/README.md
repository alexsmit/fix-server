# Token server

## Building
Pre-requisite: cmake build tools (dbutil-cmake) has to be installed
## Configuration file - config.json
### general section

key | description 
--------------------------|-----------------------------------------------------------------------------------------------
port                    | HTTP port
port_https              | HTTPS port
http_server_certificate | path to ssl certificate (should contain a full bundle starting with CA)
http_server_key         | path to ssl key
loglevel                | 0-3 log output level
logfile                 | logfile name
token_provider          | configuration for token provider
profile_provider        | mysql or remote profile provider configuration
profile                 | backends for profile provider
token                   | backends for token provider

### token_provider

key | description 
--------------------------|-----------------------------------------------------------------------------------------------
type                    | backend type (db or mysql)
mode                    | certs, when using ceritificates (server_certificate & server_key should be defined) or                           key, when using private/public key pair (public_key & private_key should be defined)
public_key              | path to public key (if mode = "key")
private_key             | path to private key (if mode = "key")
server_certificate      | path to server certificate (if mode = "certs")
server_key              | path to server certificate private key (if mode = "certs")
send_token              | on/off. default is "on" 
refresh_token_rotation  | on/off, default is "on". If "on" - every time a token is refreshed a new refresh token is generated.
pin_after_expiration    | off/on. default is "off". Using pin to refresh a token after a token expiration
audience                | auidence string
issuer                  | server URL without path
scopes                  | array of scopes
algo                    | algorithm to sign a JWT token. Default is RS256
algos                   | array of signing algorithms
clients                 | (deprecated). list of client ids/secrets
cors                    | array of origin allowed for CORS
server_id               | authorization server id, set to "default" or any other congiruted string
callback_uri            | array of callback uris registered for authorization code or PKCE flow
logout_uri              | array of whitelisted logout urls, session will be cleared (refresh, access and id token will be revoked) browser will be redirected to that url after logout, if session is already cleared a browser will be simply redirected
compatibility           | a special key to provide some output compatible with other systems (contact us for more details)
ttl_session_token       | time in seconds before session token expiration. recommended is 60 seconds
expiration              | time in seconds before access token expires. default - 300 seconds
refresh_expiration      | time in seconds before refresh and id tokens expire. default - 30 days

### profile_provider

key | description 
--------------------------|-----------------------------------------------------------------------------------------------
type                      | key name in the config object to point to a profile provider configuration. For example, if a value is `mysql` then an element under `config.mysql` will be used to select a profile provider configuration key.
config                    | list of key/value pairs as key -> profile configuration key
send_email                | on (default) / off. send emails during registration/password reset ooperations
send_token                | off (default) / on. send registration or password reset token back to the API caller. This allows to avoid email communicatin and reset a password or register by the app using some other secondary means of validation of the client.
form_verify_reset_token   | on (default) / off. Validate reset token before showing a form to update a password and redirect to a failure page if code is invalid. 
email_from                | full email in form `FromName <from@domain.tld>`
confirmation_email        | "email_confirm", email template name
confirmation_email_title  | email title for registration
confirmation_success_url  | "/register/success", endpoint to show a registration success
confirmation_failure_url  | "/register/failure", endpoint to show a registration failure
reset_success_url         | "/reset/success", endpoint to show a password reset success
reset_failure_url         | "/reset/failure", endpoint to show a password reset failure
reset_email               | "email_reset", email template with password reset email
reset_email_title         | email title for password reset

### login

key | description 
--------------------------|-----------------------------------------------------------------------------------------------
client_id                 | cleint_id will be used to run authorization code flwo (PKCE)
scope                     | space delimited list of scopes applied for the generated tokens
### profile - section to configure different profile provider configurations
Contains key-object pairs, where key is the one configured in profile_provider.
```
"profile": {
  "mysql": {
    "database": "mydb",
    ...
  },
  "remote": {
    "profile_host": "https://myhost.domain.tld",
    ...
  }
}```

#### Configuration for mysql provider
key | description 
--------------------------|-----------------------------------------------------------------------------------------------
database      | database name
host          | database host
login         | database username
password      | password
version_table | "profile_version", constant value
type          | "mysql", constant value
tables        | array of tables to verify a profile database is complete
ca            | CA certificate to enable SSL connection
key           | private key in PEM format for SSL connection
cert          | certificate in PEM format for SSL connection

#### Configuration for remote provider
key | description 
--------------------------|-----------------------------------------------------------------------------------------------
profile_host  | full URL to profile server
client_id     | client id for authentication
cleint_secret | client secret for authentication    
verify_ssl    | true (default) | false. Validate SSL certificate.

### token - section to configure different token provider configurations
Contains key-object pairs, where key is the one configured in token_provider.type

```
"token": {
  "mysql: {
    "database": "mydb",
    "type": "mysql",
    ...
  },
  "db": {
    "database": "mydb",
    "type", "db",
    ...
  }
}```
#### configuration for mysql token provider
key | description 
--------------------------|-----------------------------------------------------------------------------------------------
database        | database name
type            | "mysql"
host            | database host
login           | database user name
password        | database password
version_table   | "token_version", static value
tables          | tables to verify a token provider is configured. static value, do not modify
ca              | CA certificate to enable SSL connection
key             | private key in PEM format for SSL connection
cert            | certificate in PEM format for SSL connection
expriration     | access token TTL in seconds
refresh_expriration | refresh token TTL in seconds

#### configuration for berkeley db provider
key | description 
--------------------------|-----------------------------------------------------------------------------------------------
database      | database name
type          | "db"
environment   | location where a database will be created
expiration    | access token TTL in seconds
refresh_expriration | refresh token TTL in seconds

# Test coverage
type | number of tests
--------|------------------- 
generic | 292
profile | 60
openid  | 63
