#!/bin/bash

host='localhost'
port=9998

curl --location --request POST 'http://'${host}':'${port}'/stop' \
    --insecure \
    --header 'X-Client-Id: aaa' \
    --header 'X-Client-Secret: 111' \
    --header 'Content-Type: application/json' \
    --data-raw \
'{
    "magic": "hello"
}'
