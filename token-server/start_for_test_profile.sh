#!/bin/bash

dovalgrind=
shut=300
regttl=3
sslport=0
asdaemon=0

cont=1
while [ ! -z "$1" ] ; do
    cont=1
    case $1 in
        --help|help)
            cat <<HHH
Usage:   ./start_for_test_profile.sh [valgrind] [ssl] [ttl x] [shutdown x]

Default (if no paramers are provided): 
        registration ttl : $regttl
        shutdown         : $shut s

        Similar to running the server with following parameters    

        ./profile-server 9997 $sslport $regttl $shut
HHH
            cont=2
        ;;
        valgrind)
            dovalgrind="valgrind --leak-check=full -s --progress-interval=30 --show-leak-kinds=all"
        ;;
        ssl)
    	    sslport=9497
    	;;
        -t|--ttl|ttl)
            shift
            regttl=$1
        ;;
        -a|--shutdown|shutdown)
            shift
            shut=$1
        ;;
        -d)
    	    asdaemon=1
        ;;
        *)
            cont=0
        ;;
    esac
    if [ $cont -ne 1 ] ; then
        break
    fi
    shift
done

killall -9 profile-server 2>/dev/null
pushd ../profile-server/build > /dev/null
    if [ $asdaemon -ne 0 ] ; then
	./profile-server 9997 $sslport $regttl $shut &
	profile_pid=$!
	echo $profile_pid > /var/run/user/$EUID/profile-server.pid
    else
	$dovalgrind ./profile-server 9997 $sslport $regttl $shut
    fi
popd > /dev/null

