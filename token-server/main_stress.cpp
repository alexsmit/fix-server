/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#define THREADS 100
#define ITERATIONS 5
// #define LOGGING
// #define INDICATOR

#define CLIENT_ID "aaa"
#define CLIENT_SECRET "111"
#define STRESS_USERNAME "zzstress"
#define STRESS_PASSWORD "123"
#define STRESS_HOST "localhost"
#define STRESS_PORT 9998
#define STRESS_EMAIL "zzstress@vxpro.com"
#define STRESS_PIN "1234"
#define POST_URL "/oauth/token"
#define POST_URL_USER "/users/%s"

#include "stdafx.h"

#include <time.h>

// Added for the default_resource example
#include <algorithm>
#ifdef HAVE_OPENSSL
#include "include/crypto.hpp"
#endif

#include <thread>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/timer/timer.hpp>
#include <boost/atomic.hpp>
#include <boost/format.hpp>

#include <boost/process.hpp>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include <vx/PtreeUtil.h>
#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

using namespace std;
// Added for the json-example:
using namespace boost::property_tree;
using namespace boost::archive;
using namespace boost::program_options;
namespace bp = boost::process;
namespace bo = boost::program_options;
using namespace boost::posix_time;
using namespace vx::sql;
using boost::timer::nanosecond_type;
;

// -- 1 GET token

// auto result =
//     client->request("POST", "/oauth/token", password_request, header);

static time_t error_time = 0;

enum StressItem {
  token = 1,
  pin = 2,
  openid = 4,
  web = 8,
  refresh = 16,
  //----------
  all = 31
};

/// support "Or" operator for ClientValidationType mask
inline StressItem operator|(StressItem lhs, StressItem rhs) {
  return static_cast<StressItem>(
      static_cast<std::underlying_type<StressItem>::type>(lhs) |
      static_cast<std::underlying_type<StressItem>::type>(rhs));
}

int get_factor(const StressItem &stress) {
  int factor = 0;

  if ((stress & (StressItem::pin | StressItem::token)) != 0) {
    factor = 1;
    if (stress & StressItem::refresh) {
      factor++;
    }
    if (stress & StressItem::pin) {
      factor++;
    }
  }

  if (stress & StressItem::web) {
    factor += 3;
  }
  if (stress & StressItem::openid) {
    factor += 2;
  }

  return factor;
}

std::ostream &operator<<(std::ostream &os, const StressItem &stress) {
  int factor = get_factor(stress);

  if ((stress & (StressItem::pin | StressItem::token)) != 0) {
    os << 'L';
    if (stress & StressItem::refresh) {
      os << 'R';
    }
    if (stress & StressItem::pin) {
      os << 'P';
    }
  }

  if (stress & StressItem::web) {
    os << 'W';
  }
  if (stress & StressItem::openid) {
    os << 'O';
  }

  os << " (" << factor << " api calls per request)";

  return os;
}

/// response from a token server
struct TokenResponse {
  bool valid;            //!< validity flag
  string access_token;   //!< JWT access token
  string refresh_token;  //!< refresh token
  string token_type;     //!< token type, should be "bearer"
};

int const max_requests = 25000;                    // 32678 - system sockets =28232
int const max_speed = (int)(max_requests / 60.0);  // max requests per second on a steady load 417
int const min_speed = max_speed - 20;
int const max_slot_nano = 1000000L / max_speed;
int const max_slot_nano2 = max_slot_nano / 2;

/// telemetry class to measure speed. Make sure we do not overrun existing sockets.
struct Telemetry {
  boost::timer::cpu_timer timer;  //!< using timer to measure precise time intervals
  std::recursive_mutex mtx;       //!< mutex to allow multi-thread operations

  long count;               //!< total count of requests sinse start
  long last_count,          //!< previous count
      last_speed;           //!< previous speed
  nanosecond_type next;     //!< 1s checkpoint
  boost::atomic<int> slot;  //!< delay in nanoseconds

  Telemetry() {
    count = 0;
    last_count = 0;
    last_speed = 0;
    next = nanosecond_type(1000000000LL);
    slot = 10 * max_slot_nano;  // assume api calls timing
  }

  /// @brief register a new call. Each call may open at least 1 connection or more
  /// @param verbose if nontzero - will report some data like additional sleep events
  void add(int verbose = 0) {
    mtx.lock();
    // std::lock_guard<std::recursive_mutex> lock{mtx};
    count++;
    auto tims = timer.elapsed();

    if (tims.wall > next) {
      next += nanosecond_type(1000000000LL);

      int speed = count - last_count;  // last 1sec speed
      last_count = count;
      last_speed = speed;

      double total = tims.wall / 1000000000.0;
      int avespeed = (int)(count / total);

      long newslotAve = (slot * avespeed * 1000) / min_speed / 1000;
      long newslotLast = (slot * speed * 1000) / min_speed / 1000;
      // long newslot = max(newslotAve, newslotLast);
      long newslot = (newslotAve + newslotLast) / 2;

      if (newslot >= max_slot_nano2) {
        slot = newslot;
        if (verbose)
          cout << "---" << (slot / 1000) << "  speed=" << speed << "/" << avespeed << '\n'
               << flush;
      }

      // if (speed > max_speed) {
      //   slot = (slot * speed *1000) / min_speed / 1000;
      //   cout << '+' << slot / 1000 << "  speed=" << speed << "/" << avespeed << '\n' << flush;
      // }
      // else {
      //   if (avespeed > max_speed) {
      //     slot += 8*max_slot_nano;
      //     cout << "+++" << (slot / 1000) << "  speed=" << speed << "/" << avespeed << '\n' << flush;
      //   }
      //   else if (avespeed < min_speed) {
      //     long newslot = (slot * avespeed *1000) / min_speed / 1000;
      //     if (newslot >= max_slot_nano2) {
      //       slot = newslot;
      //       cout << "---" << (slot / 1000) << "  speed=" << speed << "/" << avespeed << '\n' << flush;
      //     }
      //   }
      // }
    }
    mtx.unlock();
  }
};

// return true if we need to try again (bCont)
bool validate_ec(error_code &ec, int &nAttempts, int &idx);
TokenResponse validate_json(string content);

/// stress api template to run multiple callse
template <typename T>
class StressApi {
public:
  Telemetry *tele;  //!< setup telemetry
  string host;      //!< target host

  /// create stress api for the provided host
  StressApi(Telemetry *tele, string host) : host(host), tele(tele) {}

  /// template selector for HTTP
  template <typename TX = T>
  typename std::enable_if<std::is_same<TX, HttpClient>::value, bool>::type
  call_url(string url, int &nAttempts, int idx, string expected, int show_progress = 0) {
    HttpClient client(host);
    return call_url_int(client, url, nAttempts, idx, expected, show_progress);
  }

  /// template selector for HTTPS
  template <typename TX = T>
  typename std::enable_if<std::is_same<TX, HttpsClient>::value, bool>::type
  call_url(string url, int &nAttempts, int idx, string expected, int show_progress = 0) {
    HttpsClient client(host, false);
    return call_url_int(client, url, nAttempts, idx, expected, show_progress);
  }

  /// template selector for HTTP
  template <typename TX = T>
  typename std::enable_if<std::is_same<TX, HttpClient>::value, int>::type
  call_token(ptree pt, int &nAttempts, int idx, TokenResponse &resp, bool bAuth, int show_progress = 0) {
    T client2(host);
    return call_token_int(client2, pt, nAttempts, idx, resp, bAuth, show_progress);
  }

  /// template selector for HTTPS
  template <typename TX = T>
  typename std::enable_if<std::is_same<TX, HttpsClient>::value, int>::type
  call_token(ptree pt, int &nAttempts, int idx, TokenResponse &resp, bool bAuth, int show_progress = 0) {
    T client2(host, false);
    return call_token_int(client2, pt, nAttempts, idx, resp, bAuth, show_progress);
  }

  /// make a call to target url
  bool call_url_int(T &client, string url, int &nAttempts, int idx, string expected, int show_progress = 0) {
    tele->add(show_progress);

    SimpleWeb::CaseInsensitiveMultimap header;
    error_code ec;
    bool bCont = false;
    string code, content;
    client.request(
        "GET", url, "", header,
        [idx, &ec, &bCont, &expected, &code, &content](
            shared_ptr<typename T::Response> response,
            const SimpleWeb::error_code &ec_) {
          if (ec_)
            ec = ec_;
          else {
            content = response->content.string();
            code = response->status_code.substr(0, 3);
            if (code != expected) bCont = true;
          }
        });
    client.io_service->run();
    bool bContRet = bCont || validate_ec(ec, nAttempts, idx);
    return bContRet;
  }

  /// call to a target URL with token
  /// <0 - fatal error, 0 - success, 1 - error, can continue
  int call_token_int(T &client2, ptree pt, int &nAttempts, int idx, TokenResponse &resp, bool bAuth, int show_progress = 0) {
    tele->add(show_progress);

    bool bCont = false;

    resp.valid = false;
    SimpleWeb::CaseInsensitiveMultimap header_token;
    error_code ec;
    header_token.insert({"X-Client-Id", CLIENT_ID});
    header_token.insert({"X-Client-Secret", CLIENT_SECRET});
    if (bAuth) {
      string bearer = string("bearer") + " " + resp.access_token;
      header_token.insert({"Authorization", bearer});
    }

    stringstream req_refresh_json;
    write_json(req_refresh_json, pt);

    client2.request(
        "POST", POST_URL, req_refresh_json, header_token,
        [idx, &ec, &bCont, &resp](
            shared_ptr<typename T::Response> response,
            const SimpleWeb::error_code &ec_) {
          if (ec_)
            ec = ec_;
          else {
            resp = validate_json(response->content.string());
          }
        });
    client2.io_service->run();

    bCont = validate_ec(ec, nAttempts, idx);
    // no erros, but call failed
    if (!bCont && !resp.valid) return -1;
    return bCont ? 1 : 0;
  }
};

TokenResponse validate_json(string content) {
  TokenResponse resp = {true};
  ptree pt;
  stringstream sin(content);
  stringstream sout;
  read_json(sin, pt);
  resp.access_token = pt.get<string>("access_token", "");
  resp.refresh_token = pt.get<string>("refresh_token", "");
  resp.token_type = pt.get<string>("token_type", "");

  if (resp.access_token.empty()) {
    sout << "invalid access token" << endl;
    resp.valid = false;
  }
  if (resp.refresh_token.empty()) {
    sout << "invalid refresh token" << endl;
    resp.valid = false;
  }
  if (resp.token_type != "bearer") {
    sout << "invalid token type" << endl;
    resp.valid = false;
  }
  if (!resp.valid) {
    sout << "Content: " << content << '\n';
    cout << sout.str();
  }
  return resp;
}

bool validate_ec(error_code &ec, int &nAttempts, int &idx) {
  if (!ec) return false;

  cerr << "+++ thread: " << idx << " ERROR: " << ec.value() << " "
       << ec.message() << endl;

  if (nAttempts++ <= 10) {
    if (error_time == 0) {
      error_time = ::time(0);
    }
    std::this_thread::sleep_for(chrono::milliseconds(6500 * nAttempts));
    return true;
  }
  else {
    cerr << "+++ thread: " << idx << " Request aborted after several attempts" << endl;
    return false;
  }
}

template <typename T>
bool call_json_one(Telemetry *tele, string host, int idx, int iteration, int stress, int show_progress) {
  // this_thread::sleep_for(chrono::milliseconds(100));
#ifdef LOGGING
  cout << "+++ thread: " << idx << " start" << endl;
#endif
  int nAttempts = 1;
  bool bCont = false;
  TokenResponse resp = {false};
  try {
#ifdef LOGGING
    cout << "+++ thread: " << idx << " sending json: " << json << endl;
#endif

    bool doTokens = ((stress & (StressItem::pin | StressItem::token)) != 0);

    do {
      bCont = false;
      error_code ec;

      StressApi<T> httpApi(tele, host);

      if (doTokens) {
        ptree pt;

        // +++ login
        pt.put("grant_type", "password");
        pt.put("username", STRESS_USERNAME);
        pt.put("password", STRESS_PASSWORD);
        auto bRes = httpApi.call_token(pt, nAttempts, idx, resp, false);
        if (bRes > 0) {
          bCont = true;
          continue;
        }
        if (bRes < 0) return false;
        if (show_progress>1) cout << 'L' << flush;

        // +++ refresh token
        if ((stress & StressItem::refresh) != 0) {
          pt.clear();
          pt.put("grant_type", "refresh_token");
          pt.put("refresh_token", resp.refresh_token);
          bRes = httpApi.call_token(pt, nAttempts, idx, resp, true);
          if (bRes > 0) {
            bCont = true;
            continue;
          }
          if (bRes < 0) return false;
          if (show_progress>1) cout << 'R' << flush;
        }

        // +++ refresh with pin
        if ((stress & StressItem::pin) != 0) {
          pt.clear();
          pt.put("grant_type", "pin");
          pt.put("pin", STRESS_PIN);
          bRes = httpApi.call_token(pt, nAttempts, idx, resp, true, show_progress);
          if (bRes > 0) {
            bCont = true;
            continue;
          }
          if (bRes < 0) return false;
          if (show_progress>1) cout << 'P' << flush;
        }
      }

      if ((stress & StressItem::web) != 0) {
        bCont = httpApi.call_url("/", nAttempts, idx, "200", show_progress);
        if (bCont) return false;
        bCont = httpApi.call_url("/index.html", nAttempts, idx, "200", show_progress);
        if (bCont) return false;
        bCont = httpApi.call_url("/phpmyadmin", nAttempts, idx, "404", show_progress);
        if (bCont) return false;
        if (show_progress>1) cout << 'W' << flush;
      }

      if ((stress & StressItem::openid) != 0) {
        bCont = httpApi.call_url("/.well-known/jwks.json", nAttempts, idx, "200", show_progress);
        if (bCont) return false;
        bCont = httpApi.call_url("/.well-known/openid-configuration", nAttempts, idx, "200", show_progress);
        if (bCont) return false;
        if (show_progress>1) cout << 'O' << flush;
      }

    } while (bCont);

#ifdef LOGGING
    cout << "--- thread: " << idx << " end" << endl;
#endif
    return true;
  }
  catch (std::exception &ex) {
    cerr << "Client exception: " << ex.what() << endl;
    return false;
  }
  catch (...) {
    cerr << "Client failed" << endl;
    return false;
  }
}

template <typename T>
void call_json(Telemetry *tele, string host, int idx, int stress, int iterations = ITERATIONS, int show_progress = 0) {
  // initial delay
  std::this_thread::sleep_for(chrono::milliseconds(10 * (idx + 1)));

  for (int i = 0; i < iterations; i++) {
    if (show_progress) std::cout << flush;

    // Wait on slot
    if (tele->slot) {
      long milli = tele->slot / 1000;
      if (milli)
        std::this_thread::sleep_for(std::chrono::milliseconds(milli + 1));
    }

    if (!call_json_one<T>(tele, host, idx, i, stress, show_progress)) {
      if (show_progress>1)
        //        cout << sidx <<  "(E) " << flush;
        cout << "E" << flush;
      else if (show_progress)
        cout << endl
             << "ERROR in client ID: " << idx << endl;
      break;
    }
    else {
      if (show_progress == 2) cout << "." << flush;
      if (show_progress == 2) {
        string sidx = (boost::format("%03d-%03d ") % idx % i).str();
        cout << sidx << flush;
      }
      // if (show_progress) cout << "." << flush;
    }
    // std::this_thread::sleep_for(chrono::milliseconds(50));
    if (show_progress) std::cout.flush();
#ifdef INDICATOR
    else {
      cout << ".(" << hex << idx << ")" << flush;
    }
#endif
  }
}

void stress_test(void fun(Telemetry *, string, int, int, int, int), string host,
                 int stress,
                 int threads_count = THREADS, int iterations = ITERATIONS, int show_progress = 0) {
  // Client stress test
  time_t start = ::time(NULL);
  auto count = threads_count;
  thread threads[count];

  boost::timer::cpu_timer timer;

  Telemetry tele;

  for (int i = 0; i < count; i++) {
    threads[i] = thread(fun, &tele, host, i, stress, iterations, show_progress);
    //    threads[i].detach();
  }

  cout << "All threads started" << endl;

  try {
    for (int i = 0; i < count; i++) {
      threads[i].join();
    }
  }
  catch (std::exception &ex) {
    cerr << "EXCEPTION in MAIN: " << ex.what() << endl;
  }

  time_t curtime = ::time(NULL);
  // time_t total = curtime - start;

  auto times = timer.elapsed();  // wall time is in nanoseconds
  double total = times.wall / 1000000000.0;

  cout << endl;
  cout << "Threads : " << threads_count << endl;
  cout << "Cycles  : " << iterations << endl;
  StressItem sitem = (StressItem)stress;
  cout << "Total requests: " << (threads_count * iterations) << " flags: " << sitem << endl;
  cout << "Done in " << total << " seconds." << endl;

  if (error_time != 0)
    cout << "Time to first error: " << (curtime - error_time) << endl;
  else
    cout << "No connection errors" << endl;

  if (total != 0) {
    int factor = get_factor((StressItem)stress);
    auto reqs = (threads_count * iterations * factor);
    int speed = (int)(reqs / total);
    cout << "Requests per second: " << speed << endl;
  }
  else {
    cout << "All requests are done within a second." << endl;
  }
}

template <typename T>
bool setup_remote_int(T &client) {
  SimpleWeb::CaseInsensitiveMultimap header_token;
  error_code ec;
  header_token.insert({"X-Client-Id", CLIENT_ID});
  header_token.insert({"X-Client-Secret", CLIENT_SECRET});

  UserRecord rec = {0, STRESS_EMAIL, STRESS_USERNAME, STRESS_PASSWORD, STRESS_PIN, 0, 0};
  rec.roles = {"admin"};

  // ptree pt;
  //  pt.put("password", STRESS_PASSWORD);
  //  pt.put("pin",STRESS_PIN);
  //  pt.put("")

  string resp;
  string json = rec.toJSON();

  string url = (boost::format("/users/%s") % STRESS_USERNAME).str();

  int nAttempts = 1;
  int idx = 1;
  bool bCont = false;
  client.request(
      "POST", url, json, header_token,
      [idx, &ec, &bCont, &resp](
          shared_ptr<typename T::Response> response,
          const SimpleWeb::error_code &ec_) {
        if (ec_) {
          ec = ec_;
        }
        else {
          resp = response->content.string();
        }
      });

  client.io_service->run();
  bCont = validate_ec(ec, nAttempts, idx);
  // no erros, but call failed
  // if (!bCont && !resp.valid) return -1;
  // return bCont ? 1 : 0;
  return bCont;
}

template <typename TX>
typename std::enable_if<std::is_same<TX, HttpClient>::value, bool>::type
setup_remote(string host_profile) {
  TX client(host_profile);
  return setup_remote_int(client);
}

template <typename TX>
typename std::enable_if<std::is_same<TX, HttpsClient>::value, bool>::type
setup_remote(string host_profile) {
  TX client(host_profile, false);
  return setup_remote_int(client);
}

void setup() {
  // create a user in local MariaDB
  DataFactory factory;
#if defined(VX_USE_MYSQL)
  shared_ptr<ProfileProvider> provider = factory.Connect<ProfileProviderMySQL>();
#elif defined(VX_USE_SOCI)
  shared_ptr<ProfileProvider> provider = factory.Connect<ProfileProviderSOCI>();
#endif

  UserRecord rec = {0, STRESS_EMAIL, STRESS_USERNAME, STRESS_PASSWORD, STRESS_PIN, 0, 0};
  rec.roles = {"admin"};
  provider->SaveUser(rec);
}

int main(int ac, char **av) {
  options_description settings{"Options"};

  // clang-format off
  settings.add_options()
    ("help", "Help")
    ("port,p", bo::value<int>()->default_value(STRESS_PORT), "Port")
    ("progress,v", bo::value<bool>()->implicit_value(true), "Show progress")
    ("vv,vv", bo::value<bool>()->implicit_value(true), "Show progress more verbose")

    ("host,h", bo::value<string>()->default_value(STRESS_HOST), "Host")
    ("ssl", bo::value<bool>()->implicit_value(true), "Using SSL")
    ("threads,t", bo::value<int>()->default_value(THREADS), "Threads")
    ("iterations,i", bo::value<int>()->default_value(ITERATIONS), "Number of iterations")
    ("no_start", bo::value<bool>()->implicit_value(true), "Do not start token server")
    ("one", bo::value<bool>()->implicit_value(true), "Start just one stress iteration")
    ("remote-profile", bo::value<bool>()->implicit_value(true), "Use remote profile servern")

    ("stress-all", "All Stress tests")
    ("stress-token", "Stress test using call to token endpoint")
    ("stress-pin", "Stress test using call to pin endpoint")
    ("stress-web", "Stress test making generic web http calls")
    ("stress-openid", "Stress test making calls to openid endpoints")
    ("stress-refresh", "Stress test making calls to refresh token (ignored if stress-token is not enabled)")

    // ("mode,m", value<string>()->default_value("token"),"Mode: token - get access tokens | user - create users.")
    ;
  // clang-format on
  command_line_parser parser{ac, av};
  parser
      .options(settings)
      .allow_unregistered()
      .style(
          command_line_style::default_style |
          command_line_style::allow_slash_for_short);

  parsed_options parsed_options = parser.run();

  variables_map vars;
  try {
    store(parsed_options, vars);
  }
  catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    exit(1);
  }
  notify(vars);

  if (vars.count("help")) {
    cout << settings << endl;
    return 0;
  }

  bool start_server = true;
  if (vars.count("no_start")) {
    start_server = false;
  }

  // HTTP port for token and profile server
  int iport = vars["port"].as<int>();
  int iport_profile = iport - 1;

  // HTTP & HTTPS port is something like: 8082 -> 8482, 9998 -> 9498, etc.
  string sport = boost::lexical_cast<string>(iport);
  string sports = sport;
  sports[1] = '4';

  // HTTP & HTTPS port for profile
  string sport_profile = boost::lexical_cast<string>(iport_profile);
  string sports_profile = sport_profile;
  sports_profile[1] = '4';

  // Token Server hostname
  stringstream s;
  if (vars.count("ssl"))
    s << boost::format("%s:%s") % vars["host"].as<string>() % sports;
  else
    s << boost::format("%s:%s") % vars["host"].as<string>() % sport;
  string shost = s.str();
  cout << "Using host: " << shost << " for stress testing." << endl;

  // Profile Server hostname
  stringstream sp;
  if (vars.count("ssl"))
    sp << boost::format("%s:%s") % vars["host"].as<string>() % sports_profile;
  else
    sp << boost::format("%s:%s") % vars["host"].as<string>() % sport_profile;
  string shost_profile = sp.str();
  if (vars.count("remote-profile"))
    cout << "Using host: " << shost_profile << " for remote profile server." << endl;

  if (vars.count("remote-profile"))
    if (vars.count("ssl"))
      setup_remote<HttpsClient>(shost_profile);
    else
      setup_remote<HttpsClient>(shost_profile);
  else
    setup();

  pid_t pid_server = 0;
  auto hostname = vars["host"].as<string>();
  if (hostname == "localhost" && start_server) {
    cout << "Starting local token server" << endl;
    bp::child server("./token-server", sport.c_str(), "--no_https", "--ttl", "3");
    this_thread::sleep_for(std::chrono::milliseconds(500));
    pid_server = server.id();
    server.detach();
  }

  if (vars.count("one")) {
    Telemetry tele;
    if (vars.count("ssl"))
      call_json_one<HttpsClient>(&tele, shost, 1, 1, StressItem::all, 0);
    else
      call_json_one<HttpClient>(&tele, shost, 1, 1, StressItem::all, 0);
  }
  else {
    void (*fun)(Telemetry *, string, int, int, int, int);
    if (vars.count("ssl"))
      fun = call_json<HttpsClient>;
    else
      fun = call_json<HttpClient>;
    int threads = vars["threads"].as<int>();
    int iterations = vars["iterations"].as<int>();
    bool show_progress = false;
    if (vars.count("progress")) show_progress = 1;
    if (vars.count("vv")) show_progress = 2;
    cout << "Threads: " << threads << " Iterations: " << iterations << endl;
    StressItem stress = StressItem::token;  //(StressItem)0;

    if (vars.count("stress-all"))
      stress = StressItem::all;
    else {
      if (vars.count("stress-token")) stress = (StressItem)(stress | StressItem::token);
      if (vars.count("stress-pin")) stress = (StressItem)(stress | StressItem::pin);
      if (vars.count("stress-web")) stress = (StressItem)(stress | StressItem::web);
      if (vars.count("stress-openid")) stress = (StressItem)(stress | StressItem::openid);
      if (vars.count("stress-refresh")) stress = (StressItem)(stress | StressItem::refresh);
    }

    cout << "Stress test:\n"
         << "   host          : " << shost << '\n'
         << "   stress        : " << stress << '\n'
         << "   threads       : " << threads << '\n'
         << "   iterations    : " << iterations << '\n'
         << "   show progress : " << ((show_progress > 0) ? ((show_progress = 1) ? "-v" : "-vv") : "")
         << '\n';

    if (show_progress) {
      cout << "Legend:\n"
              "E - error\n"
              "L - login\n"
              "R - refresh token\n"
              "P - refresh token with pin\n"
              "W - web requests (3 requests)\n"
              "O - openid config requwsts (2 requests)\n";
    }

    stress_test(*fun, shost, stress, threads, iterations, show_progress);
  }

  if (pid_server != 0) {
    bp::child server(pid_server);
    server.terminate();
  }
}
