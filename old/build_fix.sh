#!/bin/bash

if [ $USER != "admin" ] ; then 
    exit 1
fi

build_dir=/u00/git/fix-server
do_sub=1
do_test=1
do_cleanup=0
do_fetch=1
do_jobs=3

sudodo=0
if [ "$1" == "exec" ] ; then
    sudodo=1
    do_fetch=0
    do_test=0
fi

eval $(ssh-agent -s) &> /dev/null
ssh-add ~/.ssh/id_rsa_aaa &> /dev/null
ssh-add ~/.ssh/id_rsa_alex &> /dev/null

clean=1

cd $build_dir
git remote update > /dev/null
dodo=$(git status | grep ''up-to-date'')
if [ -z "$dodo" ] || [ $sudodo -ne 0 ] ; then
    
		echo Starting build fix-server
		
		if [ $do_fetch -ne 0 ] ; then
			git fetch --all
			git pull
			if [ $do_sub -ne 0 ] ; then
					git submodule update
			fi

			if [ -e pre_build.sh ] ; then
					./pre_build.sh
			fi
    fi

    if [ $do_test -ne 0 ] && [ -e prepare_tests.sh ] ; then
			if [ $do_cleanup -ne 0 ] ; then
				./cleanup.sh
			fi
			
			./prepare_tests.sh $do_jobs
			if [ $? -ne 0 ] ; then
					echo TEST FAILED
					exit 1
			fi
    fi

		./prepare_build.sh
		pushd build >/dev/null
		make -j 3
		if [ $? -ne 0 ] ; then
			echo BUILD FAILED
			exit 1
		fi
		popd >/dev/null

		if [ -e post_build.sh ] ; then
			./post_build.sh
		fi
		clean=0
fi
