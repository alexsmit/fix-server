#!/bin/bash
dnf install cmake \
make \
gcc.x86_64 \
gcc-c++.x86_64 \
boost-devel \
mariadb-connector-c-devel.x86_64 \
libdb-cxx-devel.x86_64 \
libuuid-devel.x86_64 \
cppunit-devel.x86_64 \
valgrind.x86_64 \
mariadb \
file-devel.x86_64 \
rpm-build rpm-sign \
fcgi-devel
