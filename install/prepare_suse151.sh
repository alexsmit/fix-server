#!/bin/bash
zypper install -f cmake \
gcc-c++ \
libboost_chrono1_66_0-devel \
libboost_filesystem1_66_0-devel \
libboost_system1_66_0-devel \
libboost_program_options1_66_0-devel \
libboost_serialization1_66_0-devel \
libboost_thread1_66_0-devel \
libmariadb-devel \
libuuid-devel \
libdb-4_8-devel \
cppunit-devel \
sqlite3-devel sqlite3-doc \
libvmime-devel

