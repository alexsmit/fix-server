#!/bin/bash
zypper install -f cmake \
gcc-c++ \
boost_1_61-devel \
libboost_chrono1_61_0-devel \
libboost_filesystem1_61_0-devel \
libboost_system1_61_0-devel \
libboost_program_options1_61_0-devel \
libboost_serialization1_61_0-devel \
libboost_thread1_61_0-devel \
libmariadb-devel \
libuuid-devel \
libdb-4_8-devel \
cppunit-devel \
file-devel
