#!/bin/bash

# initial installation
# sudo apt install git mc ssh

sudo apt-get install cmake \
libdb++-dev \
uuid-dev \
libxcrypt-dev \
libmariadbclient-dev libmariadbclient-dev-compat \
libssl-dev \
libcppunit-dev \
libmagic-dev \
libfcgi-dev \
libsqlite3-dev \
libboost1.65-all-dev

###libmariadb-dev-compat \
