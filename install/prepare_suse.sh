#!/bin/bash
zypper install -f cmake \
git \
gcc-c++ \
libboost_chrono1_74_0-devel \
libboost_filesystem1_74_0-devel \
libboost_system1_74_0-devel \
libboost_program_options1_74_0-devel \
libboost_serialization1_74_0-devel \
libboost_thread1_74_0-devel \
libboost_regex1_74_0-devel \
libboost_contract1_74_0-devel \
libmariadb-devel \
libuuid-devel \
libdb-4_8-devel \
cppunit-devel \
libvmime-devel \
sqlite3-devel \
file-devel \
rpm-build
