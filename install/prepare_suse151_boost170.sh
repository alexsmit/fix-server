#!/bin/bash
zypper install -f cmake \
gcc-c++ \
libboost_headers1_71_0-devel \
libboost_chrono1_71_0-devel \
libboost_filesystem1_71_0-devel \
libboost_system1_71_0-devel \
libboost_program_options1_71_0-devel \
libboost_serialization1_71_0-devel \
libboost_thread1_71_0-devel \
libboost_regex1_71_0-devel \
libmariadb-devel \
libuuid-devel \
libdb-4_8-devel \
cppunit-devel \
sqlite3-devel sqlite3-doc \
libvmime-devel

