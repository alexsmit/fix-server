#!/bin/bash

runit=0
if [ "$1" == "run" ] ; then
    runit=1
fi

killall -9 profile-server

pushd build > /dev/null

    if [ $runit -ne 0 ] ; then
	./profile-server 9997 0 5 120 &
	pid=$!
	sleep 3
	./profile-server-test -
	kill -HUP $pid
	sleep 8
    else
	./profile-server 9997 0 5 120
    fi

popd >/dev/null
