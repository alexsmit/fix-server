#include <stdafx.h>

#include <boost/variant.hpp>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestResultCollector.h>
#include <iostream>
#include <thread>


#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/process.hpp>

#include <vx/LinuxProcess.h>
#include <vx/URL.h>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <BaseTest.h>
#include "src/ProfileServerContext.h"

#include "src/ProfileProviderRemote.h"

#include <InitProfileTest.h>

#include "test/RegisterTest.h"
#include "test/BlobTest.h"
#include "test/PostUserTest.h"
#include "test/PostUserTestV1.h"
#include "test/AuthTest.h"
#include "test/ResetTokenTest.h"
#include "test/GetUserTest.h"
#include "test/PostPinTest.h"
#include "test/PropertiesTest.h"
#include "test/RolesTest.h"
#include "test/WebTest.h"
#include "test/ProfileRemoteTest.h"
#include "test/ProfileServerContextTest.h"
#include "test/ProfileRemoteFailTest.h"

#include <vx/ServiceFactory.h>
#include <vx/ServiceProfileFactory.h>

using namespace std;
using namespace vx::openid::test;
using namespace vx::sql;
namespace bp = boost::process;
using namespace vx::providers;

// Note: we don't need to include RegisteredTest
static std::shared_ptr<ServerContext> server_context;

string profile_name = "profile.mysql";
string backend;
string ssl_port = "0";
JsonConfig conf;

std::shared_ptr<ServerContext> getContext(const string& param, bool useDefault, int provider_ttl) {
  if (!server_context) {
    AppConfig::resetInstance();
    AppConfig config;
    config["profile.mysql.type"] = backend;

    server_context.reset(new ProfileServerContext(5, backend));
    server_context->Init(NULL, "config.json");

    auto profile_context = std::dynamic_pointer_cast<ProfileServerContext>(server_context);
    auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
    profile_context->AddProvider(profile_service);
  }
  return server_context;
}

#define FOCUS(x)                                                   \
  if (boost::algorithm::ends_with(x::suite()->getName(), focus)) { \
    runner.addTest(x::suite());                                    \
    runner_soci.addTest(x::suite());                               \
    runner_ssl.addTest(x::suite());                                \
  }

DebugEnvironment DebugEnvironment::env = {};

void DebugEnvironment::init(const std::string& ssl_port) {
  string hos;
  hos.resize(128);
  auto res = ::gethostname(&hos[0], hos.length());
  string myhost(hos.data());

  int port = (ssl_port == "0") ? 9997 : std::atoi(ssl_port.c_str());

  env.DEBUG_HOST = (boost::format("%s:%d") % myhost % port).str();
  if (ssl_port != "0") env.DEBUG_HOST = string("https://") + env.DEBUG_HOST;

  env.DEFAULT_TTL_REGISTRATION = 86400L;
}

bool kill_server(std::shared_ptr<boost::process::child> server, bool useKill = true) {
  if (useKill) {
    ::kill(server->id(), SIGHUP);
  }
  else {
    SimpleWeb::CaseInsensitiveMultimap header;
    header.insert({"X-Client-Id", "aaa"});
    header.insert({"X-Client-Secret", "111"});
    header.insert({"Content-Type", "application/json"});
    string payload = "{\"magic\":\"hello\"}";

    boost::regex pat("^Connection refused");  // may happen after receiving /stop request (server terminated)
    boost::smatch match;
    try {
      shared_ptr<HttpClient> client;
      client.reset(new HttpClient(DEBUG_HOST));
      client->request("POST", "/stop", payload, header);
      std::this_thread::sleep_for(std::chrono::milliseconds(500));  // LCOV_EXCL_LINE
    }
    // LCOV_EXCL_START
    catch (const std::exception& e) {
      string mes = e.what();
      if (!boost::regex_search(mes, match, pat)) {
        std::cerr << "Stopping server: " << e.what() << '\n';  // LCOV_EXCL_LINE
      }
    }
    // LCOV_EXCL_STOP
  }

  long max_timeout = 18000;
  while (server->running() && max_timeout > 0) {
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    max_timeout -= 300;
  }
  return !server->running();
}

int main(int argc, char** argv) {
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TextUi::TestRunner runner_ssl;
  CppUnit::TextUi::TestRunner runner_soci;

  vector<string> vfocus;
  bool start_server = true;
  bool run_ssl_tests = VXPRO_PROFILE_HTTPS_TEST;

  // LCOV_EXCL_START
  for (auto i = 1; i < argc; i++) {
    string v = argv[i];
    if (v == "-") {
      start_server = false;
      continue;
    }
    if (v == "--ssl") {
      ssl_port = "9497";
      continue;
    }
    if (v.length() > 1 && v[0] == '-') continue;

    vfocus.push_back(argv[i]);
  }
  // LCOV_EXCL_STOP

  std::cout << "profile-server test\n"
            << "Host         : " << DebugEnvironment::env.DEBUG_HOST << endl
            << "TTL          : " << DebugEnvironment::env.DEFAULT_TTL_REGISTRATION << endl

      ;
  // LCOV_EXCL_START
  if (ssl_port != "0") {
    cout << "SSL          : ON\n";
  }
  // LCOV_EXCL_STOP

  DebugEnvironment::env.init(ssl_port);

  if (vfocus.empty()) {
    CppUnit::TestFactoryRegistry& registry =
        CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest(registry.makeTest());
    runner_soci.addTest(registry.makeTest());
    runner_ssl.addTest(registry.makeTest());
  }
  else {
    // LCOV_EXCL_START
    for (auto i = 0; i < vfocus.size(); i++) {
      string focus = vfocus[i];

      FOCUS(AuthTest);
      FOCUS(BlobTest);
      FOCUS(GetUserTest);
      FOCUS(PostPinTest);
      FOCUS(PostUserTest);
      FOCUS(PropertiesTest);
      FOCUS(ResetTokenTest);
      FOCUS(RolesTest);
      FOCUS(PostUserTestV1);
      FOCUS(ProfileRemoteTest);

      FOCUS(RegisterTest);

      FOCUS(WebTest);
      FOCUS(ProfileServerContextTest)
      FOCUS(ProfileRemoteFailTest)
    }
    // LCOV_EXCL_STOP
  }

  // auto &res = runner.result();
  // auto &tests = res.tests();
  // int cnt = tests.size();
  // cout << "Mysql tests: " << cnt << "\n";
  // auto &failures = res.failures();

  // auto &res_soci = runner_soci.result();
  // auto &tests_soci = res_soci.tests();
  // int cnt_soci = tests_soci.size();
  // cout << "Soci tests: " << cnt_soci << "\n";
  // auto &failures = res.failures();

  ConnectProfileProvider = [](std::shared_ptr<DataFactory> factory, const char* p, const string& backend) {
    std::shared_ptr<ProfileProvider> provider;
    if (p != NULL) profile_name = p;

#if defined(VX_USE_MYSQL)
    if (backend == "mysql") {
      provider = factory->Connect<ProfileProviderMySQL>(profile_name.c_str());
      return provider;
    }
#endif

#if defined(VX_USE_SOCI)
    if (backend == "soci" || backend == "mysql") {
      provider = factory->Connect<ProfileProviderSOCI>(profile_name.c_str());
      return provider;
    }
#endif

    if (backend == "remote") {
      AppConfig& app = AppConfig::getInstance();
      JsonConfig& conf_src = app.Section(profile_name);

      string host = conf_src["remote_host"];

      URL u(host);
      if (ssl_port != "0") {
        u.port = ssl_port;
        u.protocol = "https";
        host = u.getHost();
      }

      conf[profile_name + string(".remote_host")] = host;
      conf[profile_name + string(".client_id")] = conf_src["client_id"].str();
      conf[profile_name + string(".client_secret")] = conf_src["client_secret"].str();

      JsonConfig& c = conf.getSection(profile_name);

      provider = factory->Connect<ProfileProviderRemote>(c);
      return provider;
    }

    throw runtime_error("Invalid backend");  // LCOV_EXCL_LINE
  };                                         // LCOV_EXCL_LINE

  ProfileProviderKey = []() { return profile_name; };

  // shared_ptr<LinuxProcess> server;
  shared_ptr<bp::child> server;

  int result_soci = 0;
  int result_ssl = 0;
  int result = 0;
  int result_stop = 0;
  bool run_soci_tests = true;

#if defined(VX_USE_MYSQL)
  cout << "\n*** MYSQL/current config backend tests ***\n\n";
#else
  cout << "\n*** current config backend tests ***\n\n";
  run_soci_tests = false;
#endif

#if !defined(VX_USE_SOCI)
  run_soci_tests = false;
#endif

  if (start_server) {
    vector<string> vargs = {"9997", ssl_port, "5"};
    server.reset(new bp::child("./profile-server", bp::args(vargs) /*, bp::std_err > bp::null*/));
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
  }

  backend = "mysql";

  result = (runner.run("",
                       /*wait*/ false,
                       /*printResult*/ true,
                       /*printProgress*/ true))
               ? 0
               : 1;

  if (start_server) {
    cout << "Shutting down profile server..." << endl;
    kill_server(server);
  }
  server_context.reset();

  if (!start_server) return result;

  if (ssl_port == "0" && run_ssl_tests) {
#if defined(VX_USE_MYSQL)
    cout << "\n*** MYSQL/current config backend tests over HTTPS ***\n\n";
#else
    cout << "\n*** current config backend tests over HTTPS ***\n\n";
#endif
    ssl_port = "9497";

    DebugEnvironment::env.init(ssl_port);

    if (start_server) {
      vector<string> vargs = {"9997", ssl_port, "5"};
      server.reset(new bp::child("./profile-server", bp::args(vargs) /*, bp::std_err > bp::null*/));
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    result_ssl = (runner_ssl.run("",
                                 /*wait*/ false,
                                 /*printResult*/ true,
                                 /*printProgress*/ true))
                     ? 0
                     : 1;

    if (start_server) {
      cout << "Shutting down profile server..." << endl;
      kill_server(server);
    }
    server_context.reset();

    ssl_port = "0";
    DebugEnvironment::env.init(ssl_port);
  }

  // LCOV_EXCL_START
  if (run_soci_tests) {
    cout << "\n*** SOCI backend tests ***\n\n";

    if (start_server) {
      // server.reset(new LinuxProcess);
      // server->start("./profile-server", {"9997", "0", "5"});
      std::this_thread::sleep_for(std::chrono::milliseconds(500));

      vector<string> vargs = {"9997", ssl_port, "5", "300", "1", "soci"};
      server.reset(new bp::child("./profile-server", bp::args(vargs) /*, bp::std_err > bp::null*/));

      std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    backend = "soci";

    result_soci = (runner_soci.run("",
                                   /*wait*/ false,
                                   /*printResult*/ true,
                                   /*printProgress*/ true))
                      ? 0
                      : 1;

    server_context.reset();
    if (start_server) {
      cout << "Shutting down profile server..." << endl;
      kill_server(server, true);
    }
  }
  // LCOV_EXCL_STOP

  // final test to check shutdown using /stop
  if (start_server) {
    cout << "\n*** START/STOP test ***\n\n";

    vector<string> vargs = {"9997", "0", "5"};
    server.reset(new bp::child("./profile-server", bp::args(vargs) /*, bp::std_err > bp::null*/));
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    if (!kill_server(server, false)) {
      // LCOV_EXCL_START
      cout << "Failed to stop server\n";
      result_stop = 1;
      // LCOV_EXCL_STOP
    }

    if (result_stop) {
      cout << "FAIL (1 test)\n"; // LCOV_EXCL_LINE
    } else {
      cout << "OK (1 test)\n";
    }
  }

  return result + result_soci + result_ssl + result_stop;
}
