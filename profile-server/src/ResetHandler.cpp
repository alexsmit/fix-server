/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/variant.hpp>

#include "ResetHandler.h"
#include <vx/web/Util.h>

ResetHandler::ResetHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)


{
  secure_server

      .use("^/password/reset$", Request::RequestType::POST,
           std::bind(&ResetHandler::ResetPassword, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/password/update$", Request::RequestType::POST,
           std::bind(&ResetHandler::UpdatePassword, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/password/verifytoken$", Request::RequestType::POST,
           std::bind(&ResetHandler::VerifyToken, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

ResetHandler::~ResetHandler() {}

void ResetHandler::ResetPassword(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;

  auto email = request->body["email"];
  if (email.empty()) {
    badRequest(response);
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  string token = provider->GetResetTokenByEmail(email.c_str());
  if (token.empty()) {
    sendError(response, "not_found", "", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  ptree result;
  result.put("token", token);
  sendJSON(response, result);
}

void ResetHandler::UpdatePassword(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;

  auto token = request->body["token"];
  auto password = request->body["password"];
  if (token.empty() || password.empty()) {
    badRequest(response);  // 400
    return;
  }

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  if (provider->UpdatePassword(token.c_str(), password.c_str()))
    response->write(SimpleWeb::StatusCode::success_ok, "");
  else
    response->write(SimpleWeb::StatusCode::client_error_forbidden, "");
}

void ResetHandler::VerifyToken(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;

  auto token = request->body["token"];
  if (token.empty()) {
    badRequest(response);  // 400
    return;
  }

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  if (provider->VerifyResetToken(token.c_str()))
    response->write(SimpleWeb::StatusCode::success_ok, "");
  else
    response->write(SimpleWeb::StatusCode::client_error_forbidden, "");
}
