/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/variant.hpp>

#include "RoleHandler.h"
#include <vx/web/Util.h>

RoleHandler::RoleHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  auto validate = [&](std::shared_ptr<Response> response, std::shared_ptr<Request> request){
    context.validateRequest(*response, *request);
  };

  secure_server

      .with(validate)
      .use("^/roles/([\\w \\d]+)$", Request::RequestType::POST,
           std::bind(&RoleHandler::SaveRole, *this, std::placeholders::_1, std::placeholders::_2))
      
      .with(validate)
      .use("^/roles/([\\w \\d]+)$", Request::RequestType::DELETE,
           std::bind(&RoleHandler::DeleteRole, *this, std::placeholders::_1, std::placeholders::_2))

      .with(validate)
      .use("^/roles$", Request::RequestType::GET,
           std::bind(&RoleHandler::GetRoles, *this, std::placeholders::_1, std::placeholders::_2))

      .with(validate)
      .use("^/roles/([\\w \\d]+)$", Request::RequestType::GET,
           std::bind(&RoleHandler::GetRole, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

RoleHandler::~RoleHandler() {}

void RoleHandler::SaveRole(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  // if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string role = path_match[1].str();

  auto desc = request->body["description"];
  if (desc.empty()) {
    badRequest(response);
    return;
  }

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  provider->AddRole(role.c_str(), desc.c_str());
  response->write(SimpleWeb::StatusCode::success_ok, "");
}

void RoleHandler::DeleteRole(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  // if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string role = path_match[1].str();

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  provider->DeleteRole(role.c_str());

  response->write(SimpleWeb::StatusCode::success_ok, "");
}

void RoleHandler::GetRoles(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  // if (!context.validateRequest(*response, *request)) return;
  ptree pt_root, pt;
  vector<string> roles;

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  roles = provider->Roles();
  vector2ptree(roles, pt);
  pt_root.add_child("roles", pt);
  sendJSON(response, pt_root);
}

void RoleHandler::GetRole(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  // if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string role = path_match[1].str();

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  string desc = provider->GetRoleProperty(role.c_str(), "description");
  ptree pt;
  pt.put("description", desc);
  sendJSON(response, pt);
}
