/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/variant.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "AuthHandler.h"
#include <vx/web/Util.h>

using namespace boost::property_tree;
using namespace vx::sql;

AuthHandler::AuthHandler(ProfileServerContext& context, WebServer& server, Instance& secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  secure_server
      .use("^/pin$", Request::RequestType::POST,
           std::bind(&AuthHandler::Pin, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/authenticate$", Request::RequestType::POST,
           std::bind(&AuthHandler::Authenticate, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

AuthHandler::~AuthHandler() {}

void AuthHandler::Pin(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;

  string username = request->body["username"];
  string pin = request->body["pin"];
  string userid = request->body["userid"];
  if (username.empty() && userid.empty()) {
    badRequest(response);
    return;
  }

  if (pin.empty()) {
    badRequest(response);
    return;
  }

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  if (!userid.empty()) {
    username = provider->GetUserName(userid.c_str());
  }
  if (username.empty()) {
    sendError(response, "not_found", "", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  bool bValid = provider->AuthenticatePin(username.c_str(), pin.c_str());

  if (bValid) {
    ptree pt_root, pt_roles;
    pt_root.put("userid", provider->GetProperty(username.c_str(), "userid"));
    auto vroles = provider->Roles(username.c_str());
    vector2ptree(vroles, pt_roles);
    pt_root.add_child("roles", pt_roles);
    sendJSON(response, pt_root);
  }
  else {
    sendError(response, "forbidden", "", SimpleWeb::StatusCode::client_error_forbidden);
  }
}

void AuthHandler::Authenticate(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;

  string username = request->body["username"];
  string userid = request->body["userid"];
  string password = request->body["password"];
  string grant = request->body["grant_type"];
  if (password.empty() || grant != "password" || (username.empty() && userid.empty())) {
    badRequest(response);
    return;
  }

  auto provider = context.GetProfileProvider();
  if (username.empty()) {
    username = provider->GetUserName(userid.c_str());
  }

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  UserRecord rec{0};
  bool bValid = provider->Authenticate(username.c_str(), password.c_str(), &rec);

  if (bValid) {
    ptree pt_root, pt_roles;
    pt_root.put("userid", rec.userid);
    vector2ptree(rec.roles, pt_roles);
    pt_root.add_child("roles", pt_roles);
    sendJSON(response, pt_root);
  }
  else {
    response->write(SimpleWeb::StatusCode::client_error_forbidden, "");
  }
}
