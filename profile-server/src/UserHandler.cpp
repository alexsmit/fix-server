/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/variant.hpp>

#include "UserHandler.h"
#include <vx/web/Util.h>

namespace p = std::placeholders;

UserHandler::UserHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  auto validate = [&](std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
    if (!context.validateRequest(*response, *request)) return;
  };

  secure_server
      .use("^/userid/([\\w\\d\\-]+)$", Request::RequestType::GET,
           std::bind(&UserHandler::GetUserByUUID, *this, p::_1, p::_2))
      .use("^/users/([\\w \\d]*)$", Request::RequestType::GET,
           std::bind(&UserHandler::GetUser, *this, p::_1, p::_2))
      .use("^/users/([\\w \\d]+)$", Request::RequestType::POST,
           std::bind(&UserHandler::SaveUser, *this, p::_1, p::_2))

      .use("^/users/([\\w \\d]+)/password$", Request::RequestType::POST,
           std::bind(&UserHandler::SetPassword, *this, p::_1, p::_2, false))
      .use("^/users/([\\w \\d]+)/pin$", Request::RequestType::POST,
           std::bind(&UserHandler::SetPin, *this, p::_1, p::_2, false))

      .use("^/userid/([\\w\\d\\-]+)/password$", Request::RequestType::POST,
           std::bind(&UserHandler::SetPassword, *this, p::_1, p::_2, true))
      .use("^/userid/([\\w\\d\\-]+)/pin$", Request::RequestType::POST,
           std::bind(&UserHandler::SetPin, *this, p::_1, p::_2, true))

      .use("^/users/([\\w \\d]+)$", Request::RequestType::DELETE,
           std::bind(&UserHandler::DeleteUser, *this, p::_1, p::_2, false))
      .use("^/userid/([\\w\\d\\-]+)$", Request::RequestType::DELETE,
           std::bind(&UserHandler::DeleteUser, *this, p::_1, p::_2, true))

      // v1

      .with(validate)
      .use("^/api/v1/users$", Request::RequestType::GET,
           std::bind(&UserHandler::FindUserV1, *this, p::_1, p::_2))

      .with(validate)
      .use("^/api/v1/users$", Request::RequestType::POST,
           std::bind(&UserHandler::CreateUserV1, *this, p::_1, p::_2))

      .with(validate)
      .use("^/api/v1/users/([\\w\\d\\-]*)$", Request::RequestType::GET,
           std::bind(&UserHandler::GetUserV1, *this, p::_1, p::_2))

      .with(validate)
      .use("^/api/v1/users/([\\w\\d\\-]*)$", Request::RequestType::POST,
           std::bind(&UserHandler::SaveUserV1, *this, p::_1, p::_2))

      .with(validate)
      .use("^/api/v1/users/([\\w\\d\\-]*)$", Request::RequestType::DELETE,
           std::bind(&UserHandler::DeleteUserV1, *this, p::_1, p::_2))

      ;
}

UserHandler::~UserHandler() {}

void UserHandler::GetUser(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  ptree pt, pt_roles;
  auto path_match = request->Match();
  string user = path_match[1].str();

  if (user.empty()) {
    sendError(response, "bad_request", "username is missing");
    return;
  }

  UserRecord rec;

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  rec = provider->LoadUser(user);

  if (rec.id == 0) {
    pt.put<string>("error", "not_found");
    pt.put<string>("error_description", "User not found");

    sendJSON(response, pt, SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  response->write(SimpleWeb::StatusCode::success_ok, rec.toJSON());
}

void UserHandler::SaveUser(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string user = path_match[1].str();

  ptree &pt = request->body.value();
  string password = pt.get<string>("password", "");
  string email = pt.get<string>("email", "");
  string pin = pt.get<string>("pin", "");

  UserRecord rec = {0, email, user, password, pin, 0, 0};

  boost::optional<ptree &> child_roles = pt.get_child_optional("roles");
  if (!!child_roles) {
    vector<string> vroles;
    for (auto &v : child_roles.get()) {
      vroles.push_back(v.second.data());
    }
    rec.roles = vroles;
  }

  boost::optional<ptree &> child_props = pt.get_child_optional("properties");
  if (!!child_props) {
    map<string, string> mprops;
    for (auto &v : child_props.get()) {
      mprops.insert({v.first, v.second.data()});
    }
    rec.props = mprops;
  }

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  provider->SaveUser(rec);

  response->write(SimpleWeb::StatusCode::success_ok, "");
}

void UserHandler::GetUserByUUID(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string userid = path_match[1].str();

  UserRecord rec;

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  // string user = provider->GetUserName(userid);
  // if (user.empty()) {
  //   pt.put<string>("error", "Record not found");
  //   sendJSON(response, pt, SimpleWeb::StatusCode::client_error_not_found);
  //   return;
  // }

  rec = provider->LoadUser(userid, ProfileKey::userid);

  if (rec.id == 0) {
    sendError(response, "not_found", "user does not exist", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  response->write(SimpleWeb::StatusCode::success_ok, rec.toJSON());
}

void UserHandler::SetPassword(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID) {
  if (!context.validateRequest(*res, *req)) return;
  auto path_match = req->Match();
  string user = path_match[1].str();

  // protected by route, check for proper username anyway
  // LCOV_EXCL_START
  if (user.empty()) {
    sendError(res, "bad_request", "user or uuid is empty");
    return;
  }
  // LCOV_EXCL_STOP

  auto password = req->body["password"];
  if (password.empty()) {
    sendError(res, "bad_request", "password is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};

  if (isUUID) {
    user = provider->GetUserName(user);
    if (user.empty()) {
      sendError(res, "not_found", "uuid is invalid or does not exist");
      return;
    }
  }

  provider->SetPassword(user, password);
  res->write(SimpleWeb::StatusCode::success_ok, "");
}

void UserHandler::SetPin(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID) {
  if (!context.validateRequest(*res, *req)) return;
  auto path_match = req->Match();
  string user = path_match[1].str();

  // not possible if called wia web handler
  // LCOV_EXCL_START
  if (user.empty()) {
    sendError(res, "bad_request", "user or uuid is empty");
    return;
  }
  // LCOV_EXCL_STOP

  auto pin = req->body["pin"];
  if (pin.empty()) {
    sendError(res, "bad_request", "pin is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  if (isUUID) {
    user = provider->GetUserName(user);
    if (user.empty()) {
      sendError(res, "not_found", "uuid is invalid or does not exist");
      return;
    }
  }
  provider->SetPin(user, pin);
  res->write(SimpleWeb::StatusCode::success_ok, "");
}

void UserHandler::DeleteUser(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID) {
  if (!context.validateRequest(*res, *req)) return;
  auto path_match = req->Match();
  string user = path_match[1].str();

  // not possible if called wia web handler
  // LCOV_EXCL_START
  if (user.empty()) {
    sendError(res, "bad_request", "user or uuid is empty");
    return;
  }
  // LCOV_EXCL_STOP

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  if (isUUID) {
    user = provider->GetUserName(user);
    if (user.empty()) {
      sendError(res, "not_found", "uuid is invalid or does not exist");
      return;
    }
  }
  if (provider->DeleteUser(user))
    res->write(SimpleWeb::StatusCode::success_ok, "");
  else
    sendError(res, "delete_failed", "");
}
