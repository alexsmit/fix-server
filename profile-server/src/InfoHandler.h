#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include "ProfileServerContext.h"

using namespace VxServer;

/// /info handler class
class InfoHandler {
private:
  ProfileServerContext& context;
  WebServer& server;
  Instance& secure_server;

public:
  /// /info and /version request handler
  InfoHandler(ProfileServerContext& context, WebServer& server, Instance& secure_server);
  ~InfoHandler();

  /// /info handler, uses HTML template /templates/info.html
  void Info(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// /version handler
  void Version(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// /api/v1/info
  void ApiInfo(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

  /// /api/v1/param
  void GetParam(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// /api/v1/param
  void SetParam(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
