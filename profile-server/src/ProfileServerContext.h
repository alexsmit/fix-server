#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <vx/web/ServerContext.h>
#include <vx/ContextTTL.h>
#include <vx/ServiceFactory.h>

using namespace std;
using namespace boost::property_tree;
using namespace vx::sql;

/// context class. provides access to services and parameters
class ProfileServerContext : public ServerContext {
public:
  DataFactory factory;           //!< factory to initialize services
  int pool_size,                 //!< number of threads
      port,                      //!< port for HTTP handlers
      ports;                     //!< port for HTTPS handlers
  unsigned long reg_ttl;         //!< registration token expiration time in seconds
  time_t provider_ttl = 60;      //!< time to re-create a provider
  std::string backend;           //!< if set - forced backend type

private:
  std::map<std::thread::id, std::shared_ptr<vx::ContextTTL>> profile_providers;
  // std::map<std::string, shared_ptr<vx::ServiceFactory>> providers;

public:
  /// Create context object
  ProfileServerContext(unsigned long reg_ttl);

  /// Create context object
  ProfileServerContext(unsigned long reg_ttl, const std::string& backend);

  /// return profile provider. Currently implemented is "mysql" only.
  std::shared_ptr<vx::sql::ProfileProvider> GetProfileProvider();

  /// get service by name
  //std::shared_ptr<Service> getService(const std::string& name, const std::string& type="", time_t ttl=0) override;

  /// Init
  void Init(const char* prefix = NULL, const string& configFileName = AppConfig::default_config) override;

  /// register service provider. key to register is provider->id()
  // void AddProvider(shared_ptr<vx::ServiceFactory> provider);
};
