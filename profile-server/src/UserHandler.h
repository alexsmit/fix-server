#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include "ProfileServerContext.h"

using namespace VxServer;

/// "legacy" and V1 user handler
class UserHandler {
private:
  ProfileServerContext& context;
  WebServer& server;
  Instance& secure_server;

public:
  /// create user handler
  UserHandler(ProfileServerContext& context, WebServer& server, Instance& secure_server);
  ~UserHandler();

  /// GET /users/[username] handler
  void GetUser(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /userid/[userid] handler
  void GetUserByUUID(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// POST /users/[username] handler.
  void SaveUser(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// POST /users/[username]/password  and /userid/[userid]/password handler
  void SetPassword(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID = false);
  /// POST /users/[username]/pin  and /userid/[userid]/pin handler
  void SetPin(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID = false);
  /// DELETE /users/[username] and /userid/[userid] handlers
  void DeleteUser(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool isUUID = false);

  // V1

  /// GET /api/v1/users?[search={condition}] [name={username} ][email={email}] -> [profile,...], 404
  void FindUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// Search user
  void SearchUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req, const string& searh);
  /// (C---) POST /api/v1/users -> profile
  void CreateUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// (-R--) GET /api/v1/users/{id} -> profile
  void GetUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// (--U-) POST /api/v1/user/{id} -> profile
  void SaveUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// (---D) DELETE /api/v1/users/{id} -> 200, 404
  void DeleteUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
