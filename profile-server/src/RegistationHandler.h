#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include "ProfileServerContext.h"

using namespace VxServer;

/// User registration handler
class RegistrationHandler {
private:
  ProfileServerContext &context;
  WebServer &server;
  Instance &secure_server;

public:
  /// create user registratioin handler
  RegistrationHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server);
  ~RegistrationHandler();

  /// POST /register. Initiate user registration process.
  void Register(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// POST /register/validate. Verify whether a user is registered already.
  void Validate(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /register/confirm. Confirm registration link.
  void EmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// POST /register/clean. Force to clean pending registration records.
  void Clean(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
