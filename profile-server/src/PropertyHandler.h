#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include "ProfileServerContext.h"

using namespace VxServer;

/// Properties
class PropertyHandler {
private:
  ProfileServerContext &context;
  WebServer &server;
  Instance &secure_server;

public:
  /// create property handler
  PropertyHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server);
  ~PropertyHandler();

  /// GET /users/[username]/properties. Get all properties.
  void Get(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /users/[username]/properties/[property]. Get specific property.
  void GetOne(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// POST /users/[username]/properties. Update/add properties
  void Save(std::shared_ptr<Response> res, std::shared_ptr<Request> req);

  /// POST /users/[username]/blob/[blobname]. Save blob with content type and name.
  void SaveBlob(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /users/[username]/blob/[blobname]. Retrieve blob attachment.
  void GetBlob(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
