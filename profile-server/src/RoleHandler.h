#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include "ProfileServerContext.h"

using namespace VxServer;

/// Handle roles
class RoleHandler {
private:
  ProfileServerContext &context;
  WebServer &server;
  Instance &secure_server;

public:
  /// create role handler
  RoleHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server);
  ~RoleHandler();

  /// POST /roles/[role] handler. Save/create role.
  void SaveRole(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// DELETE /roles/[role]. Delete role.
  void DeleteRole(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /roles. Retrieve a list of roles
  void GetRoles(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
  /// GET /roles/[role]. Retrive role information.
  void GetRole(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
