/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>

#include "UserHandler.h"
#include <vx/web/Util.h>
#include <vx/StringUtil.h>
#include <vx/sql/SearchContidion.h>

namespace p = std::placeholders;
using namespace vx::sql;

void UserHandler::SearchUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req, const string &search) {
  ptree pt;

  SearchCondition cond(search);
  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};

  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-Type", "application/json");

  if (cond.conditions.size() == 0) {
    res->write("[]", header);
    return;
  }

  vector<UserRecord> result;

  result = provider->SearchByCondition(cond, 1000);

  stringstream sout;
  vector<string> elems;
  for (auto &r : result) {
    elems.push_back(r.toJSON());
  }
  sout << "[" << boost::join(elems, ",") << "]";

  res->write(sout.str(), header);
}

void UserHandler::FindUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto search = req->params["search"];
  if (!search.empty()) return SearchUserV1(res, req, search);

  ptree pt;
  auto user = req->params["username"];
  auto email = req->params["email"];
  if (user.empty() && email.empty()) {
    // cerr << "find user failed\n";
    sendError(res, "bad_request", "email or username parameter is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  UserRecord rec;
  if (!user.empty()) {
    StringUtil::trim(user);
    rec = provider->LoadUser(user, ProfileKey::username);
  }
  else {
    StringUtil::trim(email);
    rec = provider->LoadUser(email, ProfileKey::email);
  }

  if (rec.userid.empty()) {
    sendError(res, "not_found", "user not found", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  res->write(SimpleWeb::StatusCode::success_ok, string("[") + rec.toJSON() + string("]"), header);
}

void UserHandler::CreateUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  ptree &pt = req->body.value();
  string user = pt.get<string>("username", "");
  StringUtil::trim(user);
  string password = pt.get<string>("password", "");
  string email = pt.get<string>("email", "");
  string pin = pt.get<string>("pin", "");

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};

  auto recTest = provider->LoadUser(user);
  if (!recTest.userid.empty()) {
    sendError(res, "conflict", "unable to create a user", SimpleWeb::StatusCode::client_error_conflict);
    return;
  }
  recTest = provider->LoadUser(email, ProfileKey::email);
  if (!recTest.userid.empty()) {
    sendError(res, "conflict", "unable to create a user", SimpleWeb::StatusCode::client_error_conflict);
    return;
  }

  UserRecord rec{0, email, user, password, pin};

  boost::optional<ptree &> child_roles = pt.get_child_optional("roles");
  if (!!child_roles) {
    vector<string> vroles;
    for (auto &v : child_roles.get()) {
      vroles.push_back(v.second.data());
    }
    rec.roles = vroles;
  }

  boost::optional<ptree &> child_props = pt.get_child_optional("properties");
  if (!!child_props) {
    map<string, string> mprops;
    for (auto &v : child_props.get()) {
      mprops.insert({v.first, v.second.data()});
    }
    rec.props = mprops;
  }

  provider->SaveUser(rec);
  rec = provider->LoadUser(user);
  // if (rec.userid.empty()) {
  //   sendError(res, "conflict", "unable to create a user", SimpleWeb::StatusCode::client_error_conflict);
  //   return;
  // }

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  res->write(SimpleWeb::StatusCode::success_ok, rec.toJSON(), header);
}

void UserHandler::GetUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto path_match = req->Match();
  string userid = path_match[1].str();

  if (userid.empty()) {
    sendError(res, "bad_request", "userid is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  UserRecord rec = provider->LoadUser(userid, ProfileKey::userid);

  if (rec.userid.empty()) {
    sendError(res, "not_found", "user not found", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  res->write(SimpleWeb::StatusCode::success_ok, rec.toJSON(), header);
}

void UserHandler::SaveUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto path_match = req->Match();
  string userid = path_match[1].str();
  if (userid.empty()) {
    sendError(res, "bad_request", "userid is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};

  UserRecord rec;
  rec = provider->LoadUser(userid, ProfileKey::userid);
  if (rec.userid.empty()) {
    sendError(res, "not_found", "user not found", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  ptree &pt = req->body.value();
  string user = rec.username;
  string email = pt.get<string>("email", "");
  string password = pt.get<string>("password", "");
  string pin = pt.get<string>("pin", "");

  rec = {0, email, user, password, pin, 0, 0};

  boost::optional<ptree &> child_roles = pt.get_child_optional("roles");
  if (!!child_roles) {
    vector<string> vroles;
    for (auto &v : child_roles.get()) {
      vroles.push_back(v.second.data());
    }
    rec.roles = vroles;
  }

  boost::optional<ptree &> child_props = pt.get_child_optional("properties");
  if (!!child_props) {
    map<string, string> mprops;
    for (auto &v : child_props.get()) {
      mprops.insert({v.first, v.second.data()});
    }
    rec.props = mprops;
  }

  rec.userid = userid;
  provider->SaveUser(rec);

  rec = provider->LoadUser(userid, ProfileKey::userid);
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  res->write(SimpleWeb::StatusCode::success_ok, rec.toJSON(), header);
}

void UserHandler::DeleteUserV1(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto path_match = req->Match();
  string userid = path_match[1].str();
  if (userid.empty()) {
    sendError(res, "bad_request", "userid is missing");
    return;
  }

  auto provider = context.GetProfileProvider();
  std::lock_guard<std::recursive_mutex> lock{context.mtx};

  UserRecord rec;
  rec = provider->LoadUser(userid, ProfileKey::userid);
  if (rec.userid.empty()) {
    sendError(res, "not_found", "user not found", SimpleWeb::StatusCode::client_error_not_found);
    return;
  }

  provider->DeleteUser(rec.username);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  res->write(SimpleWeb::StatusCode::success_ok, "{}", header);
}
