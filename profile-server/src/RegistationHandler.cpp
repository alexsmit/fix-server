/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>

#include "RegistationHandler.h"
#include <vx/web/Util.h>

RegistrationHandler::RegistrationHandler(ProfileServerContext& context, WebServer& server, Instance& secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  secure_server

      .use("^/register$", Request::RequestType::POST,
           std::bind(&RegistrationHandler::Register, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/register/validate$", Request::RequestType::POST,
           std::bind(&RegistrationHandler::Validate, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/register/confirm$", Request::RequestType::GET,
           std::bind(&RegistrationHandler::EmailConfirm, *this, std::placeholders::_1, std::placeholders::_2))

      .use("^/register/clean$", Request::RequestType::POST,
           std::bind(&RegistrationHandler::Clean, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}


RegistrationHandler::~RegistrationHandler() {}

void RegistrationHandler::Clean(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  string stime = req->body["ttl"];

  auto profile_provider = context.GetProfileProvider();

  if (stime.empty()) {
    // cout << "Cleanup registration with context TTL = " << context.reg_ttl << '\n';
    profile_provider->CleanRegistrations(context.reg_ttl);
  }
  else {
    unsigned long tim = boost::lexical_cast<unsigned long>(stime);
    profile_provider->CleanRegistrations(tim);
  }

  res->write(SimpleWeb::StatusCode::success_ok, "");
}

void RegistrationHandler::Register(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  UserRecord rec = {0};
  rec.username = req->body["username"];
  rec.password = req->body["password"];
  rec.email = req->body["email"];
  rec.pin = req->body["pin"];

  auto profile_provider = context.GetProfileProvider();

  RegistrationResult reg;
  try {
    reg = profile_provider->RegisterUser(rec);
    ptree pt_code;
    pt_code.put("registration_code", reg.registration_code);
    pt_code.put("reset_token", reg.reset_token);
    sendJSON(res, pt_code);
  }
  catch (const std::exception& e) {
    sendError(res, "invalid_registration", e.what());
  }
}

void RegistrationHandler::Validate(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;

  auto token = req->body["registration_code"];
  if (token.empty()) {
    sendError(res, "invalid_registration", "token is missing or invalid");
    return;
  }

  auto profile_provider = context.GetProfileProvider();
  auto result = profile_provider->VerifyPendingRegistration(token);

  ptree pt_result;
  if (result == RegistrationStatus::completed) {
    pt_result.put("status", "completed");
  }
  else if (result == RegistrationStatus::pending) {
    pt_result.put("status", "pending");
  }
  else {
    sendError(res, "invalid_registration", "registration does not exist");
    return;
  }

  sendJSON(res, pt_result);
}

void RegistrationHandler::EmailConfirm(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto token = req->params["code"];
  if (token.empty()) {
    sendError(res, "invalid_registration", "registration code is invalid");
    return;
  }

  auto profile_provider = context.GetProfileProvider();
  auto result = profile_provider->ConfirmRegistration(token);
  if (result) {
    res->write(SimpleWeb::StatusCode::success_ok, "{}");
  }
  else {
    sendError(res, "invalid_registration", "registration code is not valid");
  }
}
