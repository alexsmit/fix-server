/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include "ProfileServerContext.h"
#include "ProfileProviderRemote.h"

#include <boost/property_tree/json_parser.hpp>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#endif
#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#endif

#include <vx/ServiceFactory.h>

using namespace vx::sql;
using namespace boost::property_tree;
using namespace vx::providers;

ProfileServerContext::ProfileServerContext(unsigned long reg_ttl)
    : ServerContext(), reg_ttl(reg_ttl) {}

ProfileServerContext::ProfileServerContext(unsigned long reg_ttl, const std::string& backend)
    : ServerContext(), reg_ttl(reg_ttl), backend(backend) {
}

void ProfileServerContext::Init(const char* prefix, const string& configFileName) {
  ServerContext::Init(prefix, configFileName);
  auto slog = (*section)["loglevel"].str();

  try {
    if (!slog.empty())
      debugLevel = boost::lexical_cast<int>(slog);
  }
  catch (const std::exception& e) {
    Logger(LogLevel::error, string("Invalid loglevel in config: ") + e.what());
  }
}

// void ProfileServerContext::AddProvider(shared_ptr<vx::ServiceFactory> provider) {
//   providers.insert({provider->id(), provider});
// }

shared_ptr<ProfileProvider> ProfileServerContext::GetProfileProvider() {
  auto provider = std::dynamic_pointer_cast<ProfileProvider>(ServerContext::getService("profile", backend, provider_ttl));
  if (!!provider) return provider;
  throw invalid_argument("Invalid profile service type returned from the factory");

  // auto it = providers.find("profile");
  // if (it == providers.end())
  //   throw invalid_argument(
  //       "Invalid profile service type [profile_provider.type]=mysql|remote");

  // auto svc = it->second->getService(backend, provider_ttl);
  // shared_ptr<ProfileProvider> provider = std::dynamic_pointer_cast<ProfileProvider>(svc);
  // if (!!provider) return provider;

  // throw invalid_argument("Invalid profile service type returned from the factory");
}

// shared_ptr<Service> ProfileServerContext::getService(const string& name, const string& type, time_t ttl) {
//   if (name == "profile") return GetProfileProvider();
//   throw std::invalid_argument("invalid provider");
// }
