/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>
#include "PropertyHandler.h"
#include <vx/web/Util.h>
#include <vx/PasswordUtil.h>
#include <boost/property_tree/json_parser.hpp>

PropertyHandler::PropertyHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  secure_server
      .use("^/users/([\\w \\d]+)/properties$", Request::RequestType::GET,
           std::bind(&PropertyHandler::Get, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/users/([\\w \\d]+)/properties/([\\w \\d]+)$", Request::RequestType::GET,
           std::bind(&PropertyHandler::GetOne, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/users/([\\w \\d]+)/properties$", Request::RequestType::POST,
           std::bind(&PropertyHandler::Save, this, std::placeholders::_1, std::placeholders::_2))

      .use("^/users/([\\w \\d]+)/blob/([\\w\\d]+)$", Request::RequestType::POST,
           std::bind(&PropertyHandler::SaveBlob, this, std::placeholders::_1, std::placeholders::_2))
      .use("^/users/([\\w \\d]+)/blob/([\\w\\d]+)$", Request::RequestType::GET,
           std::bind(&PropertyHandler::GetBlob, this, std::placeholders::_1, std::placeholders::_2))

      ;
}

PropertyHandler::~PropertyHandler() {}

void PropertyHandler::Get(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string user = path_match[1].str();

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  map<string, string> props = provider->GetProperties(user.c_str());

  ptree pt, pt_root;
  vector2ptree(props, pt);
  pt_root.add_child("properties", pt);
  sendJSON(response, pt_root);
}

void PropertyHandler::GetOne(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string user = path_match[1].str();
  string prop = path_match[2].str();

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  string val = provider->GetProperty(user.c_str(), prop.c_str());

  ptree pt;
  pt.put(prop, val);
  sendJSON(response, pt);
}

void PropertyHandler::Save(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  auto path_match = request->Match();
  string user = path_match[1].str();

  ptree &pt = request->body.value();

  auto provider = context.GetProfileProvider();

  std::lock_guard<std::recursive_mutex> lock{context.mtx};
  for (auto &p : pt) {
    provider->SetProperty(user.c_str(), p.first.c_str(),
                          p.second.data().c_str());
  }
  response->write(SimpleWeb::StatusCode::success_ok, "");
}

void PropertyHandler::SaveBlob(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;
  /*
    1) json format
    Header: X-Client-Id, X-Client-Secret
    or
    Authorization: SSWS {{api token}}
    {
      content_type: "proper content type|empty",
      encoding_type: "base64|empty",
      blob: "some data"
    }
    2) form data application/x-www-form-urlencoded or multipart/form-data,
    form should have an element "blob". For url-encoded a content-type could be also specified
    i.e:
    blob=some%20blob%20data&content-type=text%2Fplain&client_id=id&client_secret=secret
  */
  auto path_match = req->Match();
  string user = path_match[1].str();
  string prop = path_match[2].str();

  string data;
  string content_type;
  string encoding_type;
  if (!!req->body.form()) {
    auto elem = req->body.form()->operator[]("blob");
    data = elem.data();
    content_type = elem.content_type();
  }
  else {
    data = req->body["blob"];
    content_type = req->body["content_type"];
    encoding_type = req->body["encoding_type"];
  }

  if (!encoding_type.empty()) {
    if (boost::iequals(encoding_type, "base64")) {
      data = random_util().base64decode(data);
    }
    else {
      sendError(res, "bad_request", "invalid encoding_type");
      return;
    }
  }

  // cout << ">>>> user: " << user << endl;
  // cout << ">>>> prop: " << prop << endl;
  // cout << ">>>> data size: " << data.length() << endl;
  // cout << ">>>> type: " << content_type << endl;

  auto provider = context.GetProfileProvider();
  auto ok = provider->SetBlob(user, prop, content_type, data);
  // cout << "Blob size: " << data.length() << '\n';
  if (ok)
    res->write(SimpleWeb::StatusCode::success_ok, "{}");
  else
    sendError(res, "property_error", "Unable to save blob data");
}

void PropertyHandler::GetBlob(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto path_match = req->Match();
  string user = path_match[1].str();
  string prop = path_match[2].str();

  auto provider = context.GetProfileProvider();
  auto blob = provider->GetBlob(user, prop);
  if (!!blob) {
    SimpleWeb::CaseInsensitiveMultimap header;
    header.insert({"Content-Type", blob->content_type});
    header.insert({"Content-Disposition", "inline"});
    stringstream si(blob->data);
    res->write(si, header);
  }
  else {
    sendError(res, "property_error", "property does not exist", SimpleWeb::StatusCode::client_error_not_found);
  }
}
