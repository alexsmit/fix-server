/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/variant.hpp>
#include <boost/algorithm/string.hpp>
#include "InfoHandler.h"
#include <vx/web/Util.h>
#include <vx/inja/vx_environment.h>
#include <vx/inja/vx_inja.h>
#include <vx/inja/vx_inja_page.h>

#include <vx/sql/DataProvider.h>
#include <vx/sql/ProfileProvider.h>

#ifndef PROJECT_GIT_TAG
#define PROJECT_GIT_TAG "DEBUG"
#endif

using namespace vx::inja;
using namespace vx::sql;

string get_used_mem() {
  string m;
  auto pid = ::getpid();
  auto file = (boost::format("/proc/%d/status") % pid).str();
  std::ifstream ifs(file);
  string line;
  while (std::getline(ifs, line)) {
    if (boost::algorithm::starts_with(line, "VmRSS")) {
      auto ix = line.find(' ');
      if (ix != string::npos) {
        auto ixm = line.find_first_not_of(' ', ix);
        m = line.substr(ixm);
        break;
      }
    }
  }
  ifs.close();
  return m;
}

InfoHandler::InfoHandler(ProfileServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
  server
      .use("^/info$", Request::RequestType::GET,
           std::bind(&InfoHandler::Info, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/version$", Request::RequestType::GET,
           std::bind(&InfoHandler::Version, *this, std::placeholders::_1, std::placeholders::_2))

      .use("^/api/v1/info$", Request::RequestType::GET,
           std::bind(&InfoHandler::ApiInfo, *this, std::placeholders::_1, std::placeholders::_2))

      .use("^/api/v1/param$", Request::RequestType::GET,
           std::bind(&InfoHandler::GetParam, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/api/v1/param$", Request::RequestType::POST,
           std::bind(&InfoHandler::SetParam, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

InfoHandler::~InfoHandler() {}

void InfoHandler::Info(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.emplace("Content-type", "text/html");

  vx::inja::VxPage page("");
  page
      .Title("VxPRO | Server Info")
      .PageTitle("VxPRO Profile server")
      // .Message("Registration",
      //          "<div class=\"alert alert-success\" role=\"alert\">Registration is successful.</div>"
      //          "<p>You can login now to the Application on your phone.</p>",
      //          {}, false, true)
      .CSS({"/css/all.css", "/css/style.css"});

  ;

  VxEnvironment env("./templates/");
  json data;
  data["version"] = PROJECT_GIT_TAG;
  data["threads"] = context.pool_size;
  data["httpc"] = context.getParam("port");
  data["httpsc"] = context.getParam("port_https");
  data["httpr"] = context.port;
  data["httpsr"] = context.ports;
  data["alloc"] = get_used_mem();

  string cont = env.render_file("info", data, "html");

  string result = page.render(false, cont);
  res->write(result, header);
}

void InfoHandler::Version(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  ptree pt;
  pt.put("version", PROJECT_GIT_TAG);
  pt.put("service", "profile-server");
  sendJSON(res, pt, SimpleWeb::StatusCode::success_ok);
}

void InfoHandler::ApiInfo(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto provider = context.GetProfileProvider();
  auto data_provider = std::dynamic_pointer_cast<DataProvider>(provider);
  int version = 0;
  if (!!data_provider) {
    version = data_provider->Version();
    // LCOV_EXCL_START
    if (version == 0) {
      data_provider->Verify();
      version = data_provider->Version();
    }
    // LCOV_EXCL_STOP
  }

  ptree pt;
  pt.put("database_version", version);
  sendJSON(res, pt, SimpleWeb::StatusCode::success_ok);
}

void InfoHandler::GetParam(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto provider = context.GetProfileProvider();
  auto data_provider = std::dynamic_pointer_cast<DataProvider>(provider);
  string key, val;
  if (!!data_provider) {
    key = req->params["name"];
    if (!key.empty())
      val = data_provider->GetParam(key);
  }

  ptree pt;
  if (!key.empty())
    pt.put(key, val);

  sendJSON(res, pt, SimpleWeb::StatusCode::success_ok);
}

void InfoHandler::SetParam(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  auto provider = context.GetProfileProvider();
  auto data_provider = std::dynamic_pointer_cast<DataProvider>(provider);
  if (!!data_provider) {
    for (auto &kv : req->body) {
      data_provider->SetParam(kv.first, kv.second);
    }
  }

  res->write(SimpleWeb::StatusCode::success_ok, "");
}
