/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "stdafx.h"

#define THREADS 50
#define ITERATIONS 6
// #define LOGGING
// #define INDICATOR

#define CLIENT_ID "aaa"
#define CLIENT_SECRET "111"
#define STRESS_USERNAME "zz"
#define STRESS_PASSWORD "123"
#define STRESS_HOST "localhost"
#define STRESS_PORT 8081
#define POST_URL_USER "/users/%s"

#include <time.h>
#include <thread>

// Added for the default_resource example
#include <algorithm>
#ifdef HAVE_OPENSSL
#include "include/crypto.hpp"
#endif

#include <boost/process.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>

#include <vx/PtreeUtil.h>
#include <vx/sql/DataFactory.h>
#include <DataProviderMySQL.h>
#include <UserRecord.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

using namespace std;
// Added for the json-example:
using namespace boost::property_tree;
using namespace boost::program_options;
namespace bp = boost::process;
namespace bo = boost::program_options;

static SimpleWeb::CaseInsensitiveMultimap header;

/// stress-test specific profile provider implementation
class ProfileDatabaseManagement : public DataProviderMySQL {
public:
  ProfileDatabaseManagement() {}
  ~ProfileDatabaseManagement() {}
  /// returns a configuration section name for profile provider
  string Name() override { return "profile.mysql"; }

  /// run cleanup operation
  void Cleanup() {
    auto con = GetConnection();
    auto res = con->execute("delete from users where email like 'xxx%@vxpro.com';");
    cout << "Deleted " << res << " user records" << endl;
  }
};

static time_t error_time = 0;

bool call_json_one_user(HttpClient &client, int idx, int iteration) {
  // this_thread::sleep_for(chrono::milliseconds(100));
#ifdef LOGGING
  cout << "+++ thread: " << idx << " start" << endl;
#endif
  int nAttempts = 1;
  bool bCont = false;
  try {
#ifdef LOGGING
    cout << "+++ thread: " << idx << " sending json: " << json << endl;
#endif

    do {
      bCont = false;
      error_code ec;

      stringstream su;
      su << boost::format("xxx%03d%03d") % idx % iteration;
      string suser = su.str();

      stringstream s;
      s << boost::format(POST_URL_USER) % suser;
      string surl = s.str();

      UserRecord rec = {0, suser + "@vxpro.com", suser, "123", "1234"};
      rec.roles = {"admin", "user"};
      rec.props = {{"prop1", "val1"}, {"prop2", "val2"}};

      string json = rec.toJSON();

      // cout << "trying to send json: " << json << "URL: " << surl << endl;
      client.request("POST", surl, json, header,
                     [idx, &ec, nAttempts, &bCont](
                         shared_ptr<HttpClient::Response> response,
                         const SimpleWeb::error_code &ec_) {
                       if (ec_) {
                         ec = ec_;
                       }
                       else {
                         if (SimpleWeb::status_code(response->status_code) !=
                             SimpleWeb::StatusCode::success_ok) {
                           cout << "HTTP error: " << response->status_code
                                << endl;
                           bCont = false;
                           return;
                         }
                       }
                       // cout << "+++ thread: " << idx << " " <<
                       // response->content.rdbuf()
                       // << endl;
                     });
      client.io_service->run();

      if (ec) {
        cerr << "+++ thread: " << idx << " FATAL ERROR: " << ec.value() << ""
             << ec.message() << endl;

        if (nAttempts++ <= 10) {
          if (error_time == 0) {
            error_time = ::time(0);
          }
          bCont = true;
          this_thread::sleep_for(chrono::milliseconds(6500 * nAttempts));
        }
        else {
          cerr << "+++ thread: " << idx << " FATAL ERROR: " << ec.value() << " "
               << ec.message() << endl;
          return false;
        }
      }
      cout << '.' << std::flush;

    } while (bCont);

#ifdef LOGGING
    cout << "--- thread: " << idx << " end" << endl;
#endif
    return true;
  }
  catch (std::exception &ex) {
    cerr << "Client exception: " << ex.what() << endl;
    return false;
  }
  catch (...) {
    cerr << "Client failed" << endl;
    return false;
  }
}

void call_json_user(string host, int idx, int iterations = ITERATIONS) {
  for (int i = 0; i < iterations; i++) {
    HttpClient client(host);
    if (!call_json_one_user(client, idx, i)) {
      cout << endl
           << "ERROR in client ID: " << idx << endl;
      break;
    }
#ifdef INDICATOR
    else {
      cout << ".(" << hex << idx << ")" << flush;
    }
#endif
  }
}

void stress_test(void fun(string, int, int), string host,
                 int threads_count = THREADS, int iterations = ITERATIONS) {
  // Client stress test
  time_t start = ::time(NULL);
  auto count = threads_count;
  thread threads[count];
  for (int i = 0; i < count; i++) {
    threads[i] = thread(fun, host, i, iterations);
    //    threads[i].detach();
  }

  cout << "All threads started" << endl;

  try {
    for (int i = 0; i < count; i++) {
      threads[i].join();
    }
  }
  catch (std::exception &ex) {
    cerr << "EXCEPTION in MAIN: " << ex.what() << endl;
  }

  time_t curtime = ::time(NULL);
  time_t total = curtime - start;
  cout << endl;
  cout << "Threads : " << threads_count << endl;
  cout << "Cycles  : " << iterations << endl;
  cout << "Total requests: " << (threads_count * iterations) << endl;
  cout << "Done in " << total << " seconds." << endl;

  if (error_time != 0)
    cout << "Time to first error: " << (curtime - error_time) << endl;
  else
    cout << "No connection errors" << endl;

  if (total != 0) {
    auto req_per_seq = (threads_count * iterations) / total;
    cout << "Requests per second: " << req_per_seq << endl;
  }
  else {
    cout << "All requests are done within a second." << endl;
  }
}

void setup() {
  DataFactory factory;
  auto data = factory.Connect<ProfileDatabaseManagement>();
  data->Cleanup();
}

int main(int ac, char **av) {
  options_description settings{"Options"};

  // clang-format off
  settings.add_options()
    ("help", "profile-server-stress utility. Will create a number of users by submitting requests to a profile server.")
    ("port,p", bo::value<int>()->default_value(STRESS_PORT), "Port")
    ("host,h", bo::value<string>()->default_value(STRESS_HOST), "Host")
    ("threads,t", bo::value<int>()->default_value(THREADS), "Threads")
    ("iterations,i", bo::value<int>()->default_value(ITERATIONS), "Number of iterations")
    // ("mode,m", bo::value<string>()->default_value("token"),"Mode: token - get access tokens | user - create users.")
    ;
  // clang-format on
  command_line_parser parser{ac, av};
  parser.options(settings).allow_unregistered().style(
      command_line_style::default_style |
      command_line_style::allow_slash_for_short);
  parsed_options parsed_options = parser.run();

  variables_map vars;
  try {
    store(parsed_options, vars);
  }
  catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    exit(1);
  }
  notify(vars);

  if (vars.count("help")) {
    cout << settings << endl;
    return 0;
  }

  setup();

  // prepare header and token request
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  stringstream s;
  int iport = vars["port"].as<int>();

  pid_t pid_server = 0;
  auto hostname = vars["host"].as<string>();
  if (hostname == "localhost") {
    iport = 9998;
    cout << "Starting local profile server" << endl;
    bp::child server("./profile-server", "9998", "-1", "3");
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    pid_server = server.id();
    server.detach();
  }

  int iter = vars["iterations"].as<int>();
  int threads = vars["threads"].as<int>();

  s << boost::format("%s:%d") % vars["host"].as<string>() % iport;
  string shost = s.str();
  cout << "Using host: " << shost << " for stress testing." << endl;
  cout << "Will create " << iter * threads << " user accounts in " << threads << " threads" << endl;

  void (*fun)(string, int, int);
  fun = call_json_user;

  stress_test(*fun, shost, threads, iter);

  if (pid_server != 0) {
    bp::child server(pid_server);
    server.terminate();
  }
}
