#pragma once

#include <iostream>
#include <fstream>
#include <list>
#include <map>
#include <memory>
#include <algorithm>
#include <vector>
#include <chrono>
#include <boost/variant.hpp>

/// @brief debug environment settings for profile server
struct DebugEnvironment {
  std::string DEBUG_HOST;                  //!< calculated current host for profile server
  long DEFAULT_TTL_REGISTRATION;           //!< TTL for registration, should be reduced to run a test
  static DebugEnvironment env;             //!< static instance to debug environment, should be initialized in the main test program
  void init(const std::string& ssl_port);  //!< init the environment, should be implemented in the main program
};

#define DEBUG_HOST DebugEnvironment::env.DEBUG_HOST
#define DEFAULT_TTL_REGISTRATION DebugEnvironment::env.DEFAULT_TTL_REGISTRATION
// #define DEBUG_HOST "localhost:9997"
// #define DEFAULT_TTL_REGISTRATION 86400L

#define PROFILE_SERVER 1
