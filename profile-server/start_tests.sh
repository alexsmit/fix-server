#!/bin/bash

. ./start_stress_common.sh

pushd build > /dev/null

./profile-server-test -

stop_server localhost 9997

popd >/dev/null
