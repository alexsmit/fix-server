#!/bin/bash

killall -9 profile-server

pushd build > /dev/null

valgrind ./profile-server 9997 0 5 120

popd >/dev/null
