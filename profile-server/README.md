# Profile server
Server to maintain user profiles.
Set of operations
- Create users
- Support secondary authentication method - "pin"
- Register users
- Validate password and pin
# Start/stop a server
## Using systemd
systemctl start profile-server
systemctl stop profile-server
Default ports:
http: 8081
https: 8481

## From the command line
profile-server http-port https-port [[registration_ttl] [shutdown_timeout]]
http-port should be defined or 0 to disable
https-port should be defined or 0 to be disabled
registration_ttl - maximum time allowes to proceed with the registration
shutdown_timeout - server will shut down after specified number of seconds. Use this for debugging only.

# Signals
Server can be terminated with graceful shutdown using those signals:
SIGHUP   - 1 Hangup detected on controlling terminal or death of controlling process
SIGINT   - 2 Interrupt from keyboard (Ctrl+C)
SIGQUIT  - 3 Quit from keyboard
SIGABRT  - 6 Abort signal from abort(3)
SIGTERM  - 15 Termination request
SIGTSTP  - 20 Stop typed at terminal (Ctrl+Z)

When running as a service it is always termintated using SIGHUP
