#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;
namespace vx {
  namespace openid {
    namespace test {

      /// testing /roles/ endpoints
      class RolesTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(RolesTest);

        CPPUNIT_TEST(testBackendId);
        
        CPPUNIT_TEST(testBadRequest);
        CPPUNIT_TEST(testGetRole);
        CPPUNIT_TEST(testGetRoles);
        CPPUNIT_TEST(testDeleteRole);
        CPPUNIT_TEST(testPostRole);
        CPPUNIT_TEST(testPostRoleBad);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<ClientHelper> client;
        SimpleWeb::CaseInsensitiveMultimap header;

        string roles_request, roles_wrong_request, roles_bad_request;

      public:
        RolesTest();
        ~RolesTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();  //!< verify we're using proper backend

        void testBadRequest();
        void testGetRole();
        void testGetRoles();
        void testDeleteRole();
        void testPostRole();
        void testPostRoleBad();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
