
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>

#include <BaseTest.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "GetUserTest.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

GetUserTest::GetUserTest() {}

GetUserTest::~GetUserTest() {}

void GetUserTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  provider->CreateUser(USER_NAME, USER_EMAIL, "password");
  provider->AddRole(ROLE_1, "test1 description");
  provider->AddRole(ROLE_2, "test2 description");
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});
}

void GetUserTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider->DeleteRole(ROLE_1);
  provider->DeleteRole(ROLE_2);
  provider.reset();
  factory.reset();
}

void GetUserTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

/// GET /user/{username}
void GetUserTest::testGetNoHeader() {
  auto& result = client->request("GET", string("/users/") + USER_NAME, "");
  string code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);
}

/// validate received data
void GetUserTest::verifyGet(ClientHelper& result) {
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should get successfule code", string("200"), code);

  ptree pt;
  stringstream si(result.data);
  read_json(si, pt);

  // email, username, logins, last_login, roles, userid, updated_at
  CPPUNIT_ASSERT_EQUAL_MESSAGE("JSON elements", (size_t)7, pt.size());

  auto val = pt.get<string>("username", "");
  CPPUNIT_ASSERT_MESSAGE("User name", val == USER_NAME);

  val = pt.get<string>("email");
  CPPUNIT_ASSERT_MESSAGE("Email", val == USER_EMAIL);

  // id is never returned from API call
  auto ival = pt.get<int>("id", 9999);
  CPPUNIT_ASSERT_MESSAGE("id", ival == 9999);

  ival = pt.get<int>("updated_at", 9999);
  CPPUNIT_ASSERT_MESSAGE("updated_at", ival != 9999);

  auto uid = pt.get<string>("userid", "");
  CPPUNIT_ASSERT_MESSAGE("userid", !uid.empty());

  auto pt_roles = pt.get_child("roles");
  int cnt1 = 0, cnt2 = 0;
  for (auto& v : pt_roles) {
    if (v.second.data() == ROLE_1) cnt1++;
    if (v.second.data() == ROLE_2) cnt2++;
  }
  CPPUNIT_ASSERT_MESSAGE("Role role1", cnt1 == 1);
  CPPUNIT_ASSERT_MESSAGE("Role role2", cnt2 == 1);
}


/// GET /user/{username}
void GetUserTest::testGet() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  /*
      expecting result (quotes redacted):
      {
          username: test,
          email: e@mail.com,
          logins: "123", // count of logins
          last_login: "1123434343", // time_t
          roles: [
              test1
              test2
          ],
          userid: "abc-def-xyz", // uuid
          updated_at: "12343434343" // time_t
      }
  */

  auto result = client->request("GET", string("/users/") + USER_NAME, "", header);
  verifyGet(result);
}

/// retrieve a user by name and then get a user by uuid
void GetUserTest::testGetUsingUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  auto& result = client->request("GET", string("/users/") + USER_NAME, "", header);
  ptree pt;
  stringstream s(result.data);
  read_json(s, pt);

  auto userid = pt.get("userid", "");
  CPPUNIT_ASSERT_MESSAGE("should have userid", !userid.empty());

  string url = "/userid/" + userid;
  // cout << ">>>>>>> url:" << url << endl;
  result = client->request("GET", url.c_str(), "", header);
  verifyGet(result);
}

/// try to get a user using wrong UUID
void GetUserTest::testGetUsingUUIDBad() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto& result = client->request("GET", "/userid/abc-123", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should get 404", code == "404");
}


//*******************************************************
//*******************************************************
//*******************************************************

CPPUNIT_TEST_SUITE_REGISTRATION(GetUserTest);
