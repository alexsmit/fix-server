#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// testing /password/ endpoints
      class ResetTokenTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ResetTokenTest);

        CPPUNIT_TEST(testBackendId);
        
        CPPUNIT_TEST(testBadRequest);
        CPPUNIT_TEST(testReset);
        CPPUNIT_TEST(testResetBad);
        CPPUNIT_TEST(testResetEmailNotExist);
        CPPUNIT_TEST(testResetLoginInvalidateToken);
        CPPUNIT_TEST(testUpdatePasswordWrongToken);
        CPPUNIT_TEST(testFailUpdateWrongToken);
        CPPUNIT_TEST(testValidateResetToken);
        CPPUNIT_TEST(testValidateResetTokenEmpty);
        CPPUNIT_TEST(testValidateResetTokenBad);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<ClientHelper> client;
        SimpleWeb::CaseInsensitiveMultimap header;

        string reset_request;
        string reset_request_bad;
        string reset_request_not_exists;
        string update_password_bad_request;

      public:
        ResetTokenTest();
        ~ResetTokenTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();  //!< verify we're using proper backend

        void testBadRequest();
        void testReset();
        void testResetBad();
        void testResetEmailNotExist();
        void testResetLoginInvalidateToken();
        void testUpdatePasswordWrongToken();
        void testFailUpdateWrongToken();
        void testValidateResetToken();
        void testValidateResetTokenEmpty();
        void testValidateResetTokenBad();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
