
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/regex.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "BaseTest.h"
#include "PostUserTestV1.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

/// fixture class to test /v1/users apis
PostUserTestV1Fix::PostUserTestV1Fix(std::shared_ptr<ClientHelper> client, const string& client_id, const string& client_secret)
    : client(client), client_id(client_id), client_secret(client_secret), code(0) {}

/// run method (GET/POST), for provided URL and payload
void PostUserTestV1Fix::Run(const string& method, const string& url, ptree pt) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", client_id});
  header.insert({"X-Client-Secret", client_secret});
  header.insert({"Content-Type", "application/json"});

  string content;
  if (method != "GET") {  // i.e. POST, DELETE
    stringstream s;
    write_json(s, pt);
    content = s.str();
  }

  auto result = client->request(method, url, content, header);
  auto scode = result.status_code.substr(0, 3);

  try {
    code = boost::lexical_cast<int>(scode);
    body = result.result;
  }
  catch (const std::exception& e) {
  }
}

/// run method (GET/POST), for provided URL and empty payload
void PostUserTestV1Fix::Run(const string& method, const string& url) {
  ptree pt;
  Run(method, url, pt);
}

//-----------------------------------------------

PostUserTestV1::PostUserTestV1() {}

PostUserTestV1::~PostUserTestV1() {}

// POST /user/{username}
void PostUserTestV1::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);
  provider->DeleteUser(USER_NAME);
  provider->DeleteUser(USER_NAME2);
  provider->DeleteUser(USER_NAME_SPACE);
  provider->DeleteRole(ROLE_TEST);
  string username = string(" ") + string(USER_NAME) + string(" ");
  provider->DeleteUser(username.c_str());
}

void PostUserTestV1::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider->DeleteUser(USER_NAME2);
  provider->DeleteUser(USER_NAME_SPACE);
  provider->DeleteRole(ROLE_TEST);
  provider.reset();
  factory.reset();
}

void PostUserTestV1::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

//////////////////////
/// Find user
//////////////////////

void PostUserTestV1::testFindUserBadAuth() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client, "wrong_client", "wrong_secret");
  fix.Run("GET", string("/api/v1/users?username=") + SimpleWeb::Percent::encode(USER_NAME));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)401, fix.code);
}

void PostUserTestV1::testFindUserBadQuery() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?username="));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("email or username parameter is missing"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testFindUserNotFound() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?username=unknown_user"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)404, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("user not found"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testFindUser() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?username=") + SimpleWeb::Percent::encode(USER_NAME));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, fix.body.size());

  for (auto& c : fix.body) {
    CPPUNIT_ASSERT_MESSAGE("userid", !c.second.get("userid", "").empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), c.second.get("username", "not loaded"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), c.second.get("email", "not loaded"));
    break;
  }
}

void PostUserTestV1::testFindUserSpace() {
  provider->CreateUser(USER_NAME_SPACE, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?username=") + SimpleWeb::Percent::encode(USER_NAME_SPACE));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, fix.body.size());

  for (auto& c : fix.body) {
    CPPUNIT_ASSERT_MESSAGE("userid", !c.second.get("userid", "").empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME_SPACE), c.second.get("username", "not loaded"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), c.second.get("email", "not loaded"));
    break;
  }
}

void PostUserTestV1::testFindUserEmail() {
  provider->CreateUser(USER_NAME_SPACE, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?email=") + SimpleWeb::Percent::encode(USER_EMAIL));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, fix.body.size());
  for (auto& c : fix.body) {
    CPPUNIT_ASSERT_MESSAGE("userid", !c.second.get("userid", "").empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME_SPACE), c.second.get("username", "not loaded"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), c.second.get("email", "not loaded"));
    break;
  }
}

void PostUserTestV1::testFindUserTrimSpace() {
  string username = string(" ") + string(USER_NAME) + string(" ");

  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?username=") + SimpleWeb::Percent::encode(username));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, fix.body.size());
  for (auto& c : fix.body) {
    CPPUNIT_ASSERT_MESSAGE("userid", !c.second.get("userid", "").empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), c.second.get("username", "not loaded"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), c.second.get("email", "not loaded"));
    break;
  }
}

void PostUserTestV1::testFindUserEmailTrimSpace() {
  string email = string(" ") + string(USER_EMAIL) + string(" ");

  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users?email=") + SimpleWeb::Percent::encode(email));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, fix.body.size());
  for (auto& c : fix.body) {
    CPPUNIT_ASSERT_MESSAGE("userid", !c.second.get("userid", "").empty());
    CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), c.second.get("username", "not loaded"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), c.second.get("email", "not loaded"));
    break;
  }
}

//////////////////////
/// Create user
//////////////////////

void PostUserTestV1::testCreateUserBadAuth() {
  ptree pt;
  pt.put("username", USER_NAME);
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  PostUserTestV1Fix fix(client, "wrong_client", "wrong_secret");
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)401, fix.code);
}

void PostUserTestV1::testCreateUserDuplicate() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  ptree pt;
  pt.put("username", USER_NAME);
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)409, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("conflict"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("unable to create a user"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testCreateUserDuplicateEmail() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  ptree pt;
  pt.put("username", USER_NAME2);
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)409, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("conflict"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("unable to create a user"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testCreateUserSpace() {
  ptree pt;
  pt.put("username", USER_NAME_SPACE);
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME_SPACE), fix.body.get("username", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), fix.body.get("email", "not loaded"));

  auto rec = provider->LoadUser(USER_NAME_SPACE);
  CPPUNIT_ASSERT_MESSAGE("should load a user", rec.id != 0);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME_SPACE), rec.username);
}

void PostUserTestV1::testCreateUserTrimSpace() {
  string username = string(" ") + string(USER_NAME) + string(" ");

  ptree pt;
  pt.put("username", username.c_str());
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), fix.body.get("username", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), fix.body.get("email", "not loaded"));

  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should load a user", rec.id != 0);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), rec.username);
}

void PostUserTestV1::testCreateUser() {
  ptree pt;
  pt.put("username", USER_NAME);
  pt.put("email", USER_EMAIL);
  pt.put("password", USER_PASSWORD);
  pt.put("pin", USER_PIN);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_MESSAGE("userid", !fix.body.get("userid", "").empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), fix.body.get("username", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), fix.body.get("email", "not loaded"));
  CPPUNIT_ASSERT_MESSAGE("should authenticate", provider->Authenticate(USER_NAME, USER_PASSWORD));
  CPPUNIT_ASSERT_MESSAGE("should authenticate with pin", provider->AuthenticatePin(USER_NAME, USER_PIN));
}

//////////////////////
/// Get user
//////////////////////

void PostUserTestV1::testGetUserBadAuth() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client, "wrong_client", "wrong_secret");
  fix.Run("GET", string("/api/v1/users/") + rec.userid);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)401, fix.code);
}

void PostUserTestV1::testGetUserBadRequest() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users/"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("userid is missing"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testGetUserNotFound() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users/xxxx"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)404, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("user not found"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testGetUser() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("GET", string("/api/v1/users/") + SimpleWeb::Percent::encode(rec.userid));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("userid", rec.userid, fix.body.get("userid", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", rec.username, fix.body.get("username", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", rec.email, fix.body.get("email", "not loaded"));
}

//////////////////////
/// Save user
//////////////////////

void PostUserTestV1::testSaveUserBadAuth() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("password", USER_PASSWORD2);
  PostUserTestV1Fix fix(client, "wrong_client", "wrong_secret");
  fix.Run("POST", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)401, fix.code);
}

void PostUserTestV1::testSaveUserNotFound() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("password", USER_PASSWORD2);
  pt.put("pin", USER_PIN2);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users/xxx"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)404, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("user not found"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testSaveUserBadUUID() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("password", USER_PASSWORD2);
  pt.put("pin", USER_PIN2);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users/"), pt);  // empty userid
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("userid is missing"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testSaveUser() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("password", USER_PASSWORD2);
  pt.put("pin", USER_PIN2);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("userid", rec.userid, fix.body.get("userid", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", rec.username, fix.body.get("username", "not loaded"));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", rec.email, fix.body.get("email", "not loaded"));
  CPPUNIT_ASSERT_MESSAGE("should authenticate", provider->Authenticate(USER_NAME, USER_PASSWORD2));
  CPPUNIT_ASSERT_MESSAGE("should authenticate with pin", provider->AuthenticatePin(USER_NAME, USER_PIN2));
}

//////////////////////
/// Delete user
//////////////////////

void PostUserTestV1::testDeleteUserBadAuth() {
  ptree pt;
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client, "wrong_client", "wrong_secret");
  fix.Run("DELETE", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)401, fix.code);
}

void PostUserTestV1::testDeleteUserNotFound() {
  ptree pt;
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("DELETE", string("/api/v1/users/xxxx"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)404, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("user not found"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testDeleteUserBadRequest() {
  ptree pt;
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("DELETE", string("/api/v1/users/"), pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)400, fix.code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), fix.body.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("userid is missing"), fix.body.get("error_description", ""));
}

void PostUserTestV1::testDeleteUser() {
  ptree pt;
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  PostUserTestV1Fix fix(client);
  fix.Run("DELETE", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("should delete a user", (unsigned int)0, rec.id);
}

//////////////////////
/// Password
//////////////////////

void PostUserTestV1::testSetPassword() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("password", USER_PASSWORD2);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_MESSAGE("should authenticate", provider->Authenticate(USER_NAME, USER_PASSWORD2));
  CPPUNIT_ASSERT_MESSAGE("should authenticate with pin", provider->AuthenticatePin(USER_NAME, USER_PIN));
}

//////////////////////
/// Pin
//////////////////////

void PostUserTestV1::testSetPin() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  provider->SetPin(USER_NAME, USER_PIN);
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id", !rec.userid.empty());
  ptree pt;
  pt.put("pin", USER_PIN2);
  PostUserTestV1Fix fix(client);
  fix.Run("POST", string("/api/v1/users/") + rec.userid, pt);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("code", (int)200, fix.code);
  CPPUNIT_ASSERT_MESSAGE("should authenticate", provider->Authenticate(USER_NAME, USER_PASSWORD));
  CPPUNIT_ASSERT_MESSAGE("should authenticate with pin", provider->AuthenticatePin(USER_NAME, USER_PIN2));
}

CPPUNIT_TEST_SUITE_REGISTRATION(PostUserTestV1);
