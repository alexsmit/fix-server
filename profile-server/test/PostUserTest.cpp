
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/regex.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include <BaseTest.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "PostUserTest.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

PostUserTest::PostUserTest() {}

PostUserTest::~PostUserTest() {}

// POST /user/{username}
void PostUserTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);
  provider->DeleteUser(USER_NAME);

  /* test payload
{
  "password": "some",
  "email": "some@daff.com",
  "pin": "1234",
  "roles": [
      ROLE_1,
      ROLE_2
  ],
  "properties": {
      "prop1": "value1",
      "prop1": "value1"
  }
}

*/

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  // prepare payload
  ptree pt, pt_roles, pt_props;
  pt.put<string>("password", "some");
  pt.put<string>("email", USER_EMAIL);
  pt.put<string>("pin", "1234");

  vector<string> roles = {ROLE_1, ROLE_2};
  pt.add_child("roles", vector2ptree(roles, pt_roles));

  map<string, string> props = {{"prop1", "value1"}, {"prop2", "value2"}};
  for (auto &el : props) {
    pt_props.put<string>(el.first, el.second);
  }
  pt.add_child("properties", pt_props);
  stringstream stream;
  write_json(stream, pt, true);

  //   cout << "POST /user/test: " << stream.str() << endl;

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});
  auto result = client->request("POST", string("/users/") + USER_NAME, stream.str(), header);
  post_result = result.status_code.substr(0, 3);
}

void PostUserTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider->DeleteRole(USER_NAME);
  provider->DeleteRole(ROLE_1);
  provider->DeleteRole(ROLE_2);
  provider.reset();
  factory.reset();
}

void PostUserTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

/// validate post result
void PostUserTest::testPost() {
  CPPUNIT_ASSERT_MESSAGE("Should be successful", post_result == "200");
}

/// post without client/secret
void PostUserTest::testPostNoHeader() {
  auto result = client->request("POST", string("/users/") + USER_NAME, "{}");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);
}

/// post with invalid client/secret
void PostUserTest::testPostBadClientSecret() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", "some_wrong_secret"});
  header.insert({"Content-Type", "application/json"});
  auto result = client->request("POST", string("/users/") + USER_NAME, "{}", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 401", string("401"), code);
}

/// validate data using provider api
void PostUserTest::testClientData() {
  // get data
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("should have id!=0", rec.id != 0);
  CPPUNIT_ASSERT_MESSAGE("should have email", rec.email == USER_EMAIL);
  CPPUNIT_ASSERT_MESSAGE("should have username", rec.username == USER_NAME);

  auto prop = provider->GetProperty(USER_NAME, "prop1");
  CPPUNIT_ASSERT_MESSAGE("should have prop1", prop == "value1");
  prop = provider->GetProperty(USER_NAME, "prop2");
  CPPUNIT_ASSERT_MESSAGE("should have prop2", prop == "value2");

  auto test_roles = provider->Roles(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE(
      "should have role1",
      find(test_roles.begin(), test_roles.end(), ROLE_1) != test_roles.end());
  CPPUNIT_ASSERT_MESSAGE(
      "should have role2",
      find(test_roles.begin(), test_roles.end(), ROLE_2) != test_roles.end());
}

/// test provider authenticate with PIN
void PostUserTest::testAuthPin() {
  auto bresult = provider->AuthenticatePin(USER_NAME, "1234");
  CPPUNIT_ASSERT_MESSAGE("Should validate pin", bresult);
}

/// test provider authenticate with password
void PostUserTest::testAuthPassword() {
  auto bresult = provider->Authenticate(USER_NAME, "some");
  CPPUNIT_ASSERT_MESSAGE("Should validate password", bresult);
}

/// update password via POST, test with authenticate
void PostUserTest::testSetPassword() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});
  ptree pt;
  pt.put("password", "some_new_password");
  stringstream s;
  write_json(s, pt);
  auto result = client->request("POST", string("/users/") + USER_NAME + "/password", s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto bresult = provider->Authenticate(USER_NAME, "some_new_password");
  CPPUNIT_ASSERT_MESSAGE("Should validate password", bresult);
}

/// set pin using POST, validate using authenticate with pin
void PostUserTest::testSetPin() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});
  ptree pt;
  pt.put("pin", "9999");
  stringstream s;
  write_json(s, pt);
  auto result = client->request("POST", string("/users/") + USER_NAME + "/pin", s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto bresult = provider->AuthenticatePin(USER_NAME, "9999");
  CPPUNIT_ASSERT_MESSAGE("Should validate password", bresult);
}

/// update password using uuid and validate using authenticate
void PostUserTest::testSetPasswordUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt;
  pt.put("password", "some_new_password");
  stringstream s;
  write_json(s, pt);

  auto rec = provider->LoadUser(USER_NAME);
  auto path = boost::format("/userid/%s/password") % rec.userid;
  auto result = client->request("POST", path.str(), s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto bresult = provider->Authenticate(USER_NAME, "some_new_password");
  CPPUNIT_ASSERT_MESSAGE("Should validate password", bresult);
}

/// update pin using uuid, validate using authenticate with pin
void PostUserTest::testSetPinUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt;
  pt.put("pin", "9999");
  stringstream s;
  write_json(s, pt);

  auto rec = provider->LoadUser(USER_NAME);
  auto path = boost::format("/userid/%s/pin") % rec.userid;
  auto result = client->request("POST", path.str(), s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto bresult = provider->AuthenticatePin(USER_NAME, "9999");
  CPPUNIT_ASSERT_MESSAGE("Should validate password", bresult);
}

/// should not update the password for invalid UUID
void PostUserTest::testSetPasswordBadUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt;
  pt.put("password", "some_new_password");
  stringstream s;
  write_json(s, pt);

  auto rec = provider->LoadUser(USER_NAME);
  auto path = boost::format("/userid/%s/password") % rec.userid;

  auto result = client->request("POST", "/userid/1234-1234/password", s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);

  result = client->request("POST", path.str(), "{}", header);
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
}

/// should fail to update pin using invalid UUID
void PostUserTest::testSetPinBadUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt;
  pt.put("pin", "9999");
  stringstream s;
  write_json(s, pt);

  auto rec = provider->LoadUser(USER_NAME);
  auto path = boost::format("/userid/%s/pin") % rec.userid;
  auto result = client->request("POST", "/userid/1234-1234/pin", s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);

  result = client->request("POST", path.str(), "{}", header);
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fauk", string("400"), code);
}

/// delete user, validate with load user API
void PostUserTest::testDeleteUser() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("DELETE", string("/users/") + USER_NAME, "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should complete", string("200"), code);

  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("User shold not exist (deleted)", (unsigned int)0, rec.id);
}

/// delete user by uuid, validate using load user api
void PostUserTest::testDeleteUserUUID() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto rec = provider->LoadUser(USER_NAME);
  auto url = boost::format("/userid/%s") % rec.userid;

  auto result = client->request("DELETE", url.str(), "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should complete", string("200"), code);

  rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("User shold not exist (deleted)", (unsigned int)0, rec.id);
}

/// get user by wrong uuid
void PostUserTest::testGetUserUUIDBad() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto url = boost::format("/userid/%s") % "some-wrong-id";

  auto result = client->request("GET", url.str(), "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should complete", string("404"), code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), result.result.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("user does not exist"), result.result.get("error_description",""));
}

/// should not delete invalid user
void PostUserTest::testDeleteUserBad() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("DELETE", "/users/xxxxxxxxxxx", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
}

/// should not delete a user using invalid uuid
void PostUserTest::testDeleteUserUUIDBad() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("DELETE", "/userid/xxxxxxxxxxx", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
}

/// should fail with 400 when username is empty
void PostUserTest::testGetUserBadRequest() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("GET", "/users/", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), result.result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("username is missing"), result.result.get("error_description", ""));
}

/// should fail with 404 when username does not exist
void PostUserTest::testGetUserNotFound() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("GET", "/users/xxxxxx", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("404"), code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), result.result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("User not found"), result.result.get("error_description", ""));
}

CPPUNIT_TEST_SUITE_REGISTRATION(PostUserTest);
