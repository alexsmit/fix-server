#pragma once
/*<alx.kuzza @gmail.com>
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// fixture class to test /v1/users apis
      class PostUserTestV1Fix {
        std::shared_ptr<ClientHelper> client;
        std::string client_id;
        std::string client_secret;

      public:
        int code;    //!< result code
        ptree body;  //!< result body
        PostUserTestV1Fix(std::shared_ptr<ClientHelper> client, const string& client_id = CLIENT_ID, const string& client_secret = CLIENT_SECRET);
        void Run(const string& method, const string& url, ptree pt);
        void Run(const string& method, const string& url);
      };

      /// testing /v1/users endpoints
      class PostUserTestV1 : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(PostUserTestV1);

        CPPUNIT_TEST(testBackendId);

        CPPUNIT_TEST(testFindUserBadAuth);
        CPPUNIT_TEST(testFindUserBadQuery);
        CPPUNIT_TEST(testFindUserNotFound);
        CPPUNIT_TEST(testFindUser);
        CPPUNIT_TEST(testFindUserSpace);
        CPPUNIT_TEST(testFindUserEmail);
        CPPUNIT_TEST(testFindUserTrimSpace);
        CPPUNIT_TEST(testFindUserEmailTrimSpace);

        CPPUNIT_TEST(testCreateUserBadAuth);
        CPPUNIT_TEST(testCreateUserDuplicate);
        CPPUNIT_TEST(testCreateUserDuplicateEmail);
        CPPUNIT_TEST(testCreateUserSpace);
        CPPUNIT_TEST(testCreateUserTrimSpace);
        CPPUNIT_TEST(testCreateUser);

        CPPUNIT_TEST(testGetUserBadAuth);
        CPPUNIT_TEST(testGetUserBadRequest);
        CPPUNIT_TEST(testGetUserNotFound);
        CPPUNIT_TEST(testGetUser);

        CPPUNIT_TEST(testSaveUserBadAuth);
        CPPUNIT_TEST(testSaveUserNotFound);
        CPPUNIT_TEST(testSaveUserBadUUID);
        CPPUNIT_TEST(testSaveUser);

        CPPUNIT_TEST(testDeleteUserBadAuth);
        CPPUNIT_TEST(testDeleteUserNotFound);
        CPPUNIT_TEST(testDeleteUserBadRequest);
        CPPUNIT_TEST(testDeleteUser);

        CPPUNIT_TEST(testSetPassword);
        CPPUNIT_TEST(testSetPin);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<ClientHelper> client;
        string post_result;

      public:
        PostUserTestV1();
        ~PostUserTestV1();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();  //!< verify we're using proper backend

        void testFindUserBadAuth();         //!< verify client_id/client_secret
        void testFindUserBadQuery();        //!< search user by name, wrong parameters
        void testFindUserNotFound();        //!< search user by name, may include spaces or any other characters
        void testFindUser();                //!< search user by name, may include spaces or any other characters
        void testFindUserSpace();           //!< search a user with space
        void testFindUserEmail();           //!< search a user by email
        void testFindUserTrimSpace();       //!< search a user with spaces around the name
        void testFindUserEmailTrimSpace();  //!< search a user by email with spaces around

        void testCreateUserBadAuth();         //!< verify client_id/client_secret
        void testCreateUserDuplicate();       //!< CRUD create new dupblicate user
        void testCreateUserDuplicateEmail();  //!< CRUD create new user with existing email
        void testCreateUserSpace();           //!< CRUD create new user with space inside
        void testCreateUserTrimSpace();       //!< CRUD create new user with spaces around
        void testCreateUser();                //!< CRUD create new user

        void testGetUserBadAuth();     //!< verify client_id/client_secret
        void testGetUserBadRequest();  //!< verify user id is provided
        void testGetUserNotFound();    //!< try to load non-existing user
        void testGetUser();            //!< get user by UUID

        void testSaveUserBadAuth();   //!< verify client_id/client_secret
        void testSaveUserNotFound();  //!< CRUD update user profile - user not found
        void testSaveUserBadUUID();   //!< CRUD update user profile, empty uuid
        void testSaveUser();          //!< CRUD update user profile

        void testDeleteUserBadAuth();     //!< verify client_id/client_secret
        void testDeleteUserNotFound();    //!< CRUD delete user by ID, not found
        void testDeleteUserBadRequest();  //!< CRUD delete user by ID, but it is empty
        void testDeleteUser();            //!< CRUD delete user by ID

        void testSetPassword();  //!< similar to Save user, but sends only a password portion
        void testSetPin();       //!< similar to Save user, but sends only pin portion
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
