/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "BlobTest.h"
#include "BaseTest.h"
#include <vx/PasswordUtil.h>

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

BlobTest::BlobTest() {}

BlobTest::~BlobTest() {}

void BlobTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());

  // profile_provider = factory->Connect<ProfileProviderMySQL>();
  profile_provider = ConnectProfileProvider(factory, "profile.mysql", backend);
  profile_provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);

  server_context = getContext();

  if (!server_context) {
    cout << "FATAL ERROR. Context is not initialized." << endl;
    ::exit(1);
  }
}

void BlobTest::tearDown() {
  profile_provider->DeleteUser(USER_NAME);
  profile_provider.reset();
  factory.reset();
}

void BlobTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, profile_provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), profile_provider->id());
#endif
}

/// post complete blob JSON and retreive via provider
void BlobTest::testBlobJson() {
  ptree pt;
  pt.put("content_type", "text/plain");
  pt.put("blob", "some data");

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  stringstream s;
  write_json(s, pt);

  auto url = (boost::format("/users/%s/blob/someblob") % USER_NAME).str();

  auto& result = client->request("POST", url, s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto blob = profile_provider->GetBlob(USER_NAME, "someblob");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob type", string("text/plain"), blob->content_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", string("some data"), blob->data);
}

/// post complete blob JSON using base64 encoding, retrieve it using provider
void BlobTest::testBlobJsonBase64() {
  string demo_blob = "some text data";
  string demo_blob64 = random_util().base64encode(demo_blob);

  ptree pt;
  pt.put("content_type", "text/plain");
  pt.put("encoding_type", "base64");
  pt.put("blob", demo_blob64);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  stringstream s;
  write_json(s, pt);

  auto url = (boost::format("/users/%s/blob/someblob") % USER_NAME).str();

  auto& result = client->request("POST", url, s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto blob = profile_provider->GetBlob(USER_NAME, "someblob");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob type", string("text/plain"), blob->content_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", demo_blob, blob->data);
}

/// post url encoded payload to blob endpoint
void BlobTest::testBlobUrlEncoded() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});

  string body = "content_type=text%2Fplain&blob=some%20data";

  auto url = (boost::format("/users/%s/blob/someblob2") % USER_NAME).str();

  auto& result = client->request("POST", url, body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto blob = profile_provider->GetBlob(USER_NAME, "someblob2");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob type", string("text/plain"), blob->content_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", string("some data"), blob->data);
}

/// post multipart form data
void BlobTest::testBlobFormData() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "multipart/form-data; boundary=----bound"});

  string boundary = "----bound";
  stringstream s;
  s << "--" << boundary << "\r\n";
  s << "Content-Disposition: form-data; name=\"blob\"\r\n";
  s << "Content-Type: text/plain\r\n\r\n";
  s << "some data"
    << "\r\n";
  s << "--" << boundary << "--\r\n\r\n";

  string body = s.str();
  auto url = (boost::format("/users/%s/blob/someblob3") % USER_NAME).str();

  auto& result = client->request("POST", url, body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto blob = profile_provider->GetBlob(USER_NAME, "someblob3");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob type", string("text/plain"), blob->content_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", string("some data"), blob->data);
}

/// multipart form data with transfer encoding base64
void BlobTest::testBlobFormDataBase64() {
  string demo_blob = "some text data";
  string demo_blob64 = random_util().base64encode(demo_blob);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "multipart/form-data; boundary=----bound"});

  string boundary = "----bound";
  stringstream s;
  s << "--" << boundary << "\r\n";
  s << "Content-Disposition: form-data; name=\"blob\"\r\n";
  s << "Content-Transfer-Encoding: BASE64\r\n";
  s << "Content-Type: text/plain\r\n\r\n";
  s << demo_blob64
    << "\r\n";
  s << "--" << boundary << "--\r\n\r\n";

  string body = s.str();
  auto url = (boost::format("/users/%s/blob/someblob3") % USER_NAME).str();

  auto& result = client->request("POST", url, body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  auto blob = profile_provider->GetBlob(USER_NAME, "someblob3");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob type", string("text/plain"), blob->content_type);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", demo_blob, blob->data);
}

/// set blob using provider and retrieve using API
void BlobTest::testBlobGet() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto url = (boost::format("/users/%s/blob/superblob") % USER_NAME).str();

  string demo_blob = "some text data";

  profile_provider->SetBlob(USER_NAME, "superblob", "text/plain", demo_blob);
  auto& result = client->request("GET", url, "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected content", demo_blob, result.data);

  auto it = result.header.find("Content-Type");
  CPPUNIT_ASSERT_MESSAGE("Should have content type", it != result.header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected content type", string("text/plain"), it->second);

  it = result.header.find("Content-Length");
  CPPUNIT_ASSERT_MESSAGE("Should have content length", it != result.header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected content length", demo_blob.length(), boost::lexical_cast<size_t>(it->second));
}

/// post url encoded payload to blob endpoint
void BlobTest::testBlobInvalidEncoding() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt;
  pt.put("content_type", "text/plain");
  pt.put("encoding_type", "foo-bar");
  pt.put("blob", "some data");
  stringstream st;
  write_json(st, pt);

  string body = st.str();

  auto url = (boost::format("/users/%s/blob/someblob2") % USER_NAME).str();

  auto& result = client->request("POST", url, body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("400"), code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("bad_request"), result.result.get("error", ""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("invalid encoding_type"), result.result.get("error_description", ""));
}


void BlobTest::testBlobSize(int size, const string& expected_code, const string& error) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  string sblob(size, 'a');

  ptree pt;
  pt.put("content_type", "text/plain");
  pt.put("blob", sblob);
  stringstream st;
  write_json(st, pt);

  string body = st.str();
  auto l = body.length();

  auto url = (boost::format("/users/%s/blob/someblob2") % USER_NAME).str();

  auto& result = client->request("POST", url, body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", expected_code, code);
  if (!error.empty()) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("error", error, result.result.get("error", ""));
  }
}

/// postpayload to blob endpoint
void BlobTest::testBlobTooBigMedium() {
  testBlobSize(2000000, string("200"), "");
}

/// postpayload to blob endpoint
void BlobTest::testBlobTooBigMax() {
  testBlobSize(ProfileProvider::MAX_BLOB_SIZE, string("200"), "");
}

/// postpayload to blob endpoint
void BlobTest::testBlobTooBigOver() {
  testBlobSize(16000000, string("400"), "property_error");
}

CPPUNIT_TEST_SUITE_REGISTRATION(BlobTest);
