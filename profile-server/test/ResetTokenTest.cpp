/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include <BaseTest.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "ResetTokenTest.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

ResetTokenTest::ResetTokenTest() {}

ResetTokenTest::~ResetTokenTest() {}

// POST /user/{username}
void ResetTokenTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);

  reset_request = (boost::format("{ \"email\": \"%s\" }") % USER_EMAIL).str();
  reset_request_not_exists = (boost::format("{ \"email\": \"%s\" }") % "john.doe@domain.tld").str();
  reset_request_bad = "{ \"email\": \"\" }";
  update_password_bad_request = "{ \"token\": \"\", \"password\": \"\" }";
}

void ResetTokenTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider.reset();
  factory.reset();
}


void ResetTokenTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

/// happy path. reset, verify, update. and try to login
void ResetTokenTest::testReset() {
  // 1 - reset
  auto result = client->request("POST", "/password/reset", reset_request, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
  ptree pt = result.result;

  auto token = pt.get<string>("token", "");
  CPPUNIT_ASSERT_MESSAGE("Should have token", !token.empty());

  ptree pt_update_pass;
  pt_update_pass.put("token", token);
  pt_update_pass.put("password", "newpassword");
  stringstream s;
  write_json(s, pt_update_pass);
  string update_request = s.str();

  // 2 - update password
  result = client->request("POST", "/password/update", update_request, header);
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  // 3 - login with new password
  auto auth_result = provider->Authenticate(USER_NAME, "newpassword");
  CPPUNIT_ASSERT_MESSAGE("Should validate new password", auth_result);
}

/// happy path. reset, verify, update. and try to login
void ResetTokenTest::testResetEmailNotExist() {
  // 1 - reset
  auto result = client->request("POST", "/password/reset", reset_request_not_exists, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("404"), code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("not_found"), result.result.get("error",""));
}

/// successful login should invalidate reset token
void ResetTokenTest::testResetLoginInvalidateToken() {
  // 1 - reset
  auto result = client->request("POST", "/password/reset", reset_request, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
  ptree pt = result.result;

  auto token = pt.get<string>("token", "");
  CPPUNIT_ASSERT_MESSAGE("Should have token", !token.empty());

  // 2 - login with old password
  auto auth_result = provider->Authenticate(USER_NAME, USER_PASSWORD);
  CPPUNIT_ASSERT_MESSAGE("Should validate old password", auth_result);

  // 3 - try to update password
  ptree pt_update_pass;
  pt_update_pass.put("token", token);
  pt_update_pass.put("password", "newpassword");
  stringstream s;
  write_json(s, pt_update_pass);
  string update_request = s.str();

  result = client->request("POST", "/password/update", update_request, header);
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail (token should be already deleted)", string("403"), code);

  // 4 - try login with new password
  auth_result = provider->Authenticate(USER_NAME, "newpassword");
  CPPUNIT_ASSERT_MESSAGE("Should not validate new password", !auth_result);
}

/// sending wrong payload to reset password
void ResetTokenTest::testResetBad() {
  auto result =
      client->request("POST", "/password/reset", reset_request_bad, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with code 400", string("400"), code);
}

/// sending invalid request
void ResetTokenTest::testBadRequest() {
  auto result = client->request("POST", "/password/reset", reset_request);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);
}

/// sending wrong payload to update password
void ResetTokenTest::testUpdatePasswordWrongToken() {
  auto result = client->request("POST", "/password/update",
                                update_password_bad_request, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
}

/// "wrong" means: 1) any non-existing token 2) previously used token
void ResetTokenTest::testFailUpdateWrongToken() {
  ptree pt_update_pass;
  pt_update_pass.put("token", "previously_used_token");
  pt_update_pass.put("password", "newpassword");
  stringstream s;
  write_json(s, pt_update_pass);
  string update_request = s.str();

  auto result = client->request("POST", "/password/update", update_request, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("403"), code);
}

/// reset password, verify
void ResetTokenTest::testValidateResetToken() {
  // 1 - reset
  auto result = client->request("POST", "/password/reset", reset_request, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
  ptree pt = result.result;

  stringstream s;
  write_json(s, pt);

  // 2 - verify good token (sending a response back, acutally)
  result = client->request("POST", "/password/verifytoken", s.str(), header);
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
}

/// test empty reset token
void ResetTokenTest::testValidateResetTokenEmpty() {
  auto result = client->request("POST", "/password/verifytoken", "{}", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("400"), code);
}

/// test wrong reset token
void ResetTokenTest::testValidateResetTokenBad() {
  auto result = client->request("POST", "/password/verifytoken", "{\"token\":\"bad_token\"}", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("403"), code);
}

CPPUNIT_TEST_SUITE_REGISTRATION(ResetTokenTest);
