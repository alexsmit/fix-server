#pragma once
/*<alx.kuzza @gmail.com>
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;
namespace vx {
  namespace openid {
    namespace test {

      /// complex test to manipulate user's data (pin, props, password, etc.)
      class PostUserTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(PostUserTest);
        
        CPPUNIT_TEST(testBackendId);

        CPPUNIT_TEST(testClientData);
        CPPUNIT_TEST(testPost);
        CPPUNIT_TEST(testPostNoHeader);
        CPPUNIT_TEST(testPostBadClientSecret);
        CPPUNIT_TEST(testAuthPin);
        CPPUNIT_TEST(testAuthPassword);

        CPPUNIT_TEST(testSetPassword);
        CPPUNIT_TEST(testSetPin);

        CPPUNIT_TEST(testSetPasswordUUID);
        CPPUNIT_TEST(testSetPinUUID);

        CPPUNIT_TEST(testSetPasswordBadUUID);
        CPPUNIT_TEST(testSetPinBadUUID);

        CPPUNIT_TEST(testDeleteUser);
        CPPUNIT_TEST(testDeleteUserUUID);
        CPPUNIT_TEST(testDeleteUserBad);
        CPPUNIT_TEST(testDeleteUserUUIDBad);
        CPPUNIT_TEST(testGetUserUUIDBad);
        
        CPPUNIT_TEST(testGetUserBadRequest);
        CPPUNIT_TEST(testGetUserNotFound);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<ClientHelper> client;
        string post_result;

      public:
        PostUserTest();
        ~PostUserTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();  //!< verify we're using proper backend

        void testPost();
        void testPostNoHeader();
        void testPostBadClientSecret();

        void testClientData();
        void testAuthPin();
        void testAuthPassword();

        void testSetPassword();
        void testSetPin();

        void testSetPasswordUUID();
        void testSetPinUUID();

        void testSetPasswordBadUUID();
        void testSetPinBadUUID();

        void testDeleteUser();
        void testDeleteUserUUID();

        void testDeleteUserBad();
        void testDeleteUserUUIDBad();
        void testGetUserUUIDBad();

        void testGetUserBadRequest();
        void testGetUserNotFound();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
