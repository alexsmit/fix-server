/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _AUTH_TEST_H
#define _AUTH_TEST_H

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// testing /authenticate endpoint
      class AuthTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(AuthTest);

        CPPUNIT_TEST(testBackendId);

        CPPUNIT_TEST(testPostNoHeader);
        CPPUNIT_TEST(testCorrectPassword);
        CPPUNIT_TEST(testBadPassword);
        CPPUNIT_TEST(testNoClientIdSecret);
        CPPUNIT_TEST(testBadRequest);
        CPPUNIT_TEST(testUUIDAuth);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> provider;
        shared_ptr<ClientHelper> client;
        string pin_request, password_request, password_wrong_request, password_bad_request;

      public:
        AuthTest();
        ~AuthTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();  //!< verify we're using proper backend

        void testPostNoHeader();      //!< no client/secret header
        void testCorrectPassword();   //!< happy path using correct payload and header
        void testBadPassword();       //!< correct header, bad password
        void testNoClientIdSecret();  //!< no client auth
        void testBadRequest();        //!< sending wrong payload
        void testUUIDAuth();          //!< authenticate using UUID instead of user name
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
