/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _BLOB_TEST_H
#define _BLOB_TEST_H

#include "../stdafx.h"

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>

#include <BaseTest.h>

#include "ClientHelper.h"

using namespace std;
using namespace boost::property_tree;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// testing /users/{user}/blob endpoints
      class BlobTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(BlobTest);

        CPPUNIT_TEST(testBackendId);
        CPPUNIT_TEST(testBlobJson);
        CPPUNIT_TEST(testBlobJsonBase64);
        CPPUNIT_TEST(testBlobUrlEncoded);
        CPPUNIT_TEST(testBlobFormData);
        CPPUNIT_TEST(testBlobFormDataBase64);
        CPPUNIT_TEST(testBlobGet);
        CPPUNIT_TEST(testBlobInvalidEncoding);
#if BOOST_VERSION >= 106600        
        CPPUNIT_TEST(testBlobTooBigMedium);
        CPPUNIT_TEST(testBlobTooBigMax);
#endif  
        CPPUNIT_TEST(testBlobTooBigOver);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<DataFactory> factory;
        shared_ptr<ProfileProvider> profile_provider;
        shared_ptr<ClientHelper> client;
        shared_ptr<ServerContext> server_context;

        void testBlobSize(int size, const string& code, const string& error);

      public:
        BlobTest();
        ~BlobTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testBackendId();        //!< verify we're using proper backend
        void testBlobJson();
        void testBlobJsonBase64();
        void testBlobUrlEncoded();
        void testBlobFormData();
        void testBlobFormDataBase64();
        void testBlobGet();
        void testBlobInvalidEncoding();
        void testBlobTooBigMedium();
        void testBlobTooBigMax();
        void testBlobTooBigOver();
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
#endif
