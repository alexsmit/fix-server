/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include "BaseTest.h"

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "AuthTest.h"
#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

AuthTest::AuthTest() {}

AuthTest::~AuthTest() {}

// POST /authenticate
void AuthTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());

  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  // provider = factory->Connect<ProfileProviderMySQL>();

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);
  provider->CreateUser(USER_NAME, "user.email@vxpro.com", "password");
  provider->SetPin(USER_NAME, USER_PIN);
  provider->SetRoles(USER_NAME, {ROLE_1, ROLE_2});

  pin_request =
      (boost::format("{\"grant_type\":\"pin\",\"pin\":\"1234\",\"username\":\"%s\"}") % USER_NAME).str();
  password_request =
      (boost::format("{\"grant_type\":\"password\",\"username\":\"%s\",\"password\":\"%s\"}") % USER_NAME % USER_PASSWORD).str();
  password_wrong_request =
      (boost::format("{\"grant_type\":\"password\",\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();
  password_bad_request =
      (boost::format("{\"username\":\"%s\",\"password\":\"wrong password\"}") % USER_NAME).str();
}

void AuthTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider->DeleteRole(ROLE_1);
  provider->DeleteRole(ROLE_2);
  provider.reset();
  factory.reset();
}

void AuthTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

void AuthTest::testPostNoHeader() {
  client->Call("POST", "/authenticate", password_request);
  // auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", SimpleWeb::StatusCode::client_error_bad_request, client->code);
}

void AuthTest::testCorrectPassword() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  client->Call("POST", "/authenticate", password_request, header);

  // auto code = result->status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should validate password", SimpleWeb::StatusCode::success_ok, client->code);

  ptree& pt = client->result;

  auto userid = pt.get("userid", "");
  CPPUNIT_ASSERT_MESSAGE("Should have userid in the response", !userid.empty());

  auto child = pt.get_child_optional("roles");
  auto has_roles = (!!child);
  CPPUNIT_ASSERT_MESSAGE("Should have roles", has_roles);

  auto cnt = 0, cnt1 = 0, cnt2 = 0;
  for (auto& r : child.get()) {
    if (r.second.data() == ROLE_1)
      cnt1++;
    else if (r.second.data() == ROLE_2)
      cnt2++;
    else
      cnt++;
  }
  CPPUNIT_ASSERT_MESSAGE("Should have role1 in the response", cnt1 == 1);
  CPPUNIT_ASSERT_MESSAGE("Should have role2 in the response", cnt2 == 1);
  CPPUNIT_ASSERT_MESSAGE("Should not have other roles", cnt == 0);
}

void AuthTest::testBadPassword() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  client->Call("POST", "/authenticate", password_wrong_request, header);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 403", SimpleWeb::StatusCode::client_error_forbidden, client->code);
}

void AuthTest::testNoClientIdSecret() {
  client->Call("POST", "/authenticate", password_request);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", SimpleWeb::StatusCode::client_error_bad_request, client->code);
}

void AuthTest::testBadRequest() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  client->Call("POST", "/authenticate", password_bad_request, header);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", SimpleWeb::StatusCode::client_error_bad_request, client->code);

  ptree pt = client->result;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("bad request code", string("bad_request"), pt.get("error", ""));
}

void AuthTest::testUUIDAuth() {
  auto rec = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("User should exist", rec.id != 0);

  ptree pt;
  pt.put("userid", rec.userid);
  pt.put("password", "password");
  pt.put("grant_type", "password");
  stringstream s;
  write_json(s, pt);

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  client->Call("POST", "/authenticate", s.str(), header);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", SimpleWeb::StatusCode::success_ok, client->code);
}

CPPUNIT_TEST_SUITE_REGISTRATION(AuthTest);
