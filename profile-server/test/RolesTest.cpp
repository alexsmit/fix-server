
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include <BaseTest.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "RolesTest.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

RolesTest::RolesTest() {}

RolesTest::~RolesTest() {}

// POST /user/{username}
void RolesTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  provider->DeleteRole(ROLE_1);
  provider->DeleteRole(ROLE_2);
  provider->DeleteRole(ROLE_TEST);
}

void RolesTest::tearDown() {
  provider->DeleteRole(ROLE_1);
  provider->DeleteRole(ROLE_2);
  provider.reset();
  factory.reset();
}

void RolesTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

/// request without client/secret
void RolesTest::testBadRequest() {
  auto result = client->request("GET", "/roles","");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);
}

/// get role
void RolesTest::testGetRole() {
  provider->AddRole(ROLE_TEST, ROLE_TEST);
  /*
  expected result:
  {
    "description": "test"
  }
  */
  auto result = client->request("GET", string("/roles/") + ROLE_TEST, "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");
  ptree pt = result.result;

  auto desc = pt.get<string>("description", "");
  CPPUNIT_ASSERT_MESSAGE("Should have description", desc == ROLE_TEST);
}

/// get roles
void RolesTest::testGetRoles() {
  provider->AddRole(ROLE_TEST, ROLE_TEST);
  /*
  expected result:
  {
    "roles": [
      ROLE_1,
      ROLE_2,
      ...
    ]
  }
  */
  auto result = client->request("GET", "/roles", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");
  ptree pt = result.result;
  auto roles = pt.get_child("roles");
  bool found = false;
  for (auto &r : roles) {
    if (r.second.data() == ROLE_TEST) {
      found = true;
      break;
    }
  }
  CPPUNIT_ASSERT_MESSAGE("Should have role test", found);
}

/// delete role, verify using provider api
void RolesTest::testDeleteRole() {
  provider->AddRole(ROLE_TEST, ROLE_TEST);
  auto result = client->request("DELETE", string("/roles/") + ROLE_TEST, "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  auto roles = provider->Roles();
  CPPUNIT_ASSERT_MESSAGE(
      "Should not have role test",
      std::find(roles.begin(), roles.end(), ROLE_TEST) == roles.end());
}

/// update "description" property of a role
void RolesTest::testPostRole() {
  auto result = client->request(
      "POST", string("/roles/") + ROLE_TEST, "{\"description\":\"some description\"}", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  auto roles = provider->Roles();
  CPPUNIT_ASSERT_MESSAGE(
      "Should have role test",
      std::find(roles.begin(), roles.end(), ROLE_TEST) != roles.end());
}

/// update with empty "description" property of a role, bad request
void RolesTest::testPostRoleBad() {
  auto result = client->request(
      "POST", string("/roles/") + ROLE_TEST, "{\"description\":\"\"}", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should fail with bad request", code == "400");

  ptree pt =result.result;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("bad request code", string("bad_request"), pt.get("error",""));
}


CPPUNIT_TEST_SUITE_REGISTRATION(RolesTest);
