
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include <BaseTest.h>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "PropertiesTest.h"

#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

PropertiesTest::PropertiesTest() {}

PropertiesTest::~PropertiesTest() {}

// POST /user/{username}
void PropertiesTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  factory.reset(new DataFactory());
  // provider = factory->Connect<ProfileProviderMySQL>();
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  provider->DeleteUser(USER_NAME);
}

void PropertiesTest::tearDown() {
  provider->DeleteUser(USER_NAME);
  provider.reset();
  factory.reset();
}

void PropertiesTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

/// update properties using POST, load using API
void PropertiesTest::testPostProperties() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_NAME);

  string payload = "{\"prop3\":\"val3\"}";

  auto result =
      client->request("POST", string("/users/") + USER_NAME + "/properties", payload, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  auto props = provider->GetProperties(USER_NAME);
  bool found = false;
  for (auto &e : props) {
    if (e.first == "prop3" && e.second == "val3") {
      found = true;
      break;
    }
  }

  CPPUNIT_ASSERT_MESSAGE("Should have property prop3", found);
}

/// should retrieve expected properties
void PropertiesTest::testGetProperties() {
  provider->CreateUser(USER_NAME, USER_EMAIL, USER_NAME);
  provider->SetProperty(USER_NAME, "prop1", "val1");
  provider->SetProperty(USER_NAME, "prop2", "val2");
  /*
  expected result:
  {
    "properties": [
      "prop1": "val1",
      "prop2": "val2",
    ]
  }
  */
  auto result = client->request("GET", string("/users/") + USER_NAME + "/properties", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");
  ptree pt = result.result;

  auto child_optional = pt.get_child_optional("properties");
  bool has_props = (!!child_optional);
  CPPUNIT_ASSERT_MESSAGE("Should have properties in the response", has_props);

  auto child = child_optional.get();
  int cnt1 = 0, cnt2 = 0;

  for (auto &cc : child) {
    if (cc.first == "prop1" && cc.second.data() == "val1") cnt1++;
    if (cc.first == "prop2" && cc.second.data() == "val2") cnt2++;
  }

  CPPUNIT_ASSERT_MESSAGE("Should have prop1", cnt1 == 1);
  CPPUNIT_ASSERT_MESSAGE("Should have prop2", cnt2 == 1);
}

/// load one property
void PropertiesTest::testGetProperty() {
  provider->CreateUser(USER_NAME, USER_EMAIL, "password");
  provider->SetProperty(USER_NAME, "prop1", "val1");
  provider->SetProperty(USER_NAME, "prop2", "val2");
  /*
  expected result:
  {
    "prop2": "val2"
  }
  */
  auto result =
      client->request("GET", string("/users/") + USER_NAME + "/properties/prop2", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");
  ptree pt = result.result;
  auto val = pt.get<string>("prop2", "");
  CPPUNIT_ASSERT_MESSAGE("Should have prop2", val == "val2");
}

/// sending wrong payload
void PropertiesTest::testBadRequest() {
  auto result = client->request("GET", "/users/test/properties", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);
}


CPPUNIT_TEST_SUITE_REGISTRATION(PropertiesTest);
