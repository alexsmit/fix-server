/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/PtreeUtil.h>
#include <vx/ServiceProfileFactory.h>

#include "BaseTest.h"

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include <ProfileServerContext.h>
#include "ProfileServerContextTest.h"
#include <InitProfileTest.h>

using namespace std;
using namespace vx::openid::test;

ProfileServerContextTest::ProfileServerContextTest() {}

ProfileServerContextTest::~ProfileServerContextTest() {}

// POST /authenticate
void ProfileServerContextTest::setUp() {
  AppConfig::resetInstance();
}

void ProfileServerContextTest::tearDown() {
}

void ProfileServerContextTest::testLoglevelWrong() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  AppConfig& config = AppConfig::getInstance();
  config["loglevel"] = "wrong-value";
  context->Init(NULL, "config.json");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("log level is set to 0", (int)0, context->debugLevel);
}

void ProfileServerContextTest::testLoglevelOK() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  AppConfig& config = AppConfig::getInstance();
  config["loglevel"] = "2";
  context->Init(NULL, "config.json");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("log level is set to 2", (int)2, context->debugLevel);
}

void ProfileServerContextTest::testGetProviderTypeEmpty() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3,backend));

  AppConfig& config = AppConfig::getInstance();
  config["profile_provider.type"] = "";
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov = context->GetProfileProvider();
  CPPUNIT_ASSERT_MESSAGE("got provider", !!prov);

// if mysql backend is not defined a "soci" backend is always used
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", backend, prov->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", string("soci"), prov->id());
#endif
}

void ProfileServerContextTest::testGetProviderTypeBackend() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));

  AppConfig& config = AppConfig::getInstance();
  config["profile_provider.type"] = "some_unknown_type";

  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov = context->GetProfileProvider();
  CPPUNIT_ASSERT_MESSAGE("got provider", !!prov);

// if mysql backend is not defined a "soci" backend is always used
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", backend, prov->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", string("soci"), prov->id());
#endif
}

void ProfileServerContextTest::testGetProviderNoBackendTypeEmpty() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3));

  AppConfig& config = AppConfig::getInstance();
  config["profile_provider.type"] = "";

  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  CPPUNIT_ASSERT_THROW_MESSAGE("empty type throw",{
    auto prov = context->GetProfileProvider();
  },invalid_argument);
}

void ProfileServerContextTest::testGetProviderNoBackendTypeBad() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3));

  AppConfig& config = AppConfig::getInstance();
  config["profile_provider.type"] = "some_unknown_type";

  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  CPPUNIT_ASSERT_THROW_MESSAGE("unknown type throw",{
    auto prov = context->GetProfileProvider();
  },invalid_argument);
}

void ProfileServerContextTest::testGetProviderTypeRemote() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3));

  AppConfig& config = AppConfig::getInstance();
  config["profile_provider.type"] = "remote";
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov = context->GetProfileProvider();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", string("remote"), prov->id());
}

void ProfileServerContextTest::testGetServiceDefault() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov = context->getService("profile");
  CPPUNIT_ASSERT_MESSAGE("got service", !!prov);
}

void ProfileServerContextTest::testGetServiceWrong() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "throw for unknown provider name", {
        auto prov = context->getService("some-unknown-provider");
      },
      std::invalid_argument);
}

void ProfileServerContextTest::testGetServiceBadDBType() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));

  AppConfig& config = AppConfig::getInstance();
  config["profile.mysql.type"] = "foo-bar";
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "throw for unknown database type", {
        auto prov = context->getService("profile");
      },
      std::invalid_argument);
}

void ProfileServerContextTest::testGetServiceNotRegistered() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));

  AppConfig& config = AppConfig::getInstance();
  context->Init(NULL, "config.json");

  // NOTE: do not register profile factory

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "throw when factory not registered", {
        auto prov = context->getService("profile");
      },
      std::invalid_argument);
}

namespace vx {
  /// @brief Implementation of the service factory to create profile provider based on the configuration file or parameter
  class ServiceDummyFactory : public ServiceFactory {

  public:
    /// @brief Service factory to create profile provider instance or retrieve from the cache
    /// @param ttl amount of time (in seconds) a service instance considered to be valid for a current thread
    /// @param conf coniguration name
    ServiceDummyFactory(time_t ttl = 60, const string& conf = "profile_provider") {}

    std::string id() const override {
      return "profile";
    }

  protected:
    /// @brief create an instance of profile provider for specified type. if type is invalid, @throw argument_exception
    /// @param type profile provider type, if empty - a default type should be retrieved as configured
    /// @return service pointer
    std::shared_ptr<Service> createService(const std::string& type) override {
      return make_shared<Service>();
    }
  };

}  // namespace vx


void ProfileServerContextTest::testGetServiceBadRegistered() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));

  AppConfig& config = AppConfig::getInstance();
  context->Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceDummyFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  CPPUNIT_ASSERT_THROW_MESSAGE(
      "throw when wrong factory not registered", {
        auto prov = context->GetProfileProvider();
      },
      std::invalid_argument);
}

void ProfileServerContextTest::testGetProviderTTLDiff() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  context->Init(NULL, "config.json");
  context->provider_ttl = 2;  // set 2 seconds TTL, i.e. if we ask for provider after 2 seconds we will get a new one

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov1 = context->GetProfileProvider();
  cout << "W" << flush;

  std::this_thread::sleep_for(std::chrono::milliseconds(3000));
  auto prov2 = context->GetProfileProvider();
  auto prov3 = context->GetProfileProvider();

  // Logger spills to cout .....
  // stringstream out;
  // std::streambuf* out_buf(std::cout.rdbuf(out.rdbuf()));
  // std::cout.rdbuf(out_buf);
  // string output = out.str();

  CPPUNIT_ASSERT_MESSAGE("different providers", prov2.get() != prov1.get());
  CPPUNIT_ASSERT_MESSAGE("same providers", prov2.get() == prov3.get());
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("Log output", string("Reset profile provider for current thread\n"), output);
}

void ProfileServerContextTest::testGetProviderTTLSame() {
  shared_ptr<ProfileServerContext> context;
  context.reset(new ProfileServerContext(3, backend));
  context->Init(NULL, "config.json");
  context->provider_ttl = 10;  // set 10 seconds TTL, good enough to make 2 consequent calls

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context->AddProvider(profile_service);

  auto prov0 = context->GetProfileProvider();

  auto prov1 = context->GetProfileProvider();
  auto prov2 = context->GetProfileProvider();

  CPPUNIT_ASSERT_MESSAGE("same providers", prov2.get() == prov1.get());
}


CPPUNIT_TEST_SUITE_REGISTRATION(ProfileServerContextTest);
