#pragma once
/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/ProfileProviderMySQL.h>
#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// testing /authenticate endpoint
      class ProfileServerContextTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ProfileServerContextTest);

        CPPUNIT_TEST(testLoglevelWrong);
        CPPUNIT_TEST(testLoglevelOK);
        CPPUNIT_TEST(testGetProviderTypeEmpty);
        CPPUNIT_TEST(testGetProviderTypeBackend);
        CPPUNIT_TEST(testGetProviderNoBackendTypeEmpty);
        CPPUNIT_TEST(testGetProviderNoBackendTypeBad);
        CPPUNIT_TEST(testGetProviderTypeRemote);
        CPPUNIT_TEST(testGetServiceDefault);
        CPPUNIT_TEST(testGetServiceWrong);
        CPPUNIT_TEST(testGetServiceBadDBType);
        CPPUNIT_TEST(testGetServiceNotRegistered);
        CPPUNIT_TEST(testGetServiceBadRegistered);
        CPPUNIT_TEST(testGetProviderTTLDiff);
        CPPUNIT_TEST(testGetProviderTTLSame);

        CPPUNIT_TEST_SUITE_END();

      private:
      public:
        ProfileServerContextTest();
        ~ProfileServerContextTest();

        void setUp() override;     //!< initialize test suite
        void tearDown() override;  //!< cleanup test suite

        void testLoglevelWrong();                  //!< log level is wrong in config, set to default = 0
        void testLoglevelOK();                     //!< set log level to other value
        void testGetProviderTypeEmpty();           //!< get profile provider if type is empty
        void testGetProviderTypeBackend();         //!< get profile provider, backend is set
        void testGetProviderNoBackendTypeEmpty();  //!< get profile provider,backend is not set, type is empty
        void testGetProviderNoBackendTypeBad();    //!< get profile provider,backend is not set, type is wrong
        void testGetProviderTypeRemote();          //!< get remote profile provider
        void testGetServiceDefault();              //!< get service provider
        void testGetServiceWrong();                //!< get service provider using wrong name
        void testGetServiceBadDBType();            //!< get service provider using wrong database name
        void testGetServiceNotRegistered();        //!< get service provider, profile factory not registered
        void testGetServiceBadRegistered();        //!< get service provider, wrong profile factory is registered
        void testGetProviderTTLDiff();             //!< check we will get 2 different providers after provider_ttl timeout
        void testGetProviderTTLSame();             //!< check we will get 2 same providers within provider_ttl timeout
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
