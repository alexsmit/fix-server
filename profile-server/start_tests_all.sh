#!/bin/bash

. ./start_stress_common.sh

pid=
trystart=1
if [ $trystart -ne 0 ] ; then
    cnt=$(ps -ef | grep profile-server | wc -l)
    if [ $cnt -lt 2 ] ; then
        echo -n 'Profile server is not running. Start? [Y/n] :'
        read ans
        params+=(shutdown $shutdown)
        if [ -z "$ans" ] || [[ $ans =~ [Yy] ]] ; then
            pushd build >/dev/null
            ./profile-server 9997 0 5 300&
            pid=$!
            popd >/dev/null
        else
    	    return 1 2>/dev/null || exit 1
        fi
    fi
fi

pushd build > /dev/null

./profile-server-test -

echo Waiting for profile server to shut down...
if [ ! -z "$pid" ] ; then
    kill -HUP $pid
    wait $pid
else
    stop_server localhost 9997
    sleep 15
fi

sleep 3

echo Continue testing....
./profile-server-test

popd >/dev/null
