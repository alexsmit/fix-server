/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define OPENSSL_SUPPRESS_DEPRECATED

#include "stdafx.h"

#include <boost/variant.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>

#include <vx/JWT.h>
#include <vx/PtreeUtil.h>
#include <vx/RSAKeyInfo.h>
#include <vx/RSACertInfo.h>
#include <vx/web/WebServer.h>

#include <vx/web/ServerContext.h>
#include <vx/web/Util.h>

#include "src/ProfileServerContext.h"
#include "src/RegistationHandler.h"
#include "src/AuthHandler.h"
#include "src/RoleHandler.h"
#include "src/UserHandler.h"
#include "src/PropertyHandler.h"
#include "src/InfoHandler.h"
#include "src/ResetHandler.h"
#include "src/DebugHandler.h"
#include "src/StopHandler.h"

#include <vx/ServiceFactory.h>
#include <vx/ServiceProfileFactory.h>

#define POOL_SIZE 4

#ifndef VXPROD
#define CLEAN_THREAD_SLEEP 1
#define DRAIN_TIMEOUT 5
#else
#define CLEAN_THREAD_SLEEP 1800
#define DRAIN_TIMEOUT 15
#endif

using namespace std;
using namespace boost::property_tree;
using namespace VxServer::Handlers;

/// Profile server startup parameters
struct RunParams {
  // positional parameters
  int port;               //!< HTTP port, if 0 a default port will be set
  int ports;              //!< HTTPS port, if 0 a default port will be set
  unsigned long reg_ttl;  //!< registration token ttl, default is 2 days
  int debug;              //!< debug level for loggint
  // extra parameters
  unsigned int shutdown;             //!< if non-zero stop the server after speciried number of seconds
  bool clean;                        //!< run cleanup thread
  string backend;                    //!< running backend (forced, if provided)
  RunParams(int argc, char **argv);  //!< default constructor to process command line parameters
};

static int server_done = 0;

void run(RunParams params) {
  ProfileServerContext context(params.reg_ttl, params.backend);
  context.Init(NULL, "config.json");

  auto profile_service = make_shared<vx::ServiceProfileFactory>(60, "profile_provider");
  context.AddProvider(profile_service);

  params.port = (params.port >= 0) ? params.port : boost::lexical_cast<int>(context.getParam("port"));
  params.ports = (params.ports >= 0) ? params.ports : boost::lexical_cast<int>(context.getParam("port_https"));

  context.pool_size = POOL_SIZE;
  context.port = params.port;
  context.ports = params.ports;

#ifndef VXPROD
  if (params.reg_ttl < 60) context.debugLevel = 3;
#endif

  cout << "Profile server   : " << PROJECT_GIT_TAG
#ifndef NDEBUG
       << " (debug build)"
#endif
       << endl
       << "threads          : " << POOL_SIZE << endl
       << "ports            : " << context.port << "," << context.ports << endl
       << "registration ttl : " << params.reg_ttl << endl
#ifndef VXPROD
      ;
  if (params.shutdown) {
    cout << "shutdown after   : " << params.shutdown << " seconds\n";
  }
  if (!params.clean) {
    cout << "Cleanup          :  disabled\n";  // LCOV_EXCL_LINE
  }
  if (!params.backend.empty()) {
    cout << "Backend          : " << params.backend << "\n";  // LCOV_EXCL_LINE
  }

  cout
#endif
      << endl;

  // LCOV_EXCL_START
  if (params.port == 0 && params.ports == 0) {
    cout << "No listening ports (all disabled)\n";
    return;
  }
  // LCOV_EXCL_STOP

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGHUP);   // 1 Hangup detected on controlling terminal or death of controlling process
  sigaddset(&set, SIGINT);   // 2 Interrupt from keyboard (Ctrl+C)
  sigaddset(&set, SIGQUIT);  // 3 Quit from keyboard
  sigaddset(&set, SIGABRT);  // 6 Abort signal from abort(3)
  sigaddset(&set, SIGTERM);  // 15 Termination request
  sigaddset(&set, SIGTSTP);  // 20 Stop typed at terminal (Ctrl+Z)

  auto psig = pthread_sigmask(SIG_BLOCK, &set, NULL);

  WebServer server(POOL_SIZE, 32000000);

  shared_ptr<Instance> http_server;
  shared_ptr<Instance> https_server;

  if (params.port != 0) {
#ifndef VXPROD
    cout << "Starting HTTP listener on port " << params.port << endl;
#endif
    http_server = server.CreateHttpListener(params.port);
  }

  string server_cert = context.getParam("http_server_certificate");
  string server_key = context.getParam("http_server_key");

  // LCOV_EXCL_START
  if (!server_cert.empty() && !server_key.empty() && params.ports > 0) {
#ifndef VXPROD
    cout << "Starting HTTPS listener on port " << params.ports << endl;
#endif
    https_server = server.CreateHttpsListener(params.ports, server_cert, server_key);
  }
  // LCOV_EXCL_STOP


  // secure_server instance will be used to handle all API requests. If SSL is enabled that will be
  // a https instance, otherwise - a default server
  Instance &secure_server = *((!https_server) ? http_server : https_server).get();

  prettyPtree = false;

  RegistrationHandler registrationHandler(context, server, secure_server);
  ResetHandler resetHandler(context, server, secure_server);
  DebugHandler debugHandler(context, server, secure_server);
  AuthHandler authHandler(context, server, secure_server);
  InfoHandler infoHandler(context, server, secure_server);
  PropertyHandler propertyHandler(context, server, secure_server);
  RoleHandler roleHandler(context, server, secure_server);
  UserHandler userHandler(context, server, secure_server);
  StopHandler stopHandler(context, server, secure_server);

  vector<thread> service_threads;
  thread sig_handler(
      [&]() {  // LCOV_EXCL_LINE
        int sig;
        sigwait(&set, &sig);
        context.Logger(LogLevel::none, (boost::format("Server terminated with signal: %d") % sig).str());
        server_done = 1;
      });
  sig_handler.detach();

  if (params.clean) {
    service_threads.push_back(
        thread([&context]() {
          try {
            while (!server_done) {
              {
                std::lock_guard<std::recursive_mutex> lock{context.mtx};
                // auto profile_provider = context.factory.Connect<ProfileProviderMySQL>();
                auto profile_provider = context.GetProfileProvider();
                if (profile_provider->id() == "remote") break;
                profile_provider->CleanRegistrations(context.reg_ttl);
              }
              int wait_cycle = CLEAN_THREAD_SLEEP;
              while (!server_done && wait_cycle > 0) {
                wait_cycle -= DRAIN_TIMEOUT;
                this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT));
              }
            }
          }
          // LCOV_EXCL_START
          catch (const std::exception &e) {
            std::cerr << "Profile server exception: [clean] " << e.what() << endl;
          }
      // LCOV_EXCL_STOP
#ifndef VXPROD
          cout << "Cleanup thread ended\n";
#endif
        }));
  }

  service_threads.push_back(
      thread([&context, &server, &params] {
        auto timestamp_end = ::time(NULL) + params.shutdown;

        while (!server_done) {
          this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT));
          if (params.shutdown != 0) {
            auto timestamp_cur = ::time(NULL);
            // LCOV_EXCL_START
            if (timestamp_cur > timestamp_end) {
              context.Logger(LogLevel::none, "Automatic server shutdown");
              break;
            }
            // LCOV_EXCL_STOP
          }
        }

        if (!server_done || server.running()) {
          server_done = 1;
          this_thread::sleep_for(chrono::seconds(DRAIN_TIMEOUT + 5));
          server.stop();
          this_thread::sleep_for(chrono::seconds(5));
        }
#ifndef VXPROD
        if (params.shutdown != 0) {
          cout << "Shutdown thread ended\n";
        }
#endif
      }));

  server.listen();
  server_done = 1;

  // drain

  for (auto &t : service_threads) {
    if (t.joinable()) t.join();
  }

  cout << "Profile server stopped\n";
}

RunParams::RunParams(int argc, char **argv) {
  port = 8081;
  ports = 0;
  reg_ttl = 172800UL;
  shutdown = 0;
  clean = true;
  try {
    if (argc > 1) port = boost::lexical_cast<int>(argv[1]);
    if (argc > 2) ports = boost::lexical_cast<int>(argv[2]);
    if (argc > 3) reg_ttl = boost::lexical_cast<unsigned long>(argv[3]);
    if (argc > 4) shutdown = boost::lexical_cast<unsigned long>(argv[4]);

    // LCOV_EXCL_START
    if (argc > 5) {
      int iclean = 1;
      try {
        iclean = boost::lexical_cast<int>(argv[5]);
      }
      catch (const std::exception &e) {
        std::cerr << e.what() << '\n';
      }
      clean = (iclean != 0);
    }
    if (argc > 6) {
      backend = argv[6];
    }
    // LCOV_EXCL_STOP
  }
  // LCOV_EXCL_START
  catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
  }
  // LCOV_EXCL_STOP
}

int main(int argc, char **argv) {
  RunParams params(argc, argv);

  run(params);
}
