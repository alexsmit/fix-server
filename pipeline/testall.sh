#!/bin/bash

# pipeline script to test all dependencies

do_sync=1
test_projects=(token-server profile-server)
prep_projects=(token-server profile-server)

if [ -z "$BUILD_JOBS" ] ; then
    BUILD_JOBS=2
fi

curdir=$(pwd)

if [ ! -z "${BUILD_DEBUG}" ] ; then
    echo '+++ before prepare build on top level ***'
fi

./prepare_build.sh

if [ ! -z "${BUILD_DEBUG}" ] ; then
    echo '--- after prepare build on top level ***'
fi

pushd test >/dev/null
    ./prepare_build.sh -j $BUILD_JOBS run
popd >/dev/null

if [ ! -z "${BUILD_DEBUG}" ] ; then
    echo '--- after completed build ***'
fi

failed=()

pushd test >/dev/null
for dirr in ${test_projects[@]}
do
    isfailed=0
    pushd build/$dirr > /dev/null
	echo -n Running $dirr " ... "
	#echo ./$dirr-test - log file: $curdir/test-$dirr.lst
	./$dirr-test &> $curdir/test-$dirr.lst
	if [ $? -ne 0 ] ; then
	    isfailed=1
	fi
    popd > /dev/null

    if [ $isfailed -eq 1 ] ; then
#        if [ "$dirr" == "dbutil" ] ; then
#            echo FAILED WARNING
#        else
            failed+=( "$dirr" )
            echo FAILED
            printf "+++ test report for %s\n" $dirr
            cat $curdir/test-$dirr.lst
            printf "\n--- end of report\n"
#        fi
    else
	    echo OK
    fi
done
popd >/dev/null

if [ ${#failed[*]} -ne 0 ] ; then
    echo -n "Failed tests:"
    for key in "${failed[*]}"; do
	echo -n " $key"
    done
    echo " "
    return 1 2>/dev/null || exit 1
else
    echo All tests are successful
    return 0 2>/dev/null || exit 0
fi
