#!/bin/bash
sudo systemctl stop profile-server > /dev/null
ret1=$?
sudo systemctl stop token-server >/dev/null
ret2=$?

if [ $ret1 -ne 0 ] || [ $ret2 -ne 0 ]; then
    return 1 2>/dev/null || exit 1
fi
