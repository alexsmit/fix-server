#!/bin/bash

echo '*** RESTART services ***'

for (( a=0; a<3; a++ )) ; do
    sudo systemctl restart profile-server
    if [ $? -eq 0 ] ; then
        break
    fi
    sleep 2
    echo trying to restart profile-server
done

for (( a=0; a<3; a++ )) ; do
    sudo systemctl restart token-server
    if [ $? -eq 0 ] ; then
        break
    fi
    sleep 2
    echo trying to restart token-server
done

sudo systemctl status profile-server
sudo systemctl status token-server

if [ ! -z "$SSH_AGENT_PID" ] ; then
    kill -9 $SSH_AGENT_PID
fi

return 0 2>/dev/null || exit 0
