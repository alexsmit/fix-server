#!/bin/bash
if [ -z "$REPO_DIR" ] ; then
    echo REPO_DIR variable is not set
    return 1 2>/dev/null || exit 1
fi

pushd build >/dev/null
cp -u *.rpm $REPO_DIR
ret=$?
popd >/dev/null

return $ret 2>/dev/null || exit $ret
