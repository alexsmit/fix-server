#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/JsonConfig.h>
#include <vx/sql/DataProvider.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/sql/DataProviderConfig.h>

using namespace vx::sql;

namespace vx {
  namespace providers {

    /// remote versioon of profile provider
    class ProfileProviderRemote : public ProfileProvider, public DataProvider {
    private:
      DataProviderConfig providerConfig;  //!< config loaded from JSON file using AppConfig

      // virtual shared_ptr<ProfileResponse> ProfileCall(
      //     ptree& pt, string method, string url, bool capture = false, bool blob = false);

    public:
      ProfileProviderRemote();
      ~ProfileProviderRemote();

      // Service

      /// profile provider identifier for remote provider. some operations should be disabled based on this ID.
      string id() override;

      // DataProvider

      /// profile name (configuration hint)
      string Name() override { return "profile.remote"; }
      /// connect to data provider
      void Connect(JsonConfig& config) override;
      /// return provider version (hard-coded)
      int Version() override;

      // Roles

      /// Retrieve all roles
      vector<string> Roles() override;
      /// Create new role
      bool AddRole(const string& role, const string& description) override;
      /// Delete role. Note - all associated users will "lose" this role.
      bool DeleteRole(const string& role) override;

      /// create a user (shortcut api)
      long CreateUser(const string& userName, const string& email,
                      const string& password) override;

      /// update password for a login
      bool SetPassword(const string& userName, const string& password) override;
      /// set pin for a login
      bool SetPin(const string& userName, const string& pin) override;
      /// update a property for a login
      bool SetProperty(const string& userName, const string& flag,
                       const string& value) override;
      /// update list of roles
      bool SetRoles(const string& userName, set<string> roles) override;

      // GET
      /// read property for a specified login
      string GetProperty(const string& userName, const string& flag) override;
      /// load all properties for a provided login name
      map<string, string> GetProperties(const string& userName) override;
      /// load all roles for a provided login
      vector<string> Roles(const string& userName) override;
      /// get property for a role (only description is supported)
      string GetRoleProperty(const string& role, const string& propery) override;
      /// @brief return list of User records that satisfy search criteria. Search will occur over the allowed properites
      /// in the profile and any user property using AND condition.
      /// @param criteria 
      /// @param limit max number of records to return
      /// @return 
      vector<UserRecord> Search(const map<std::string, string>& criteria, unsigned int limit=1) override;

      /// @brief return list of User records that satisfy search criteria. Search will occur over the allowed properites
      /// in the profile and any user property using AND condition.
      /// @param criteria 
      /// @param limit max number of records to return
      /// @return 
      vector<UserRecord> SearchByCondition(const vx::sql::SearchCondition& criteria, unsigned int limit=1) override;

      // VALIDATE
      /// authenticate using login/password
      bool Authenticate(const string& userName, const string& password, UserRecord* user = NULL) override;
      /// authenticate using pin/password
      bool AuthenticatePin(const string& userName, const string& pin) override;
      // UserRecord AuthenticateResetToken(const string& token) override;

      // DELETE
      /// delete user by login
      bool DeleteUser(const string& userName) override;

      // USER

      /// load user record by login or uuid (check key value)
      UserRecord LoadUser(const string& userKey, ProfileKey key = ProfileKey::username) override;
      /// update user record, should contain either a username or userid to be able to update existing record
      bool SaveUser(UserRecord& record) override;

      // RESET TOKEN
      /// get password reset token
      string GetResetTokenByEmail(const string& email) override;
      /// update password using
      bool UpdatePassword(const string& resetToken, const string& password) override;
      /// check whether a reset token used or not
      bool VerifyResetToken(const string& resetToken) override;

      // UUID
      /// translate from UUID to login
      string GetUserName(const string& uid) override;

      // Registration
      /// Start a registration
      RegistrationResult RegisterUser(UserRecord& record) override;
      /// confirm registration with the token
      bool ConfirmRegistration(const string& token) override;
      /// check whether registration is in progress
      RegistrationStatus VerifyPendingRegistration(const string& token) override;
      /// delete all expired registrations
      void CleanRegistrations(unsigned long maxttl = DEFAULT_TTL_REGISTRATION) override;

      // Blobs
      /// set user blob
      bool SetBlob(string userName, string propName,
                   string content_type, string blob) override;
      /// retrieve user blob
      std::shared_ptr<PropertyBlob> GetBlob(string userName,
                                            string prop) override;

      /// Remote call to set a parameter (Warning: may crash the remote server)
      std::string GetParam(const std::string& param) override;

      /// Remate call to get a parameter
      void SetParam(const std::string& param, const std::string& val) override;

      /////////////////////////////
      // Dummy Overrides
      /////////////////////////////

      /// Dummy implementation
      bool IsSecure() override { return true; }  // LCOV_EXCL_LINE

      /// Dummy implementation
      void CloseConnection() override {}  // LCOV_EXCL_LINE

      /// Dummy implementation
      void Verify(bool useUpgrade = true) override {}  // LCOV_EXCL_LINE

      /// Dummy implementation
      void Upgrade(int version_start) override {}  // LCOV_EXCL_LINE
    };

  }  // namespace providers
}  // namespace vx
