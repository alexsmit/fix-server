/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <boost/property_tree/json_parser.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include "ProfileProviderRemote.h"
#include <vx/URL.h>
#include <vx/EncodingUtil.h>
#include <vx/StringUtil.h>

const string POST_METHOD = "POST";

using namespace vx::sql;
using namespace vx::providers;

//---------------------------------------------

namespace vx {
  namespace providers {
    /// profile provider response object
    struct ProfileResponse {
      SimpleWeb::StatusCode code;  //!< success status
      ptree data;                  //!< profile data
      string content_type;         //!< content type for a blob
      string blob;                 //!< blob data (if any)
    };

    shared_ptr<ProfileResponse> ProfileCall(
        DataProviderConfig &providerConfig,
        ptree &pt,
        string method,
        string url,
        bool capture = false,
        bool blob = false) {
      // cout << "Profile call host: " << host << " url: " << url << endl;

      shared_ptr<ProfileResponse> resp(new ProfileResponse);
      string json_request;
      SimpleWeb::CaseInsensitiveMultimap header;
      if (POST_METHOD == method) {
        stringstream s;
        write_json(s, pt);
        json_request = s.str();
        header.insert({"Content-Type", "application/json"});
      }
      URL u(providerConfig.remote_host);
      if (u.host.empty()) throw runtime_error("invalid profile server URL");

      header.insert({"X-Client-Id", providerConfig.client_id});
      header.insert({"X-Client-Secret", providerConfig.client_secret});

      auto f = [&](const string &code, SimpleWeb::CaseInsensitiveMultimap &header, const string &content) {
        resp->code = SimpleWeb::status_code(code);
        if (capture) {
          try {
            stringstream s(content);
            read_json(s, resp->data); /* code */
          }
          catch (const std::exception &e) {
          }
        }
        else if (blob) {
          auto it = header.find("Content-Type");
          if (it != header.end()) {
            resp->content_type = it->second;
            resp->blob = content;
          }
        }

        if (resp->code != SimpleWeb::StatusCode::success_ok) {
          string error_description = resp->data.get("error_description", "");
          if (!error_description.empty()) throw runtime_error(error_description);
        }
      };

      string canonical_host = u.getHost(false);
      // try {
      if (u.protocol == "http") {
        shared_ptr<HttpClient> client(new HttpClient(canonical_host));
        auto result = client->request(method, url, json_request, header);
        f(result->status_code, result->header, result->content.string());
      }
      // NOTE: tested in ProfileServer Suite
      else {
        shared_ptr<HttpsClient> client(new HttpsClient(canonical_host, providerConfig.verify_ssl));
        auto result = client->request(method, url, json_request, header);
        f(result->status_code, result->header, result->content.string());
      }
      //       }
      //       catch (const std::exception &e) {
      // #ifndef NDEBUG
      //         std::cerr << e.what() << '\n';
      // #endif
      //         throw;
      //       }

      return resp;
    }

  }  // namespace providers

  static string remote_provider_error = "remote_error";
}  // namespace vx

//---------------------------------------------

ProfileProviderRemote::ProfileProviderRemote() {}

ProfileProviderRemote::~ProfileProviderRemote() {}

string ProfileProviderRemote::id() { return "remote"; }

void ProfileProviderRemote::Connect(JsonConfig &_config) {
  config = _config;
  config.getTree("") >> providerConfig;
}

vector<string> ProfileProviderRemote::Roles() {
  vector<string> roles;
  ptree pt;
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", "/roles", true);
    for (auto &kv : resp->data.get_child("roles")) {
      roles.push_back(kv.second.data());
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("Roles failed: ") + e.what();
  }

  return roles;
}

bool ProfileProviderRemote::AddRole(const string &role, const string &description) {
  ptree pt;
  pt.add("description", description);
  try {
    auto res = ProfileCall(providerConfig, pt, "POST", string("/roles/") + role);
    return res->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("AddRole failed: ") + e.what();
    return false;
  }
}

bool ProfileProviderRemote::DeleteRole(const string &role) {
  ptree pt;
  try {
    auto res = ProfileCall(providerConfig, pt, "DELETE", string("/roles/") + role);
    return res->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("DeleteRole failed: ") + e.what();
    return false;
  }
}

long ProfileProviderRemote::CreateUser(const string &userName, const string &email, const string &password) {
  ptree pt;
  pt.put("username", userName);
  pt.put("password", password);
  pt.put("email", email);
  try {
    auto result = ProfileCall(providerConfig, pt, "POST", "/api/v1/users");
    return (result->code == SimpleWeb::StatusCode::success_ok) ? 1 : 0;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("CreateUser failed: ") + e.what();
    return 0;
  }
}

bool ProfileProviderRemote::SetPassword(const string &userName, const string &password) {
  auto rec = LoadUser(userName);
  if (rec.userid.empty()) return false;

  ptree pt;
  pt.put("password", password);
  try {
    string url = (boost::format("/api/v1/users/%s") % rec.userid).str();
    auto resp = ProfileCall(providerConfig, pt, "POST", url);
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  // LCOV_EXCL_START
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetPassword failed: ") + e.what();
    return false;
  }
  // LCOV_EXCL_STOP
}

bool ProfileProviderRemote::SetPin(const string &userName, const string &pin) {
  auto rec = LoadUser(userName);
  if (rec.userid.empty()) return false;

  ptree pt;
  pt.put("pin", pin);
  try {
    string url = (boost::format("/api/v1/users/%s") % rec.userid).str();
    auto resp = ProfileCall(providerConfig, pt, "POST", url);
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  // LCOV_EXCL_START
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetPin failed: ") + e.what();
    return false;
  }
  // LCOV_EXCL_STOP
}

bool ProfileProviderRemote::SetProperty(const string &userName, const string &flag, const string &value) {
  ptree pt;
  pt.put(flag, value);
  try {
    string url = (boost::format("/users/%s/properties") % userName).str();
    auto res = ProfileCall(providerConfig, pt, "POST", url);
    return res->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetProperty failed: ") + e.what();
    return false;
  }
}

bool ProfileProviderRemote::SetRoles(const string &userName, set<string> roles) {
  UserRecord rec = {0};
  for (auto &r : roles) {
    rec.roles.push_back(r);
  }
  ptree pt = rec.toPtree();
  try {
    auto res = ProfileCall(providerConfig, pt, "POST", string("/users/") + userName);
    return res->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetRoles failed: ") + e.what();
    return false;
  }
}

string ProfileProviderRemote::GetProperty(const string &userName, const string &flag) {
  ptree pt;
  string val;
  try {
    string url = (boost::format("/users/%s/properties/%s") % userName % flag).str();
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    val = resp->data.get(flag, "");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetProperty failed: ") + e.what();
  }
  return val;
}

map<string, string> ProfileProviderRemote::GetProperties(const string &userName) {
  ptree pt;
  map<string, string> result;
  try {
    string url = (boost::format("/users/%s/properties") % userName).str();
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    for (auto &kv : resp->data.get_child("properties")) {
      result.insert({kv.first, kv.second.data()});
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetProperties failed: ") + e.what();
    // std::cerr << "GetProperties failed: " << e.what() << '\n';
  }

  return result;
}

vector<string> ProfileProviderRemote::Roles(const string &userName) {
  ptree pt;
  vector<string> roles;
  string url = (boost::format("/users/%s") % userName).str();
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    UserRecord rec;
    rec.fromJSON(resp->data);
    roles = rec.roles;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("Roles failed: ") + e.what();
    // std::cerr << "Roles failed: " << e.what() << '\n';
  }
  return roles;
}

string ProfileProviderRemote::GetRoleProperty(const string &role, const string &propery) {
  ptree pt;
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", string("/roles/") + role, true);
    return resp->data.get(propery, "");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetRoleProperty failed: ") + e.what();
    // std::cerr << "GetRoleProperty failed: " << e.what() << '\n';
    return "";
  }
}

bool ProfileProviderRemote::Authenticate(const string &userName, const string &password, UserRecord *user) {
  ptree pt;
  pt.put("password", password);
  pt.put("username", userName);
  pt.put("grant_type", "password");
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", string("/authenticate"), true);
    bool ok = (resp->code == SimpleWeb::StatusCode::success_ok);
    if (user != NULL) {
      if (ok)
        user->fromJSON(resp->data);
      else
        user->clear();
    }
    return ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("Authenticate failed: ") + e.what();
    // std::cerr << "Authenticate failed: " << e.what() << '\n';
    if (user != NULL) user->clear();
    return false;
  }
}

bool ProfileProviderRemote::AuthenticatePin(const string &userName, const string &pin) {
  ptree pt;
  pt.put("pin", pin);
  pt.put("username", userName);
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", string("/pin"), true);
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("AuthenticatePin failed: ") + e.what();
    // std::cerr << "AuthenticatePin failed: " << e.what() << '\n';
    return false;
  }
}

// UserRecord ProfileProviderRemote::AuthenticateResetToken(const string &token) {
//   throw runtime_error("not implemented or not ");
// }

bool ProfileProviderRemote::DeleteUser(const string &userName) {
  ptree pt;
  try {
    auto resp = ProfileCall(providerConfig, pt, "DELETE", string("/users/") + userName);
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("DeleteUser failed: ") + e.what();
    // std::cerr << "DeleteUser failed: " << e.what() << '\n';
    return false;
  }
}

UserRecord ProfileProviderRemote::LoadUser(const string &userKey, ProfileKey key) {
  UserRecord rec = {0};
  string url;
  bool array_return = true;
  switch (key) {
    case ProfileKey::username:
      url = (boost::format("/api/v1/users?username=%s") % SimpleWeb::Percent::encode(userKey)).str();
      break;
    case ProfileKey::email:
      url = (boost::format("/api/v1/users?email=%s") % SimpleWeb::Percent::encode(userKey)).str();
      break;
    case ProfileKey::userid:
      array_return = false;
      url = (boost::format("/api/v1/users/%s") % SimpleWeb::Percent::encode(userKey)).str();
      break;

      // default:
      //   return rec;
      //   break;
  }

  try {
    ptree pt;
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    if (array_return) {
      if (resp->data.size() == 1) {
        for (auto &c : resp->data) {
          rec.fromJSON(c.second);
          break;
        }
      }
    }
    else
      rec.fromJSON(resp->data);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("LoadUser failed: ") + e.what();
  }
  return rec;
}

bool ProfileProviderRemote::SaveUser(UserRecord &record) {
  string url = string("/api/v1/users");
  if (!record.userid.empty()) {
    url += string("/") + record.userid;
  }

  ptree pt = record.toPtree();
  // string sjson = record.toJSON();
  // stringstream s(sjson);
  // read_json(s, pt);
  try {
    auto res = ProfileCall(providerConfig, pt, "POST", url);
    return res->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SaveUser failed: ") + e.what();
    return false;
    // std::cerr << "SaveUser failed: " << e.what() << '\n';
  }
}

// RESET TOKEN
string ProfileProviderRemote::GetResetTokenByEmail(const string &email) {
  ptree pt;
  pt.put("email", email);
  string token;
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", "/password/reset", true);
    if (resp->code == SimpleWeb::StatusCode::success_ok) {
      token = resp->data.get("token", "");
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetResetTokenByEmail failed: ") + e.what();
    // std::cerr << "GetResetTokenByEmail failed: " << e.what() << '\n';
  }

  return token;
}

bool ProfileProviderRemote::UpdatePassword(const string &resetToken, const string &password) {
  ptree pt;
  pt.put("token", resetToken);
  pt.put("password", password);
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", "/password/update");
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("UpdatePassword failed: ") + e.what();
    // std::cerr << "UpdatePassword failed: " << e.what() << '\n';
    return false;
  }
}

bool ProfileProviderRemote::VerifyResetToken(const string &resetToken) {
  ptree pt;
  pt.put("token", resetToken);
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", "/password/verifytoken");
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("VerifyResetToken failed: ") + e.what();
    // std::cerr << "VerifyResetToken failed: " << e.what() << '\n';
  }

  return false;
}

// UUID
string ProfileProviderRemote::GetUserName(const string &uid) {
  if (uid.empty()) {
    throw invalid_argument("Invalid uuid");
  }

  ptree pt;
  string username;
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", string("/userid/") + uid, true);
    username = resp->data.get("username", "");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetUserName failed: ") + e.what();
    // std::cerr << "GetUserName failed: " << e.what() << '\n';
  }
  return username;
}

// Registration
RegistrationResult ProfileProviderRemote::RegisterUser(UserRecord &record) {
  ptree pt;
  pt.put("username", record.username);
  pt.put("password", record.password);
  pt.put("email", record.email);
  pt.put("pin", record.pin);

  RegistrationResult reg;
  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", string("/register"), true);
    reg.registration_code = resp->data.get("registration_code", "");
    reg.reset_token = resp->data.get("reset_token", "");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("RegisterUser failed: ") + e.what();
    // std::cerr << "RegisterUser failed: " << e.what() << '\n';
  }

  return reg;
}

bool ProfileProviderRemote::ConfirmRegistration(const string &token) {
  ptree pt;
  string url = (boost::format("/register/confirm?code=%s") % token).str();
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    return (resp->code == SimpleWeb::StatusCode::success_ok);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("ConfirmRegistration failed: ") + e.what();
    // std::cerr << "ConfirmRegistration failed: " << e.what() << '\n';
    return false;
  }
}

RegistrationStatus ProfileProviderRemote::VerifyPendingRegistration(const string &token) {
  ptree pt;
  pt.put("registration_code", token);

  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", "/register/validate", true);
    if (resp->code == SimpleWeb::StatusCode::success_ok) {
      string status = resp->data.get("status", "");
      if (status == "completed") return RegistrationStatus::completed;
      if (status == "pending") return RegistrationStatus::pending;
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("VerifyPendingRegistration failed: ") + e.what();
    // std::cerr << "VerifyPendingRegistration failed: " << e.what() << '\n';
  }

  return RegistrationStatus::invalid;
}

void ProfileProviderRemote::CleanRegistrations(unsigned long maxttl) {
  string sttl = std::to_string(maxttl);
  ptree pt;
  pt.put("ttl", sttl);

  try {
    auto resp = ProfileCall(providerConfig, pt, "POST", "/register/clean");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("CleanRegistrations failed: ") + e.what();
    // cerr << "CleanRegistrations failed: " << e.what() << endl;
  }
}

// Blobs
bool ProfileProviderRemote::SetBlob(string userName, string propName, string content_type, string blob) {
  ptree pt;
  pt.put("content_type", content_type);
  pt.put("encoding_type", "base64");
  pt.put("blob", random_util().base64encode(blob));

  try {
    string url = (boost::format("/users/%s/blob/%s") % userName % propName).str();
    auto resp = ProfileCall(providerConfig, pt, "POST", url);
    return resp->code == SimpleWeb::StatusCode::success_ok;
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetBlob failed: ") + e.what();
    // cerr << "SetBlob failed: " << e.what() << endl;
    return false;
  }
}

std::shared_ptr<PropertyBlob> ProfileProviderRemote::GetBlob(string userName, string prop) {
  shared_ptr<PropertyBlob> pblob(new PropertyBlob());
  ptree pt;
  string url = (boost::format("/users/%s/blob/%s") % userName % prop).str();
  try {
    auto resp = ProfileCall(providerConfig, pt, "GET", url, false, true);
    if (resp->code == SimpleWeb::StatusCode::success_ok) {
      pblob->content_type = resp->content_type;
      pblob->data = resp->blob;
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetBlob failed: ") + e.what();
    // cerr << "GetBlob failed: " << e.what() << endl;
  }
  return pblob;
}

int ProfileProviderRemote::Version() {
  int version = 0;
  try {
    ptree pt;
    auto resp = ProfileCall(providerConfig, pt, "GET", "/api/v1/info", true);
    auto sver = resp->data.get("database_version", "");
    version = boost::lexical_cast<int>(sver);
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("Version failed: ") + e.what();
    // cerr << "Version failed: " << e.what() << endl;
  }
  return version;
}

std::string ProfileProviderRemote::GetParam(const std::string &param) {
  string val;
  if (param.empty()) return val;
  try {
    ptree pt;
    string url = (boost::format("/api/v1/param?name=%s") % StringUtil::url_encode(param)).str();
    auto resp = ProfileCall(providerConfig, pt, "GET", url, true);
    val = resp->data.get(param, "");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("GetParam failed: ") + e.what();
    // cerr << "GetParam failed: " << e.what() << endl;
  }
  return val;
}  // LCOV_EXCL_LINE

void ProfileProviderRemote::SetParam(const std::string &param, const std::string &val) {
  try {
    if (param.empty()) return;
    ptree pt;
    pt.put(param, val);
    ProfileCall(providerConfig, pt, "POST", "/api/v1/param");
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("SetParam failed: ") + e.what();
    // cerr << "SetParam failed: " << e.what() << endl;
  }
}

vector<UserRecord> ProfileProviderRemote::SearchByCondition(const vx::sql::SearchCondition &criteria, unsigned int limit) {
  vector<UserRecord> result;

  auto s = criteria.str();

  string url = string("/api/v1/users?search=") + StringUtil::url_encode(s);

  try {
    ptree pt;
    auto res = ProfileCall(providerConfig, pt, "GET", url, true);

    // write_json(cout, res->data, true);

    for (auto &p : res->data) {
      UserRecord rec;
      rec.fromJSON(p.second);
      if (rec.userid.empty()) continue;
      result.push_back(rec);
    }
  }
  catch (const std::exception &e) {
    error = remote_provider_error;
    error_description = string("Search failed: ") + e.what();
    // cerr << "Search failed: " << e.what() << endl;
  }

  return result;
}

vector<UserRecord> ProfileProviderRemote::Search(const map<std::string, string> &criteria, unsigned int limit) {
  vector<UserRecord> result;
  if (criteria.empty()) return result;

  vx::sql::SearchCondition conds("");
  for (auto &kv : criteria) {
    conds.add(kv.first, kv.second);
  }

  return SearchByCondition(conds, limit);
}
