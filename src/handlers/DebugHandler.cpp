/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>
#include "DebugHandler.h"
#include <vx/web/Util.h>

using namespace VxServer::Handlers;

DebugHandler::DebugHandler(ServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
#ifndef NDEBUG
  server
      .use("^/debug$", Request::RequestType::GET,
           std::bind(&DebugHandler::Debug, *this, std::placeholders::_1, std::placeholders::_2))
      .use("^/debug$", Request::RequestType::POST,
           std::bind(&DebugHandler::Debug, *this, std::placeholders::_1, std::placeholders::_2))

      ;
#endif
}

DebugHandler::~DebugHandler() {}

void DebugHandler::Debug(std::shared_ptr<Response> response, std::shared_ptr<Request> request) {
  if (!context.validateRequest(*response, *request)) return;
  ptree pt;
  pt.put<string>("success", "ok");
  sendJSON(response, pt);
}
