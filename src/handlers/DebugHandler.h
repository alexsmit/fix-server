#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>

//using namespace VxServer;

namespace VxServer {
  namespace Handlers {
    /// debug endpoint, added only in debug build
    class DebugHandler {
    private:
      ServerContext& context;
      WebServer& server;
      Instance& secure_server;

    public:
      /// create debug handler
      DebugHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      /// destructor. noop
      ~DebugHandler();

      /// /debug endpoint
      void Debug(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer
