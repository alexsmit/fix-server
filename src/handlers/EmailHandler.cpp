/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "EmailHandler.h"
#include "Util.h"
#include "SimpleMail.h"

#include <nlohmann/json.hpp>
#include <vx_environment.h>

using json = nlohmann::json;

using namespace VxServer::Handlers;

EmailHandler::EmailHandler(ServerContext &context, WebServer &server, Instance &secure_server)
    : context(context),
      server(server),
      secure_server(secure_server)

{
  auto validate = [&](std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
    context.validateRequest(*res, *req);
  };

  secure_server.with(validate)
      .use("^/email/send$", Request::RequestType::POST,
           std::bind(&EmailHandler::Send, *this, std::placeholders::_1, std::placeholders::_2))

      .with(validate)
      .use("^/email/sendasync$", Request::RequestType::POST,
           std::bind(&EmailHandler::SendAsync, *this, std::placeholders::_1, std::placeholders::_2))

      ;
}

EmailHandler::~EmailHandler() {}

bool EmailHandler::SendInternal(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool sendResponse) {
  string from = req->body["from"];
  string to = req->body["to"];
  string subject = req->body["subject"];
  string tmpl = req->body["template"];
  if (from.empty() || to.empty() || subject.empty() || tmpl.empty()) {
    string msg = "Invalid header data";
    cerr << "email_error: " << msg << endl;
    sendError(res, "email_error", msg, SimpleWeb::StatusCode::client_error_bad_request);
    return false;
  }

  auto data = req->body.value().get_child_optional("data");
  json emaildata;
  if (!!data) {
    for (auto &kv : *data) {
      auto sz = kv.second.size();
      if (sz > 0) {
        vector<string> vals;
        for(auto &av: kv.second) {
          vals.push_back(av.second.data());
        }
        emaildata[kv.first] = vals;
      } else {
        emaildata[kv.first] = kv.second.data();
      }
    }
  }

  vector<inja::VxAttachment> attachments;

  auto child_attachments = req->body.value().get_child_optional("attachments");
  if (!!child_attachments) {
    for (auto &vv : *child_attachments) {
      inja::VxAttachment att;
      att.id = vv.first;
      att.content_type = vv.second.get("content-type", "");
      att.filename = vv.second.get("filename", "");
      att.data = vv.second.get("data", "");
      attachments.push_back(att);
    }
  }

  inja::VxEnvironment env("data/");
  string message;

  try {
    message = env.render_multipart(tmpl, emaildata, attachments);
  }
  catch (const std::exception &e) {
    sendError(res, "email_error", e.what(), SimpleWeb::StatusCode::server_error_internal_server_error);
    cerr << "email_error: " << e.what() << endl;
    return false;
  }

  if (env.GetErrorsCount() > 0) {
    stringstream serr;
    auto cnt = 1;
    serr << "count: " << env.GetErrorsCount() << endl;
    for (auto &e : env.GetErrorsMessages()) {
      serr << cnt++ << ": " << e << endl;
    }
    sendError(res, "email_error", serr.str(), SimpleWeb::StatusCode::server_error_internal_server_error);
    cerr << "email_error: " << serr.str() << endl;
    return false;
  }

  if (sendResponse) {
    SendSMTP(res, from, to, subject, message);
  }
  else {
    thread t(
        [&](string from, string to, string subject, string message) {
          shared_ptr<Response> dummy;
          SendSMTP(dummy, from, to, subject, message);
        },
        from, to, subject, message);
    t.detach();
  }

  return true;
}

void EmailHandler::SendSMTP(std::shared_ptr<Response> res, std::string from, std::string to, std::string subject,
                            std::string message) {
  SimpleMail sm(from, to /*, true, true*/);
  if (!sm.Send(subject, message)) {
    cerr << "email_error: " << sm.ExitErrorMessage() << endl;
    if (res)
      sendError(res, "email_error", sm.ExitErrorMessage(), SimpleWeb::StatusCode::server_error_internal_server_error);
    return;
  }

  if (res) res->write("OK");
}

void EmailHandler::SendAsync(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  SendInternal(res, req, false);
  if (!res->IsDone()) res->write("OK");
}

void EmailHandler::Send(std::shared_ptr<Response> res, std::shared_ptr<Request> req) { SendInternal(res, req, true); }
