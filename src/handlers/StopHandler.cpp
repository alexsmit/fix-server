/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/variant.hpp>

#include "StopHandler.h"
#include <vx/web/Util.h>

StopHandler::StopHandler(ServerContext &context, WebServer &server, Instance &secure_server)
    : context(context), server(server), secure_server(secure_server)

{
#ifndef NDEBUG
  server
      .use("^/stop$", Request::RequestType::POST,
           std::bind(&StopHandler::Stop, *this, std::placeholders::_1, std::placeholders::_2))

      ;
#endif
}

StopHandler::~StopHandler() {}

void StopHandler::Stop(std::shared_ptr<Response> res, std::shared_ptr<Request> req) {
  if (!context.validateRequest(*res, *req)) return;
  string magic = req->body["magic"];
  if (magic != "hello") {
    badRequest(res);
    return;
  }
  res->write("");

  // async stop

  thread t([&](){ // LCOV_EXCL_LINE
    this_thread::sleep_for(chrono::milliseconds(500));
    server.stop();
  });
  t.detach();
}
