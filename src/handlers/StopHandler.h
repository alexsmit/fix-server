#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"
#include <vx/Auth.h>
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>

using namespace VxServer;

/// stop handler. allows to stop the server via API
class StopHandler {
private:
  ServerContext& context; //!< current server context
  WebServer& server; //!< reference to a web server
  Instance& secure_server; //!< reference to secure web interface

public:
  /// /stop handler object
  StopHandler(ServerContext& context, WebServer& server, Instance& secure_server);
  /// destructor. noop
  ~StopHandler();

  /// process /stop endpoint
  void Stop(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
};
