/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _EMAIL_HANDLER_H
#define _EMAIL_HANDLER_H

#include "../stdafx.h"
#include "Auth.h"
#include <WebServer.h>
#include "ServerContext.h"

using namespace VxServer;
namespace VxServer {
  namespace Handlers {

    /// Handler to send emails using templates
    class EmailHandler {
    private:
      ServerContext& context;
      WebServer& server;
      Instance& secure_server;

      bool SendInternal(std::shared_ptr<Response> res, std::shared_ptr<Request> req, bool sendResponse = true);
      void SendSMTP(std::shared_ptr<Response> res, std::string from, std::string to, std::string subject,
                    std::string message);

    public:
      /// email handler instance, uses a current server context, server and secure server (HTTPS)
      EmailHandler(ServerContext& context, WebServer& server, Instance& secure_server);
      ~EmailHandler();

      /**
    Send email
    {
      from: "email",                //or empty to use default email
      to: "email",
      subject: "subject",
      template: "template name",
      format: "txt|html|multipart", // default - multipart if avaiable
      data: {
        key: value // set of parameters to pass to template
      },
      attachments: {
        "smile@here": {
          "content-type": "image/gif"
        },
        "frown@here": {
          "content-type": "image/gif"
        },
        "file": {
          "content-type": "text/plain",
          "filename": "test.txt",
          "data": "aGVsbG8gaW5qYQo="
        }
      }
    }
  */
      void Send(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
      /// Similar to Send, but runs asynchronously. If send mail command is not successful that won't be captured.
      void SendAsync(std::shared_ptr<Response> res, std::shared_ptr<Request> req);
    };

  }  // namespace Handlers
}  // namespace VxServer
#endif