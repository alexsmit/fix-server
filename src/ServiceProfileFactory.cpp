/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <boost/variant.hpp>
#include <boost/property_tree/json_parser.hpp>

#if defined(VX_USE_MYSQL)
#include <vx/sql/ProfileProviderMySQL.h>
#include <vx/sql/DataProviderMySQL.h>
#endif

#if defined(VX_USE_SOCI)
#include <vx/sql/ProfileProviderSOCI.h>
#include <vx/sql/DataProviderSoci.h>
#endif

// #include <client_http.hpp>
// #include <client_https.hpp>
// using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
// using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

#include <ProfileProviderRemote.h>

#include <vx/ServiceProfileFactory.h>

using namespace vx;
using namespace vx::sql;
using namespace vx::providers;

ServiceProfileFactory::ServiceProfileFactory(time_t ttl, const string& _conf) {
  provider_ttl = ttl;
  string conf = (_conf.empty()) ? "profile_provider" : _conf;
  auto app = AppConfig::getInstancePtr();
  section = app->SectionPtr(conf);
}

std::string ServiceProfileFactory::id() const {
  return "profile";
}

std::shared_ptr<Service> ServiceProfileFactory::createService(const std::string& _type) {
  std::lock_guard<std::recursive_mutex> lock{mtx};

  shared_ptr<ProfileProvider> profile_provider;

  // auto p = (unsigned long) AppConfig::getInstancePtr().get();
  // cout << "getService: profile service: config ptr: " << p << '\n';
  // auto &pt = section->getTree("");
  // cout << "getService: service factory type = " << pt.get("type","") << '\n';
  // write_json(cout, pt);

  string type = _type;
  if (type.empty()) {
    type = section->getParam("type");
  }
  auto param = section->getParam("send_email");
  auto key = string("config.") + type;
  auto profile_config = section->getParam(key.c_str());

  DataProviderConfig providerConfig;
  AppConfig& conf = AppConfig::getInstance();
  conf.Section(profile_config).getTree("") >> providerConfig;

  if (providerConfig.db_type.empty())
    throw invalid_argument(
        "Invalid profile provider database type [type]=mysql|remote");

  if (providerConfig.db_type == "remote") {
    // if (profile_config.empty()) profile_config = "profile.remote";
    profile_provider = factory.Connect<ProfileProviderRemote>(profile_config.c_str());
    return profile_provider;
  }

  if (providerConfig.db_type == "mysql") {
#if defined(VX_USE_MYSQL)
    if (providerConfig.db_backend == "mysql") {
      // if (profile_config.empty()) profile_config = "profile.mysql";
      profile_provider = factory.Connect<ProfileProviderMySQL>(profile_config.c_str());
      return profile_provider;
    }
#else
    providerConfig.db_backend = "soci";
#endif

#if defined(VX_USE_SOCI)
    if (providerConfig.db_backend == "soci") {
      // if (profile_config.empty()) profile_config = "profile.mysql";
      profile_provider = factory.Connect<ProfileProviderSOCI>(profile_config.c_str());
      return profile_provider;
    }
#endif
  }

  throw invalid_argument("Invalid profile service type [profile_provider.type]=mysql|remote");
}
