/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "../stdafx.h"

#include <boost/variant.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <vx/PtreeUtil.h>

#include "BaseTest.h"
#include "WebTest.h"

using namespace std;
using namespace vx::openid::test;

WebTest::WebTest() {}

WebTest::~WebTest() {}

void WebTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
}

void WebTest::tearDown() {
}

/// should get 404 for non-existent url
void WebTest::testBadURL() {
  auto result = client->request("GET", "/some_invalid_url", "");
  string res = result.data;

  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 404", string("404"), code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should get Object Not Found", string("File not found"), res);
}

#ifndef NDEBUG

/// hit /debug endpoint with client/sceret in the header
void WebTest::testClientHeader() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  try {
    auto result = client->request("GET", "/debug", "", header);
    ptree pt = result.result;
    string ok = pt.get<string>("success", "");
    CPPUNIT_ASSERT_EQUAL_MESSAGE("ClientHeader: Should get success message", string("ok"), ok);
    CPPUNIT_ASSERT_MESSAGE("Should get 1 element", pt.size() == 1);
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
    std::cerr << "DEBUG host - " << DEBUG_HOST << endl;
    exit(1);
  }
}

/// hit /debug endpoint w/o client/secret in the header
void WebTest::testClientNoHeader() {
  auto result = client->request("GET", "/debug", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);

  ptree pt = result.result;
  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Missing client id or secret"), error_description);
}

/// hit /debug endpoint with wrong client/secret in the header
void WebTest::testClientWrongHeader() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", "wrong_id"});
  header.insert({"X-Client-Secret", "wrong_secret"});

  auto result = client->request("GET", "/debug", "", header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 401", string("401"), code);

  ptree pt = result.result;

  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), error_description);
}

/// post sample form to /debug, client/secret are provided in the form post data
void WebTest::testPostForm() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});

  string form = "some=data&client_id=aaa&client_secret=111";
  auto result = client->request("POST", "/debug", form, header);
  ptree pt = result.result;
  string ok = pt.get<string>("success", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("PostFOrm: Should get success message", string("ok"), ok);
  CPPUNIT_ASSERT_MESSAGE("Should get 1 element", pt.size() == 1);
}

/// post form to /debug w/o client/secret
void WebTest::testPostFormNoAuth() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});

  string form = "some=data";
  auto result = client->request("POST", "/debug", form, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);

  ptree pt = result.result;

  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Missing client id or secret"), error_description);
}

/// post form to /debug with wrong client/secret
void WebTest::testPostFormWrongAuth() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});
  string form = "some=data&client_id=wrong_client&client_secret=wrong_secret";
  auto result = client->request("POST", "/debug", form, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 401", string("401"), code);

  ptree pt = result.result;

  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), error_description);
}

/// ok path, client id/secret in the json body
void WebTest::testPostJson() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});

  ptree pt_json;
  pt_json.put("some", "data");
  pt_json.put("client_id", "aaa");
  pt_json.put("client_secret", "111");
  stringstream ss;
  write_json(ss, pt_json);

  string sjson = ss.str();
  auto result = client->request("POST", "/debug", sjson, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);

  ptree pt = result.result;

  string ok = pt.get<string>("success", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Post JSON: Should get success message", string("ok"), ok);
  CPPUNIT_ASSERT_MESSAGE("Should get 1 element", pt.size() == 1);
}

/// client_id/secret not provided in the json body
void WebTest::testPostJsonNoAuth() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});

  ptree pt_json;
  pt_json.put("some", "data");
  stringstream ss;
  write_json(ss, pt_json);

  string sjson = ss.str();
  auto result = client->request("POST", "/debug", sjson, header);

  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 400", string("400"), code);

  ptree pt = result.result;

  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Missing client id or secret"), error_description);
}

/// using wrong client_id/secret in the json body
void WebTest::testPostJsonWrongAuth() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});

  ptree pt_json;
  pt_json.put("some", "data");
  pt_json.put("client_id", "wrong_client");
  pt_json.put("client_secret", "wrong_secret");
  stringstream ss;
  write_json(ss, pt_json);

  string sjson = ss.str();
  auto result = client->request("POST", "/debug", sjson, header);

  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail with 401", string("401"), code);

  ptree pt = result.result;

  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), error_description);
}

#endif

/// using wrong client_id/secret in the json body
void WebTest::testSearchEmpty() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  auto result = client->request("GET", "/api/v1/users?search=username", "", header);

  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("ok", string("200"), code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("responsi is empty array", string("[]"), result.data);

  // auto error = pt.get("error", "");
  // auto error_description = pt.get("error_description", "");
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), error);
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), error_description);
}

/// able to access root "/" path
void WebTest::testIndex() {
  auto result = client->request("GET", "/", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");
  auto content = result.data;
  auto html = content.substr(0, 6);
  CPPUNIT_ASSERT_MESSAGE("Should have <html>", html == "<html>");
}

/// able to get "index.html"
void WebTest::testIndexFile() {
  auto resultFile = client->request("GET", "/index.html", "");
  auto code = resultFile.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  auto result = client->request("GET", "/", "");

  auto contentFile = resultFile.data;
  auto content = result.data;
  CPPUNIT_ASSERT_MESSAGE("Should have same response for / and /index.html",
                         content == contentFile);
}

/// able to retrieve /info endpoint
void WebTest::testInfo() {
  auto result = client->request("GET", "/info", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  auto str = result.data;
  auto html = str.substr(0, 6);
  CPPUNIT_ASSERT_MESSAGE("Should have <html>", html == "<html>");
}

/// bad request path, wrong payload to /stop
void WebTest::testStopBadRequest() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});

  ptree pt_json;
  pt_json.put("foo", "bar");
  stringstream ss;
  write_json(ss, pt_json);

  string sjson = ss.str();
  auto result = client->request("POST", "/stop", sjson, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);

  ptree pt = result.result;

  // string ok = pt.get<string>("success", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error message", string("bad_request"), pt.get("error",""));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error description", string(""), pt.get("error_description",""));
}


/// able to retrieve /version endpoint
void WebTest::testVersion() {
  auto result = client->request("GET", "/version", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  ptree pt = result.result;

#if defined(SERVER_TOKEN)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("service>", string("token-server"), pt.get("service", ""));
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("service>", string("profile-server"), pt.get("service", ""));
#endif
  CPPUNIT_ASSERT_EQUAL_MESSAGE("service>", string(PROJECT_GIT_TAG), pt.get("version", ""));
}

#if defined(SERVER_TOKEN)
/// able to retrieve /help endpoint
void WebTest::testHelp() {
  auto result = client->request("GET", "/help", "");
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  ptree pt = result.result;

  auto str = result.data;
  auto html = str.substr(0, 6);
  CPPUNIT_ASSERT_MESSAGE("Should have <html>", html == "<html>");

  // one sub-link

  client->request("GET", "/help/reg", "");
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  pt = result.result;

  str = result.data;
  html = str.substr(0, 6);
  CPPUNIT_ASSERT_MESSAGE("Should have <html>", html == "<html>");

  // one sub-link (non-existing)

  client->request("GET", "/help/somenonexistingpage", "");
  code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should be successful", code == "200");

  pt = result.result;

  str = result.data;
  html = str.substr(0, 6);
  CPPUNIT_ASSERT_MESSAGE("Should have <html>", html == "<html>");
}
#endif

//*******************************************************
//*******************************************************
//*******************************************************

CPPUNIT_TEST_SUITE_REGISTRATION(WebTest);
