/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/variant.hpp>

#include <vx/PtreeUtil.h>
#include <vx/web/WebServer.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/URL.h>
#include <vx/EncodingUtil.h>

#include <ProfileProviderRemote.h>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <BaseTest.h>
#include "ProfileRemoteTest.h"

#include <InitProfileTest.h>

#if defined(SERVER_TOKEN)
#include <vx/ServiceProfileFactory.h>
#endif

using namespace std;
using namespace vx::openid::test;
using namespace vx::providers;

#define REG_TTL 3

ProfileRemoteTest::ProfileRemoteTest() {}
ProfileRemoteTest::~ProfileRemoteTest() {}

void ProfileRemoteTest::setUp() {
  AppConfig::resetInstance();
  setUpInternal();
}

void ProfileRemoteTest::setUpInternal() {
#if defined(SERVER_TOKEN)
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("remote"));
  context->setParam("profile_provider.type", "remote");

  provider_remote = context->GetProfileProvider();

  provider = ConnectProfileProvider(factory, "profile.mysql", backend);
#else
  context.reset(new ProfileServerContext(3));
  context->Init(NULL, "config.json");
  // provider = factory.Connect<ProfileProviderMySQL>("profile.mysql");

  factory.reset(new DataFactory());
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);
  provider_remote = ConnectProfileProvider(factory, "profile.remote", "remote");

  URL u(DEBUG_HOST);
  // u.port = "1111";
  // cout << DEBUG_HOST << " -> " << u.getHost() << endl;

  context->setParam("profile.remote.remote_host", u.getHost());
  // provider_remote = factory->Connect<ProfileProviderRemote>("profile.remote");
#endif

  provider->DeleteUser(USER_NAME);
  provider->DeleteUser(USER_NAME2);

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  UserRecord rec2 = {0, USER_EMAIL2, USER_NAME2, USER_PASSWORD, USER_PIN};
  rec2.roles.push_back(ROLE_1);
  rec2.roles.push_back(ROLE_2);
  rec2.props.emplace("prop1", "val1");
  rec2.props.emplace("prop2", "val2");
  provider->SaveUser(rec2);
}

void ProfileRemoteTest::tearDown() {
  tearDownInternal();
}

void ProfileRemoteTest::tearDownInternal() {
  if (!!provider) {
    provider->DeleteRole(ROLE_1);
    provider->DeleteRole(ROLE_2);
  }
}

void ProfileRemoteTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

void ProfileRemoteTest::testId() {
  auto id = provider_remote->id();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("provider id", string("remote"), id);
}

void ProfileRemoteTest::testName() {
  shared_ptr<ProfileProviderRemote> p = dynamic_pointer_cast<ProfileProviderRemote>(provider_remote);
  auto name = p->Name();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("provider id", string("profile.remote"), name);
}

void ProfileRemoteTest::testVersion() {
  shared_ptr<ProfileProviderRemote> p = dynamic_pointer_cast<ProfileProviderRemote>(provider_remote);
  auto ver = p->Version();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("provider database version", (int)CURRENT_PROFILE_VER, ver);
}

void ProfileRemoteTest::testKey() {
  auto key = provider_remote->key(); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("provider key", string("profile.remote"), key);
}

void ProfileRemoteTest::testRoles() {
  auto r1 = provider->Roles();
  auto r2 = provider_remote->Roles(); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same size", r1.size(), r2.size());
  CPPUNIT_ASSERT_MESSAGE(ROLE_1, std::find(r2.begin(), r2.end(), ROLE_1) != r2.end());
  CPPUNIT_ASSERT_MESSAGE(ROLE_2, std::find(r2.begin(), r2.end(), ROLE_2) != r2.end());
}

void ProfileRemoteTest::testRolesUser() {
  auto r1 = provider->Roles(USER_NAME2);
  auto r2 = provider_remote->Roles(USER_NAME2); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same size", r1.size(), r2.size());
  CPPUNIT_ASSERT_MESSAGE(ROLE_1, std::find(r2.begin(), r2.end(), ROLE_1) != r2.end());
  CPPUNIT_ASSERT_MESSAGE(ROLE_2, std::find(r2.begin(), r2.end(), ROLE_2) != r2.end());
}

void ProfileRemoteTest::testDeleteRole() {
  provider_remote->DeleteRole(ROLE_2); /* remote test */
  auto r1 = provider->Roles();
  CPPUNIT_ASSERT_MESSAGE("deleted role2", std::find(r1.begin(), r1.end(), ROLE_2) == r1.end());
}

void ProfileRemoteTest::testCreateUser() {
  provider_remote->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD); /* remote test */
  auto r1 = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("created user", !r1.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), r1.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), r1.email);
}

void ProfileRemoteTest::testSetPassword() {
  auto r0 =provider_remote->SetPassword(USER_NAME2, "test"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("ok", r0);
  auto r1 = provider->Authenticate(USER_NAME2, "test");
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new password", r1);
}

void ProfileRemoteTest::testSetPin() {
  provider_remote->SetPin(USER_NAME2, "1111"); /* remote test */
  auto r1 = provider->AuthenticatePin(USER_NAME2, "1111");
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new pin", r1);
}

void ProfileRemoteTest::testSetApiPassword() {
  provider->SetPassword(USER_NAME2, "test");
  auto r2 = provider_remote->Authenticate(USER_NAME2, "test"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new password", r2);
}

void ProfileRemoteTest::testSetApiPin() {
  provider->SetPin(USER_NAME2, "1111");
  auto r2 = provider_remote->AuthenticatePin(USER_NAME2, "1111"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new pin", r2);
}

void ProfileRemoteTest::testSetProperty() {
  provider_remote->SetProperty(USER_NAME2, "ppp", "vvv"); /* remote test */
  auto r1 = provider->GetProperty(USER_NAME2, "ppp");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("property ppp", string("vvv"), r1);
}

void ProfileRemoteTest::testGetProperty() {
  provider->SetProperty(USER_NAME2, "ppp", "vvv");
  auto r2 = provider_remote->GetProperty(USER_NAME2, "ppp"); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("property ppp", string("vvv"), r2);
}

void ProfileRemoteTest::testGetProperties() {
  auto r1 = provider->GetProperties(USER_NAME2);
  auto r2 = provider_remote->GetProperties(USER_NAME2); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same size", r1.size(), r2.size());
  for (auto& k : r1) {
    auto v2 = r2[k.first];
    string msg = string("same prop value for ") + k.first;
    CPPUNIT_ASSERT_EQUAL_MESSAGE(msg, k.second, v2);
  }
}

void ProfileRemoteTest::testAddRole() {
  provider->DeleteRole(ROLE_TEST);
  auto r = provider->Roles();
  CPPUNIT_ASSERT_MESSAGE("no test role", std::find(r.begin(), r.end(), ROLE_TEST) == r.end());

  provider_remote->AddRole(ROLE_TEST, ROLE_TEST_DESC);

  r = provider->Roles();
  CPPUNIT_ASSERT_MESSAGE("has test role", std::find(r.begin(), r.end(), ROLE_TEST) != r.end());
}

void ProfileRemoteTest::testSetRoles() {
  provider_remote->SetRoles(USER_NAME2, {"user", "admin"}); /* remote test */
  auto r1 = provider->Roles(USER_NAME2);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("2 roles", (size_t)2, r1.size());
  CPPUNIT_ASSERT_MESSAGE("user", std::find(r1.begin(), r1.end(), "user") != r1.end());
  CPPUNIT_ASSERT_MESSAGE("admin", std::find(r1.begin(), r1.end(), "admin") != r1.end());
}

void ProfileRemoteTest::testGetRoleProperty() {
  auto r1 = provider->GetRoleProperty(ROLE_1, "description");
  auto r2 = provider_remote->GetRoleProperty(ROLE_1, "description"); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same description", r1, r2);
}

void ProfileRemoteTest::testAuthenticate() {
  provider->SetPassword(USER_NAME2, "test");
  auto r1 = provider_remote->Authenticate(USER_NAME2, "test"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new password", r1);
}

void ProfileRemoteTest::testAuthenticateRec() {
  provider->SetPassword(USER_NAME2, "test");
  provider->SetRoles(USER_NAME2, {"user", "admin"});
  UserRecord rec{0};
  auto r1 = provider_remote->Authenticate(USER_NAME2, "test", &rec); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new password", r1);

  CPPUNIT_ASSERT_MESSAGE("Contains uid", !rec.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("2 roles", (size_t)2, rec.roles.size());
  CPPUNIT_ASSERT_MESSAGE("has role1", std::find(rec.roles.begin(), rec.roles.end(), "user") != rec.roles.end());
  CPPUNIT_ASSERT_MESSAGE("has role2", std::find(rec.roles.begin(), rec.roles.end(), "admin") != rec.roles.end());
}

void ProfileRemoteTest::testAuthenticatePin() {
  provider->SetPin(USER_NAME2, "1111");
  auto r1 = provider_remote->AuthenticatePin(USER_NAME2, "1111"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new pin", r1);
}

void ProfileRemoteTest::testAuthenticateWrongPwd() {
  provider->SetPassword(USER_NAME2, "test");
  UserRecord rec{111};
  auto r1 = provider_remote->Authenticate(USER_NAME2, "wrong_password", &rec); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("should not authenticate with wrong password", !r1);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("id=0", (unsigned int)0, rec.id);
}

void ProfileRemoteTest::testDeleteUser() {
  provider_remote->DeleteUser(USER_NAME2); /* remote test */
  auto r1 = provider->LoadUser(USER_NAME2);
  CPPUNIT_ASSERT_MESSAGE("should delete user", r1.userid.empty());
}

void ProfileRemoteTest::testLoadUser() {
  auto r1 = provider->LoadUser(USER_NAME2);
  auto r2 = provider_remote->LoadUser(USER_NAME2); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("user loaded", !r2.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME2), r2.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL2), r2.email);
  CPPUNIT_ASSERT_MESSAGE("props count", r2.props.size() == 2);
  CPPUNIT_ASSERT_MESSAGE("roles count", r2.roles.size() == 2);
}

void ProfileRemoteTest::testLoadUserByEmail() {
  auto r1 = provider->LoadUser(USER_NAME2);
  auto r2 = provider_remote->LoadUser(USER_EMAIL2, ProfileKey::email); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("user loaded", !r2.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME2), r2.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL2), r2.email);
  CPPUNIT_ASSERT_MESSAGE("props count", r2.props.size() == 2);
  CPPUNIT_ASSERT_MESSAGE("roles count", r2.roles.size() == 2);
}

void ProfileRemoteTest::testLoadUserByUUID() {
  auto r1 = provider->LoadUser(USER_NAME2);
  auto r2 = provider_remote->LoadUser(r1.userid.c_str(), ProfileKey::userid); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("user loaded", !r2.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME2), r2.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL2), r2.email);
  CPPUNIT_ASSERT_MESSAGE("props count", r2.props.size() == 2);
  CPPUNIT_ASSERT_MESSAGE("roles count", r2.roles.size() == 2);
}

void ProfileRemoteTest::testSaveUser() {
  UserRecord rec = {0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  rec.roles.push_back(ROLE_1);
  rec.roles.push_back(ROLE_2);
  rec.props.emplace("prop1", "val1");
  rec.props.emplace("prop2", "val2");
  provider_remote->SaveUser(rec); /* remote test */

  auto r1 = provider->LoadUser(USER_NAME);
  CPPUNIT_ASSERT_MESSAGE("user loaded", !r1.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME), r1.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL), r1.email);
  CPPUNIT_ASSERT_MESSAGE("props count", r1.props.size() == 2);
  CPPUNIT_ASSERT_MESSAGE("roles count", r1.roles.size() == 2);
}

void ProfileRemoteTest::testSaveUserByUUID() {
  // 1) load record
  auto r = provider_remote->LoadUser(USER_NAME2);

  // 2) change roles to have only 1 and save (will use UUID api to avoid creating a new record)
  r.roles.clear();
  r.roles.push_back(ROLE_1);
  provider_remote->SaveUser(r);

  // 3) load the updated record
  auto r1 = provider_remote->LoadUser(USER_NAME2);

  CPPUNIT_ASSERT_MESSAGE("user loaded", !r1.userid.empty());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME2), r1.username);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(USER_EMAIL2), r1.email);
  CPPUNIT_ASSERT_MESSAGE("props count", r1.props.size() == 2);
  CPPUNIT_ASSERT_MESSAGE("roles count", r1.roles.size() == 1);
}

void ProfileRemoteTest::testGetResetTokenByEmail() {
  auto r2 = provider_remote->GetResetTokenByEmail(USER_EMAIL2); /* remote test */
  auto r1 = provider->VerifyResetToken(r2.c_str());
  CPPUNIT_ASSERT_MESSAGE("created valid token", r1);
}

void ProfileRemoteTest::testUpdatePassword() {
  auto r2 = provider_remote->GetResetTokenByEmail(USER_EMAIL2); /* remote test */
  provider_remote->UpdatePassword(r2.c_str(), "new_password");
  // provider_remote->UpdatePassword(r2.c_str(), NULL);

  auto r1 = provider->Authenticate(USER_NAME2, "new_password");
  CPPUNIT_ASSERT_MESSAGE("should authenticate with new password", r1);
}

void ProfileRemoteTest::testVerifyResetToken() {
  auto r1 = provider->GetResetTokenByEmail(USER_EMAIL2);
  auto r2 = provider_remote->VerifyResetToken(r1.c_str()); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("created valid token", r2);
}

void ProfileRemoteTest::testVerifyResetTokenInvalid() {
  auto r2 = provider_remote->VerifyResetToken("invalid_reset_token"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("created valid token", !r2);
}

void ProfileRemoteTest::testGetUserName() {
  auto r1 = provider->LoadUser(USER_NAME2);
  auto r2 = provider_remote->GetUserName(r1.userid.c_str()); /* remote test */
  CPPUNIT_ASSERT_EQUAL_MESSAGE("username", string(USER_NAME2), r2);
}

void ProfileRemoteTest::testGetUserNameInvalid() {
  CPPUNIT_ASSERT_THROW_MESSAGE(
      "uuid is empty exception", {
        auto r2 = provider_remote->GetUserName(""); /* remote test */
      },
      std::invalid_argument);
}

void ProfileRemoteTest::testRegisterUser() {
  UserRecord rec2{0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto r2 = provider_remote->RegisterUser(rec2); /* remote test */

  auto r1 = provider->VerifyPendingRegistration(r2.registration_code);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("pending registration", RegistrationStatus::pending, r1);
}

void ProfileRemoteTest::testConfirmRegistration() {
  UserRecord rec2{0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto r1 = provider->RegisterUser(rec2);
  auto r2 = provider_remote->ConfirmRegistration(r1.reset_token); /* remote test */
  auto r1s = provider->VerifyPendingRegistration(r1.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("completed registration", RegistrationStatus::completed, r1s);
  CPPUNIT_ASSERT_MESSAGE("should authenticate registerd user", provider->Authenticate(USER_NAME, USER_PASSWORD));
}

void ProfileRemoteTest::testVerifyPendingRegistration() {
  UserRecord rec2{0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto r1 = provider->RegisterUser(rec2);

  auto r2 = provider_remote->VerifyPendingRegistration(r1.registration_code); /* remote test */

  CPPUNIT_ASSERT_EQUAL_MESSAGE("pending registration", RegistrationStatus::pending, r2);
}

void ProfileRemoteTest::testCleanRegistrations() {
  UserRecord rec2{0, USER_EMAIL, USER_NAME, USER_PASSWORD, USER_PIN};
  auto r1 = provider->RegisterUser(rec2);
  std::cout << "W" << std::flush;
  std::this_thread::sleep_for(std::chrono::milliseconds(1500));

  auto r1s = provider->VerifyPendingRegistration(r1.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("pending registration", RegistrationStatus::pending, r1s);

  provider_remote->CleanRegistrations(0); /* remote test */

  r1s = provider->VerifyPendingRegistration(r1.registration_code);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("pending registration", RegistrationStatus::invalid, r1s);
}

void ProfileRemoteTest::testInvalidRegistration() {
  START_CAPTURE
  auto result = provider_remote->VerifyPendingRegistration("wrong_code"); /* remote test */
  STOP_CAPTURE

  CPPUNIT_ASSERT_EQUAL_MESSAGE("pending registration", RegistrationStatus::invalid, result);
}


void ProfileRemoteTest::testSetBlobSize(size_t size, bool isSuccess) {
  string blob(size, '1');
  auto ok = provider_remote->SetBlob(USER_NAME2, "blo", "text/plain", blob); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("Blob saved result", ok == isSuccess);
  if (isSuccess) {
    auto r2 = provider->GetBlob(USER_NAME2, "blo");
    CPPUNIT_ASSERT_MESSAGE("Blob received", (!!r2));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", blob, r2->data);
    CPPUNIT_ASSERT_EQUAL_MESSAGE("type", string("text/plain"), r2->content_type);
  }
}

void ProfileRemoteTest::testSetBlob() {
  testSetBlobSize(100, true);
  // string blob("12345678");
  // provider_remote->SetBlob(USER_NAME2, "blo", "text/plain", blob); /* remote test */
  // auto r2 = provider->GetBlob(USER_NAME2, "blo");
  // CPPUNIT_ASSERT_MESSAGE("Blob received", (!!r2));
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", string("12345678"), r2->data);
  // CPPUNIT_ASSERT_EQUAL_MESSAGE("type", string("text/plain"), r2->content_type);
}

void ProfileRemoteTest::testSetBlobLimits() {
  testSetBlobSize(ProfileProvider::MAX_BLOB_SIZE, true);
  testSetBlobSize(ProfileProvider::MAX_BLOB_SIZE + 5000000, false);  // assuming max is ~16M + 5M more
}

void ProfileRemoteTest::testGetBlob() {
  string blob("12345678");
  provider->SetBlob(USER_NAME2, "blo", "text/plain", blob);
  auto r2 = provider_remote->GetBlob(USER_NAME2, "blo"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("Blob received", (!!r2));
  CPPUNIT_ASSERT_EQUAL_MESSAGE("blob data", string("12345678"), r2->data);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("type", string("text/plain"), r2->content_type);
}

void ProfileRemoteTest::testGetBlobNE() {
  auto r2 = provider_remote->GetBlob(USER_NAME2, "nonexistingblob"); /* remote test */
  CPPUNIT_ASSERT_MESSAGE("Blob received", (!!r2));
  CPPUNIT_ASSERT_MESSAGE("no blob data", r2->data.length() == 0);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("no type", string(""), r2->content_type);
}

void ProfileRemoteTest::testSetParam() {
  auto data_provider = std::dynamic_pointer_cast<DataProvider>(provider_remote);
  CPPUNIT_ASSERT_MESSAGE("data provider is valid", !!data_provider);
  auto sample = encoding_util().unix_random64(10);
  data_provider->SetParam("sample", sample);
  auto result = data_provider->GetParam("sample");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same data", sample, result);
}

void ProfileRemoteTest::testGetParam() {
  auto data_provider = std::dynamic_pointer_cast<DataProvider>(provider_remote);
  CPPUNIT_ASSERT_MESSAGE("data provider is valid", !!data_provider);
  auto result = data_provider->GetParam("type");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("same data", string("mysql"), result);
}

void ProfileRemoteTest::testSearchEmpty() {
  auto res = provider_remote->Search({}, 100);
  CPPUNIT_ASSERT_MESSAGE("empty", res.empty());
}

// USER_EMAIL2, USER_NAME2,
void ProfileRemoteTest::testSearchName() {
  auto res = provider_remote->Search({{"username", USER_NAME2}}, 100);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, res.size());
}

void ProfileRemoteTest::testSearchEmail() {
  auto res = provider_remote->Search({{"email", USER_EMAIL2}}, 100);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, res.size());
}

void ProfileRemoteTest::testSearchNameEmail() {
  auto res = provider_remote->Search({{"username", USER_NAME2}, {"email", USER_EMAIL2}}, 100);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("1 record", (size_t)1, res.size());
}

void ProfileRemoteTest::testSearchEmmpty() {
  auto res = provider_remote->Search({}, 100);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("0 records", (size_t)0, res.size());
}

CPPUNIT_TEST_SUITE_REGISTRATION(ProfileRemoteTest);
