/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/variant.hpp>

#include <vx/PtreeUtil.h>
#include <vx/web/WebServer.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/URL.h>
#include <vx/EncodingUtil.h>

#include <ProfileProviderRemote.h>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <boost/property_tree/json_parser.hpp>

#include <BaseTest.h>
#include "ProfileRemoteFailTest.h"

#include <InitProfileTest.h>

#if defined(SERVER_TOKEN)
#include <vx/ServiceProfileFactory.h>
#endif

using namespace std;
using namespace vx::openid::test;

#define REG_TTL 3


namespace vx {
  namespace openid {
    namespace test {

      string remote_protocol;

      string get_expected(const string& expected) {
        string exp = expected + ((remote_protocol == "http") ? ": Connection refused" : ": certificate verify");
        return exp;
      }

      string actual_starts_with(const string& actual, const string& expected) {
        if (actual.length() < expected.length()) return actual;
        return actual.substr(0, expected.length());
      }

    }  // namespace test
  }    // namespace openid
}  // namespace vx

/*
  Tests scenario:
  - profile server is not accessible
  - all post/get operations fails

  For all API callse eExpect some error output and no data.
*/


ProfileRemoteFailTest::ProfileRemoteFailTest() {}
ProfileRemoteFailTest::~ProfileRemoteFailTest() {}

void ProfileRemoteFailTest::setUp() {
  AppConfig::resetInstance();

  URL u(DEBUG_HOST);

  u.port = "1111";
  remote_protocol = u.protocol;

#if defined(SERVER_TOKEN)
  context = std::dynamic_pointer_cast<TokenServerContext>(getContext("remote"));
  context->setParam("profile_provider.type", "remote");

  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  context->setParam("profile.remote.remote_host", u.getHost());
  provider_remote = context->GetProfileProvider();
#else
  context.reset(new ProfileServerContext(3));
  context->Init(NULL, "config.json");
  // provider = factory.Connect<ProfileProviderMySQL>("profile.mysql");

  factory.reset(new DataFactory());
  provider = ConnectProfileProvider(factory, "profile.mysql", backend);

  context->setParam("profile.remote.remote_host", u.getHost());
  provider_remote = ConnectProfileProvider(factory, "profile.remote", "remote");

  // cout << DEBUG_HOST << " -> " << u.getHost() << endl;

  // provider_remote = factory->Connect<ProfileProviderRemote>("profile.remote");
#endif

  provider->DeleteUser(USER_NAME);
  provider->DeleteUser(USER_NAME2);

  provider->AddRole(ROLE_1, ROLE_1);
  provider->AddRole(ROLE_2, ROLE_2);

  UserRecord rec2 = {0, USER_EMAIL2, USER_NAME2, USER_PASSWORD, USER_PIN};
  rec2.roles.push_back(ROLE_1);
  rec2.roles.push_back(ROLE_2);
  rec2.props.emplace("prop1", "val1");
  rec2.props.emplace("prop2", "val2");
  provider->SaveUser(rec2);
}

void ProfileRemoteFailTest::tearDown() {
  if (!!provider) {
    provider->DeleteUser(USER_NAME);
    provider->DeleteUser(USER_NAME2);
    provider->DeleteRole(ROLE_1);
    provider->DeleteRole(ROLE_2);
  }
}

void ProfileRemoteFailTest::testBackendId() {
#if defined(VX_USE_MYSQL)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", backend, provider->id());
#else
  CPPUNIT_ASSERT_EQUAL_MESSAGE("backend", string("soci"), provider->id());
#endif
}

void ProfileRemoteFailTest::testId() {
  auto id = provider_remote->id();
  CPPUNIT_ASSERT_EQUAL_MESSAGE("provider id", string("remote"), id);
}

void ProfileRemoteFailTest::testRolesFail() {
  auto roles = provider_remote->Roles();

  CPPUNIT_ASSERT_EQUAL_MESSAGE("empty roles", (size_t)0, roles.size());

  const string expected = get_expected("Roles failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testAddRoleFail() {
  auto res = provider_remote->AddRole(ROLE_1, ROLE_TEST_DESC);
  CPPUNIT_ASSERT_MESSAGE("failed", !res);

  const string expected = get_expected("AddRole failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testDeleteRoleFail() {
  auto res = provider_remote->DeleteRole(ROLE_1);
  CPPUNIT_ASSERT_MESSAGE("failed", !res);

  const string expected = get_expected("DeleteRole failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testCreateUserFail() {
  auto result = provider_remote->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("failed", (long)0, result);

  const string expected = get_expected("CreateUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testSetPasswordFail() {
  auto result = provider_remote->SetPassword(USER_NAME, USER_PASSWORD);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("LoadUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testSetPinFail() {
  auto result = provider_remote->SetPin(USER_NAME, USER_PIN);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("LoadUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testSetPropertyFail() {
  auto res = provider_remote->SetProperty(USER_NAME, "prop", "value");
  CPPUNIT_ASSERT_MESSAGE("failed", !res);

  const string expected = get_expected("SetProperty failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testSetRolesFail() {
  auto res = provider_remote->SetRoles(USER_NAME, {ROLE_1, ROLE_2});
  CPPUNIT_ASSERT_MESSAGE("failed", !res);

  const string expected = get_expected("SetRoles failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testGetPropertyFail() {
  auto result = provider_remote->GetProperty(USER_NAME, "email");

  CPPUNIT_ASSERT_MESSAGE("failed", result.empty());

  const string expected = get_expected("GetProperty failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testGetPropertiesFail() {
  auto result = provider_remote->GetProperties(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("failed", result.size() == 0);

  const string expected = get_expected("GetProperties failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetProperties failed: ") != string::npos);
}

void ProfileRemoteFailTest::testRolesUserFail() {
  auto result = provider_remote->Roles(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("failed", result.size() == 0);

  const string expected = get_expected("Roles failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("Roles failed: ") != string::npos);
}

void ProfileRemoteFailTest::testGetRolePropertyFail() {
  auto result = provider_remote->GetRoleProperty(ROLE_1, "dexcription");

  CPPUNIT_ASSERT_MESSAGE("failed", result.empty());

  const string expected = get_expected("GetRoleProperty failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetRoleProperty failed: ") != string::npos);
}

void ProfileRemoteFailTest::testAuthenticateFail() {
  UserRecord rec{1, USER_EMAIL, USER_NAME};
  auto result = provider_remote->Authenticate(USER_NAME, USER_PASSWORD2, &rec);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("Authenticate failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("Authenticate failed: ") != string::npos);

  // check that a record is cleared (partial check)
  CPPUNIT_ASSERT_EQUAL_MESSAGE("id", (unsigned int)0, rec.id);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("email", string(""), rec.email);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("user name", string(""), rec.username);
}

void ProfileRemoteFailTest::testAuthenticatePinFail() {
  auto result = provider_remote->AuthenticatePin(USER_NAME, USER_PIN);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("AuthenticatePin failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("AuthenticatePin failed: ") != string::npos);
}

void ProfileRemoteFailTest::testDeleteUserFail() {
  auto result = provider_remote->DeleteUser(USER_NAME);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("DeleteUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("DeleteUser failed: ") != string::npos);
}

void ProfileRemoteFailTest::testLoadUserFail() {
  auto result = provider_remote->LoadUser(USER_NAME, ProfileKey::username);

  const string expected = get_expected("LoadUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);
}

void ProfileRemoteFailTest::testSaveUserFail() {
  UserRecord rec{0, USER_EMAIL2, USER_NAME2, USER_PASSWORD, USER_PIN};
  rec.roles.push_back(ROLE_1);
  rec.roles.push_back(ROLE_2);
  rec.props.emplace("prop1", "val1");
  rec.props.emplace("prop2", "val2");

  auto res = provider_remote->SaveUser(rec);
  CPPUNIT_ASSERT_MESSAGE("failed", !res);

  const string expected = get_expected("SaveUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("failed", result.id==0);
  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("SaveUser failed: ") != string::npos);
}

void ProfileRemoteFailTest::testGetResetTokenByEmailFail() {
  auto result = provider_remote->GetResetTokenByEmail(USER_EMAIL);

  CPPUNIT_ASSERT_MESSAGE("failed", result.empty());

  const string expected = get_expected("GetResetTokenByEmail failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetResetTokenByEmail failed: ") != string::npos);
}

void ProfileRemoteFailTest::testUpdatePasswordFail() {
  auto result = provider_remote->UpdatePassword("token", USER_PASSWORD);

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("UpdatePassword failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("UpdatePassword failed: ") != string::npos);
}

void ProfileRemoteFailTest::testVerifyResetTokenFail() {
  auto result = provider_remote->VerifyResetToken("token");

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("VerifyResetToken failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("VerifyResetToken failed: ") != string::npos);
}

void ProfileRemoteFailTest::testGetUserNameFail() {
  auto result = provider_remote->GetUserName("uuid");

  CPPUNIT_ASSERT_MESSAGE("failed", result.empty());

  const string expected = get_expected("GetUserName failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetUserName failed: ") != string::npos);
}

void ProfileRemoteFailTest::testRegisterUserFail() {
  UserRecord rec{0, USER_EMAIL2, USER_NAME2, USER_PASSWORD, USER_PIN};

  auto result = provider_remote->RegisterUser(rec);

  CPPUNIT_ASSERT_MESSAGE("failed", result.registration_code.empty());
  CPPUNIT_ASSERT_MESSAGE("failed", result.reset_token.empty());

  const string expected = get_expected("RegisterUser failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("RegisterUser failed: ") != string::npos);
}

void ProfileRemoteFailTest::testConfirmRegistrationFail() {
  auto result = provider_remote->ConfirmRegistration("token");

  CPPUNIT_ASSERT_MESSAGE("failed", !result);

  const string expected = get_expected("ConfirmRegistration failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("ConfirmRegistration failed: ") != string::npos);
}

void ProfileRemoteFailTest::testVerifyPendingRegistrationFail() {
  auto result = provider_remote->VerifyPendingRegistration("token");

  CPPUNIT_ASSERT_MESSAGE("failed", result == RegistrationStatus::invalid);

  const string expected = get_expected("VerifyPendingRegistration failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("VerifyPendingRegistration failed: ") != string::npos);
}

void ProfileRemoteFailTest::testCleanRegistrationsFail() {
  provider_remote->CleanRegistrations();

  const string expected = get_expected("CleanRegistrations failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("failed", result == RegistrationStatus::invalid);
  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("CleanRegistrations failed: ") != string::npos);
}

void ProfileRemoteFailTest::testSetBlobFail() {
  provider_remote->SetBlob(USER_NAME, "prop", "text/plain", "data");

  const string expected = get_expected("SetBlob failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("failed", result == RegistrationStatus::invalid);
  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("SetBlob failed: ") != string::npos);
}

void ProfileRemoteFailTest::testGetBlobFail() {
  auto result = provider_remote->GetBlob(USER_NAME, "prop");

  CPPUNIT_ASSERT_MESSAGE("failed", result->data.empty());

  const string expected = get_expected("GetBlob failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetBlob failed: ") != string::npos);
}

void ProfileRemoteFailTest::testVersionFail() {
  auto prov = std::dynamic_pointer_cast<DataProvider>(provider_remote);
  auto result = prov->Version();

  const string expected = get_expected("Version failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  CPPUNIT_ASSERT_MESSAGE("failed", result == 0);
  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("Version failed: ") != string::npos);
}

void ProfileRemoteFailTest::testSetParamFail() {
  auto prov = std::dynamic_pointer_cast<DataProvider>(provider_remote);
  prov->SetParam("prop", "data");

  const string expected = get_expected("SetParam failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("SetParam failed: ") != string::npos);
}

void ProfileRemoteFailTest::testGetParamFail() {
  auto prov = std::dynamic_pointer_cast<DataProvider>(provider_remote);
  auto result = prov->GetParam("prop");

  CPPUNIT_ASSERT_MESSAGE("failed", result.empty());

  const string expected = get_expected("GetParam failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("GetParam failed: ") != string::npos);
}

void ProfileRemoteFailTest::testSearchByConditionFail() {
  SearchCondition cond;
  cond.add("username",USER_NAME);
  auto result = provider_remote->SearchByCondition(cond);

  CPPUNIT_ASSERT_EQUAL_MESSAGE("failed", (size_t)0, result.size());

  const string expected = get_expected("Search failed");
  const string actual = actual_starts_with(provider_remote->GetProfileErrorMessage(), expected);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("remote_error"), provider_remote->GetProfileError());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", expected, actual);

  // CPPUNIT_ASSERT_MESSAGE("Err output", cerr_output.find("ConfirmRegistration failed: ") != string::npos);
}

CPPUNIT_TEST_SUITE_REGISTRATION(ProfileRemoteFailTest);
