#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <boost/variant.hpp>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <vx/PtreeUtil.h>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <BaseTest.h>

#include "ClientHelper.h"

using namespace std;
using namespace boost::property_tree;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {

      /// Test registration handler (for profile and token servers)
      class RegisterTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(RegisterTest);

        CPPUNIT_TEST(testRegister);
        CPPUNIT_TEST(testRegisterForm);
        CPPUNIT_TEST(testRegisterBadAuth);
        CPPUNIT_TEST(testRegisterBadAuthNoClientIDSecret);
        CPPUNIT_TEST(testRegisterBad);
        CPPUNIT_TEST(testEmailConfirm);
        CPPUNIT_TEST(testEmailConfirmBad);
        CPPUNIT_TEST(testEmailConfirmNoCode);
        CPPUNIT_TEST(testValidate);
        CPPUNIT_TEST(testValidateBad);
        CPPUNIT_TEST(testValidateBadNE);
        CPPUNIT_TEST(testRegisterCleanup);
#if !defined(SERVER_TOKEN)
        CPPUNIT_TEST(testRegisterCleanupForceDefault);
        CPPUNIT_TEST(testRegisterCleanupForceImmediate);
#endif
        CPPUNIT_TEST(testRegisterExUser);
        CPPUNIT_TEST(testRegisterExEmail);
        CPPUNIT_TEST(testRegisterWrongEmail);
        CPPUNIT_TEST(testRegisterWrongEmailDomain);
        CPPUNIT_TEST(testRegisterWrongEmailDomainShort);

        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ProfileProvider> profile_provider;
        shared_ptr<ClientHelper> client;
        shared_ptr<ServerContext> server_context;

        string CreateUserJSON(
            const string& goodEmail = USER_EMAIL,
            const string& goodPassword = USER_PASSWORD,
            const string& goodPin = USER_PIN,
            const string& goodUsername = USER_NAME);
        // debug version will return both reset_token and registration_code
        ptree CallRegisterUser(const string& code,
                               const string& error = "",
                               const string& goodEmail = USER_EMAIL,
                               const string& goodPassword = USER_PASSWORD,
                               const string& goodPin = USER_PIN,
                               const string& goodUsername = USER_NAME);
        void CallValidate(string registration_code, string expected, string http_code, const string& error = "", const string& error_description="");

        ClientHelper& CallUrl(ptree& pt, const string& url);

        static string Authenticate(shared_ptr<ClientHelper>& client);

      public:
        RegisterTest();
        ~RegisterTest();

        void setUp() override;     //!< cppunit setup, deletes USER_NAME
        void tearDown() override;  //!< cppunit teardown

        void testRegister();                         //!< happy path (user/email does not exist)
        void testRegisterForm();                     //!< happy path sending form
        void testRegisterBadAuth();                  //!< wrong client_id
        void testRegisterBadAuthNoClientIDSecret();  //!< no client_id and client_secret
        void testRegisterBad();                      //!< 4 cases: bad username, bad password, bad pin, bad username
        void testEmailConfirm();                     //!< happy path: register, email confirm link, authenticate should work
        void testEmailConfirmBad();                  //!< call email/confirm with wrong confirmation code
        void testEmailConfirmNoCode();               //!< call email/confirm with no confirmation code
        void testValidate();                         //!< validate pending registration
        void testValidateBad();                      //!< fail: validate pending registration with bad code
        void testValidateBadNE();                    //!< fail: validate pending registration with missing code
        void testRegisterCleanup();                  //!< cleanup process will be run by the server ~15 seconds, should delete all pending registrations
        void testRegisterCleanupForceDefault();      //!< execute cleanup process, should delete all pending registrations (~3s)
        void testRegisterCleanupForceImmediate();    //!< execute cleanup process, should delete all pending registrations immediately using 1s

        void testRegisterExUser();                 //!< register using existing user name
        void testRegisterExEmail();                //!< register using existing email
        void testRegisterWrongEmail();             //!< register using wrong email, i.e. not an email at all
        void testRegisterWrongEmailDomain();       //!< register using wrong email with wrong top domain part
        void testRegisterWrongEmailDomainShort();  //!< register using wrong email with wrong top domain part
      };

    }  // namespace test
  }    // namespace openid
}  // namespace vx
