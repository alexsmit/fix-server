#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <boost/property_tree/ptree.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#if defined(SERVER_TOKEN)
#include <TokenServerContext.h>
#else
#include "../src/ProfileServerContext.h"
#endif

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {
      /// Test uses msql and remote profile provider. Any data modified by msql provider will be verified by remote provider
      /// and vice versa. Naming conventions: r1 - results for msql profile provider, r2 - results for remote profile provider
      /// for remote profile provider a running profile server will be used. remote configuration is provided in the file
      /// config.json under profile.remote section.
      class ProfileRemoteTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ProfileRemoteTest);

        // CPPUNIT_TEST(testBackendId);

        CPPUNIT_TEST(testId);
        CPPUNIT_TEST(testName);
        CPPUNIT_TEST(testVersion);
        CPPUNIT_TEST(testKey);
        CPPUNIT_TEST(testRoles);
        CPPUNIT_TEST(testRolesUser);
        CPPUNIT_TEST(testDeleteRole);
        CPPUNIT_TEST(testCreateUser);
        CPPUNIT_TEST(testSetPassword);
        CPPUNIT_TEST(testSetPin);
        CPPUNIT_TEST(testSetApiPassword);
        CPPUNIT_TEST(testSetApiPin);
        CPPUNIT_TEST(testSetProperty);
        CPPUNIT_TEST(testGetProperty);
        CPPUNIT_TEST(testGetProperties);
        CPPUNIT_TEST(testAddRole);
        CPPUNIT_TEST(testSetRoles);
        CPPUNIT_TEST(testGetRoleProperty);
        CPPUNIT_TEST(testAuthenticate);
        CPPUNIT_TEST(testAuthenticateRec);
        CPPUNIT_TEST(testAuthenticatePin);
        CPPUNIT_TEST(testAuthenticateWrongPwd);
        CPPUNIT_TEST(testDeleteUser);
        CPPUNIT_TEST(testLoadUser);
        CPPUNIT_TEST(testLoadUserByEmail);
        CPPUNIT_TEST(testLoadUserByUUID);
        CPPUNIT_TEST(testSaveUser);
        CPPUNIT_TEST(testSaveUserByUUID);
        CPPUNIT_TEST(testGetResetTokenByEmail);
        CPPUNIT_TEST(testUpdatePassword);
        CPPUNIT_TEST(testVerifyResetToken);
        CPPUNIT_TEST(testVerifyResetTokenInvalid);
        CPPUNIT_TEST(testGetUserName);
        CPPUNIT_TEST(testGetUserNameInvalid);
        CPPUNIT_TEST(testRegisterUser);
        CPPUNIT_TEST(testConfirmRegistration);
        CPPUNIT_TEST(testVerifyPendingRegistration);
        CPPUNIT_TEST(testCleanRegistrations);
        CPPUNIT_TEST(testInvalidRegistration);
        CPPUNIT_TEST(testSetBlob);
#if BOOST_VERSION >= 106600        
        CPPUNIT_TEST(testSetBlobLimits);
#endif
        CPPUNIT_TEST(testGetBlob);
        CPPUNIT_TEST(testGetBlobNE);
        CPPUNIT_TEST(testSetParam);
        CPPUNIT_TEST(testGetParam);
        CPPUNIT_TEST(testSearchEmpty);
        CPPUNIT_TEST(testSearchName);
        CPPUNIT_TEST(testSearchEmail);
        CPPUNIT_TEST(testSearchNameEmail);
        CPPUNIT_TEST(testSearchEmmpty);

        CPPUNIT_TEST_SUITE_END();

      protected:
        shared_ptr<ProfileProvider> provider;         //!< local profile provider
        shared_ptr<ProfileProvider> provider_remote;  //!< remote profile provider
        shared_ptr<DataFactory> factory;              //!< factory instance
#if defined(SERVER_TOKEN)
        shared_ptr<TokenServerContext> context;  //!< server context
#else
        shared_ptr<ProfileServerContext> context;  //!< server context
#endif

      public:
        ProfileRemoteTest();
        virtual ~ProfileRemoteTest();

        void setUp();             //!< ccpunit setup
        void setUpInternal();     //!< init all providers
        void tearDown();          //!< cppunit teardown
        void tearDownInternal();  //!< cppunit teardown for sub-tests

        void testBackendId();  //!< verify we're using proper backend

        void testId();                                      //!< profile provider "remote"
        void testName();                                    //!< should return proper name
        void testVersion();                                 //!< should return proper version
        void testKey();                                     //!< current config section
        void testRoles();                                   //!< get roles
        void testRolesUser();                               //!< get roles for user
        void testDeleteRole();                              //!< delete role
        void testCreateUser();                              //!< basic create user
        void testSetPassword();                             //!< set by remote/verify new password
        void testSetPin();                                  //!< set by remote/verify pin
        void testSetApiPassword();                          //!< set by api/verify new password
        void testSetApiPin();                               //!< set by api/verify pin
        void testSetProperty();                             //!< set propery/verify
        void testAddRole();                                 //!< add role
        void testSetRoles();                                //!< set roles for user
        void testGetProperty();                             //!< set/verify property
        void testGetProperties();                           //!< get list of properties
        void testGetRoleProperty();                         //!< get description property for the role
        void testAuthenticate();                            //!< authenticate with password
        void testAuthenticateRec();                         //!< authenticate with password and fill-in user record
        void testAuthenticatePin();                         //!< authenticate with pin
        void testAuthenticateWrongPwd();                    //!< authenticate with wrong password
        void testDeleteUser();                              //!< delete user/verify
        void testLoadUser();                                //!< load user record
        void testLoadUserByEmail();                         //!< load user record
        void testLoadUserByUUID();                          //!< load user record
        void testSaveUser();                                //!< save/load user record
        void testSaveUserByUUID();                          //!< save user record using existing UUID
        void testGetResetTokenByEmail();                    //!< reset password, verify reset token (using remote provider)
        void testUpdatePassword();                          //!< reset/update password and authenticate
        void testVerifyResetToken();                        //!< reset password, verify reset token (using local provider)
        void testVerifyResetTokenInvalid();                 //!< verify invalid reset token
        void testGetUserName();                             //!< get username property
        void testGetUserNameInvalid();                      //!< get username property using invalid UUID (throws exception)
        void testRegisterUser();                            //!< register user
        void testConfirmRegistration();                     //!< register, confirm registration using token
        void testVerifyPendingRegistration();               //!< register, verify pending registration
        void testCleanRegistrations();                      //!< cleanup registrations
        void testInvalidRegistration();                     //!< try to test registration using invalid token
        void testSetBlobSize(size_t size, bool isSuccess);  //!< run a test for sepified blob size
        void testSetBlob();                                 //!< set blob
        void testSetBlobLimits();                           //!< set blob below, at and above the limit
        void testGetBlob();                                 //!< get blob
        void testGetBlobNE();                               //!< get blob that does not exist
        void testSetParam();                                //!< Set param on remote
        void testGetParam();                                //!< Get remote param
        void testSearchEmpty();                             //!< empty criteria => empty result
        void testSearchName();                              //!< search by name -> 1 record
        void testSearchEmail();                             //!< search by email -> 1 record
        void testSearchNameEmail();                         //!< search by name+email -> 1 record
        void testSearchEmmpty(); //!< search with condition empty
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
