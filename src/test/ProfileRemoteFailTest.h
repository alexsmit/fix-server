#pragma once
/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include <boost/property_tree/ptree.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>
#include <vx/JWT.h>
#include <vx/PtreeUtil.h>

#if defined(SERVER_TOKEN)
#include <TokenServerContext.h>
#else
#include "../src/ProfileServerContext.h"
#endif

using namespace std;
using namespace vx::sql;

namespace vx {
  namespace openid {
    namespace test {
      /// Test uses msql and remote profile provider. Any data modified by msql provider will be verified by remote provider
      /// and vice versa. Naming conventions: r1 - results for msql profile provider, r2 - results for remote profile provider
      /// for remote profile provider a running profile server will be used. remote configuration is provided in the file
      /// config.json under profile.remote section.
      class ProfileRemoteFailTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(ProfileRemoteFailTest);

        CPPUNIT_TEST(testBackendId);
        CPPUNIT_TEST(testId);
        CPPUNIT_TEST(testRolesFail);
        CPPUNIT_TEST(testAddRoleFail);
        CPPUNIT_TEST(testDeleteRoleFail);
        CPPUNIT_TEST(testCreateUserFail);
        CPPUNIT_TEST(testSetPasswordFail);
        CPPUNIT_TEST(testSetPinFail);
        CPPUNIT_TEST(testSetPropertyFail);
        CPPUNIT_TEST(testSetRolesFail);
        CPPUNIT_TEST(testGetPropertyFail);
        CPPUNIT_TEST(testGetPropertiesFail);
        CPPUNIT_TEST(testRolesUserFail);
        CPPUNIT_TEST(testGetRolePropertyFail);
        CPPUNIT_TEST(testAuthenticateFail);
        CPPUNIT_TEST(testAuthenticatePinFail);
        CPPUNIT_TEST(testDeleteUserFail);
        CPPUNIT_TEST(testLoadUserFail);
        CPPUNIT_TEST(testSaveUserFail);
        CPPUNIT_TEST(testGetResetTokenByEmailFail);
        CPPUNIT_TEST(testUpdatePasswordFail);
        CPPUNIT_TEST(testVerifyResetTokenFail);
        CPPUNIT_TEST(testGetUserNameFail);
        CPPUNIT_TEST(testRegisterUserFail);
        CPPUNIT_TEST(testConfirmRegistrationFail);
        CPPUNIT_TEST(testVerifyPendingRegistrationFail);
        CPPUNIT_TEST(testCleanRegistrationsFail);
        CPPUNIT_TEST(testSetBlobFail);
        CPPUNIT_TEST(testGetBlobFail);
        CPPUNIT_TEST(testVersionFail);
        CPPUNIT_TEST(testSetParamFail);
        CPPUNIT_TEST(testGetParamFail);
        CPPUNIT_TEST(testSearchByConditionFail);

        CPPUNIT_TEST_SUITE_END();

      protected:
        shared_ptr<ProfileProvider> provider;         //!< local profile provider
        shared_ptr<ProfileProvider> provider_remote;  //!< remote profile provider
        shared_ptr<DataFactory> factory;              //!< factory instance
#if defined(SERVER_TOKEN)
        shared_ptr<TokenServerContext> context;  //!< server context
#else
        shared_ptr<ProfileServerContext> context;  //!< server context
#endif

      public:
        ProfileRemoteFailTest();
        virtual ~ProfileRemoteFailTest();

        void setUp();     //!< ccpunit setup
        void tearDown();  //!< cppunit teardown

        void testBackendId();                      //!< verify we're using proper backend
        void testId();                             //!< profile provider "remote"
        void testRolesFail();                      //!< get Roles failure
        void testAddRoleFail();                    //!< AddRole failure
        void testDeleteRoleFail();                 //!< DeleteRole failure
        void testCreateUserFail();                 //!< CreateUser failure
        void testSetPasswordFail();                //!< SetPassword failure
        void testSetPinFail();                     //!< SetPin failure
        void testSetPropertyFail();                //!< SetProperty failure
        void testSetRolesFail();                   //!< SetRoles failure
        void testGetPropertyFail();                //!< GetProperty failure
        void testGetPropertiesFail();              //!< GetProperties failure
        void testRolesUserFail();                  //!< get Roles(user) failure
        void testGetRolePropertyFail();            //!< GetRoleProperty failure
        void testAuthenticateFail();               //!< Authenticate failure
        void testAuthenticatePinFail();            //!< AuthenticatePin failure
        void testDeleteUserFail();                 //!< DeleteUser failure
        void testLoadUserFail();                   //!< LoadUser failure
        void testSaveUserFail();                   //!< SaveUser failure
        void testGetResetTokenByEmailFail();       //!< GetResetTokenByEmail failure
        void testUpdatePasswordFail();             //!< UpdatePassword failure
        void testVerifyResetTokenFail();           //!< VerifyResetToken failure
        void testGetUserNameFail();                //!< GetUserName failure
        void testRegisterUserFail();               //!< RegisterUser failure
        void testConfirmRegistrationFail();        //!< ConfirmRegistration failure
        void testVerifyPendingRegistrationFail();  //!< VerifyPendingRegistration failure
        void testCleanRegistrationsFail();         //!< CleanRegistrations failure
        void testSetBlobFail();                    //!< SetBlob failure
        void testGetBlobFail();                    //!< GetBlob failure
        void testVersionFail();                    //!< Version failure
        void testSetParamFail();                   //!< GetParam failure
        void testGetParamFail();                   //!< GetParam failure
        void testSearchByConditionFail();           //!< SearchByCondition failure
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
