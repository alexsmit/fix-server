/*
 * Copyright 2023 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include "ClientHelper.h"
#include <vx/URL.h>

#include <boost/property_tree/json_parser.hpp>

using namespace std;

ClientHelper::ClientHelper(const std::string host) : host(host) {}

void ClientHelper::Call(const std::string &method, const std::string &url, const std::string &json_request, const SimpleWeb::CaseInsensitiveMultimap &headers, bool capture) {
  auto f = [&](const string &_code, SimpleWeb::CaseInsensitiveMultimap &_header, const string &content) {
    status_code = _code;
    code = SimpleWeb::status_code(_code);
    if (capture) {
      data = content;
      stringstream s(content);
      try {
        read_json(s, result);
      }
      catch (const std::exception &e) {
        result.clear();
      }
    }
    header = _header;
  };

  URL u(host);

  string canonical_host = u.getHost(false);
  if (u.protocol.empty() || u.protocol == "http") {
    shared_ptr<HttpClient> client(new HttpClient(canonical_host));
    auto result = client->request(method, url, json_request, headers);
    f(result->status_code, result->header, result->content.string());
  }
  else {
    shared_ptr<HttpsClient> client(new HttpsClient(canonical_host, false));
    auto result = client->request(method, url, json_request, headers);
    f(result->status_code, result->header, result->content.string());
  }
}

void ClientHelper::Call(const std::string &method, const std::string &url, ptree pt, const SimpleWeb::CaseInsensitiveMultimap &headers, bool capture) {
  stringstream s;
  write_json(s, pt);
  string json_request = s.str();
  Call(method, url, json_request, header, capture);
}

ClientHelper& ClientHelper::request(
    const std::string &method,
    const std::string &url,
    ptree pt,
    const SimpleWeb::CaseInsensitiveMultimap &headers,
    bool capture) {
  Call(method, url, pt, headers, capture);
  return *this;
}

ClientHelper& ClientHelper::request(
    const std::string &method,
    const std::string &url,
    const std::string &json_request,
    const SimpleWeb::CaseInsensitiveMultimap &headers,
    bool capture) {
  Call(method, url, json_request, headers, capture);
  return *this;
}
