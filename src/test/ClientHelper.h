#pragma once

#include <stdafx.h>
#include <boost/property_tree/ptree.hpp>

#include <client_http.hpp>
#include <client_https.hpp>
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;
using HttpsClient = SimpleWeb::Client<SimpleWeb::HTTPS>;

using namespace boost::property_tree;

/// @brief Helper class to make HTTP/HTTPS requests
class ClientHelper {
private:
  std::string host;

public:
  std::string data;                           //!< string response body
  SimpleWeb::StatusCode code;                 //!< response code
  SimpleWeb::CaseInsensitiveMultimap header;  //!< response headers
  ptree result;                               //!< parsed JSON respons if "capture" set to true (default)

  /// Initialize helper with host. Host may be a full URL or just a hostname. Default protocol is HTTP if not provided explicitly.
  ClientHelper(const std::string host);

  /// call with ptree payload
  void Call(
      const std::string& method,
      const std::string& url,
      ptree pt,
      const SimpleWeb::CaseInsensitiveMultimap& headers = SimpleWeb::CaseInsensitiveMultimap(),
      bool capture = true);

  /// call with string payload
  void Call(
      const std::string& method,
      const std::string& url,
      const std::string& json_request,
      const SimpleWeb::CaseInsensitiveMultimap& headers = SimpleWeb::CaseInsensitiveMultimap(),
      bool capture = true);

  /// call with ptree payload
  ClientHelper& request(
      const std::string& method,
      const std::string& url,
      ptree pt,
      const SimpleWeb::CaseInsensitiveMultimap& headers = SimpleWeb::CaseInsensitiveMultimap(),
      bool capture = true);

  /// call with string payload
  ClientHelper& request(
      const std::string& method,
      const std::string& url,
      const std::string& json_request,
      const SimpleWeb::CaseInsensitiveMultimap& headers = SimpleWeb::CaseInsensitiveMultimap(),
      bool capture = true);

  // compatibility
  std::string status_code; //!< response status code as string
};