/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define BOOST_BIND_GLOBAL_PLACEHOLDERS

#include <stdafx.h>

#include <boost/variant.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
// #include <boost/archive/text_oarchive.hpp>
// #include <boost/archive/binary_oarchive.hpp>
#include <boost/property_tree/json_parser.hpp>

#include <BaseTest.h>
#include "RegisterTest.h"

#if defined(SERVER_TOKEN)
#include <TokenServerContext.h>
#else
#include <ProfileServerContext.h>
#endif

#if defined(SERVER_TOKEN)
#include <vx/ServiceProfileFactory.h>
#endif

using namespace std;
using namespace vx::openid::test;

RegisterTest::RegisterTest() {}

RegisterTest::~RegisterTest() {}

void RegisterTest::setUp() {
  client.reset(new ClientHelper(DEBUG_HOST));
  // factory.reset(new DataFactory());
  server_context = getContext();

#if defined(SERVER_TOKEN)
  TokenServerContext* token_context = dynamic_cast<TokenServerContext*>(server_context.get());
  profile_provider = token_context->GetProfileProvider();
#else
  ProfileServerContext* profile_context = dynamic_cast<ProfileServerContext*>(server_context.get());
  profile_provider = profile_context->GetProfileProvider();
#endif

  profile_provider->DeleteUser(USER_NAME);
  profile_provider->DeleteUser(USER_NAME2);

  if (!server_context) {
    cout << "FATAL ERROR. Context is not initialized." << endl;
    ::exit(1);
  }
  server_context->section->getParam("profile_provider.send_email", "off");
}

void RegisterTest::tearDown() {
  profile_provider.reset();
  // factory.reset();
  AppConfig::resetInstance();
}

string RegisterTest::CreateUserJSON(const string& goodEmail, const string& goodPassword, const string& goodPin,
                                    const string& goodUsername) {
  ptree pt;
  if (goodUsername.length() > 0) pt.put("username", goodUsername);
  if (goodEmail.length() > 0) pt.put("email", goodEmail);
  if (goodPassword.length() > 0) pt.put("password", goodPassword);
  if (goodPin.length() > 0) pt.put("pin", goodPin);
  stringstream s;
  write_json(s, pt);
  return s.str();
}

ClientHelper& RegisterTest::CallUrl(ptree& pt, const string& url) {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  stringstream st;
  write_json(st, pt);

  return client->request("POST", url, st.str(), header);
}

ptree RegisterTest::CallRegisterUser(const string& expected_code, const string& error,
                                     const string& goodEmail, const string& goodPassword, const string& goodPin,
                                     const string& goodUsername) {
  client.reset(new ClientHelper(DEBUG_HOST));

  ptree pt;
  string registration_code, reset_token;
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result =
      client->request("POST", "/register", CreateUserJSON(goodEmail, goodPassword, goodPin, goodUsername), header);
  auto code = result.status_code.substr(0, 3);

  // string expected = (goodEmail) ? string("200") : string("400");

  // cout << "CallRegisterUser (" << code << "," << error << "," << goodEmail << "," << goodPassword << "," << goodPin << "," << goodUsername
  //      << ") code:" << code << endl;
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected code", expected_code, code);

  if (expected_code == "200") {
    pt = result.result;
    registration_code = pt.get("registration_code", "");
    CPPUNIT_ASSERT_MESSAGE("Should get a code", !registration_code.empty());
  }
  else {
    ptree pt_error = result.result;
    // write_json(cout, pt_error);
    if (goodEmail.empty()) {
      CPPUNIT_ASSERT_EQUAL_MESSAGE("Should get a message", string("email is required"), pt_error.get("error_description", ""));
    }
    else {
      if (!error.empty()) {
        CPPUNIT_ASSERT_EQUAL_MESSAGE("Should get a message", error, pt_error.get("error_description", ""));
      }
    }
  }

  return pt;
}

// POST /register
void RegisterTest::testRegister() { CallRegisterUser("200"); }

// POST /register
void RegisterTest::testRegisterForm() {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/x-www-form-urlencoded"});

  string body =
      (boost::format("username=%s&password=%s&email=%s&pin=%s") % USER_NAME % USER_PASSWORD % USER_EMAIL % USER_PIN)
          .str();

  auto result = client->request("POST", "/register", body, header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected code", string("200"), code);

  ptree pt = result.result;

  auto registration_code = pt.get("registration_code", "");
  CPPUNIT_ASSERT_MESSAGE("Should get registration code", !registration_code.empty());
}

// POST /register
void RegisterTest::testRegisterBad() {
  CallRegisterUser("400", "", "");
  CallRegisterUser("400", "password is required", USER_EMAIL, "");
  CallRegisterUser("400", "pin is required", USER_EMAIL, USER_PASSWORD, "");
  CallRegisterUser("400", "username is required", USER_EMAIL, USER_PASSWORD, USER_PIN, "");
}

// POST /register
void RegisterTest::testRegisterBadAuth() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", "wrong_client_id"});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("POST", "/register", CreateUserJSON(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("401"), code);

  ptree pt = result.result;
  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_client"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Wrong client id or secret"), error_description);
}

void RegisterTest::testRegisterBadAuthNoClientIDSecret() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"Content-Type", "application/json"});

  auto result = client->request("POST", "/register", CreateUserJSON(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);

  ptree pt = result.result;
  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_request"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("Missing client id or secret"), error_description);
}

// GET /register/confirm
void RegisterTest::testEmailConfirm() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  // 1 register user
  ptree pt = CallRegisterUser("200");
  string registration_code = pt.get("registration_code", "");
  CPPUNIT_ASSERT_MESSAGE("Should have registration code", !registration_code.empty());

  // 2 confirm
  string reset_token = pt.get("reset_token", "");
  CPPUNIT_ASSERT_MESSAGE("Should have reset_token for DEBUG", !reset_token.empty());

  auto query = boost::format("/register/confirm?code=%s") % reset_token;
  auto& result = client->request("GET", query.str(), "");
  auto code = result.status_code.substr(0, 3);

#ifdef PROFILE_SERVER
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should succeed", string("200"), code);
#else
  string url = server_context->getParam("profile_provider.confirmation_success_url");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect", string("302"), code);
  auto it = result.header.find("Location");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", it != result.header.end());
  CPPUNIT_ASSERT_MESSAGE("Should redirect to configured page", it->second == url);
#endif

  // 3 authenticate should work
  RegisterTest::Authenticate(client);

  // 4 validate should be "completed"
  CallValidate(registration_code, "completed", "200");
}

void RegisterTest::testEmailConfirmBad() {
  auto result = client->request("GET", "/register/confirm?code=some_bad_code", "");
  auto code = result.status_code.substr(0, 3);

#ifdef PROFILE_SERVER
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
  ptree pt = result.result;
  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_registration"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("registration code is not valid"), error_description);
#else
  string url = server_context->getParam("profile_provider.confirmation_failure_url");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect", string("302"), code);
  auto it = result.header.find("Location");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", it != result.header.end());
  CPPUNIT_ASSERT_MESSAGE("Should redirect to configured page", it->second == url);
#endif
}

void RegisterTest::testEmailConfirmNoCode() {
  auto& result = client->request("GET", "/register/confirm?code", "");
  auto code = result.status_code.substr(0, 3);
  // auto body = result->content.string();

#ifdef PROFILE_SERVER
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should fail", string("400"), code);
  ptree pt = result.result;
  auto error = pt.get("error", "");
  auto error_description = pt.get("error_description", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error", string("invalid_registration"), error);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", string("registration code is invalid"), error_description);
#else
  string url = server_context->getParam("profile_provider.confirmation_failure_url");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect", string("302"), code);
  auto it = result.header.find("Location");
  CPPUNIT_ASSERT_MESSAGE("Should have Location header", it != result.header.end());
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Should redirect to configured page", url, it->second);
#endif
}

// POST /register/validate
void RegisterTest::testValidate() {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  // 1) register a user first
  ptree pt_reg = CallRegisterUser("200");
  string registration_code = pt_reg.get("registration_code", "");

  // 2) call the validate backend
  CallValidate(registration_code, "pending", "200");
}

void RegisterTest::testValidateBad() { CallValidate("wrong_code", "", "400", "invalid_registration", "registration does not exist"); }
void RegisterTest::testValidateBadNE() { CallValidate("", "", "400", "invalid_registration","token is missing or invalid"); }

//---------------------------------------------

string RegisterTest::Authenticate(shared_ptr<ClientHelper>& client) {
  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});
  ptree pt;
  pt.put("grant_type", "password");
  pt.put("password", USER_PASSWORD);
  pt.put("username", USER_NAME);
  stringstream s;
  write_json(s, pt);

  ptree pt_token;
#ifdef PROFILE_SERVER
  auto result = client->request("POST", "/authenticate", s.str(), header);
#else
  auto result = client->request("POST", "/oauth/token", s.str(), header);
#endif
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_MESSAGE("Should succeed", code == "200");

  pt_token = result.result;
  return pt_token.get("access_token", "");
}

void RegisterTest::CallValidate(string registration_code, string expected, string http_code, const string& error, const string& error_description) {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  ptree pt_check;
  if (!registration_code.empty())
    pt_check.put("registration_code", registration_code);
  stringstream s;
  write_json(s, pt_check);

  auto result = client->request("POST", "/register/validate", s.str(), header);
  auto code = result.status_code.substr(0, 3);
  CPPUNIT_ASSERT_EQUAL_MESSAGE("http code", http_code, code);
  if (http_code != "200") {
    if (!error.empty()) {
      CPPUNIT_ASSERT_EQUAL_MESSAGE("error", error, result.result.get("error",""));
    }
    if (!error_description.empty()) {
      CPPUNIT_ASSERT_EQUAL_MESSAGE("error_description", error_description, result.result.get("error_description",""));
    }
    return;
  }

  ptree pt_validate = result.result;
  string status = pt_validate.get("status", "");
  CPPUNIT_ASSERT_EQUAL_MESSAGE("Expected registration code", expected, status);
}

void RegisterTest::testRegisterCleanup() {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  // 1) register a user first
  ptree pt_reg = CallRegisterUser("200");
  string registration_code = pt_reg.get("registration_code", "");

  // 2) wait 15 seconds to make sure a clenup thread will delete a registration
  cout << "R" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(17));

  // 3) call the validate backend
  CallValidate(registration_code, "", "400");
}

void RegisterTest::testRegisterCleanupForceDefault() {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  // 1) register a user first
  ptree pt_reg = CallRegisterUser("200");
  string registration_code = pt_reg.get("registration_code", "");

  // 2) wait 4 seconds to make sure a clenup call will delete a registration (default reg TTL for test = 5)
  cout << "R" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(6));

  ptree pt;
  CallUrl(pt, "/register/clean");

  // 3) call the validate backend
  CallValidate(registration_code, "", "400");
}

void RegisterTest::testRegisterCleanupForceImmediate() {
  client.reset(new ClientHelper(DEBUG_HOST));

  SimpleWeb::CaseInsensitiveMultimap header;
  header.insert({"X-Client-Id", CLIENT_ID});
  header.insert({"X-Client-Secret", CLIENT_SECRET});
  header.insert({"Content-Type", "application/json"});

  // 1) register a user first
  ptree pt_reg = CallRegisterUser("200");
  string registration_code = pt_reg.get("registration_code", "");

  // 2) wait 2 seconds to make sure a clenup call will delete a registration (default reg TTL for test = 3)
  cout << "R" << flush;
  std::this_thread::sleep_for(std::chrono::seconds(2));

  ptree pt;
  pt.put("ttl", "1");
  CallUrl(pt, "/register/clean");

  // 3) call the validate backend
  CallValidate(registration_code, "", "400");
}

//---------------------------------------------

void RegisterTest::testRegisterExUser() {
  profile_provider->CreateUser(USER_NAME, USER_EMAIL, USER_PASSWORD);
  CallRegisterUser("400", "User or email already exists");
}
void RegisterTest::testRegisterExEmail() {
  profile_provider->CreateUser(USER_NAME2, USER_EMAIL, USER_PASSWORD);
  CallRegisterUser("400", "User or email already exists");
}

void RegisterTest::testRegisterWrongEmail() {
  CallRegisterUser("400", "invalid email", "not-an-email");
}

void RegisterTest::testRegisterWrongEmailDomain() {
  CallRegisterUser("400", "invalid email", "user@domain.abc123");
}

void RegisterTest::testRegisterWrongEmailDomainShort() {
  CallRegisterUser("400", "invalid email", "user@domain.a");
}

CPPUNIT_TEST_SUITE_REGISTRATION(RegisterTest);
