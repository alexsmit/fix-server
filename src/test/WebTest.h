#pragma once
/*
 * Copyright 2018 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "../stdafx.h"

#include <boost/variant.hpp>

#include <vx/sql/DataFactory.h>
#include <vx/sql/ProfileProvider.h>

#include <cppunit/TestCaller.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

#include "ClientHelper.h"

using namespace std;
using namespace vx::sql;
namespace vx {
  namespace openid {
    namespace test {
      /// basic token server endpoints tests
      class WebTest : public CppUnit::TestFixture {
        CPPUNIT_TEST_SUITE(WebTest);

#ifndef NDEBUG
        CPPUNIT_TEST(testClientHeader);
        CPPUNIT_TEST(testClientNoHeader);
        CPPUNIT_TEST(testClientWrongHeader);

        CPPUNIT_TEST(testPostForm);
        CPPUNIT_TEST(testPostFormNoAuth);
        CPPUNIT_TEST(testPostFormWrongAuth);
        CPPUNIT_TEST(testPostJson);
        CPPUNIT_TEST(testPostJsonNoAuth);
        CPPUNIT_TEST(testPostJsonWrongAuth);
#endif

#if !defined(SERVER_TOKEN)
        CPPUNIT_TEST(testSearchEmpty);
#endif

        CPPUNIT_TEST(testBadURL);
        CPPUNIT_TEST(testIndex);
        CPPUNIT_TEST(testIndexFile);
        CPPUNIT_TEST(testInfo);
        CPPUNIT_TEST(testVersion);
        CPPUNIT_TEST(testStopBadRequest);
#if defined(SERVER_TOKEN)
        CPPUNIT_TEST(testHelp);
#endif
        CPPUNIT_TEST_SUITE_END();

      private:
        shared_ptr<ClientHelper> client;

      public:
        WebTest();
        ~WebTest();

        void setUp() override;     //!< cppunit setup
        void tearDown() override;  //!< cppunit teardown

#ifndef NDEBUG
        void testClientHeader();
        void testClientNoHeader();
        void testClientWrongHeader();
        void testPostForm();
        void testPostFormNoAuth();
        void testPostFormWrongAuth();
        void testPostJson();           
        void testPostJsonNoAuth();     
        void testPostJsonWrongAuth();  
#endif

        void testSearchEmpty();

        void testBadURL();
        void testIndex();
        void testIndexFile();
        void testInfo();
        void testVersion();
        void testStopBadRequest();
#if defined(SERVER_TOKEN)
        void testHelp();
#endif
      };
    }  // namespace test
  }    // namespace openid
}  // namespace vx
