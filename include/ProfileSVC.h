#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/sql/ProfileProvider.h>
#include <vx/web/ServerContext.h>
#include "ProfileSVCBase.h"

using namespace vx::sql;

/// profile service (extention over profile provider)
class ProfileSVC : public ProfileSVCBase {
protected:
  /// profile provider
  std::shared_ptr<ProfileProvider> provider;

public:
  /// create profile service
  ProfileSVC(ServerContext &context, std::shared_ptr<ProfileProvider> provider);
  /// destructor. noop
  ~ProfileSVC();

  /// get current profile provider
  inline std::shared_ptr<ProfileProvider> GetProvider() { return provider; }

  /// Authenticate login/password and return basic data
  std::shared_ptr<ProfileData> Authenticate(const string &userName, const string &password, bool uuid = false) override;
  /// Authenticate login/password and return basic data
  std::shared_ptr<ProfileData> AuthenticatePin(const string &userName, const string &pin, bool uuid = false) override;
  /// get basic data by login or uuid
  std::shared_ptr<ProfileData> GetProfileData(const string &userName, bool uuid = false) override;

  /// start user registration 
  RegistrationResult RegisterUser(UserRecord &record, bool sendEmail = false) override;
  /// check pending registration
  RegistrationStatus ValidateRegistration(const string& token) override;
  /// confirm registration with the token
  bool ConfirmRegistration(const string& token) override;

  /// reset password, returns a token
  string Reset(const string& email) override;
  /// update a password using a token (one time only)
  bool UpdatePassword(const string& reset_code, const string& password) override;
  /// verify whether a token is valid
  bool ResetVerify(const string& reset_code) override;

  /// update pin for UUID
  bool ChangePin(const string &uuid, const string &pin) override;
  /// update password for UUID
  bool ChangePassword(const string &uuid, const string &password) override;
};
