#pragma once
/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <vx/sql/DataFactory.h>

#include "TokenProvider.h"
#include <vx/sql/ProfileProvider.h>
#include "ProfileSVC.h"
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/ServiceFactory.h>

namespace vx {

  /// @brief Send notification emails
  class EmailService : public Service {
  private:
    std::string template_dir;

  public:
    /// @brief email service
    /// @param conf app config
    /// @param template_dir location of the templates using inja
    EmailService(JsonConfig& conf, const std::string& template_dir = "data/");

    /// @brief return service id
    /// @return string
    std::string id() override;
    /// Send email
    virtual bool SendCodeURL(const std::string& template_name,
                     const std::string& email_from,
                     const std::string& email_to,
                     const std::string& title,
                     const std::string& url);
  };


  /// @brief Implementation of the service factory to create email provider
  class ServiceEmailFactory : public ServiceFactory {
  protected:
    shared_ptr<JsonConfig> section;  //!< current configuration section in the config.json

  public:
    /// @brief Service factory to create provider instance or retrieve from the cache
    /// @param cache_ttl amount of time (in seconds) a service instance considered to be valid for a current thread
    /// @param conf coniguration name
    ServiceEmailFactory(time_t cache_ttl = 60, const string& conf = "");

    /// @brief return service id
    /// @return string
    std::string id() const override;

  protected:
    /// @brief create an instance of a provider for specified type.
    /// @param type token provider type,
    /// @return service pointer
    std::shared_ptr<Service> createService(const std::string& type) override;
  };

}  // namespace vx
