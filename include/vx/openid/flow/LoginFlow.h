#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>

namespace vx {
  namespace openid {
    namespace flow {

      /// /login processing (part of authorize flow or via login page)
      /// 1) authorize flow - will verify authorize metadata and render a login page
      /// 2) post with authorize metadata - a redirect will be executed as a part of authorize flow
      /// 3) post from just login page - a redirect will go to the configured "main" page (config.json)
      class LoginFlow : public BaseFlow {
      private:
        BaseFlow& Verify();  //!< validate authorize request parameters or prepare login page
        BaseFlow& Exec();    //!< process login (post from login page or from authorize call)

      public:
        /// /login handler
        LoginFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// destructor, noop
        ~LoginFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx