#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <set>
#include <vx/web/WebServer.h>
#include <vx/JWT.h>

#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

namespace vx {
  namespace openid {
    namespace flow {

      enum TokenAPIProperty {
        issuer = 0,
        jwks_uri = 1
      };

      /// /token process
      class TokenAPI {
        class TokenApiContext;
        friend class TokenApiContext;

      private:
        TokenServerContext& context;
        std::function<void(const std::string& key, const std::string& val)> meta_callback;
        ptree result;
        std::vector<string> scopes;
        std::string nonce;               //!< provided nonce to put into the token
        std::string scope;               //!< comma or space separated list of scopes
        std::string standard_scope;      //!< comma or space separated list of scopes (standard), if defined will be put to metadata instead of the scope
        std::string flow;                //!< current flow name, like PKCE, set using method Flow()
        std::string refresh_token_hint;  //!< previous refresh token ID
        std::set<std::string> tokens;    //!< list of tokens to generate, if empty, will create both

        // RESULT
        std::string
            jwt_access_token,  //!< access token after running CreateAccessToken
            jwt_id_token,      //!< id_token
            refresh_token;     //!< refresh_token
        unsigned long ttl;
        std::string error, error_description;

        void WriteTokenMetadata(
            shared_ptr<TokenProvider> token_svc,
            const string& token_nonce,
            map<string, string> meta,
            const string& type);

        void WriteTokenMetadata(
            shared_ptr<TokenProvider> token_svc,
            const string& token_nonce,
            ptree& pt,
            const string& type);

        TokenInfo create_token(
            JWT& access_token,
            TokenApiContext& apicontext);

        TokenInfo create_id_token(
            JWT& id_token,
            TokenApiContext& apicontext,
            const string& access_token);

        class TokenApiContext {
        private:
          TokenServerContext& context;
          TokenAPI& api;
          shared_ptr<Service> profile_svc;
          shared_ptr<Service> token_svc;
          string GetIssuer();

        public:
          TokenApiContext(
              TokenServerContext& context,       //!< ref to context
              TokenAPI& api,                     //!< ref to the API
              const std::string& userid,         //!< userid
              const std::string& audience = "",  //!< expected to be a current client_id or empty (will be taken from config)
              const std::string& cookie = ""     //!< session cookie reference
          );

          shared_ptr<ProfileProvider> profile_provider;
          shared_ptr<TokenProvider> token_provider;
          bool refresh_token_rotation;
          string issuer, algo, aud, cookie;
          UserRecord rec;
          bool add_refresh;
        };

      public:
        /// /authorize handler
        TokenAPI(
            TokenServerContext& context,
            std::function<void(const std::string& key, const std::string& val)> meta_callback = nullptr);
        ~TokenAPI();

        /// consider those scopes, returns false if scope is invalid
        bool Scope(const std::string& scope);
        /// set "scopes", save standard_scopes to metadata
        bool Scope(const std::string& scope, const std::string& standard_scopes);

        /// set what tokens should be generated
        void Tokens(std::set<std::string> tokens);

        void Flow(const std::string& flow);    //!< set flow name, like PKCE
        void Nonce(const std::string& nonce);  //!< set nonce provided by the client
        // void Refresh(const std::string& refresh_token);  //!< set previous refresh token
        void Refresh(const std::string& refresh_token);  //!< set prefious refreshtoken

        /// create access/id/refresh tokens
        void CreateAccessToken(
            const std::string& userid,         //!< userid
            const std::string& audience = "",  //!< expected to be a current client_id or empty (will be taken from config)
            const std::string& cookie = ""     //!< session cookie reference
        );
        ptree Result();                     //!< retrieve a result
        std::string GetError();             //!< get a string for "error" return property
        std::string GetErrorDescription();  //!< get a string for "error_description" return property
        bool HasErrors();                   //!< true if a response is non-200 status code

        /// real issuer based on the config
        std::string GetProperty(TokenAPIProperty type);

        // validations
        bool Validate(const string& token);  //!< validate token and return payload into result

        /// get assigned and validated scope string
        std::string GetScope() { return scope; }
        /// get assigned and validated scope array
        std::vector<std::string> GetScopes() { return scopes; }
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx