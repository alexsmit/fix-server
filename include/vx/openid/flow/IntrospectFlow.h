#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>
// #include <ThisFlowRequest.h>

namespace vx {
  namespace openid {
    namespace flow {

      /// /introspect processing
      class IntrospectFlow : public BaseFlow {
      private:
        IntrospectRequest ar;

        BaseFlow& Verify();                       //!< validate request parameters
        BaseFlow& ExecRefresh();                  //!< process refresh token
        BaseFlow& ExecToken(const string& type);  //!< process selected token type
        BaseFlow& ExecId();                       //!< process id token
        BaseFlow& ExecAccess();                   //!< process access token

      public:
        /// /introspect handler
        IntrospectFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// /introspect handler
        IntrospectFlow(ServerContext& context, IntrospectRequest _req);
        /// destructor, noop
        ~IntrospectFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx