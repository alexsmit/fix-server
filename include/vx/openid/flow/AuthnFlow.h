#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

namespace vx {
  namespace openid {
    namespace flow {
      /// /authn
      class AuthnFlow : public BaseFlow {
      private:
        AuthRequest ar;  //!< deserialized request

        BaseFlow& Verify();        //!< validate request parameters
        BaseFlow& ExecuteAuthn();  //!< process /authn

      public:
        /// /authorize handler
        AuthnFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// /authorize handler
        AuthnFlow(ServerContext& context, AuthRequest _req);
        /// destructor, noop
        ~AuthnFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx
