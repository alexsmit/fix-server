#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>
// #include <ThisFlowRequest.h>

namespace vx {
  namespace openid {
    namespace flow {

      /// .../.well-known/oauth-authorization-server
      class AuthServerFlow : public BaseFlow {
      private:
        BaseFlow& Verify();  //!< validate request parameters

      public:
        /// /authorize handler
        AuthServerFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// destructor, noop
        ~AuthServerFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx