#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/models/Models.h>

#define SESSION_COOKIE "sid"

namespace vx {
  namespace openid {
    namespace flow {

      /// flow states
      enum VerificationState {
        none,      //!< initial state
        verified,  //!< verify result
        done,      //!< verify result (error or any non-exec returns)
        sent       //!< final state, response sent
      };

      /// Utility class to execute oauth hndlers
      class BaseFlow {
      protected:
        ServerContext& context;                                      //!< current context
        std::shared_ptr<Response> res;                               //!< from constructor, response
        std::shared_ptr<Request> req;                                //!< from constructor, request
        VerificationState verification_state;                        //!< flow state
        map<VerificationState, function<BaseFlow&()>> step_methods;  //!< stat->method map
        std::string redirect_uri;                                    //!< redirect url, should be set to be able to process 302
        SimpleWeb::CaseInsensitiveMultimap returnHeader;             //!< response header items
        bool returnCORS;                                             //!< if true - CORS header will be included
        bool returnEmpty;                                            //!< if true - returns empty data
        bool returnQuery;                                            //!< if true - redirect with query parameters, if false - fragment
        ptree metadata;                                              //!< execution metadata (keys are vary, depends on the flow)
        /// prepare to return an error (error, error-description)
        BaseFlow& Error(
            SimpleWeb::StatusCode code,
            const std::string& error,
            const std::string& error_description,
            const std::string& error_url = "",
            const std::string& state = "",
            bool corsHeader = true);
        /// send response back or redirect
        BaseFlow& Send();

        TokenProvider* getTokenProvider();                      //!< init and return token provider or lodaded instance
        std::shared_ptr<ProfileProvider> getProfileProvider();  //!< init and return profile provider or lodaded instance
        ProfileSVC* getProfileService();                        //!< init and return profile provider or lodaded instance

        /// send json response
        virtual void SendResponse(std::shared_ptr<Response> res, ptree& pt,
                                  SimpleWeb::CaseInsensitiveMultimap& header, SimpleWeb::StatusCode code);

      private:
        // SimpleWeb::CaseInsensitiveMultimap returnData;  //!< response data
        SimpleWeb::StatusCode returnCode;          //!< response code
        ptree returnDataTree;                      //!< response data as property_tree
        std::shared_ptr<Service> token_service;    //!< singleton instance of token provider
        std::shared_ptr<Service> profile_service;  //!< singleton instance of profile provider

      public:
        /// create flow object
        BaseFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// create flow object (response won't be sent)
        BaseFlow(ServerContext& context);
        /// destructor. noop
        ~BaseFlow();

        virtual void Execute();  //!< run the state machine, every handler should change the state, terminal state is "sent"


        /// Retrun a result from Auth flow as ptree
        BaseFlow& Result(ptree result,
                         SimpleWeb::StatusCode code = SimpleWeb::StatusCode::success_ok,
                         map<string, string> extra_header = {},
                         bool corsHeader = false);
        /// Retrun a result from Auth flow as map
        BaseFlow& Result(std::map<string, string> result,
                         SimpleWeb::StatusCode code = SimpleWeb::StatusCode::success_ok,
                         map<string, string> extra_header = {},
                         bool corsHeader = false);

        /// Return empty result
        BaseFlow& Empty(std::map<string, string> result,
                        SimpleWeb::StatusCode code = SimpleWeb::StatusCode::success_ok,
                        map<string, string> extra_header = {},
                        bool corsHeader = false);

        /// retrieve result as ptree.
        ptree GetResult();
        /// retrieve execution metadata
        ptree GetMetadata();
        /// retrieve an HTTP status code
        SimpleWeb::StatusCode GetResultCode();
        /// retrieve complete output headers
        SimpleWeb::CaseInsensitiveMultimap GetResultHeader();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx