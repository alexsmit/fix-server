#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>

namespace vx {
  namespace openid {
    namespace flow {

      /// process /authorize endpoint
      class AuthorizeFlow : public BaseFlow {
      private:
        AuthorizeRequest ar;              //!< request object
        std::string vxcookie;             //!< session cookie
        std::set<string> response_types;  //!< parsed response_type parameter
        UserToken user_token;             //!< token loaded from session or vxcookie
        TokenInfo cookie_token;           //!< created cookie token
        TokenInfo code_token;             //!< created authorization code token

        /// could be 302 to return via callback or 200 via post
        SimpleWeb::StatusCode authorize_return_code;
        /// return parameters from /authorize call
        std::map<string, string> authorize_return_map;
        std::map<string, string> authorize_header_map;

        BaseFlow& Verify();           //!< verify parameters. state: none->veerified, none->done
        BaseFlow& ExecuteVerified();  //!< step: verified, route to proper process
        BaseFlow& LoginRequired();    //!< return login required error

        BaseFlow& ProcessSession();  //!< make sure vxcookie or sessionToken are valid
        BaseFlow& ExecuteCode();     //!< execute authorize code logic, state: verified->done
        BaseFlow& ExecuteTokens();   //!< execute token/id_token logic, state: verified->done
        BaseFlow& CreateCookie();    //!< create session cookie

        void DeleteSessionCookie();  //!< wipe out database reference for session cookie (vxcookie) and make it empty

      protected:
        /// send json response
        virtual void SendResponse(std::shared_ptr<Response> res, ptree& pt,
                                  SimpleWeb::CaseInsensitiveMultimap& header, SimpleWeb::StatusCode code) override;

        /// Send HTML data when response_mode = form_post
        void SendResponseFormPost(std::shared_ptr<Response> res, ptree& pt);

        /// Send HTML data when response_mode = okta_post_message
        void SendResponseOkta(std::shared_ptr<Response> res, ptree& pt);

      public:
        /// create flow object (for use inside handlers)
        AuthorizeFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// create flow object using request object
        AuthorizeFlow(ServerContext& context, AuthorizeRequest _req, const std::string& session_cookie);
        /// destructor. noop
        ~AuthorizeFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx