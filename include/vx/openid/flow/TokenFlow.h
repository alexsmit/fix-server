#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/openid/flow/Flow.h>
#include <vx/openid/models/Models.h>

namespace vx {
  namespace openid {
    namespace flow {

      /// /token process
      class TokenFlow : public BaseFlow {
      private:
        TokenRequest ar;  //!< deserialized request

        BaseFlow& Verify();               //!< validate request parameters
        BaseFlow& ExecuteCode();          //!< process authorization code
        BaseFlow& ExecuteRefreshToken();  //!< process refresh_token
        BaseFlow& ExecutePassword();      //!< process password grant

        string MergeScopes(const string& old_scopes, const string& new_scopes);

      public:
        /// /authorize handler
        TokenFlow(ServerContext& context, std::shared_ptr<Response> res, std::shared_ptr<Request> req);
        /// /authorize handler
        TokenFlow(ServerContext& context, TokenRequest _req);
        /// destructor, noop
        ~TokenFlow();
      };

    }  // namespace flow
  }    // namespace openid
}  // namespace vx