#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <vx/web/WebServer.h>
#include "ModelBase.h"
#include "ModelsMetadata.h"

using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// /token request object
      struct TokenRequest : public ModelBase {
        std::string code;           //!< pkce or code flow
        std::string code_verifier;  //!< code verifier for pkce flow
        std::string grant_type;     //!< valid grant type, require some other parameters to exist depending on the flow
        std::string password;       //!< required for password grant
        std::string redirect_uri;   //!< required for code/pkce
        std::string refresh_token;  //!< required for refresh_token grant
        std::string scope;          //!< should match the list requested originally in the authorize call
        std::string username;       //!< required for password grant
        std::string id_token_hint;  //!< required if grant_type is "pin", id_token preveiously retrieved, could be expired
        std::string pin;            //!< required if grant_type is "pin"

        // // client authentication
        // std::string client_id; //!< embedded client auth, client_id
        // std::string client_secret; //!< embedded client auth, secret

        /// deserialize from index object
        friend TokenRequest& operator>>(VxServer::VxIndex& j, TokenRequest& request);
        /// serialize to query params
        friend std::string& operator<<(std::string& j, TokenRequest& request);
        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&TokenRequest::code, "code"),
            property(&TokenRequest::code_verifier, "code_verifier"),
            property(&TokenRequest::grant_type, "grant_type"),
            property(&TokenRequest::password, "password"),
            property(&TokenRequest::redirect_uri, "redirect_uri"),
            property(&TokenRequest::refresh_token, "refresh_token"),
            property(&TokenRequest::scope, "scope"),
            property(&TokenRequest::username, "username"),
            property(&TokenRequest::client_id, "client_id"),
            property(&TokenRequest::client_secret, "client_secret"),
            property(&TokenRequest::id_token_hint, "id_token_hint"),
            property(&TokenRequest::pin, "pin"));
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx