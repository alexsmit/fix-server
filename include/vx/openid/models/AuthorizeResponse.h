#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <vx/Metadata.h>

namespace vx {
  namespace openid {
    namespace models {

      /// /authorize response object (usually via redirect) if type is "code"
      struct AuthorizeResponse {
        std::string state;  //!< state received previously
        std::string code;   //!< code to exchange for a token(s)
        /// serialize to ptree
        friend ptree& operator<<(ptree& j, const AuthorizeResponse& request);
        /// deserialize from ptree
        friend AuthorizeResponse& operator>>(ptree& j, AuthorizeResponse& request);
        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&AuthorizeResponse::state, "state"),
            property(&AuthorizeResponse::code, "code"));
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx