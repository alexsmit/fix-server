#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <iostream>
#include <vx/web/WebServer.h>
#include <vx/openid/models/ModelBase.h>
#include <vx/openid/models/ModelsMetadata.h>

// <vx/openid/models/

using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {


      /// request sent to .../authorize endpoint
      struct AuthorizeRequest : public ModelBase {
        std::string code_challenge;         //!< PKCE
        std::string code_challenge_method;  //!< PKCE, method, only S256 is supported
        std::string display;                //!< will be passed to socail login providers
        std::string idp_scope;              //!< scopes for social login idp
        std::string idp;                    //!< social login idp
        std::string login_hint;             //!< username when show the login
        std::string max_age;                //!< time since last login
        std::string nonce;                  //!< value for id token
        std::string prompt;                 //!< prompt the user to authenticate or consent
        std::string redirect_uri;           //!< required
        std::string response_type;          //!< code, or not supported token and id_token
        std::string response_mode;          //!< the way to respond back
        std::string request;                //!< all parameters in JWT
        std::string scope;                  //!< required: list of scopes
        std::string sessionToken;           //!< if not defined a session vill be validated via current established session
        std::string state;                  //!< required

        /// de-serialize from VxIndex objects
        friend AuthorizeRequest& operator>>(VxServer::VxIndex& j, AuthorizeRequest& request);
        /// serialize to query string
        friend std::string& operator<<(std::string& j, AuthorizeRequest& request);

        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&AuthorizeRequest::client_id, "client_id"),
            property(&AuthorizeRequest::code_challenge, "code_challenge"),
            property(&AuthorizeRequest::code_challenge_method, "code_challenge_method"),
            property(&AuthorizeRequest::display, "display"),
            property(&AuthorizeRequest::idp_scope, "idp_scope"),
            property(&AuthorizeRequest::idp, "idp"),
            property(&AuthorizeRequest::login_hint, "login_hint"),
            property(&AuthorizeRequest::max_age, "max_age"),
            property(&AuthorizeRequest::nonce, "nonce"),
            property(&AuthorizeRequest::prompt, "prompt"),
            property(&AuthorizeRequest::redirect_uri, "redirect_uri"),
            property(&AuthorizeRequest::response_type, "response_type"),
            property(&AuthorizeRequest::response_mode, "response_mode"),
            property(&AuthorizeRequest::request, "request"),
            property(&AuthorizeRequest::scope, "scope"),
            property(&AuthorizeRequest::sessionToken, "sessionToken"),
            property(&AuthorizeRequest::state, "state"));
      };


    }  // namespace models
  }    // namespace openid
}  // namespace vx
