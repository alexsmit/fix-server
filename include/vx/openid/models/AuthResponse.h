#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <vx/openid/models/Models.h>
using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// profile inner object
      struct AuthUserProfile {
        std::string id;         //!< user id (guid)
        std::string username;   //!< username (login)
        std::string email;      //!< email
        std::string firstName;  //!< first name (from props)
        std::string lastName;   //!< last name (from props)
      };

      /// /authn response object
      struct AuthResponse {
        std::string expiresAt;     //!< time string
        std::string status;        //!< status, success or else
        std::string sessionToken;  //!< session token if authenticated
        AuthUserProfile profile;   //!< loaded on success
        /// serialize into ptree
        friend ptree& operator<<(ptree& j, const AuthResponse& request);
        /// deserialize from ptree
        friend AuthResponse& operator>>(ptree& j, AuthResponse& request);
        /// serialize into OKTA ptree
        ptree toOKTA();
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx