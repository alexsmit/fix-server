#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include "ModelBase.h"
#include <vx/openid/models/Models.h>

using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// /authorize response object (usually via redirect) if type is "code"
      struct IntrospectRequest : public ModelBase {
        std::string token;            //!< An access token, ID token, refresh token, or device secret
        std::string token_type_hint;  //!< Indicates the type of token being passed. Valid values: access_token, id_token, refresh_token, and device_secret

        /// deserialize from request objects
        friend IntrospectRequest& operator>>(VxServer::VxIndex& j, IntrospectRequest& request);

        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&IntrospectRequest::client_id, "client_id"),
            property(&IntrospectRequest::client_secret, "client_secret"),
            property(&IntrospectRequest::token, "token"),
            property(&IntrospectRequest::token_type_hint, "token_type_hint"));
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx