#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <vx/openid/models/Models.h>
using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// /authorize response object (usually via redirect) if type is "code"
      struct IntrospectResponse {
        bool active;             //!< 	Indicates whether the token is active or not. 	Boolean
        std::string aud;         //!< 	The audience of the token. 	String
        std::string client_id;   //!< 	The ID of the client associated with the token. 	String
        std::string device_id;   //!< 	The ID of the device associated with the token 	String
        int exp;                 //!< 	The expiration time of the token in seconds since January 1, 1970 UTC. 	Integer
        int iat;                 //!< 	The issuing time of the token in seconds since January 1, 1970 UTC. 	Integer
        std::string iss;         //!< 	The issuer of the token. 	String
        std::string jti;         //!< 	The identifier of the token. 	String
        int nbf;                 //!<	  Identifies the time (a timestamp in seconds since January 1, 1970 UTC) before which the token must not be accepted for processing. 	Integer
        std::string scope;       //!< 	A space-delimited list of scopes. 	String
        std::string sub;         //!< 	The subject of the token. 	String
        std::string token_type;  //!< 	The type of token. The value is always Bearer. 	String
        std::string uid;         //!< 	The user ID. This parameter is returned only if the token is an access token and the subject is an end user. 	String
        std::string username;    //!< 	The username associated with the token.

        /// deserialize from request objects
        friend IntrospectResponse& operator>>(VxServer::VxIndex& j, IntrospectResponse& request);

        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&IntrospectResponse::active, "active"),
            property(&IntrospectResponse::aud, "aud"),
            property(&IntrospectResponse::client_id, "client_id"),
            property(&IntrospectResponse::device_id, "device_id"),
            property(&IntrospectResponse::exp, "exp"),
            property(&IntrospectResponse::iat, "iat"),
            property(&IntrospectResponse::iss, "iss"),
            property(&IntrospectResponse::jti, "jti"),
            property(&IntrospectResponse::nbf, "nbf"),
            property(&IntrospectResponse::scope, "scope"),
            property(&IntrospectResponse::sub, "sub"),
            property(&IntrospectResponse::token_type, "token_type"),
            property(&IntrospectResponse::uid, "uid"),
            property(&IntrospectResponse::username, "username"));
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx