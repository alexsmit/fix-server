#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "AuthorizeRequest.h"
#include "AuthorizeResponse.h"
#include "AuthRequest.h"
#include "AuthResponse.h"
#include "IntrospectResponse.h"
#include "IntrospectRequest.h"
#include "LogoutRequest.h"
#include "ModelBase.h"
#include "ModelsMetadata.h"
#include "TokenRequest.h"

using namespace vx::openid::models;
