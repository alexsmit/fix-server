#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <iostream>
// #include <boost/property_tree/ptree.hpp>
#include <vx/web/WebServer.h>
#include "ModelBase.h"
#include "ModelsMetadata.h"

using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// request sent to .../authorize endpoint
      struct LogoutRequest : public ModelBase {
        std::string id_token_hint;             //!< valid id token
        std::string post_logout_redirect_uri;  //!< matches one of the configured logout URL
        std::string state;                     //!< will be passed to the logout endpoint

        /// de-serialize from VxIndex objects
        friend LogoutRequest& operator>>(VxServer::VxIndex& j, LogoutRequest& request);
        /// serialize to query string
        friend std::string& operator<<(std::string& j, LogoutRequest& request);

        /// metadata for serialization
        constexpr static auto properties = std::make_tuple(
            property(&LogoutRequest::id_token_hint, "id_token_hint"),
            property(&LogoutRequest::post_logout_redirect_uri, "post_logout_redirect_uri"),
            property(&LogoutRequest::state, "state"));
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx