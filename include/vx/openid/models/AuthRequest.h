#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <string>
#include <boost/property_tree/ptree.hpp>
#include <vx/web/WebServer.h>

using namespace boost::property_tree;

namespace vx {
  namespace openid {
    namespace models {

      /// inner object of AuthRequest
      struct AuthRequestOptions {
        bool multiOptionalFactorEnroll;  //!< n/a
        bool warnBeforePasswordExpired;  //!< n/a
      };

      /// device token context object, ignored
      struct AuthRequestContext {
        std::string deviceToken;  //!< n/a
      };

      /// request object for /authn
      struct AuthRequest {
        std::string username;  //!< username (login)
        std::string password;  //!< password
        // not supported fields (completely or partially ignored)
        AuthRequestOptions options;  //!< options, ignored
        std::string audience;        //!< audience, ignored
        std::string token;           //!< token, ignored
        AuthRequestContext context;  //!< device token, ignored

        /// serialize to ptree
        friend ptree& operator<<(ptree& j, const AuthRequest& request);
        /// deserialize from index objects
        friend AuthRequest& operator>>(VxServer::VxIndex& j, AuthRequest& request);
        /// deserialize from ptree
        friend AuthRequest& operator>>(ptree& j, AuthRequest& request);
      };

    }  // namespace models
  }    // namespace openid
}  // namespace vx