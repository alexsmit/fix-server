#pragma once
/*
 * Copyright 2021 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <tuple>
#include <utility>
#include <string>
#include <functional>
// #include <boost/property_tree/ptree.hpp>
// #include <boost/property_tree/json_parser.hpp>
// #include <boost/lexical_cast.hpp>
#include <vx/Metadata.h>
#include <vx/web/WebServer.h>

using namespace std;
using namespace boost::property_tree;

// namespace vx {
//   namespace openid {
//     namespace models {
//       unsigned char op_touppar(unsigned char c) {
//         return std::toupper(c);
//       }
//     }
//   }
// }

template <typename T>
void fromVxIndex(const VxServer::VxIndex& data, T& object) {
  constexpr auto nbProperties = std::tuple_size<decltype(T::properties)>::value;
  for_sequence(std::make_index_sequence<nbProperties>{}, [&](auto i) {
    constexpr auto property = std::get<i>(T::properties);
    using Type = typename decltype(property)::Type;
    auto str = data[property.name];
    try {
      if (std::is_same<Type, bool>::value) {
        auto my_lower = [](unsigned char c) { return std::tolower(c); };
        string sval;
        sval.resize(str.length());
        std::transform(str.begin(), str.end(), sval.begin(), my_lower);
        auto bval = (sval == "true" || sval == "1" || sval == "yes" || sval == "y" || sval == "on");
        object.*(property.member) = bval;
      }
      else if (std::is_same<Type, int>::value) {
        int ival = 0;
        if (str.empty())
          ival = 0;
        else {
          ival = boost::lexical_cast<int>(str);
        }
        object.*(property.member) = ival;
      }
      else {
        object.*(property.member) = boost::lexical_cast<Type>(str);
      }
    }
    catch (const std::exception& e) {
      if (std::is_same<Type, int>::value) {
        int ival = 0;
        object.*(property.member) = ival;
      }
    }
  });
}
