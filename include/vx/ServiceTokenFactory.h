#pragma once
/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <vx/sql/DataFactory.h>

#include "TokenProvider.h"
#include <vx/sql/ProfileProvider.h>
#include "ProfileSVC.h"
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/ServiceFactory.h>

namespace vx {
  /// @brief Implementation of the service factory to create token provider based on the configuration file or parameter
  class ServiceTokenFactory : public ServiceFactory {
  protected:
    DataFactory factory;             //!< to create provider objects
    shared_ptr<JsonConfig> section;  //!< current configuration section in the config.json
    string provider_token_type;      //!< token provider type (overridden by the command line only)

  public:
    /// @brief Service factory to create token provider instance or retrieve from the cache
    /// @param cache_ttl amount of time (in seconds) a service instance considered to be valid for a current thread
    /// @param conf coniguration name
    ServiceTokenFactory(time_t cache_ttl = 60, const string& conf = "token_provider");

    /// @brief return service id
    /// @return string
    std::string id() const override;

  protected:
    /// @brief create an instance of token provider for specified type. if type is invalid, @throw argument_exception
    /// @param type token provider type, if empty - a default type should be retrieved as configured
    /// @return service pointer
    std::shared_ptr<Service> createService(const std::string& type) override;
  };

}  // namespace vx
