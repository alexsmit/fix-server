#pragma once
/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <vx/web/ServerContext.h>

/// token transfer object
struct TokenInfo {
  std::string token;  //!< related token, i.e. refresh token JWT
  std::string nonce;  //!< nonce
  time_t expried;     //!< unix timestamp when a token will expire
};

/// user token transfer object
struct UserToken : public TokenInfo {
  std::string username;    //!< username
  std::string agent;       //!< agent (arbitrary name)
  std::string metadata;    //!< metadata string
  ptree parse_metadata();  //!< return parsed metadata
};

/// all token related operations handler
class TokenProvider : virtual public Service {
public:
  static string id;                                 //!< id
  static string getIssuer(ServerContext& context);  //!< generate an issuer

protected:
  unsigned long ttl = 0;          //!< access token ttl
  unsigned long refresh_ttl = 0;  //!< refresh token ttl

public:

  /// create token
  virtual TokenInfo CreateToken(std::string userName,
                                std::string agent,
                                std::string old_nonce,
                                unsigned long ttl = 0,
                                const std::string& metadata = "",
                                bool createRefreshRecord = false) = 0;
  /// create token with metadata map
  virtual TokenInfo CreateTokenMeta(std::string userName,
                                    std::string agent,
                                    std::string old_nonce,
                                    unsigned long ttl = 0,
                                    std::map<std::string, std::string> metadata = {},
                                    bool createRefreshRecord = false) = 0;

  /// retrieve token by nonce
  virtual UserToken GetTokenByNonce(std::string nonce) = 0;
  /// retrieve tokey by refresh_token
  virtual UserToken GetTokenByRefresh(std::string refresh_token) = 0;
  /// delete token by nonce
  virtual void DeleteTokenByNonce(std::string nonce) = 0;

  /// validate token (expired, exists, etc)
  virtual bool ValidateToken(std::string userName, TokenInfo info, bool nonceOnly = false) = 0;
  /// validate token (expired, exists, etc)
  virtual bool ValidateToken(const string& user_name, UserToken& user_token, bool nonceOnly = false) = 0;
  /// cleanup expired tokens from the DB
  virtual void Clean() = 0;
  /// access token record with the failure (max 3 strikes to delete a token). if max = -1 will reset the count
  virtual void Hit(std::string nonce, int max = 3) = 0;
  /// retrieve token metadata by nonce
  virtual string GetMetadata(std::string nonce) = 0;
  /// update token metadata by nonce
  virtual bool SetMetadata(std::string nonce, const std::string& medatada) = 0;

  /// create nonce, recommended sizes: 12 & 24
  string CreateNonce(int size = 16);

  /// set access token ttl
  void SetTTL(unsigned long _ttl) { ttl = _ttl; };
  /// get access token ttl (could be set im multiple places)
  virtual unsigned long GetEffectiveTTL() = 0;
  /// set refresh token ttl
  void SetRefreshTTL(unsigned long _ttl) { refresh_ttl = _ttl; };
  /// get refresh token ttl (could be set im multiple places)
  virtual unsigned long GetEffectiveRefreshTTL() = 0;
};
