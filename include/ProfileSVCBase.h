#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/sql/UserRecord.h>
#include <vx/web/ServerContext.h>
#include <vx/sql/ProfileProvider.h>

using namespace vx::sql;

/// profile transfer object
struct ProfileData {
  std::vector<std::string> roles;  //!< roles
  std::string userid;              //!< userid
};

/// base implentation of profile service
class ProfileSVCBase : public Service {
public:
  /// create profile service object
  ProfileSVCBase(ServerContext &context);
  /// destructor. noop
  ~ProfileSVCBase();

  ServerContext &context;  //!< server context reference

  /// Authenticate login/password and return basic data
  virtual std::shared_ptr<ProfileData> Authenticate(const string &userName, const string &password, bool uuid = false) = 0;
  /// Authenticate login/password and return basic data
  virtual std::shared_ptr<ProfileData> AuthenticatePin(const string &userName, const string &pin, bool uuid = false) = 0;
  /// get basic data by login or uuid
  virtual std::shared_ptr<ProfileData> GetProfileData(const string &userName, bool uuid = false) = 0;

  /// will create a registration record
  virtual RegistrationResult RegisterUser(UserRecord &record, bool sendEmail = false) = 0;
  /// check if we can login using this token
  virtual RegistrationStatus ValidateRegistration(const string& token) = 0;
  /// confirm registration with registration token. token will be transferred to a user record
  /// as password reset token
  virtual bool ConfirmRegistration(const string& token) = 0;

  /// send email
  bool SendConfirmationEmail(const string& email, const string& token, const string& key, const string& path);

  /// reset password
  virtual string Reset(const string& email) = 0;
  /// update a password using a token (one time only)
  virtual bool UpdatePassword(const string& reset_code, const string& password) = 0;
  /// verify whether a token is valid
  virtual bool ResetVerify(const string& reset_code) = 0;

  // update pin/password

  /// update pin for UUID
  virtual bool ChangePin(const string &uuid, const string &pin) = 0;
  /// update password for UUID
  virtual bool ChangePassword(const string &uuid, const string &password) = 0;
};
