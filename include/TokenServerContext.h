#pragma once
/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdafx.h>

#include <vx/sql/DataFactory.h>

#include "TokenProvider.h"
#include <vx/sql/ProfileProvider.h>
#include "ProfileSVC.h"
#include <vx/web/WebServer.h>
#include <vx/web/ServerContext.h>
#include <vx/ContextTTL.h>
#include <vx/ServiceFactory.h>

using namespace std;
using namespace boost::property_tree;
using namespace VxServer;
using namespace vx;

/// token server specific context
class TokenServerContext : public ServerContext {
public:
  DataFactory factory;              //!< to create provider objects
  unsigned long reg_ttl;            //!< retistration ttl
  unsigned long refresh_ttl;        //!< refresh token ttl
  unsigned long ttl_session_token;  //!< access token ttl
  string provider_profile_type;     //!< profile provider type (overridden by the command line only)
  time_t provider_ttl = 60;         //!< time to re-create a provider, if set to zero - thread context cache is disabled for all providers

private:
  std::map<std::thread::id, std::shared_ptr<ContextTTL>> profile_providers;
  std::map<std::thread::id, std::shared_ptr<ContextTTL>> token_providers;

  // std::map<std::string, shared_ptr<vx::ServiceFactory>> providers;

public:
  /// create context object
  TokenServerContext(int port = DEFAULT_HTTP_PORT,                      // http port
                     int ports = DEFAULT_HTTPS_PORT,                    // https port
                     unsigned long ttl = 0,                             // jwt token TTL
                     LogLevel debugLevel = LogLevel::none,              //
                     unsigned long reg_ttl = DEFAULT_TTL_REGISTRATION,  // TTL for a registration record
                     unsigned long refresh_ttl = DEFAULT_TTL_REFRESH,   // refresh token ttl
                     const string& provider_profile_type = ""           // from the command line, set profile provider type
  );
  ~TokenServerContext();

  /// mandatory method to call early to load a config file and initialize prefixed config (to use parameters not from the root)
  void Init(const char* prefix = NULL, const string& configFileName = AppConfig::default_config) override;
  /// get token provider by configuration key
  shared_ptr<TokenProvider> GetTokenProvider(const string& type = "");
  /// get profile provider by configuration key
  shared_ptr<ProfileProvider> GetProfileProvider(const string& type = "");
  /// get profile service by configuration key
  shared_ptr<ProfileSVC> GetProfileSVC(const string& type = "");
  /// get service by name (token, profile_service) NOTE: profile_service is not a profile_provider
  shared_ptr<Service> getService(const string& name, const string& type="", time_t ttl=0) override;
};
