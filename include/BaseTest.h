#pragma once
/*
 * Copyright 2018-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <vx/web/ServerContext.h>

#include <TestConstants.h>

std::shared_ptr<ServerContext> getContext(const string& param = "", bool useDefault = false, int cache_ttl = 0);

#define START_CAPTURE                                          \
  stringstream cout_out, cerr_out;                             \
  std::streambuf* cerr_buf(std::cerr.rdbuf(cerr_out.rdbuf())); \
  std::streambuf* cout_buf(std::cout.rdbuf(cout_out.rdbuf()));

#define STOP_CAPTURE                 \
  std::cerr.rdbuf(cerr_buf);         \
  std::cout.rdbuf(cout_buf);         \
  auto cerr_output = cerr_out.str(); \
  auto cout_output = cout_out.str();

