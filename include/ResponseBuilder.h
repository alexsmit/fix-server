#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <openssl/bio.h>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/x509.h>

#include <vx/JWT.h>
#include <vx/RSAKeyInfo.h>
#include <vx/StringUtil.h>
#include <vx/web/Util.h>

#include <vx/Auth.h>
#include "ProfileSVC.h"
#include "TokenServerContext.h"
#include "TokenProvider.h"

using namespace std;
using namespace boost::property_tree;

namespace auth {

  /// create token response
  class ResponseBuilder {
  public:
    /// @brief flow flags
    enum class ResponseBuilderStep {
      Initial = 0,
      Identity = 1,            //!< identity ok, but not verified
      Authenticated = 2,       //!< user/password or bearer/pin authenticated
      FullyAuthenticated = 3,  //!< fully authenticated
      Refresh = 4,             //!< refresh token created
      Completed = 7            //!< access token generated (flow completed)
    };

  private:
    // counstructor params
    std::shared_ptr<Response> response;
    std::shared_ptr<Request> request;
    ServerContext &context;

    // vars
    string grant_type, grant_value;
    vector<string> roles;
    string issuer;
    string algo;
    string audience;
    bool pin_after_expiration;

    string user, userid;

    string access_token, refresh_token, nonce, refresh_nonce;
    std::set<std::string> aud;
    string scope;
    ResponseBuilderStep step;

    string client_id, client_secret;

    map<string, function<void()>> map_grants;

  private:
    shared_ptr<Service> profile_service;
    shared_ptr<Service> token_service;
    ProfileSVC *GetProfileService();
    TokenProvider *GetTokenService();

  public:
    /// create builder object
    ResponseBuilder(std::shared_ptr<Response> response, std::shared_ptr<Request> request,
                    ServerContext &context);

    /// execute flow (depends on the initial parameters)
    void Exec();
    /// apply bearer token
    ResponseBuilder &Bearer(bool bWithExpiration = true);
    /// get UUID for the user
    string GetUserId();
    /// check whether a user has a role assigned
    bool HasRole(const string role);

  protected:
    /// throw auth exception with detailed reason
    void Failed(const string& message, int code = 401);
    /// current grant_type
    string GetGrantType() { return grant_type; }
    /// current grant type value
    string GetGrantValue() { return grant_value; }
    /// load grant value from ther request
    void LoadGrant();
    /// apply clientid/secret
    ResponseBuilder &WithAuthenticate();
    /// apply refresh_token
    ResponseBuilder &WithRefreshToken();
    /// apply pin authentication
    ResponseBuilder &WithPINAuthenticate();
    /// generate access token
    ResponseBuilder &CreateAccessToken();
    /// generate refresh token
    ResponseBuilder &CreateRefreshToken();
    /// validate authentication and token expiration
    ResponseBuilder &WithValidate();
    /// deliver a response
    ResponseBuilder &SendResponse();
    /// send just ok response
    ResponseBuilder &SendOKResponse();

    //-----
    /// validate client credentials
    ResponseBuilder &WithClientCredentials();
    /// send client credentials response
    ResponseBuilder &SendClientCredentialsResponse();
  };
}  // namespace auth
