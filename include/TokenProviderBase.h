#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>

#include <vx/dbm/DBIO.h>
#include <vx/sql/DataFactory.h>
#include "TokenProvider.h"

/// base token provider
class TokenProviderBase : public TokenProvider {
protected:
  unsigned long configured_ttl = 0;          //!< default ttl
  unsigned long configured_refresh_ttl = 0;  //!< default refresh ttl
  void SetDefaults(JsonConfig& config);      //!< reload parameters from config for current connection. May override settings for token provider.

public:
  unsigned long GetEffectiveTTL() override;
  unsigned long GetEffectiveRefreshTTL() override;
  virtual string Name() = 0;  //!< arbitrary provider name
  /// validate token (expired, exists, etc)
  virtual bool ValidateToken(const string& user_name, UserToken& user_token, bool nonceOnly = false) override;
  /// create token with metadata map
  TokenInfo CreateTokenMeta(std::string userName,
                            std::string agent,
                            std::string old_nonce,
                            unsigned long ttl = 0,
                            std::map<std::string, std::string> metadata = {},
                            bool createRefreshRecord = false) override;
};
