/*
 * Copyright 2019-2020 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _VX_MESSAGE_H
#define _VX_MESSAGE_H

#include <string>

using namespace std;

/// message constantrs
struct VxMessages {
  static string bad_request;      //!< bad_request
  static string not_authorized;   //!< not_authorized
  static string invalid_request;  //!< invalid_request
  static string invalid_client;   //!< invalid_client
  static string invalid_grant;    //!< invalid_grant
  static string invalid_token;    //!< invalid_token
  static string server_error;     //!< server_error

  static string msg_username_password_invalid;  //!< human readable wrong password message
  static string msg_password_not_defined;       //!< human readable password is missing
  static string msg_username_not_defined;       //!< human readable username is missing
  static string msg_client_id_invalid;          //!< human readable invalid_client

  static string msg_svc_unable_to_get_token;    //!< service error: unable to get token provider
  static string msg_svc_unable_to_get_profile;  //!< service error: unable to get profile_provider
};

#endif
