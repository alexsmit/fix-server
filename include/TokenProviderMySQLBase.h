#pragma once
/*
 * Copyright 2019 Alexander Kuznetsov <alx.kuzza@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// #include <vx/sql/DataFactory.h>
#include <vx/sql/DataProviderMySQLBase.h>
#include "TokenProviderBase.h"

using namespace vx::sql;

/// token provider (mysql implementation)
class TokenProviderMySQLBase : public TokenProviderBase {
private:
  /// current data provider
  std::shared_ptr<DataProviderMySQLBase> prov;

public:
  /// @brief token provider using injected data provider
  /// @param provider data provider
  TokenProviderMySQLBase(std::shared_ptr<DataProviderMySQLBase> provider);
  ~TokenProviderMySQLBase();

  string Name() override { return "token.mysql"; }

  TokenInfo CreateToken(std::string userName,
                        std::string agent,
                        std::string old_nonce,
                        unsigned long ttl = 0,
                        const std::string& metadata = "",
                        bool createRefreshRecord = false) override;
  UserToken GetTokenByNonce(std::string nonce) override;
  UserToken GetTokenByRefresh(std::string refresh_token) override;
  void DeleteTokenByNonce(std::string nonce) override;

  virtual bool ValidateToken(std::string userName, TokenInfo info, bool nonceOnly = false) override;
  void Clean() override;
  void Hit(std::string nonce, int max = 3) override;
  string GetMetadata(std::string nonce) override;
  bool SetMetadata(std::string nonce, const std::string& medatada) override;
};
