#!/bin/bash

#1 - filename
#2 - sign by
#3 - CN
#4 - days (3600 - default)
#5 - serial
function create_cert() {
    if [ -e $1-cert.pem ] ; then
        return 0
    fi
    if [ -z "$3" ] ; then
        return 1
    fi
    days=3600
    if [ ! -z "$4" ] ; then
        days=$4
    fi
    
    touch tmp/index.txt
    truncate tmp/index.txt --size=0
    echo $5 > tmp/serial.dat
    
    openssl req \
    -newkey rsa:2048 -nodes -keyout $1-key.pem \
    -subj "/C=US/ST=California/L=Reseda/O=FYKS/CN=$3" \
    -out $1-req.pem \
    -addext "subjectAltName=DNS:$3,DNS:www.$3" \
    -addext "extendedKeyUsage=serverAuth,clientAuth" \
    -addext "keyUsage=critical,digitalSignature,keyEncipherment" \
    -addext "basicConstraints=CA:FALSE"
    
    cp $1-key.pem privkey.pem
    openssl ca -batch -in $1-req.pem -out $1-cert.pem -days $days -config openssl.conf
    rm privkey.pem
    
    rm $1-req.pem
}

#1 - CN
#2 - days
function create_ca() {
    if [ ! -e ca.pem ] ; then
        # Create CA certificate
        
        if [ -z "$1" ] ; then
            return 1
        fi
        
        days=3600
        if [ ! -z "$2" ] ; then
            days=$2
        fi
        
        openssl genrsa 2048 > ca-key.pem
        openssl req -new -x509 -nodes -days $days \
        -subj "/C=US/ST=California/L=Reseda/O=VXPRO/CN=$1" \
        -key ca-key.pem -out ca.pem
    fi
}

#1 - keyfile
function remove_password() {
    if [ -z "$1" ] ; then
        return 1
    fi
    openssl rsa -in $1-key.pem -out $1-key.pem
}

#-----------------------------------------------------

create_ca "ca.vxpro.com" 3360

create_cert server - "server.vxpro.com" 720 "01"

create_cert client - "client.vxpro.com" 720 "02"

if [ ! -e server.crt ] ; then
    cp server-cert.pem server.crt
    cat ca.pem >> server.crt
    cp server-key.pem server.key
    openssl x509 -pubkey -noout -in server.crt  > server.pub.pem
fi

if [ ! -e client.crt ] ; then
    cp client-cert.pem client.crt
    cat ca.pem >> client.crt
    cp client-key.pem client.key
fi
