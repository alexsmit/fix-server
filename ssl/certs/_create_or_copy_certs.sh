#!/bin/bash

mydir=$(pwd)

if [ -e ../../../ssl/certs/api/server.crt ] ; then
    pushd ../../../ssl/certs >/dev/null
    for c in server.crt server.key client-cert.pem client-key.pem server-cert.pem server-key.pem
    do
        cp api/$c $mydir
    done
    cp ca.pem $mydir
    popd >/dev/null
else
    . _create_certs.sh
fi

