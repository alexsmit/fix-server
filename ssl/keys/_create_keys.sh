#!/bin/bash

cert=certificate

if [ ! -e $cert.pem ] ; then
openssl req -new -newkey rsa:2048 -sha256 -nodes -keyout key.pem \
    -subj "/C=US/ST=CA/L=Reseda/O=VXPRO/CN=sample.vxpro.com" \
    -out $cert.csr

openssl x509 -req -days 365 -in $cert.csr -signkey key.pem -out $cert.pem

# -config openssl.cnf
#
openssl x509 -pubkey -noout -in $cert.pem  > pubkey.pem
fi

