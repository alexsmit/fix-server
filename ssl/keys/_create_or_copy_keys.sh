#!/bin/bash

if [ -e ../../../ssl/keys/key.pem ] ; then
    cp ../../../ssl/keys/{key.pem,pubkey.pem} ./
else
    . _create_keys.sh
fi
