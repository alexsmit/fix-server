# Demo SSL certificates and keys #

Demo keys and certificates are installed into /ssl.sample folder. Similar certificates has to be added
to /ssl folder. For demo purposes it is possible to copy all certificates from ssl.sample.
Those are self-signed certificates and should not be used in production environment.

*** Certifcates/Keys ***
ca.pem - CA certificate

client-cert.pem, client-key.pem - Certificate/Key for clients.

server-cert.pem, server-key.pem - Certificate/Key for MySQL server.

key.pem, pubkey.pem - private/public keys to sign JWT token if a key is being used for signing

server.crt, server.key - certificate for HTTPS server (token or profile server)

*** MySQL certs ***

Should be configured in my.cnf:

[mysqld]
....
\# Configure the MariaDB server to use SSL.
ssl-ca=/etc/mysql/ssl/ca.pem
ssl-cert=/etc/mysql/ssl/server-cert.pem
ssl-key=/etc/mysql/ssl/server-key.pem

*** Re-create all certs ***
Folder: ssl/certs.

1) delete all PEM/CRT files and run:
_create_certs.sh

Output:
ca.pem, ca-key.pem
client-cert.pem, client-key.pem
server-cert.pem, server-key.pem
server.crt
server.key

*** Public/Private keys for JWT signature ***
Folder: ssl/keys
It is possible to use key pair instead of the certificates to sign JWT tokens and encrypt passwords.
To create keys:
1) delete key.pem and pubkey.pem
2) run _create_keys.sh

*** Automated build ***

All demo certs and keys will be automatically generated to allow running token and profile server securely.
All configuration files are using those certificates and/or keys.

In production environmennt you should deploy keys/certs to replace those demo keys.
After the installation ssl folder may contain files:
ca.pem
client-cert.pem
client-key.pem
key.pem
pubkey.pem
server.crt
server.key
server-cert.pem
server-key.pem

For web server you need server.crt. This should include a certificate and full chain down to CA certificate.
key/pubkey - may be used to sign JWT token if you set "mode" to "keys" in clients.json

Please check server.crt for more details. It contains CA chain.


