#!/bin/bash
if [ -d certs ] ; then
    rm certs/*.pem
    rm certs/*.crt
    rm certs/*.key
fi
if [ -d keys ] ; then
    rm keys/*.pem
fi
