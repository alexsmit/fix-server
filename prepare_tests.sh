#!/bin/bash

# Usage: prepare_test.sh [1]
# if 1st parameter is defined a build will run syncronously, i.e. all makes will run in sequence.

if [ $EUID -eq 0 ] ; then
    echo -n 'Current user is root. Continue? [y/N] '
    read ans
    if [[ ! "$ans" =~ [yY] ]] ; then
	return 1 2>/dev/null || exit 1
    fi
fi

test_projects=(token-server profile-server)
prep_projects=(token-server profile-server)

BUILD_JOBS=2
BUILD_JOBS_MAX=18
BUILD_CLEAN=
BUILD_TEST=1
do_sync=1

function calculate_jobs() {
    local newjobs=0
    local cpus=$(cat /proc/cpuinfo | grep processor | wc -l)
    local maxcpujobs=0
    let maxcpujobs=$cpus+2
    local memfree=($(cat /proc/meminfo | grep MemAvailable))
    let newjobs=${memfree[1]}/500000+1
    let BUILD_JOBS_MAX=$maxcpujobs

    if [ $newjobs -gt $maxcpujobs ] ; then
        newjobs=$maxcpujobs
    fi
    if [ $newjobs -gt $BUILD_JOBS ] ; then
        BUILD_JOBS=$newjobs
        BUILD_JOBS_SET=$newjobs
    fi
}

export TEMP=/run/user/$EUID

curdir=$(pwd)

calculate_jobs

# process command line parameters
while [ ! -z "$1" ] ; do
    cont=0
    case $1 in
        -c)
            BUILD_CLEAN="-c"
            cont=1
        ;;
        -j)
            shift
            BUILD_JOBS=$1
            cont=1
        ;;
        -n)
    	    shift
    	    BUILD_TEST=0
    	    cont=1
    	;;
    esac
    if [ $cont -ne 0 ] ; then
        shift
    else
        break
    fi
done

if [ $BUILD_JOBS -gt $BUILD_JOBS_MAX ] ; then
    BUILD_JOBS=$BUILD_JOBS_MAX
fi

echo jobs: $BUILD_JOBS  max: $BUILD_JOBS_MAX

start_time=$(date +%s)

#return 1 2>/dev/null || exit 1

export VXPRO_NO_STRESS=1
export VXPRO_NO_INSTALL=1

##########################
### Top level configs  ###
##########################

./prepare_build.sh $BUILD_CLEAN

##########################
### Running build      ###
##########################

opts=
if [ ! -z "$BUILD_CLEAN" ] ; then
    opts="-c"
fi
opts="$opts -j $BUILD_JOBS"
pushd test >/dev/null
    ./prepare_build.sh $opts
popd >/dev/null

end_time=$(date +%s)

let total_time=$end_time-$start_time

echo "Build time: $total_time s"

unset VXPRO_NO_STRESS
unset VXPRO_NO_INSTALL

##########################
### Running tests      ###
##########################

if [ $BUILD_TEST -eq 1 ] ; then
failed=()

for dirr in ${test_projects[@]}
do
    isfailed=0

    if [ -e test/build/$dirr/$dirr-test ] ; then
    	pushd test/build/$dirr > /dev/null
    	echo -n Running $dirr " ... "
    	./$dirr-test > $curdir/$dirr.lst 2> $curdir/$dirr.error.lst
    	if [ $? -ne 0 ] ; then
    	    isfailed=1
    	fi
        popd > /dev/null
    else
    	echo binary test/build/$dirr/$dirr-test does not exist
    	isfailed=1
    fi

    if [ $isfailed -eq 1 ] ; then
        if [ "$dirr" == "dbutil" ] ; then
            echo FAILED WARNING
        else
            failed+=( "$dirr" )
            echo FAILED
        fi
    else
	    echo OK
    fi
done

if [ ${#failed[*]} -ne 0 ] ; then
    echo -n "Failed tests:"
    for key in "${failed[*]}"; do
	    echo -n " $key"
    done
    echo " "
    return 1 2>/dev/null || exit 1
else
    echo All tests are successful
    return 0 2>/dev/null || exit 0
fi
else
    echo Tests are skipped
    return 0 2>/dev/null || exit 0
fi

