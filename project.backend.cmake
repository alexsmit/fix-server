set(VXPRO_TOKEN_MYSQL_BACKEND false)
set(VXPRO_TOKEN_SOCI_BACKEND true)

if(DEFINED ENV{VXPRO_TOKEN_MYSQL_BACKEND})
    set(VXPRO_VERSION_MAJOR $ENV{VXPRO_TOKEN_MYSQL_BACKEND})
endif()
if(DEFINED ENV{VXPRO_TOKEN_SOCI_BACKEND})
    set(VXPRO_VERSION_MAJOR $ENV{VXPRO_TOKEN_SOCI_BACKEND})
endif()

if(VXPRO_TOKEN_MYSQL_BACKEND)
    message(STATUS "*** Usning mysql backend ***")
    add_compile_options("-DVX_USE_MYSQL=1")
endif()

if(VXPRO_TOKEN_SOCI_BACKEND)
    message(STATUS "*** Usning soci backend ***")
    add_compile_options("-DVX_USE_SOCI=1")
endif()

if (NOT VXPRO_TOKEN_MYSQL_BACKEND AND NOT VXPRO_TOKEN_SOCI_BACKEND)
    message(FATAL_ERROR "*** ERROR: no backend is enabled ***")
endif()
