var jwt = require('jsonwebtoken');
var jwksClient = require('jwks-rsa');
var fs = require('fs');
var fetch = require('isomorphic-fetch');

//-------------------------

//var token = fs.readFileSync('./token', 'utf8');
// console.log('token:', token);
// var decoded = jwt.decode(token);
// console.log('decoded:', decoded);

let token = '';

function dofetch(url, options, ok = false) {
    const promise = fetch(url, options);
    if (ok) return promise;
    return promise.then(resp => {
        return resp.json().then(json => {
            return resp.ok ? json : Promise.reject(json);
        })
    })
}


//-----------------------------
//--- Create user -------------
//-----------------------------

function create_user() {
    console.log('>>>1 create user');
    var payload = {
        "password": "123",
        "pin": "1234",
        "email": "zzzz@vxpro.com",
        "roles": [
            "admin",
            "other",
            "user"
        ],
        "properties": {
            "description": "zzzz description",
            "prop1": "val1-1",
            "prop2": "val2-1"
        }
    };
    var options = {
        headers: {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json',
            'X-Client-Id': 'aaa',
            'X-Client-Secret': '111'
        },
        method: 'POST',
        body: JSON.stringify(payload),
        credentials: 'include',
    };

    let p1 = dofetch('http://127.0.0.1:8081/users/zzzz', options, true);
    p1.then(x => {
        console.log('User created');
    })
        .catch(e => {
            console.log('ERROR: ', e);
        });

    return p1;
}

//-----------------------------
//--- Login -------------------
//-----------------------------
function login() {
    console.log('>>>2 login');
    let payload = {
        "grant_type": "password",
        "username": "zzzz",
        "password": "123"
    };
    var options = {
        headers: {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json',
            'X-Client-Id': 'aaa',
            'X-Client-Secret': '111'
        },
        method: 'POST',
        body: JSON.stringify(payload),
        credentials: 'include',
    };
    options.body = JSON.stringify(payload);

    let p2 = dofetch('http://localhost:8082/oauth/token', options)

    p2.then(res => {
        // console.log('JWT:', res);
        token = res.access_token;
        console.log('>>> login successful');
        // console.log('token: ', token);
    })
        .catch(e => {
            console.log('login error', e);
        })

    return p2;
}

//-----------------------------
//--- Verify token ------------
//-----------------------------
function verify_token() {
    var client = jwksClient({
        jwksUri: 'http://localhost:8082/.well-known/jwks.json'
    });

    function getKey(header, callback) {
        client.getSigningKey(header.kid, function (err, key) {
            var signingKey = key.publicKey || key.rsaPublicKey;
            callback(null, signingKey);
        });
    }

    var options = { algorithms: ['RS512'] };
    jwt.verify(token, getKey, options, function (err, decoded) {
        if (err) {
            console.log('>>>ERROR:', err);
        } else {
            console.log('>>>JWT decoded and verified:');
            console.log(decoded);
        }
    });
}

create_user()
    .then(() => {
        login()
            .then(() => {
                verify_token();
            })
    })
