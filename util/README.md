# Utilities #

*** add_mysql_user.sh ***

Create demo user.
You may need to create a database first:
CREATE DATABASE ko32example;

*** test_user_login.js ***

Note: Token and profile servers should run both on the localhost.

You need to install node modules to run this test.

Run:
npm install
node test_user_login.js

*** test_user_login.sh ***

Note: Token and profile servers should run both on the localhost.

Script uses curl to connect to profile and token server.
It creates a user and obtains a refresh_token.

*** OpenSusePrepare.sh ***

Install packages required to build a project on OpenSuSE 15.x. 
