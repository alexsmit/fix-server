#!/bin/bash
zypper install cmake \
gcc-c++ \
libboost_chrono1_66_0-devel libboost_filesystem1_66_0-devel libboost_system1_66_0-devel \
libmariadb-devel \
libboost_serialization1_66_0-devel libboost_thread1_66_0-devel \
libuuid-devel \
libdb-4_8-devel \
cppunit-devel \
rpm-build

# proxygen deps:
# libevent-devel glog-devel gflags-devel gflags-devel-static libsodium-devel
# liblz4-devel libzstd-devel (libzstd-devel-static optional)
# snappy-devel libdwarf-devel double-conversion-devel libunwind-devel

# gmock gtest


