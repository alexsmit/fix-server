#!/bin/bash

#if [ ! -d node_modules ] ; then
#    npm install
#fi

url="http://localhost:8081/users/zzz"
curl -s -X POST $url \
    -H "X-Client-Id: aaa" \
    -H "X-Client-Secret: 111" \
    -d @- << EOF
{
    "password": "123",
    "pin": "1234",
    "email": "zzz@vxpro.com",
    "roles": [
        "admin",
        "other",
        "user"
    ],
    "properties": {
        "description": "zzz description",
	"prop1": "val1-1",
	"prop2": "val2-1"
    }
}
EOF

url="http://localhost:8082/oauth/token"
token=$( (curl -s -X POST $url \
    -H "X-Client-Id: aaa" \
    -H "X-Client-Secret: 111" \
    -d @- << EOF
{
    "grant_type" : "password",
    "username": "zzz",
    "password": "123"
}
EOF
) | python -c "import sys, json; print(json.load(sys.stdin)['access_token'])") 2> /dev/null

echo Login result
echo Authorization: bearer $token

#if [ -z "$token" ] ; then
#    echo Login successful
#    echo Token: $token
#else
#    echo Login failed
#fi

#echo -n $token > token
#node test_user_login.js
