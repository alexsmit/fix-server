#!/bin/bash

### Create user for testing

user="kohana"
password="hanako"
database="ko32example"

mysql -u root -p << HHH
CREATE USER IF NOT EXISTS '$user'@'localhost' IDENTIFIED BY '$password';
GRANT ALL PRIVILEGES ON $database.* TO '$user'@'localhost' REQUIRE SSL;

CREATE USER IF NOT EXISTS '$user'@'%' IDENTIFIED BY '$password';
GRANT ALL PRIVILEGES ON $database.* TO '$user'@'%' REQUIRE SSL;

HHH
