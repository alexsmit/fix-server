# Profile and Token servers

Profile server allows to maintain user logins with roles and any properties.

Token server exports /oauth/token endpoint to generate JWT token signed with SSL key or certificate using RS512 algorithm.
JWT token could be refreshed using "pin" or "refresh token".
Algorithm may be configured in config.json

# Build instructions

## Pre-requisite (required step)

### Install "build system"
For systems using RPM package managers import key from:
https://vxpro.com/keys/vxpro.asc

For systems using DEB package managers import key from:
https://vxpro.com/keys/vxpro.apt.asc

Use one of the following repositories for your distribution:
https://vxpro.com/RPMS/el8 - for Centos 8 (deprecated)
https://vxpro.com/RPMS/fc33 - for Fedora 33
https://vxpro.com/RPMS/fc35 - for Fedora 35
https://vxpro.com/RPMS/fcXX - for Fedora XX (current version, currently is 35)
https://vxpro.com/RPMS/suse - for OpenSuSE Tumbleweed (current)
https://vxpro.com/RPMS/suse151 - for OpenSuSE 15.1 (deprecated)
https://vxpro.com/RPMS/suse15.2  - for OpenSuSE 15.2 (deprecated)
https://vxpro.com/RPMS/suse152 - for OpenSuSE 15.2 (deprecated)
https://vxpro.com/RPMS/suse15.3 - for OpenSuSE 15.3
https://vxpro.com/RPMS/suse153 - for OpenSuSE 15.3
https://vxpro.com/RPMS/suse423 - for OpenSuSE 42.3 (deprecated)
https://vxpro.com/RPMS/ubuntu focal contrib - for Ubuntu 20.04
https://vxpro.com/RPMS/ubuntu bionic contrib - for Ubuntu 18.04
https://vxpro.com/RPMS/debian buster contrib - for Debian 10

Install package vx-build

### Install libraries

Run command
vx install

## Initial step
At first Download and update all the dependent projects and some internal symlinks will be created also.
Run:
`prepare_dep.sh`

Alternatively it is possible to run just
`git submodule update --init`
However that will require to run the same command inside mariadbpp folder

## Release build:

For builds a provided script prepare_build.sh could be used.
Some main parameters:
-c - cleanup build area
-j xx - set xx number of processes for compilation (automatically detected based on number of CPU and memory if not defined)
prod - run release build
dev - run debug build (default)

It may take about 2.5 minutes to run a complete build. Be patient.

Run:
`prepare_build.sh -c prod run`

## Debug build (default)
Run:
`prepare_build.sh run`
Or:
`prepare_build.sh dev run`

## Run all tests

NOTE:
Database has to be configured to run all the tests successfully. How to setup a database step is
out of scope for this document.

The script will compile all related projects and tests. It may take up to 3 minutes to fully compile
everything.

Run:
`prepare_tests.sh`

## Pre build script (optional)

pre_build.sh will stop token and profile servers.

## Post build script (optional)

post_build.sh will restart both token and profile servers.

# Installation instructions:

Build an rpm file.
Run:
`cd build
make package`

Depending on your system you will get .DEB package for Ubuntu or .rpm package for Fedora/OpenSuSE.
After a successful build run a package manager to install it.

For OpenSuSE:
zypper install ```package name```


For token and profile servers the package contains a folder ```ssl.sample``` with demo certificates and keys.
To run in production you need to provide a proper keys/certificates.
For development environment you may copy the entire content from that folder to folder ```ssl```

