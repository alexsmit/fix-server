#!/bin/bash
function verify_stress() {
    if [ ! -e build.env ] ; then
        echo build.env file does not exist
        echo create this file and add VXPRO_STRESS=1
        return 1
    fi

    . build.env
    if [ -z "$VXPRO_STRESS" ] ; then
        echo file build.env exist
        echo however VXPRO_STRESS=1 is not defined
        return 1
    fi

    if [ ! -e build/token-server-stress ] ; then
        echo stress target has not been built
        return 1
    fi

    return 0
}

# 1 - host
# 2 - port
# 3 - use ssl if not empty
function stop_server() {

local proto='http'
if [ ! -z "$3" ] ; then
    proto='https'
fi

curl --location --request POST ${proto}'://'${1}':'${2}'/stop' \
    --insecure \
    --header 'X-Client-Id: aaa' \
    --header 'X-Client-Secret: 111' \
    --header 'Content-Type: application/json' \
    --data-raw \
'{
    "magic": "hello"
}' &> /dev/null
}

# 1 - pid
function wait_for_exit() {
    local err=0
    echo 'Waiting up to 30 seconds....'
    for((i=0;i<15;i++)); do
        kill -s 0 $1 2>/dev/null
        ret=$?
        if [ $ret -ne 0 ] ; then
            break
        fi
        sleep 2
    done

    echo ''
    if [ $ret -eq 0 ] ; then
        echo 'Fail (1 test)'
        err=1
    else
        echo 'OK (1 test)'
    fi
    echo ''

    return $err
}
